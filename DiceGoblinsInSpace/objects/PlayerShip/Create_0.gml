/// @description Insert description here
// You can write your code in this editor

frame = 0;
thrust_resource_cost_modifer = 0.5;

forward_thrust_points = [{x:2, y:66}]
reverse_thrust_points = [{x:52, y:16}, {x:52, y:116}];
up_thrust_points = [{x:41, y:119}, {x:10, y:119}, {x:47, y:119}, {x:16, y:119}]
down_thrust_points = [{x:41, y:10}, {x:10, y:10}, {x:47, y:10}, {x:16, y:10}]
weapon_points = [{x:80, y:64}];

resources = ds_map_create();
resources[? "fire"] = 0;
resources[? "matter"] = 0;
resources[? "anti-matter"] = 0;
resources[? "power"] = 0;
caps = ds_map_create(); // populated dynamically

active_a = {
	name: "Dice Chucker",
	
	strength: new Roller( new Die([
		new DieFace("matter", 1), 
		new DieFace("matter", 1),
		new DieFace("matter", 2),
		new DieFace("matter", 2),
		new DieFace("matter", 3),
		new DieFace("matter", 3)
	]), global.ROLLER_MODE_LOW),
	
    rate: new Roller( new Die([
		new DieFace("matter", 1), 
		new DieFace("matter", 1),
		new DieFace("matter", 2),
		new DieFace("matter", 2),
		new DieFace("matter", 3),
		new DieFace("matter", 3)
	]), global.ROLLER_MODE_LOW),
	
    spread: new Roller( new Die([
		new DieFace("matter", 1), 
		new DieFace("matter", 1),
		new DieFace("matter", 2),
		new DieFace("matter", 2),
		new DieFace("matter", 3),
		new DieFace("matter", 3)
	]), global.ROLLER_MODE_LOW),
	
	multi: new Roller( new Die([
		new DieFace("matter", 1), 
		new DieFace("matter", 1),
		new DieFace("matter", 2),
		new DieFace("matter", 2),
		new DieFace("matter", 3),
		new DieFace("matter", 3)
	]), global.ROLLER_MODE_LOW),
	
	cooldown: 0,
	resource: "matter",
	
	activate: function(owner){
		if(owner.resources[? resource] <= 0) return;
		if(cooldown > owner.frame) return;
		var sr = strength.roll();
		var rr = rate.roll();
		var spr = spread.roll();
		var mr = multi.roll();
		
		var s = sr.tag == resource ? sr.value : 0;
		var r = rr.tag == resource ? rr.value : 0;
		var sp = spr.tag == resource ? spr.value : 0;
		var m = mr.tag == resource ? mr.value : 0;
		
		var cost = s*m/6;
		owner.resources[?resource] -= cost;
		
		var angle = sp * choose(-1,1);
		for(var i = 0; i < m; ++i){
			var weapon_point = array_shuffle(owner.weapon_points)[0];
			var n = instance_create_depth(owner.x+weapon_point.x, owner.y+weapon_point.y, owner.depth, DieProjectile);	
			n.image_speed = 0;
			n.image_index = s-1;
			n.direction = angle;
			n.speed = owner.vspeed + 10;
			
			angle = -angle + sp*-sign(angle);
			cooldown = 60 / r + owner.frame;
		}
	}
}

active_b = {}

generatorRoller = new Roller( new Die([
	new DieFace("fire", 1), 
	new DieFace("matter", 1),
	new DieFace("fire", 2),
	new DieFace("matter", 2),
	new DieFace("fire", 3),
	new DieFace("matter", 3)
]), global.ROLLER_MODE_LOW);

reserveDie = new Die([
	new DieFace("fire", 1), 
	new DieFace("matter", 1),
	new DieFace("fire", 2),
	new DieFace("matter", 2),
	new DieFace("fire", 3),
	new DieFace("matter", 3)
]);

horizontalThrustRoller = new Roller( new Die([
	new DieFace("thrust", 1),
	new DieFace("thrust", 1),
	new DieFace("thrust", 2),
	new DieFace("thrust", 2),
	new DieFace("thrust", 3),
	new DieFace("thrust", 3)
]), global.ROLLER_MODE_LOW);

h_thrust = 0;

verticalThrustRoller = new Roller( new Die([
	new DieFace("thrust", 1),
	new DieFace("thrust", 1),
	new DieFace("thrust", 2),
	new DieFace("thrust", 2),
	new DieFace("thrust", 3),
	new DieFace("thrust", 3)
]), global.ROLLER_MODE_LOW);

v_thrust = 0;

function do_horizontal_movement(){
	hspeed *= 0.5;
	hspeed += h_thrust *1.75;
	
	var input_horizontal = keyboard_check_direct(vk_right) - keyboard_check_direct(vk_left);
	
	var thrust = 0;
	var r = horizontalThrustRoller.roll();
	if(r.tag == "thrust") {
		thrust += r.value;
	}
	thrust /= 60
	thrust = min(abs(thrust*input_horizontal), resources[? "fire"]) * sign(thrust);
	resources[? "fire"] -= abs(thrust)*thrust_resource_cost_modifer;
	thrust *= 60;
	h_thrust = lerp(h_thrust, thrust * input_horizontal, 5/60);

}

function do_vertical_movement() {
	vspeed *= 0.5;
	vspeed += v_thrust * 1.25;

	var input_vertical = keyboard_check_direct(vk_down) - keyboard_check_direct(vk_up);

	

	thrust = 0;
	var r = verticalThrustRoller.roll();
	if(r.tag == "thrust") {
		thrust += r.value;
	}
	thrust /= 60
	thrust = min(abs(thrust*input_vertical), resources[? "fire"]) * sign(thrust);
	resources[? "fire"] -= abs(thrust)*thrust_resource_cost_modifer;
	thrust *= 60;
	v_thrust = lerp(v_thrust, thrust * input_vertical, 5/60);
}

function do_movement() {
	var movement_funcs = array_shuffle([do_horizontal_movement, do_vertical_movement]);
	for(var i = 0; i < array_length(movement_funcs); ++i){
		var move_func = movement_funcs[i];
		move_func();
	}
	
	// boundaries
	if (x < 0) x = 0;
	if (y < 0) y = 0;
	if (x > room_width-sprite_width) x = room_width-sprite_width;
	if (y > room_height-sprite_height) y = room_height-sprite_height;
}

function do_resources() {
	// resource generation
	var g_roll = generatorRoller.roll();
	resources[? g_roll.tag] += g_roll.value / 60;

	// compute resource caps
	ds_map_clear(caps)
	for(var key = ds_map_find_first(resources);!is_undefined(key); key = ds_map_find_next(resources, key)) {
		caps[? key] = 0;
	}

	for(var key = ds_map_find_first(resources);!is_undefined(key); key = ds_map_find_next(resources, key)) {
		for(var fi = 0; fi < 6; ++fi){
			caps[? key] += reserveDie.faces[fi].tag == key ? reserveDie.faces[fi].value : 0;
		}
	}

	// apply resource caps
	for(var key = ds_map_find_first(resources);!is_undefined(key); key = ds_map_find_next(resources, key)) {
		resources[? key] = min(resources[? key], caps[? key]);
	}
}

function draw_resources(){
	var height_base = 16;
	var offset = 0;
	for(var key = ds_map_find_first(resources);!is_undefined(key); key = ds_map_find_next(resources, key)) {
		var resource = resources[? key]
		var cap = caps[? key]
		if(resource == cap) continue;
		var height = height_base * (1- (resource/cap))-1;
		if(height < 1) continue;
		var resource_color = c_white;
		switch(key) {
			case "fire": resource_color = c_orange; break;	
			case "matter": resource_color = c_ltgray; break;	
			case "anti-matter": resource_color = c_purple; break;	
			case "power": resource_color = c_yellow; break;	
		}
		
		if(resource < 0) {
			resource_color = make_color_hsv(
				(color_get_hue(resource_color)+180) mod 255,
				color_get_saturation(resource_color),
				(color_get_value(resource_color)+180) mod 255);
		}
		
		var background_darkening_factor = 2;
		var resource_back_color = make_color_rgb(color_get_red(resource_color) / background_darkening_factor, color_get_blue(resource_color) / background_darkening_factor, color_get_green(resource_color) / background_darkening_factor)
	
		draw_healthbar(x,y-offset, x+sprite_width, y-offset-height, abs(resource/cap*100), resource_back_color, resource_color, resource_color, 0, true, true);
		var spacer_width = 36/cap;
		var spacer_seperation = sprite_width / cap;
		for(var spacer_offset = spacer_seperation; spacer_offset <= sprite_width; spacer_offset += spacer_seperation) {
			draw_line_color(x+spacer_offset, y-offset-height, x+spacer_offset, y-offset, c_gray, c_dkgray);	
		}
		draw_rectangle_color(x,y-offset, x+sprite_width, y-offset-height, c_gray, c_gray, c_dkgray, c_dkgray, true);
		
		offset += height+2;
	}	
}

function draw_thrusters(){
	// horizontal thrusters
	var thrust_points = h_thrust > 0 ? forward_thrust_points : reverse_thrust_points;
	var thrust_cone_length = -h_thrust * 8;
	var thrust_cone_width = abs(h_thrust) * 8;
	var max_cone_width = 16 / array_length(thrust_points) / array_length(thrust_points);
	if(thrust_cone_width > max_cone_width) {
		var delta = max_cone_width - thrust_cone_width;
		thrust_cone_width += delta;
		thrust_cone_length -= delta * sign(thrust_cone_length);
	}
	draw_set_alpha(abs(h_thrust/2));
	for(var i = 0; i < array_length(thrust_points); ++i){
		var tp = thrust_points[i];
		//base up, base down, tip
		draw_triangle_color(x+tp.x, y+tp.y-thrust_cone_width, x+tp.x, y+tp.y+thrust_cone_width, x+tp.x+thrust_cone_length, y+tp.y, c_white, c_white, c_orange, false);
	}

	draw_set_alpha(1);


	// Vertical thrusters
	thrust_points = v_thrust > 0 ? down_thrust_points : up_thrust_points;
	thrust_cone_length = -v_thrust * 8;
	thrust_cone_width = abs(v_thrust) * 8;
	max_cone_width = 16 / array_length(thrust_points) / array_length(thrust_points);
	if(thrust_cone_width > max_cone_width) {
		var delta = max_cone_width - thrust_cone_width;
		thrust_cone_width += delta;
		thrust_cone_length -= delta * sign(thrust_cone_length);
	}
	draw_set_alpha(abs(v_thrust/2));
	for(var i = 0; i < array_length(thrust_points); ++i){
		var tp = thrust_points[i];
		//base left, base right, tip
		draw_triangle_color(x+tp.x-thrust_cone_width, y+tp.y, x+tp.x+thrust_cone_width, y+tp.y, x+tp.x, y+tp.y+thrust_cone_length, c_white, c_white, c_orange, false);
	}
	draw_set_alpha(1);	
}

function do_active_a() {
	var a_input = keyboard_check_direct(ord("Z"));
	if(!a_input) return;
	
	var activate_func = variable_struct_get(active_a, "activate");
	if( !is_undefined(activate_func)) {
		activate_func(id);	
	}
}

function do_active_b() {
	var b_input = keyboard_check_direct(ord("X"));
	if(!b_input) return;
	
	var activate_func = variable_struct_get(active_b, "activate");
	if( !is_undefined(activate_func)) {
		activate_func(id);	
	}
}