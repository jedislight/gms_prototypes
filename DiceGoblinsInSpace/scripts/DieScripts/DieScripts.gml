function DieFace(_tag, _value) constructor {
    tag= _tag;
    value= _value;
}

function Die(_faces) constructor {
	if(array_length(_faces) != 6) {
		show_error($"Die constructor called with invalid number of faces: {array_length(_faces)}", true);
	}
		
    faces = _faces;
		
    function roll() {
        return choose(faces[0], faces[1], faces[2], faces[3], faces[4], faces[5]);
	}
}

global.ROLLER_MODE_FAIR = "Fair";
global.ROLLER_MODE_SEQUENTIAL = "Sequential";
global.ROLLER_MODE_LOW = "Low";
global.ROLLER_MODE_HIGH = "High";
global.ROLLER_MODE_CENTER = "Center";
global.ROLLER_MODE_DRAW = "Draw";

function Roller(_die, _mode) constructor {

		
    die= _die;
    mode= _mode;
	cache = [];

	function roll() {
		// if the cache is empty fill the cache
        if(array_length(cache) == 0) switch (mode) {
            case "Fair":
                cache[0] = die.roll();
				break;
            case "Sequential":
				//NOTE: this relies on the cache being empty to start
                array_copy(cache, 0, die.faces, 0, 6);
				cache = array_reverse(cache)
				break;
            case "Low":
                cache[0] = die.faces[min(irandom_range(0,5), irandom_range(0,5))];
				break;
            case "High":
                cache[0] = die.faces[max(irandom_range(0,5), irandom_range(0,5))];
				break;
            case "Draw":
				//NOTE: this relies on the cache being empty to start
				array_copy(cache, 0, die.faces, 0, 6);
                cache = array_shuffle(cache);
				break;
            case "Center":
				var raw_roll = (irandom_range(0,5)+irandom_range(0,5))/2;
                cache[0] = die.faces[round(raw_roll)];
				break;
			default:
				show_error($"Unknown roller mode: {mode}", true);
				break;
        }
		
		// pop from the now non-empty cache
		return array_pop(cache);
    }
}



function assert(value, msg){
	if (value == false) {
		show_error(msg, true);
	}
}

function assertEquals(expected, actual, msg) {
	assert(expected == actual, $"{expected} != {actual}: {msg}");	
}

function assertApproximatelyEquals(expected, tolerance, actual, msg) {
	assert(tolerance > abs(expected - actual), $"{expected} !~ {actual}: {msg}");	
}

random_set_seed(0);

var faces = [
	new DieFace("test", 1),
	new DieFace("test", 2),
	new DieFace("test", 3),
	new DieFace("test", 4),
	new DieFace("test", 5),
	new DieFace("test", 6),
];

var die = new Die(faces);
var fairRoller = new Roller(die, global.ROLLER_MODE_FAIR);

assertEquals("test", fairRoller.roll().tag, "tag did not match input faces after roll");
assert(fairRoller.roll().value >= 1, "Rolled value out of range");
assert(fairRoller.roll().value <= 6, "Rolled value out of range");

var centerRoller = new Roller(die, global.ROLLER_MODE_CENTER);
var accumulator = 0;
repeat(1000) accumulator += centerRoller.roll().value;
assertApproximatelyEquals(3.5, 1, accumulator / 1000, "center roller rolling outside center on average");

var highRoller = new Roller(die, global.ROLLER_MODE_HIGH);
accumulator = 0;
repeat(1000) accumulator += highRoller.roll().value;
assertApproximatelyEquals(5, 1, accumulator / 1000, "high roller rolling outside high on average");

var lowRoller = new Roller(die, global.ROLLER_MODE_LOW);
accumulator = 0;
repeat(1000) accumulator += lowRoller.roll().value;
assertApproximatelyEquals(2, 1, accumulator / 1000, "low roller rolling outside low on average");

var drawRoller = new Roller(die, global.ROLLER_MODE_DRAW);
accumulator = 0;
repeat(6*50) accumulator += drawRoller.roll().value;
assertEquals((1+2+3+4+5+6)*50, accumulator, "draw roller not rolling all values exactly");

randomize();