// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function creature_block(species_) constructor{
	species = species_
	base = new attributes_block();
	trained = new attributes_block();
	name = species.name;
	
	function _get_attribute(spec, base, trained) {
		return spec * (1 + base/100) *	(1 + trained/100)
	}
	
	function get_strength() {
		return _get_attribute(species.attributes.strength, base.strength, trained.strength)	
	}
	
	function get_stamina() {
		return _get_attribute(species.attributes.stamina, base.stamina, trained.stamina)	
	}
	
	function get_recovery() {
		return _get_attribute(species.attributes.recovery, base.recovery, trained.recovery)	
	}
	
	function get_agility() {
		return _get_attribute(species.attributes.agility, base.agility, trained.agility)	
	}
	
	function get_level() {
		return 1 + base.get_sum() + trained.get_sum();
	}
	
	function to_string() {
		var text = $"--{name} ( Level {get_level()} {species.name} )--\n";
		text += $"Stamina {get_stamina()} ( {base.stamina} | {trained.stamina} )\n";
		text += $"Recovery {get_recovery()} ( {base.recovery} | {trained.recovery} )\n";
		text += $"Power {get_strength()} ( {base.strength} | {trained.strength} )\n";
		text += $"Agility {get_agility()} ( {base.agility} | {trained.agility} )\n";
		return text;
	}
}

function attributes_block() constructor{
	strength = 0
	agility = 0
	stamina = 0
	recovery = 0
	
	function get_sum(){
		return strength + agility + stamina + recovery;	
	}
}

function species_block() constructor {
	name = "Species Name"	
	attributes = new attributes_block();
	attributes.strength = 30;
	attributes.agility = 20;
	attributes.stamina = 100;
	attributes.recovery = 20;
    sprite = Sprite2;
    decays_into = "";
    max_racing_seasons = 20;
    min_racing_seasons = 4;
    max_breeding_age = 30;
    
	function get_speed(node){
		return 4;	
	}
}