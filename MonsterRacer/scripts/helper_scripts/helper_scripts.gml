global._sprite_cache = ds_map_create();

function sprite_cache(path) {
    var fetch = global._sprite_cache[? path];
    if(is_undefined(fetch)) {
        fetch = sprite_add(path, 0, false, false,0,0);
        global._sprite_cache[? path] = fetch;
    }
    
    if (fetch < 0) {
        show_error($"Could not load sprite {path}", true);
    }
    
    return fetch;
}


/**
* Read a files text contents
* @param {string} filename the file to open
* @returns {string} file content string or undefined
*/
function file_read(filename){
    if (not file_exists(filename)) return undefined;
    
    var file = file_text_open_read(filename);
    var text = "";
    while (not file_text_eof(file)) {
        text += file_text_readln(file) + "\n";
    }
    file_text_close(file);
    return text;
    
}

/**
* Merge one struct's content into another
* @param {struct} dest struct to write to
* @param {struct} src struct to read from
* @param {string} [field] optional field to merge
*/
function struct_merge(dest, src, field=undefined) {
    if (is_undefined(field)) {
        var fields = struct_get_names(src);
        for(var i = 0; i < array_length(fields); ++i) {
            field = fields[i];
            struct_merge(dest,src, field);
        }
    } else {
        var value = variable_struct_get(src, field);
        variable_struct_set(dest, field, value);
    }
}

