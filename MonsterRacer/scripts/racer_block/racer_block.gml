// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function racer_block(_creature) constructor{
	creature = _creature;
	node = noone;
	link_target_index = 0;
	progress = 0;
	momentum = 0;
	current_stamina = creature.get_stamina();
    place = 1;
    remaining = 0;
	
	function to_string(){
		var text = creature.to_string();
		text += $"{node.to_string()}\n";
		text += $"Progress {progress}\n";
		text += $"Momentun {momentum}\n";
		text += $"Current Stamina {current_stamina}\n";
		
		return text;
	}
}