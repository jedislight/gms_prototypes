// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function race_context() constructor {
	starting_nodes = [];
	racers = [];
	turn_counter = 1;
	racer_cursor = 0;
	finished_racers = [];
    
    function update_remaining(racer){
        var sum = -racer.progress + racer.place;
        var node_it = racer.node;
        while(array_length(node_it.links)) {
            sum += node_it.progress;
            node_it = node_it.links[0];
        }
        racer.remaining = sum;
    }
    
    function update_places(){
        for(var i = 0; i < array_length(racers); ++i) {
            var racer = racers[i];
            update_remaining(racer);
        }
        
        array_sort(racers, function(a,b){ return a.remaining - b.remaining;})
        var locked_placed = array_length(finished_racers);
        for(var i = 0; i < array_length(racers); ++i) {
            var racer = racers[i];
            racer.place = locked_placed + i + 1;
        }
        for(var i = 0; i < array_length(finished_racers); ++i) {
            var racer = finished_racers[i];
            racer.place = i + 1;
        }
    }
	
	function update_link_target(racer) {
		racer.link_target_index = irandom_range(0, array_length(racer.node.links)-1)
	}
	
	function update_pawns(){
		with(NodePawn) {
			racers = [];
		}
		
		for(var i = 0; i < array_length(racers); ++i){
			var racer = racers[i];
			var nodePawn = noone;
			with(NodePawn) {
				if(node == racer.node) {
					array_push(racers, racer);	
				}
			}
		}
		
		for(var i = 0; i < array_length(finished_racers); ++i){
			var racer = finished_racers[i];
			var nodePawn = noone;
			with(NodePawn) {
				if(node == racer.node) {
					array_push(racers, racer);	
				}
			}
		}
	}
	
	function step(stamina_spend, node_link_index) {
		if(is_undefined(stamina_spend)) {
			stamina_spend = -1;	
		}
		
		if(is_undefined(node_link_index)) {
			node_link_index = 0;	
		}
		
		if(array_length(racers) == 0) {
			return true;	
		}
		
		var del_index = racer_cursor;
		var racer = racers[racer_cursor++];
		if(racer_cursor >= array_length(racers)) {
			racer_cursor = 0;
		}
		
		if ( stamina_spend == -1) {
			var turns_till_exhastion_resting = compute_exhaustion(racer.current_stamina, 0, racer.creature.get_recovery(), racer.momentum);
			var max_stamina_spend = 0;
		
			for(var spend = 1; spend <= racer.creature.get_strength(); ++spend){
				var turns_till_exhaustion_for_spend = compute_exhaustion(racer.current_stamina, spend, racer.creature.get_recovery(), racer.momentum)
				if(is_infinity(turns_till_exhaustion_for_spend)) {
					max_stamina_spend = spend;
				}
			}
			
			stamina_spend = max_stamina_spend;
		}
		
		show_debug_message($"{racer.to_string()}\n")
		
		racer.momentum = racer.momentum div 2;
		if(stamina_spend == 0) {
			racer.current_stamina += racer.creature.get_recovery();
		} else {
			racer.momentum += stamina_spend;	
		}
		
		racer.current_stamina -= racer.momentum;
		
		var s = racer.creature.species.get_speed(racer.node);
		var current_speed = 0;
		repeat((racer.momentum div 10)+1) {
			var roll = random_range(0, s);
			if(roll > current_speed){
				current_speed = roll;	
			}
		}
		
		while(current_speed > 0 or (racer.progress > racer.node.progress)) {
			var left = racer.node.progress - racer.progress;
			if(left <= 0) {
				var done = array_length(racer.node.links) == 0;
				if (done) {
					array_push(finished_racers, racer);
					array_delete(racers, del_index, 1);
					racer_cursor = 0;
					break;
				}
				
				racer.node = racer.node.links[node_link_index];
				update_link_target(racer);
				node_link_index = racer.link_target_index; // TODO make index an array to path multiple nodes ahead
				racer.progress = 0;
				
				current_speed -= left;
			} else {
				racer.progress += current_speed;	
				current_speed = 0;
			}
		}
		
		show_debug_message($"{racer.to_string()}\n")
		update_places();
		return false;
	}
	
	function compute_exhaustion(current_stamina, stamina_spend, recovery, momentum) {
		var turns = 0;
		while(true){
			if (current_stamina < 0) {
				return turns;	
			}
			if(stamina_spend == 0) {
				momentum = momentum div 2;
			} else {
				momentum = momentum + stamina_spend;
			}
			var delta = momentum - recovery;
			if(delta <= 0) {
				return infinity;	
			}
			
			current_stamina -= delta;
			stamina_spend = 0;
		}
	}
}