// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function node_block() constructor{
	progress = 10;
	links = []
	name = "Node Name";
	
	function to_string() {
		var link_names = array_map_copy(links, function(l){return l.name});
		var text = $"{name} ({progress}) -> [{string_join_ext(",",link_names)}]";
		
		return text;
	}
	
	function new_link(name, progress) {
		var n = new node_block();
		n.name = name;
		n.progress = progress;
		array_push(links, n);
		return n;
	}
}

function array_map_copy(array, f) {
	var n = array_clone(array);
	array_map_ext(n, f);
	return n;
}

function array_clone(array){
	var n = [];
	array_copy(n, 0, array, 0, array_length(array))
	return n;
}