/// @description Insert description here
// You can write your code in this editor

if (instance_exists(parent)) {
	x = parent.x + parent_x_offset;
	y = parent.y + parent_y_offset;
}

var front_most_clicked_depth = 9999;
if (raw_clicked_this_frame) with(UI) {
	if(raw_clicked_this_frame and front_most_clicked_depth > depth) {
		front_most_clicked_depth = depth;
	}
}

if(front_most_clicked_depth == depth) {
    var f = on_click;
	with(execution_context){
        f();
    }
}

