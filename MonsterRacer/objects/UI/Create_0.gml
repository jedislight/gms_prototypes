/// @description Insert description here
// You can write your code in this editor
parent = noone
parent_x_offset = 0
parent_y_offset =  0
raw_clicked_this_frame = false;
text = "";
execution_context = id;;

function create_child(obj, xx, yy) {
	var n = instance_create_depth(0,0, depth-1, obj);
	n.parent = id;
	n.parent_x_offset = xx;
	n.parent_y_offset = yy;
	
}

function on_click() {
	show_debug_message($"Click: {object_get_name(object_index)}::{id}")	
}