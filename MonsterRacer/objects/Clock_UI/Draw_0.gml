/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

var season_name;
switch(Game.time.season) {
    case 0: season_name = "Spring"; break;
    case 1: season_name = "Summer"; break;
    case 2: season_name = "Fall"; break;
    case 3: season_name = "Winter"; break;
}

draw_set_halign(fa_right);
draw_text_color(room_width, 0, $"{season_name} Year {Game.time.year}", c_white, c_white, c_white, c_white, 1.0);
draw_set_halign(fa_left);

