/// @description Insert description here
// You can write your code in this editor
draw_self();

var split = room_height div 3 * 2;
var sprite = sprite_cache(conversation.lines[line_index].speaker_sprite);
var xx = room_width - sprite_get_width(sprite);
var yy = split - sprite_get_height(sprite);
draw_sprite(sprite, 0, xx, yy);

draw_text_ext_color(0, split, conversation.lines[line_index].text, 20, room_width, c_white, c_white, c_white, c_white, 1.0)
