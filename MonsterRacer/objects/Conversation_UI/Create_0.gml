/// @description Insert description here
// You can write your code in this editor
event_inherited();

x = 0;
y = room_height - sprite_height;
line_index = 0;

function load_conversation(conversation_name) {
    var raw_json = file_read($"conversations/{conversation_name}.json");
    conversation = json_parse(raw_json);
}

function on_click() {
    line_index += 1;
    if(line_index >= array_length(conversation.lines)) {
        instance_destroy();
    }
    
}