/// @description Insert description here
// You can write your code in this editor
if (node == noone) exit;

draw_set_color(c_lime);
draw_circle(x,y, 64, true)
draw_set_color(c_white);

function draw_node_connection(s, e) {
	var x1, x2, y1, y2, dir, radius, dist;
	radius = 64;
	dir = point_direction(s.x, s.y, e.x, e.y);
	dist = point_distance(s.x , s.y, e.x, e.y);
	if(dist< radius*2){
		draw_line_width_color(x,y, link_pawn.x, link_pawn.y, 5, c_red, c_blue);
	} else {
		x1 = s.x + lengthdir_x(radius, dir);
		x2 = e.x - lengthdir_x(radius, dir);
		y1 = s.y + lengthdir_y(radius, dir);
		y2 = e.y - lengthdir_y(radius, dir);
		draw_line_width_color(x1, y1, x2, y2, 5, c_red, c_blue);
	}
}

function find_racer_pawn(racer) {
    var rp = noone;
    with(RacerPawn) {
        if (id.racer = racer) {
            rp = id;
        }
    } 
    
    return rp;
}

for(var i = 0; i < array_length(node.links); ++i){
	var link = node.links[i];
	var link_pawn = noone;
	with(NodePawn) {
		if(node == link) {
			link_pawn = id;	
		}
	}
	
	if(instance_exists(link_pawn)) {
		draw_node_connection(id, link_pawn);
		
		for(var r = 0; r< array_length(racers); ++r) {
			var racer = racers[r];
            var racer_pawn = find_racer_pawn(racer);
			var target_node = node.links[racer.link_target_index];
			if(link_pawn.node != target_node) continue;
			var p = racer.progress / node.progress;
			var dir = point_direction(x, y, link_pawn.x, link_pawn.y);
			var dist = point_distance(x,y, link_pawn.x, link_pawn.y) * p;
			var xx = x + lengthdir_x(dist, dir);
			var yy = y + lengthdir_y(dist, dir);
            racer_pawn.real_x = xx;
            racer_pawn.real_y = yy;
		}
	} 
}

if (array_length(node.links) == 0){
	for(var r = 0; r< array_length(racers); ++r) {
		var racer = racers[i];
        var racer_pawn = find_racer_pawn(racer);
        racer_pawn.real_x = x;
        racer_pawn.real_y = y;
	}	
}