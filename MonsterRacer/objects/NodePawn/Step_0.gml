/// @description Insert description here
// You can write your code in this editor

instance_deactivate_object(id);
var nearest = instance_nearest(x,y, NodePawn);
if(instance_exists(nearest)) {
	var d = point_distance(x,y, nearest.x, nearest.y);
	if(d < 128){
		var dir = point_direction(nearest.x, nearest.y, x, y);
		x += lengthdir_x(1, dir);
		y += lengthdir_y(1, dir);
	}
}
instance_activate_object(id)