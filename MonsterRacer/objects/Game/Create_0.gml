/// @desc Insert description here
// You can write your code in this editor
randomize();
draw_set_font(GameDefaultFont);

time = {
    year: 1,
    season: 0
}

function advance_season() {
    time.season += 1;
    if (time.season >= 4) {
        time.year += 1;
        time.season = 0;
    }
}

// load species data files
species = {}
var species_it = file_find_first("species/*.json", 0)
while (species_it != "") {
    var path = $"species/{species_it}";
    species_it = file_find_next();
    
    var filename = filename_name(path)
    var species_name = filename_change_ext(filename, "")
    var json_string = file_read(path);
    var species_data = json_parse(json_string);
    var s = new species_block();
    s.name = species_name;
    struct_merge(s.attributes, species_data, "strength")
    struct_merge(s.attributes, species_data, "agility")
    struct_merge(s.attributes, species_data, "recovery")
    struct_merge(s.attributes, species_data, "stamina")
    struct_merge(s, species_data, "decays_into")
    struct_merge(s, species_data, "max_racing_seasons")
    struct_merge(s, species_data, "min_racing_seasons")
    struct_merge(s, species_data, "max_breeding_age")
    
    variable_struct_set(species, species_name, s);
    var sprite_file_path = $"species/{species_name}.png";
    if(file_exists(sprite_file_path)) {
        s.sprite = sprite_add(sprite_file_path, 1, false, false, 0,0);
    }
    show_debug_message($"Loaded species: {species_name} [{s}]");
}

debug_creature = new creature_block(species.Zombie);
show_debug_message($"{debug_creature.to_string()}")

debug_creature.base.strength = 10;
show_debug_message($"{debug_creature.to_string()}")

debug_creature.trained.strength = 10;
show_debug_message($"{debug_creature.to_string()}")

function debug_test_race(){
   debug_racer = new racer_block(debug_creature);
   
   debug_track = new node_block();
   debug_track.progress = 100;
   debug_track.name = "Debug Node Start"
   var a = debug_track.new_link("Debug Node A", 110);
   var b = debug_track.new_link("Debug Node B", 90);
   var e = a.new_link("Debug Node End", 0);
   b.links = [e];
   	
   debug_race = new race_context();
   debug_race.racers = [debug_racer];
   debug_race.starting_nodes = [debug_track];
   debug_racer.node = debug_track;
   repeat(5) {
   	array_push(debug_race.racers, new racer_block(new creature_block(choose(species.Zombie, species.Skeleton))));
   	debug_race.racers[array_length(debug_race.racers)-1].node = debug_track;
   }
   
   debug_button = instance_create_depth(0,0,0, UI);
   _on_click = debug_button.on_click;
   debug_button.on_click = function() {
   	Game._on_click();
   	var next_racer = noone;
   	if (array_length(Game.debug_race.racers) > 0) {
   		next_racer = Game.debug_race.racers[Game.debug_race.racer_cursor];
   	}
   	var target_node_index = 0;
   	if(next_racer != noone) {
   		target_node_index = next_racer.link_target_index;	
   	}
   	Game.debug_race.step(-1, target_node_index);	
   	Game.debug_race.update_pawns();
   }
   
   array_foreach(Game.debug_race.racers, function(racer) {
       var n = instance_create_depth(0,0,0, RacerPawn);
       n.racer = racer;
       n.race = Game.debug_race;
   });
   
   debug_node_pawn = instance_create_depth(200, 200, 0, NodePawn);
   debug_node_pawn.node = debug_track;
   
   debug_node_pawn = instance_create_depth(400, 100, 0, NodePawn);
   debug_node_pawn.node = a;
   
   debug_node_pawn = instance_create_depth(400, 200, 0, NodePawn);
   debug_node_pawn.node = b;
   
   debug_node_pawn = instance_create_depth(800, 200, 0, NodePawn);
   debug_node_pawn.node = e;
   
   debug_race.update_pawns();
   for(var i = 0; i < array_length(debug_race.racers); ++i) {
        debug_race.update_link_target(debug_race.racers[i]);
        var lp = instance_create_depth(room_width-414, 100, 0, LeaderBoardCardPawn);
        lp.racer = debug_race.racers[i];
   }
}

function debug_test_conversation() {
    debug_conversation = instance_create_depth(0,0,0, Conversation_UI);
    debug_conversation.load_conversation("muldread_visuals_test")
}

var debug_start_race_button = instance_create_depth(0,0,0, UI);
debug_start_race_button.on_click = debug_test_race;
debug_start_race_button.text = "Test: Race";
debug_start_race_button.execution_context = id;

var debug_start_conversation_button = instance_create_depth(0,64,0, UI);
debug_start_conversation_button.on_click = debug_test_conversation;
debug_start_conversation_button.text = "Test: Conversation"
debug_start_conversation_button.execution_context = id;

var debug_next_season_button = instance_create_depth(0,128, 0, UI);
debug_next_season_button.on_click = advance_season;
debug_next_season_button.execution_context = id;
debug_next_season_button.text = "Test: Advance Time"

var clock_ui = instance_create_depth(0,0,0, Clock_UI);


