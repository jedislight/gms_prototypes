/// @description Insert description here
// You can write your code in this editor
var spin_input = keyboard_check(ord("Z"));
var jump_input = keyboard_check(ord("X"));
var run_input = {
	x: -keyboard_check(vk_left) + keyboard_check(vk_right),
	y: -keyboard_check(vk_up) + keyboard_check(vk_down)
}
var run_dir = point_direction(0,0, run_input.x, run_input.y);
var run_input_magnitude = point_distance(0,0, run_input.x, run_input.y);
if(run_input_magnitude > 1) {
	run_input.x = lengthdir_x(1, run_dir);
	run_input.y = lengthdir_y(1, run_dir);
}

if(spin_input and sprite_index != SpriteOtterSpin) {
	sprite_index = SpriteOtterSpin;
	image_index = 0;
}

if(jump_input and sprite_index != SpriteOtterSpin and sprite_index != SpriteOtterJump) {
	image_index = 0;
	sprite_index = SpriteOtterJump;
}

if((run_input.x != 0 or run_input.y != 0) and sprite_index != SpriteOtterJump and sprite_index != SpriteOtterSpin) {
	sprite_index = SpriteOtterRun;	
}

if(sprite_index != SpriteOtterSpin) {
	var move_speed = 8;
	x += run_input.x * move_speed;
	y += run_input.y * move_speed;
	
	var new_facing = sign(run_input.x);
	if(new_facing != 0) {
		facing = new_facing;	
	}
}