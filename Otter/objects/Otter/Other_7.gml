/// @description Insert description here
// You can write your code in this editor

if(sprite_index == SpriteOtterIdle) {
	sprite_index = choose(SpriteOtterIdle, SpriteOtterIdle, SpriteOtterIdle, SpriteOtterIdleAlternate);	
	idle_count += 1;
} else if (sprite_index == SpriteOtterIdleAlternate) {
	sprite_index = SpriteOtterIdle;
	idle_count += 2;
	if(idle_count > 10) {
		sprite_index = SpriteOtterSleep;	
	}
} else if (sprite_index == SpriteOtterJump) {
	sprite_index = SpriteOtterLand;	
	idle_count = 0;	
} else if (sprite_index == SpriteOtterSleep) {
	idle_count += 1;	
} else {
	sprite_index = SpriteOtterIdle;	
	idle_count = 0;	
}