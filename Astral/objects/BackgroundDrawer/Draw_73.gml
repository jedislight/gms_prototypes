/// @description Insert description here
// You can write your code in this editor


var camera = view_camera[0]

var cam_x = camera_get_view_x(camera)
var cam_y = camera_get_view_y(camera);
surface_set_target(stary_night_surface);
draw_clear(c_black)
var day_paramater = Game.surface.atmosphere ? Game.day_paramater : 0.75;
var distance_from_noon = min(abs(Game.day_paramater-0.25), abs(Game.day_paramater-1.25));
var sky_value = power(distance_from_noon, 3)
if(sky_value > 0.25) sky_value = power(sky_value, 1/8);
var sky_color = make_color_hsv(Game.surface.sky_hue, 0.133*255, sky_value*255);
var offset = Game.day_paramater * surface_get_width(stary_night_surface);
draw_sprite_stretched_ext(Sprite20, 0, -offset, 0, surface_get_width(stary_night_surface), surface_get_height(stary_night_surface), sky_color, 1.0)
draw_sprite_stretched_ext(Sprite20, 0, -offset+surface_get_width(stary_night_surface), 0, surface_get_width(stary_night_surface), surface_get_height(stary_night_surface), sky_color, 1.0)
draw_sprite_stretched_ext(Sprite916, 0, 0, 0, surface_get_width(stary_night_surface), surface_get_height(stary_night_surface), c_black, 1.0)
var ground_pixel_y = Game.chunk_manager.get_ground_pixel()
draw_rectangle_color(0, ground_pixel_y-cam_y, surface_get_width(stary_night_surface), ground_pixel_y + Game.surface.mantle_depth * Game.chunk_manager.chunk_height_pixels-cam_y, c_black, c_black, c_black, c_black, false);
surface_reset_target();
gpu_set_blendmode(bm_max)
repeat(8) draw_surface(stary_night_surface, cam_x, cam_y);
gpu_set_blendmode(bm_normal)