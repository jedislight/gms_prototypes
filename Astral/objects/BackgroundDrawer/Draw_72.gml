/// @description Insert description here
// You can write your code in this editor

var camera = view_camera[0]

var cam_x = camera_get_view_x(camera)
var cam_y = camera_get_view_y(camera);
var sky_color = make_color_hsv(Game.surface.sky_hue, 0.133*255, 255);
draw_sprite_stretched_ext(Sprite17, 0, cam_x, cam_y, camera_get_view_width(camera), camera_get_view_height(camera), sky_color, 1.0)


draw_self();
var sky_sprite = Sprite916;

draw_sprite_stretched(Sprite916, 0, cam_x, cam_y, camera_get_view_width(camera), camera_get_view_height(camera))

var ground_pixel_y = Game.chunk_manager.get_ground_pixel()
var ground_background_color = #878733
draw_rectangle_color(cam_x, ground_pixel_y, cam_x+camera_get_view_width(camera), ground_pixel_y + Game.surface.mantle_depth * Game.chunk_manager.chunk_height_pixels, ground_background_color, ground_background_color, ground_background_color, ground_background_color, false);


