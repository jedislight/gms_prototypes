/// @description Insert description here
// You can write your code in this editor


var surface_distance_from_star = Game.surface.orbital_distance;
var scale = (star.get_radius()) / surface_distance_from_star;
image_xscale = scale;
image_yscale = scale;
image_angle += random_range(-1.5, 1.5) * (1- scale);

var day_paramater = Game.day_paramater;

var camera = view_get_camera(0);
var cx = camera_get_view_x(camera);
var cy = camera_get_view_y(camera);


var px = abs(day_paramater-0.5)*2;
var py = cos(day_paramater*2*pi+2.5*pi) * 0.5 + 0.55;

x = lerp(cx, cx + camera_get_view_width(camera), px);
y = lerp(cy, cy + camera_get_view_height(camera), py);

var dir = point_direction(px, py, 0.5, 0.5)
with(DirectionalLight){ 
	var atmo_impact = clamp(abs(angle_difference(270,direction)) / 128, 0, 1);
	atmo_impact = atmo_impact;
	strength_mod = 1-atmo_impact;
	shift_hue = atmo_impact * 25;
	shift_value = atmo_impact * -128;
	direction = dir
}