/// @description Insert description here
// You can write your code in this editor

light_surface = surface_create(room_width, room_height);
swap_surface = surface_create(room_width, room_height);
surface_set_target(light_surface)
draw_clear(c_black);
surface_reset_target();
frame_lines = ds_list_create();
sunlight_sample_seperation = 12;
light_ray_drawing = false;
fade_speed = 1;
fade_intensity = 211;
auto_performance = true;
sample_rate_mod = 1;
light_ray_width = 73;
collision_group_add(COLLISION_GROUP.LIGHT_BLOCKING, layer_tilemap_get_id("Tiles"));

custom_dbgview = dbg_view("Light Controller", false, 0,0,300, 256);

cg_light_blocking = [];

dbg_checkbox(ref_create(self, "light_ray_drawing"), "Draw Light Rays")
dbg_slider_int(ref_create(self, "fade_speed"), 1, 60);
dbg_slider_int(ref_create(self, "fade_intensity"), 0, 255);
dbg_slider_int(ref_create(self, "light_ray_width"), 1, 255);
dbg_slider(ref_create(self, "sample_rate_mod"), 0.5, 10);
dbg_checkbox(ref_create(self, "auto_performance"), "Auto Performance")
show_debug_overlay(false);


function color_devalue(c, percent_keep){
	var value = color_get_value(c);
	var final_value = value * percent_keep;
	return make_color_hsv(color_get_hue(c), color_get_saturation(c), final_value);
}


function project_point(v, from_width, to_width){
	return (v / from_width) * to_width;
}

processing_list = ds_list_create()
function process_light_ray(ray){
	var cx = ray.x2;
	var cy = ray.y2;
	var cc = c_black;
	var emitter = ray.emitter;
	var bleed = 0;
	
	ds_list_clear(processing_list);
	var hit = false;
	with(instance_exists(emitter) ? emitter: self) {
		hit = collision_line_list(ray.x, ray.y, ray.x2, ray.y2, other.cg_light_blocking, true, true, other.processing_list, true);
	}
	if(hit){
		var hit_object = processing_list[| 0];
		var is_tilemap_hit = layer_tilemap_exists("Tiles", hit_object)
		with(is_tilemap_hit ? self : hit_object) {
			var bc = is_tilemap_hit ? c_white : bounce_color;
			// Calculate real distance via ray shortening
			var cast_distance = 0;
			var test_p = 0.5;
			var test_step = 0.5;
			while(test_step * ray.distance > 1.0) {
				var test_hit = collision_line(ray.x, ray.y, lerp(ray.x, ray.x2, test_p), lerp(ray.y,ray.y2, test_p), hit_object, true, true) > 0;
				test_step *= 0.5;
				if(test_hit) {
					test_p -= test_step;
				} else {
					test_p += test_step;
					cast_distance = ray.distance * test_p;
				}
			}
			
			
			bleed = other.project_point(72, room_width, surface_get_width(other.light_surface)); 
			cx = ray.x + lengthdir_x(cast_distance, ray.direction);
			cy = ray.y + lengthdir_y(cast_distance, ray.direction);
			cc = other.color_devalue(ray.color, cast_distance / ray.distance);
			
			
			var distance_remaining = (ray.distance-cast_distance)*(color_get_value(cc)/255);
			var reflectivity = 0.25;
			var final_color = make_color_rgb(
				min(color_get_red(cc), color_get_red(bc)) * reflectivity,
				min(color_get_green(cc), color_get_green(bc)) * reflectivity,
				min(color_get_blue(cc), color_get_blue(bc)) * reflectivity
			);
			if(distance_remaining > 50) repeat(8*other.sample_rate_mod){
				var dir = random(360);
				var fx = cx + lengthdir_x(32, dir);
				var fy = cy + lengthdir_y(32, dir);
				if(collision_point(fx, fy, other.cg_light_blocking, true, false) == noone){
					var bounce_ray = new LightRay(fx, fy, dir, distance_remaining-32, final_color, LightController.id);
					other.submit(bounce_ray);
				}
			}
		}
	}
	
	ds_list_add(frame_lines, {x:ray.x, y:ray.y, x2:cx, y2:cy, color:ray.color});
	
	surface_set_target(light_surface);
	gpu_set_blendmode_ext(bm_src_alpha, bm_inv_src_color)
	draw_line_width_color(
		project_point(ray.x, room_width, surface_get_width(light_surface)),
		project_point(ray.y, room_height, surface_get_height(light_surface)),
		project_point(cx, room_width, surface_get_width(light_surface)),
		project_point(cy, room_height, surface_get_height(light_surface)),
		light_ray_width,
		ray.color,
		cc
	);
	draw_circle_color(
		project_point(ray.x, room_width, surface_get_width(light_surface)),
		project_point(ray.y,  room_height, surface_get_height(light_surface)),
		bleed, ray.color, c_black, false);
	
	draw_circle_color(
		project_point(cx, room_width, surface_get_width(light_surface)),
		project_point(cy,  room_height, surface_get_height(light_surface)),
		bleed, cc, c_black, false);
	gpu_set_blendmode(bm_normal);
	surface_reset_target();
}

ray_queue = ds_queue_create();
function submit(ray){
	ds_queue_enqueue(ray_queue, ray);	
}

frame = 0;

function on_world_shift(xx,yy){
	surface_set_target(swap_surface);
	gpu_set_blendmode(bm_normal)
	draw_set_color(c_black);
	draw_rectangle(0,0, surface_get_width(swap_surface), surface_get_height(swap_surface), false);
	draw_surface(
		light_surface,
		xx,
		yy
	);
	surface_reset_target();
	
	var temp = light_surface;
	light_surface = swap_surface;
	swap_surface = temp;
	
}