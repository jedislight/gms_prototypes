/// @description Insert description here
// You can write your code in this editor

++frame;
cg_light_blocking = collision_group_get(COLLISION_GROUP.LIGHT_BLOCKING);
ds_list_clear(frame_lines);

// light processing 
while(ds_queue_size(ray_queue) > 0){
	var light_ray = ds_queue_dequeue(ray_queue);
	process_light_ray(light_ray);
}
if(auto_performance){
	if(fps_real > 80) sample_rate_mod = clamp(sample_rate_mod + .1, 0.5,4);
	else if(fps_real < 65) sample_rate_mod *= 0.95;
} else {
	sample_rate_mod = 1.0;	
}