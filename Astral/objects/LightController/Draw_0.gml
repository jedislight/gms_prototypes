/// @description Insert description here
// You can write your code in this editor
gpu_set_blendenable(true)

gpu_set_blendmode_ext(bm_dest_colour, bm_zero)
shader_set(BlurShader);
draw_surface_stretched(light_surface,0,0, room_width, room_height);
shader_reset();
gpu_set_blendmode(bm_normal);

//repeat(max(1, sample_rate_mod))
{
	surface_set_target(light_surface)	
	gpu_set_blendmode_ext(bm_dest_colour, bm_zero)
	var c = make_color_hsv(0,0, fade_intensity);
	draw_rectangle_color(0, 0, surface_get_width(light_surface), surface_get_height(light_surface), c,c,c,c, false)
	gpu_set_blendmode(bm_normal);
	surface_reset_target();
}

if(light_ray_drawing) for(var i = 0; i < ds_list_size(frame_lines); ++i){
	var l = frame_lines[| i];
	draw_set_color(l.color);
	draw_arrow(l.x, l.y, l.x2, l.y2, 5);
}
draw_set_color(c_white);