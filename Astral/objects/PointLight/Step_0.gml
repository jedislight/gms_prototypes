/// @description Insert description here
// You can write your code in this editor
if(mouse_follow){
	x = mouse_x;
	y = mouse_y;
}
++frame;

var c = 2 * pi * distance;
var density = 720 / (c / LightController.light_ray_width)/LightController.id.sample_rate_mod;
var in_matter = false;
var emitter = id;
if(instance_exists(emitter_anchor)) {
	emitter = emitter_anchor;
	x = emitter_anchor.x;
	y = emitter_anchor.y;
}
with(emitter) {
	in_matter = collision_point(x,y, collision_group_get(COLLISION_GROUP.LIGHT_BLOCKING), true, true) > 0
}
for(var d = 0 + random(density); d < 360; d+=density){
	var ray = new LightRay(x+offset_x,y+offset_y, d, distance, color, emitter);
	LightController.id.submit(ray);
	if(in_matter) break;
}