/// @description Insert description here
// You can write your code in this editor
with(LightController){
	surface_set_target(light_surface);
	gpu_set_blendmode_ext(bm_src_alpha, bm_inv_src_color)
	draw_rectangle_color(
		0,0,
		surface_get_width(light_surface),
		surface_get_height(light_surface),
		other.color,
		other.color,
		other.color,
		other.color,
		false
	);
	gpu_set_blendmode(bm_normal)
	surface_reset_target();
}