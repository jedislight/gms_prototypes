/// @description Insert description here
// You can write your code in this editor

function from_star(star){
	color = star.get_light();
	show_debug_message($"Setting light from star type {star.type} with paramater {star.radius_paramater} with final color {color_get_red(color)} {color_get_blue(color)} {color_get_green(color)}")
}

strength_mod = 1.0;
shift_hue = 0;
shift_value = 0;