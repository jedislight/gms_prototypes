/// @description Insert description here
// You can write your code in this editor
var sun_density = LightController.id.light_ray_width/1/LightController.id.sample_rate_mod;

var origin_x = room_width / 2 + lengthdir_x(room_width/2, direction+180);
var origin_y = room_height / 2 + lengthdir_y(room_height/2, direction+180);
var perp = direction + 90;
if (perp > 360) perp -= 360;
var sh = color_get_hue(color) + shift_hue;
if(sh > 255) sh -= 255;
if(sh < 0) sh += 255;
var ss = color_get_saturation(color);
var sv = clamp(color_get_value(color)+shift_value, 0, 255);
var shifted_color = make_color_hsv(sh, ss, sv);
for(var offset=random(sun_density); offset< max(room_width, room_height); offset+=sun_density){
	var strength = 1/max(1, ChunkManager.world_chunk_y) * room_width*2 * strength_mod;
	if(strength < ChunkManager.chunk_width_pixels) continue;
	
	var offset_x = lengthdir_x(offset, perp);
	var offset_y = lengthdir_y(offset, perp);
	
	var offset_x2 = lengthdir_x(offset, perp+180);
	var offset_y2 = lengthdir_y(offset, perp+180);
	
	
	LightController.submit(new LightRay(origin_x+offset_x,  origin_y+offset_y, direction, strength, shifted_color, id));
	LightController.submit(new LightRay(origin_x+offset_x2, origin_y+offset_y2, direction, strength, shifted_color, id));
}