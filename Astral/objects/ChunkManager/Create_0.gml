/// @description Insert description here
// You can write your code in this editor
chunk_width_tiles = 21;
chunk_height_tiles = 12;
tile_size = 64;

cam_x = 0;
cam_y = 0;
world_chunk_x = 0;
world_chunk_y = 1;

world_width_chunks = 8;
world_height_chunks = 8;

draw_chunk_boundaries = true;

loaded_chunks = [];

dbg_view("Chunk Manager", false, -1,-1, 300, 128);
dbg_watch(ref_create(self, "cam_x"))
dbg_watch(ref_create(self, "cam_y"))
dbg_watch(ref_create(self, "world_chunk_x"))
dbg_watch(ref_create(self, "world_chunk_y"))
dbg_checkbox(ref_create(self, "draw_chunk_boundaries"));
show_debug_overlay(false)

chunk_width_pixels = chunk_width_tiles * tile_size;
chunk_height_pixels = chunk_height_tiles * tile_size;



function chunk_wrap_x(v_){
	var r = v_;
	while(r >= world_width_chunks) {
		r -= world_width_chunks;
	}
	while(r < 0) {
		r += world_width_chunks;	
	}
	
	return r;
}

function chunk_wrap_y(v_){
	return clamp(v_, 0, world_height_chunks-1);
}

function get_loaded_chunk(xx, yy){
	for(var i = 0; i < array_length(loaded_chunks); ++i){
		var chunk = loaded_chunks[i];
		if(point_in_rectangle(xx,yy, chunk.px, chunk.py, chunk.px + chunk_width_pixels-1, chunk.py + chunk_height_pixels-1)){
			return chunk;	
		}
	}
	return noone;
}

function unload_chunk(i, surface=undefined, star=undefined){
	if(is_undefined(surface)) surface = Game.surface;
	if(is_undefined(star)) star = Game.star;
	var chunk = loaded_chunks[i];
	chunk.owned_instances = array_filter(chunk.owned_instances, instance_exists);
	chunk.save(chunk.get_filename(star.name, surface.name));
	array_delete(loaded_chunks, i, 1);
	array_foreach(chunk.owned_instances, instance_destroy);	
	show_debug_message($"Unloading Chunk {chunk.cx} {chunk.cy} @ {chunk.px} {chunk.py}")	
}

function unload_all(){
	while(array_length(loaded_chunks)) unload_chunk(0);
}

function get_chunk_prefix(){
	return $"{Game.surface.name}_";	
}

function get_sub_chunk_paramater_x(xx){
	return (xx mod chunk_width_pixels) / chunk_width_pixels;
}

function get_sub_chunk_paramater_y(yy){
	return (yy mod chunk_height_pixels) / chunk_height_pixels;
}

function get_world_x_paramater(xx){
	var raw = world_chunk_x + get_sub_chunk_paramater_x(xx);
	return raw / world_width_chunks;
}

function get_ground_pixel(){
	var camera = view_get_camera(0);
	var cam_y = camera_get_view_y(camera);
	
	var chunk_ground = 3.5;
	var pixel_ground = chunk_ground * chunk_height_pixels;
	var ground_py_zeroed = chunk_height_pixels * chunk_ground;
	var ground_py = ground_py_zeroed - chunk_height_pixels * world_chunk_y;
	return ground_py;
}