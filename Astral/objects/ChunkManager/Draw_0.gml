/// @description Insert description here
// You can write your code in this editor
if(draw_chunk_boundaries){
	draw_set_color(c_black)
	for(var xx = 0; xx < 3; ++xx) for(var yy =0; yy < 3; ++yy){
		draw_rectangle(
			xx*chunk_width_pixels,
			yy*chunk_height_pixels,
			(xx+1)*(chunk_width_pixels),
			(yy+1)*(chunk_height_pixels),
			true
		);
		
		draw_set_font(ChunkDebugFont);
		draw_text(
			xx*chunk_width_pixels+chunk_width_pixels*0.5,
			yy*chunk_height_pixels+chunk_height_pixels*0.5,
			$"CHUNK {chunk_wrap_x(xx-1+world_chunk_x)} {chunk_wrap_y(yy-1+world_chunk_y)}");
	}
}