/// @description Insert description here
// You can write your code in this editor
// update world from surface
world_height_chunks = Game.surface.mantle_depth;
world_width_chunks = Game.surface.width;

var camera = view_camera[0]

cam_x = camera_get_view_x(camera)
cam_y = camera_get_view_y(camera);
var cam_x2 = cam_x + camera_get_view_width(camera);
var cam_y2 = cam_y + camera_get_view_height(camera);

// re-centering
var shift_x = 0;
var shift_y = 0;
if(cam_x >= chunk_width_pixels*2) {
	shift_x = - chunk_width_pixels;	
}
if (cam_x < chunk_width_pixels) {
	shift_x = chunk_width_pixels;	
}
if(cam_y >= chunk_height_pixels*2) {
	shift_y = -chunk_height_pixels;
}
if(cam_y < chunk_height_pixels) {
	shift_y = chunk_height_pixels;	
}

//block y shifts outside of the surface chunks
if(world_chunk_y == 0 && sign(shift_y) == 1) {
	shift_y = 0;	
}

if(world_chunk_y == world_height_chunks-2 && sign(shift_y) == -1) {
	shift_y = 0;	
}

if(shift_x != 0 or shift_y != 0){
	show_debug_message($"World shift {shift_x} {shift_y}")
	
	// clear before moving objects, objects will replace tiles
	tilemap_clear(layer_tilemap_get_id("Tiles"), 0);
	
	with(all) {
		x += shift_x;
		y += shift_y;
		
		if(variable_instance_exists(id, "on_world_shift")){
			on_world_shift(shift_x, shift_y);	
		}
	}
	
	camera_set_view_pos(camera, cam_x+shift_x, cam_y+shift_y)
	world_chunk_x = chunk_wrap_x(world_chunk_x -sign(shift_x));
	world_chunk_y = chunk_wrap_y(world_chunk_y-sign(shift_y));
	for(var i = 0; i < array_length(loaded_chunks); ++i){
		var chunk = loaded_chunks[i];
		chunk.px += shift_x;
		chunk.py += shift_y;
	}
	show_debug_message($"World shift successful")
	exit; // don't update chunks during shift frame
}

// chunks
var cl = world_chunk_x;
var cr = chunk_wrap_x(world_chunk_x+1);
var ct = world_chunk_y;
var cb = chunk_wrap_y(world_chunk_y+1); 
var cll = chunk_wrap_x(world_chunk_x-1);
var ctt = chunk_wrap_y(world_chunk_y-1); 

var cl_pixel = (cam_x div chunk_width_pixels) * chunk_width_pixels
var ct_pixel = (cam_y div chunk_height_pixels) * chunk_height_pixels


// unload chunks and look for existing chunks already loaded
var chunk_load_structs = [
	{ cl_load : true, cl_x: cl, cl_y: ct , cl_px: cl_pixel, cl_py: ct_pixel},
	{ cl_load : true, cl_x: cr, cl_y: ct , cl_px: cl_pixel+chunk_width_pixels, cl_py: ct_pixel},
	{ cl_load : true, cl_x: cl, cl_y: cb , cl_px: cl_pixel, cl_py: ct_pixel+chunk_height_pixels},
	{ cl_load : true, cl_x: cr, cl_y: cb , cl_px: cl_pixel+chunk_width_pixels, cl_py: ct_pixel+chunk_height_pixels},
	{ cl_load : true, cl_x: cl, cl_y: ctt , cl_px: cl_pixel, cl_py: ct_pixel-chunk_height_pixels},
	{ cl_load : true, cl_x: cll, cl_y: ct , cl_px: cl_pixel-chunk_width_pixels, cl_py: ct_pixel},
	{ cl_load : true, cl_x: cll, cl_y: ctt , cl_px: cl_pixel-chunk_width_pixels, cl_py: ct_pixel-chunk_height_pixels},
	{ cl_load : true, cl_x: cr, cl_y: ctt , cl_px: cl_pixel+chunk_width_pixels, cl_py: ct_pixel-chunk_height_pixels},
]
for(var i = array_length(loaded_chunks)-1; i >= 0; --i){
	var chunk = loaded_chunks[i];
	var keep = false;
	for(var c = 0; c < array_length(chunk_load_structs); ++c){
		var cls = chunk_load_structs[c];
		if (chunk.cx == cls.cl_x and chunk.cy == cls.cl_y){
			keep = true;
			cls.cl_load = false;
		}
	}
		
	if(not keep){
		unload_chunk(i);		
	}
}

// load chunks de-dupe
for(var i = array_length(chunk_load_structs)-1; i >= 0; --i){
	var me = chunk_load_structs[i];
	for(var j = 0; j < i; ++j) {
		var them = chunk_load_structs[j];
		if(me.cl_x == them.cl_x and me.cl_y == them.cl_y){
			array_delete(chunk_load_structs, i, 1);
			break;
		}
	}
}
// load chunks
for(var i = 0; i < array_length(chunk_load_structs); ++i){
	var chunk_load_struct = chunk_load_structs[i];
	if(not chunk_load_struct.cl_load) continue;
	
	var chunk = new Chunk(
		id,
		chunk_load_struct.cl_x,
		chunk_load_struct.cl_y
	);
	chunk.load();
	chunk.spawn(chunk_load_struct.cl_px, chunk_load_struct.cl_py);
	
	array_push(loaded_chunks,(chunk))
	show_debug_message($"Loading Chunk {chunk.cx} {chunk.cy} @ {chunk.px} {chunk.py}")
}