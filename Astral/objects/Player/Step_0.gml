/// @description Insert description here
// You can write your code in this editor
event_inherited();
++frame;
if(instance_exists(light_source)) {
	light_source.direction = facing == 1  ? 0 : 180;
}

g_speed = g_speed_base * sqrt(Game.surface.get_g());
movement_speed = movement_speed_base * sqrt(Game.surface.get_g());
jump_height_blocks = jump_height_blocks_base / sqrt(Game.surface.get_g());

var solids = collision_group_get(COLLISION_GROUP.SOLIDS);
var climbables = collision_group_get(COLLISION_GROUP.CLIMBABLE);

var h_input = keyboard_check(ord("D")) - keyboard_check(ord("A")) + gamepad_button_check(0, gp_padr) - gamepad_button_check(0, gp_padl);
var v_input = keyboard_check(ord("S")) - keyboard_check(ord("W")) + gamepad_button_check(0, gp_padd) - gamepad_button_check(0, gp_padu);
var jump_input = keyboard_check(vk_space) or gamepad_button_check(0, gp_face2);
var jump_input_pressed = keyboard_check_pressed(vk_space) or gamepad_button_check_pressed(0, gp_face2);

// H Move
var h_movement = h_input
var v_movement = v_input;

if(h_movement != 0) facing = sign(h_movement);
move_and_collide(h_movement*movement_speed, 0, solids);
if(h_movement == 0) image_index = 0;
if(h_movement != 0) image_index = round(frame * movement_speed/115) % 2 + 1;

// V Move
if(jump_input_pressed){
	jump_buffer_time = jump_input_pressed * jump_buffer_time_max;
}
jump_buffer_time--;
var on_solid = collision_line(x+1, y+sprite_height+2, x+sprite_width-1, y+sprite_height+2, solids, true, true)
if(on_solid) cyotee_time = cyotee_time_max; else --cyotee_time;
if(cyotee_time and jump_buffer_time > 0){
	jump_apex = y - ((jump_height_blocks + 0.5) * 64)
	jump_phase = 1;
	cyotee_time = 0;
} else if (not on_solid and not jump_input){
	jump_apex = y;
}

if(jump_apex >= y){
	jump_phase = -1;
}

// Ladder Move
var ladder_collision = instance_place(x,y, climbables);
if(not instance_exists(ladder_collision)) on_ladder = false;
if(instance_exists(ladder_collision) and v_movement != 0){
	x = ladder_collision.x;
	move_and_collide(0, v_movement, solids);
	jump_apex = y;
	on_ladder = true;
} else if (not on_ladder) { // gravity/jump
	var amount = jump_phase == 1 ? -g_speed*2-jump_height_blocks : g_speed;
	var collisions = move_and_collide(0, amount, solids, abs(amount));
	if(array_length(collisions) > 0) jump_apex = y;
}

// invul
if (invulnerable > 0) invulnerable -= 1;

// placement
if(mouse_check_button(mb_right)){
	if(collision_point(mouse_x, mouse_y, Block, false, false) == noone) {
		var nx = mouse_x div 64 * 64;
		var ny = mouse_y div 64 * 64;		
		var chunk = ChunkManager.get_loaded_chunk(nx, ny);		
		if(chunk == noone){
			show_debug_message("Player trying to place object outside of chunks, ignoring placement");
		} else {
			var n = instance_create_layer(nx, ny, "Background_Instances", BlockRegolith)	
			array_push(chunk.owned_instances, n);
			show_debug_message($"Object {object_get_name(n.object_index)} placed by player assigned to chunk {chunk.cx} {chunk.cy}");
		}
	}
}