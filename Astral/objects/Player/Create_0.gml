/// @description Insert description here
// You can write your code in this editor

event_inherited();
frame = 0;
image_speed = 0; // manually animating

facing = 1;
jump_apex = y;
jump_phase = 1;

on_ladder = false;

cyotee_time_max = 8;
cyotee_time = 0;

jump_buffer_time_max = 8;
jump_buffer_time = 0;

invulnerable = 0;

jump_height_blocks_base = 5;
jump_height_blocks = jump_height_blocks_base;

movement_speed_base = 64 / 60 * 5;
movement_speed = movement_speed_base;
g_speed_base = 64/60 * 8;
g_speed = g_speed_base

bounce_color = c_ltgray

light_source = instance_create_depth(x,y,depth, SpotLight);
light_source.emitter_anchor = id;
light_source.offset_x = sprite_width/2;
light_source.offset_y = sprite_height /2;
//light_source.color = #050510

function on_world_shift(xx,yy){
	jump_apex += yy;	
}

collision_group_add(COLLISION_GROUP.SOLIDS, layer_tilemap_get_id("Tiles"));