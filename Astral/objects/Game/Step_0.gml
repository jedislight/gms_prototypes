/// @description Insert description here
// You can write your code in this editor
day_paramater_velocity = 1/ 60000 * surface.rotation_speed;
day_paramater_local += day_paramater_velocity;
if(day_paramater_local > 1) day_paramater_local -= 1;
var camera = view_get_camera(0);
var cam_x = camera_get_view_x(camera);
day_paramater = ChunkManager.get_world_x_paramater(cam_x) + day_paramater_local;
if(day_paramater > 1) day_paramater -= 1;


var next_transition = 0.5;
if(day_paramater > next_transition) {
	next_transition = 1.0;	
}

var frames_until_day_transition = (next_transition - day_paramater) / day_paramater_velocity;
seconds_until_day_transition = frames_until_day_transition / 60;