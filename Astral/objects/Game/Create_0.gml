/// @description Insert description here
// You can write your code in this editor
randomize();
star = new Star();
surface = new Surface();
chunk_manager = noone;
light_controller = noone;
background_drawer = noone;
global_ambient_light = instance_create_layer(0,0, "Instances", AmbientLight)
global_ambient_light.color = #010102;
global_directional_light = instance_create_layer(0,0, "Instances", DirectionalLight)

function set_star(star_){
	star = star_;
	with(BackgroundDrawer) set_star(star_);
	with(DirectionalLight) from_star(star_);
}

dbg_view("Game", false, -1,-1, 250,250);
dbg_watch(ref_create(self, "day_paramater"));
dbg_slider(ref_create(self, "day_paramater_local"), 0, 1);
dbg_watch(ref_create(self, "seconds_until_day_transition"));

day_paramater = 0.1;
day_paramater_local = 0.1;
day_paramater_velocity = 0;
seconds_until_day_transition = 0;

use_F = true;
buffered_temp = 0;
buffered_radiation = 0;
function get_temp(){
	return surface.get_temp(day_paramater, chunk_manager.world_chunk_y)
}

function get_radiation() {
	return surface.get_radiation(day_paramater, chunk_manager.world_chunk_y);
}

function init_star(name=undefined){
	var new_star = new Star();
	var loaded = false;
	if(not is_undefined(name)) {
		new_star.name = name;	
		loaded = new_star.load();
		if(not loaded){
			new_star = game_generate_random_star(name);
		}
	} else {
		new_star = game_generate_random_star();	
	}
	
	set_star(new_star);
	surface = new_star.surfaces[0];
	chunk_manager.unload_all();
	day_paramater_local = 0.1;
	
	if(not loaded){
		star.save();	
	}
}

var temp_updater = time_source_create(time_source_game, 2, time_source_units_seconds, method(self, function(){
	buffered_temp = get_temp()
	buffered_radiation = get_radiation();
}), [], -1 );
time_source_start(temp_updater);