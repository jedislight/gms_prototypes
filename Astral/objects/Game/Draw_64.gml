// rise/set
var minutes_text = $"{seconds_until_day_transition div 60}";
var seconds_text =  $"{(seconds_until_day_transition mod 60) div 15 * 15}";
if(string_length(minutes_text) == 1) minutes_text = "0"+minutes_text;
if(string_length(seconds_text) == 1) seconds_text = "0"+seconds_text;
var text = $"{minutes_text}:{seconds_text}";

//temp
var temp = buffered_temp
var unit_temp = temp;
var unit = "C"
if(use_F) {
	unit_temp = temp * 9 / 5 + 32;
	unit = "F"
}
var final_temp = round(unit_temp);
var temp_string = $"{final_temp}{unit}";

//gravity
var grav = surface.get_g();
var grav_string = $"{round(grav*10)/10}G"

// o2
var o2 = surface.get_o2(chunk_manager.world_chunk_y);
var o2_earth_percent = round(o2 * 21);
var o2_string = $"{o2_earth_percent}%";

var radiation = buffered_radiation
var R = radiation_to_R(radiation);
var mR = radiation_to_mR(radiation);
var show_R = R == 0 ? mR : R;
var show_unit = R == 0 ? "mR" : "R";

var radiation_string = $"{show_R}{show_unit}"

// draw it all
var max_string = "999mR"
var yy = 0;
yy += draw_icon_with_text(Sprite19, text, 0, yy, UiFont, max_string, Game.day_paramater < 0.5);
yy += draw_icon_with_text(Sprite1921, temp_string, 0, yy, UiFont, max_string);
yy += draw_icon_with_text(Sprite192122, grav_string, 0, yy, UiFont, max_string);
yy += draw_icon_with_text(Sprite19212223, o2_string, 0, yy, UiFont, max_string);
yy += draw_icon_with_text(Sprite19212224, radiation_string, 0, yy, UiFont, max_string);
