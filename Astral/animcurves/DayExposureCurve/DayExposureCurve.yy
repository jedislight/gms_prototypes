{
  "$GMAnimCurve":"",
  "%Name":"DayExposureCurve",
  "channels":[
    {"$GMAnimCurveChannel":"","%Name":"curve1","colour":4290799884,"name":"curve1","points":[
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":0.0,"y":0.5,},
        {"th0":1.0,"th1":-1.0,"tv0":0.0,"tv1":0.0,"x":0.25,"y":1.0,},
        {"th0":1.0,"th1":-1.0,"tv0":0.0,"tv1":0.0,"x":0.75,"y":0.0,},
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":1.0,"y":0.5,},
      ],"resourceType":"GMAnimCurveChannel","resourceVersion":"2.0","visible":true,},
  ],
  "function":0,
  "name":"DayExposureCurve",
  "parent":{
    "name":"Animation Curves",
    "path":"folders/Animation Curves.yy",
  },
  "resourceType":"GMAnimCurve",
  "resourceVersion":"2.0",
}