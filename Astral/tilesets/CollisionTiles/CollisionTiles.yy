{
  "$GMTileSet":"",
  "%Name":"CollisionTiles",
  "autoTileSets":[],
  "macroPageTiles":{
    "SerialiseHeight":0,
    "SerialiseWidth":0,
    "TileSerialiseData":[],
  },
  "name":"CollisionTiles",
  "out_columns":1,
  "out_tilehborder":0,
  "out_tilevborder":0,
  "parent":{
    "name":"Tile Sets",
    "path":"folders/Tile Sets.yy",
  },
  "resourceType":"GMTileSet",
  "resourceVersion":"2.0",
  "spriteId":{
    "name":"Sprite11",
    "path":"sprites/Sprite11/Sprite11.yy",
  },
  "spriteNoExport":false,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "tileAnimation":{
    "FrameData":[0,1,],
    "SerialiseFrameCount":1,
  },
  "tileAnimationFrames":[],
  "tileAnimationSpeed":15.0,
  "tileHeight":64,
  "tilehsep":0,
  "tilevsep":0,
  "tileWidth":64,
  "tilexoff":0,
  "tileyoff":0,
  "tile_count":2,
}