
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
    vec4 accum =  texture2D(gm_BaseTexture, v_vTexcoord);
    float offset = 5.0 / 3000.0; // Adjust the offset based on your texture size
    float den = 1.0; // Start at 1 to include the center pixel
    float range = 16.0;
	float step_by = 4.0;

    for (float i = range * -1.0; i <= range; i += step_by) {
        for (float j = range * -1.0; j <= range; j += step_by) {
			vec2 offset = vec2(offset * i, offset * j);
			vec2 lookup = offset + v_vTexcoord;
            vec4 part = texture2D(gm_BaseTexture, lookup );
            accum = accum +  (part);
            den += 1.0;
        }
    }

    accum /= den; // Normalize the sum by the number of samples
	gl_FragColor = v_vColour * accum;
}
