// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Surface() constructor{
	name = "A"
	rotation_speed = 1;
	width = 8;
	mantle_depth = 8;
	size_paramater = 1;
	sky_hue = 0;
	atmosphere = true;
	orbital_distance = 1;
	minimum_temp_c = -40;
	maximum_temp_c = 50;
	core_temp_c = 1600;
	o2_amount = 1;
	o2_amount_core = 0.1;
	core_radiation = 0;
	exposure_radiation = 0;
	surface_radiation = 0.0; 
	star = new Star();
	
	function get_temp(day_paramater, current_depth){
		var noon_adjusted_day_paramater = animcurve_channel_evaluate(animcurve_get_channel(DayExposureCurve, 0), day_paramater);
		var exposure_temp = lerp(minimum_temp_c, maximum_temp_c, noon_adjusted_day_paramater);
		
		return lerp(exposure_temp, core_temp_c, power(current_depth/mantle_depth, 3));
	}
	
	function get_g() {
		return power(size_paramater, 2);
	}
	
	function get_o2(current_depth){
		return lerp(o2_amount, o2_amount_core, power(current_depth/mantle_depth, 3));
	}
	
	function get_radiation(day_paramater, current_depth){
		var noon_adjusted_day_paramater = animcurve_channel_evaluate(animcurve_get_channel(DayExposureCurve, 0), day_paramater);
		var radiation = surface_radiation;
		
		var radiation = lerp(surface_radiation, core_radiation, power(current_depth/mantle_depth, 3));
		var ex_r = exposure_radiation * noon_adjusted_day_paramater / sqrt(max(1,current_depth))
		radiation += ex_r;
		return radiation;
	}
	
	function get_filename(){
		return $"{star.name}/{name}.surface";
	}
	
	function save(){
		var file = file_text_open_write(get_filename());
		var save_map = ds_map_create();
		var var_names = variable_struct_get_names(self);
		for(var i = 0; i < array_length(var_names); ++i){
			var var_name = var_names[i];
			var value = variable_struct_get(self, var_name);
			if(is_numeric(value) or is_string(value)){
				save_map[? var_name] = value;	
			}
		}
		file_text_write_string(file, ds_map_write(save_map));
		file_text_close(file)
		ds_map_destroy(save_map);
	}
	
	function load(){
		var filename = get_filename();
		if(not file_exists(filename)){
			return false;	
		}
		var file = file_text_open_read(filename);
		var load_map = ds_map_create();
		ds_map_read(load_map, file_text_read_string(file));
		
		var var_names = variable_struct_get_names(self);
		for(var i = 0; i < array_length(var_names); ++i){
			var var_name = var_names[i];
			var value = load_map[? var_name];
			if(is_numeric(value) or is_string(value)){
				variable_struct_set(self, var_name, value);
			}
		}
		ds_map_destroy(load_map);
		file_text_close(file);
		return true;
	}
}

function radiation_to_R(radiation){
	return floor(radiation*125);
}
function radiation_to_mR(radiation){
	return floor(radiation*125*1000);
}

function game_generate_random_surface(star) {
	var surface = new Surface();
	surface.name = choose("We", "Are", "Pre", "Nea", "Car") + choose("ku", "gle", "ler", "ars", "gort")
	var earth_like_width = 12 * 12;
	surface.star = star;
	surface.size_paramater = random_range(0.08, 1.6);
	surface.width = ceil(earth_like_width * surface.size_paramater);
	surface.mantle_depth = ceil(36 * surface.size_paramater);
	surface.rotation_speed = random(3) * random(3) / sqrt(surface.size_paramater);
	surface.sky_hue = round(random(255));
	surface.atmosphere = random(surface.size_paramater) > 0.2;
	surface.orbital_distance = random_range(star.get_orbital_minimum(), star.get_orbital_maximum());
	surface.core_temp_c = choose(random_range(-100,0), random_range(1400,1800));
	var minimum_temp_c = -272;
	var maximum_temp_c = 1000;
	var hab_minimum_temp_c = -50;
	var hab_maximum_temp_c = 50;
	var hab_distance_min = star.get_habitable_zone_minimum();
	var hab_distance_max = star.get_habitable_zone_maximum();
	var distance_from_hab_zone = 0;
	var hab_close = 0;
	if(surface.orbital_distance < hab_distance_min){
		distance_from_hab_zone = hab_distance_min - surface.orbital_distance;	
		hab_close = hab_distance_min;
	} else if (surface.orbital_distance > hab_distance_max){
		distance_from_hab_zone = surface.orbital_distance - hab_distance_max;	
		hab_close = hab_distance_max;
	}
	
	var param1 = random(1);
	var param2 = random(1);	
	var min_param = min(param1, param2);
	var max_param = max(param1, param2);
	if(distance_from_hab_zone == 0){	
		surface.minimum_temp_c = lerp(hab_minimum_temp_c, hab_maximum_temp_c, min_param);
		surface.maximum_temp_c = lerp(hab_minimum_temp_c, hab_maximum_temp_c, max_param);
	} else {
		var orbital_param = (surface.orbital_distance - star.get_orbital_minimum() )  / abs(star.get_orbital_minimum() - star.get_orbital_maximum())
		orbital_param = 1 - orbital_param;
		
		var unhab_min = minimum_temp_c;
		var unhab_max = hab_minimum_temp_c;
		if(hab_close == hab_distance_max){
			var unhab_min = hab_maximum_temp_c;
			var unhab_max = maximum_temp_c;	
		}
		surface.minimum_temp_c = lerp(unhab_min, unhab_max, (min_param+orbital_param)/2);
		surface.maximum_temp_c = lerp(unhab_min, unhab_max, (max_param+orbital_param)/2);
	}
	
	surface.o2_amount = random(2*surface.size_paramater);
	surface.o2_amount_core = random(0.3) / sqrt(abs(surface.core_temp_c));

	// atmo free planets shift down in temp to min->half standard
	if(not surface.atmosphere){
		surface.minimum_temp_c = minimum_temp_c;	
		surface.maximum_temp_c = lerp(surface.maximum_temp_c, surface.minimum_temp_c, 0.5);	
		
		surface.o2_amount = 0;
		surface.o2_amount_core /= 10;
	}
	
	if(surface.core_temp_c > 1000){
		surface.exposure_radiation = 0;		
	} else {
		var temp_paramater = (surface.maximum_temp_c+280) / (maximum_temp_c+280);
		surface.exposure_radiation = random(temp_paramater);	
	}
	surface.core_radiation = random(1)*random(1)*random(1)*random(1);
	surface.surface_radiation = random(1.0)*random(.75)*random(.50)*random(.25); 
	
	game_generate_surface_chunks(surface, star);
	
	return surface;
}

/// @param {Struct.Surface} surface
/// @param {Struct.Star} star
function game_generate_surface_chunks(surface, star){
	var cm = ChunkManager.id;
	
	var chunk_tiles = ds_grid_create(surface.width * cm.chunk_width_tiles, surface.mantle_depth * cm.chunk_height_tiles);
	ds_grid_clear(chunk_tiles, noone);
	
	game_generate_surface_chunks_ground_clear(surface, star, cm, chunk_tiles);
	game_generate_surface_chunks_add_pois(surface, star, cm, chunk_tiles);
	
	for(var cx = 0; cx < surface.width; ++cx) for(var cy = 0; cy < surface.mantle_depth; ++cy){
		var chunk = new Chunk(cm, cx, cy); // contains the debug generation from the chunk level
		ds_grid_copy_from_position(chunk.chunk_tiles, chunk_tiles, cx*cm.chunk_width_tiles, cy*cm.chunk_height_tiles);
		chunk.save(chunk.get_filename(star.name, surface.name), true);
	}
}

function game_generate_surface_chunks_add_pois(surface, star, cm, chunk_tiles) {
	var failures_till_stop = 100;
	var pois_added_count = 0;
	var poi_room_names = tag_get_assets("POI")
	
	while(failures_till_stop > 0){
		var gx = irandom_range(0, ds_grid_width(chunk_tiles)-1);
		var gy = irandom_range(0, ds_grid_height(chunk_tiles)-1);
		
		var poi_index = irandom_range(0, array_length(poi_room_names)-1);
		var poi_name = poi_room_names[poi_index];
		var poi_room_id = asset_get_index(poi_name);
		
		var poi_room_struct = room_get_info(poi_room_id, false, true);
		var poi_width_tiles = poi_room_struct.width div 64;
		var poi_height_tiles = poi_room_struct.height div 64;
		if(poi_height_tiles + gy >= ds_grid_height(chunk_tiles)) {
			failures_till_stop -= 1;
			continue;
		}
		
		var fits = true;
		for(var xx = gx; xx < gx+poi_width_tiles and fits; ++xx) for(var yy = gy; yy < gy+poi_height_tiles and fits; ++yy){
			var wx = xx;
			if(wx >= ds_grid_width(chunk_tiles)) wx -= ds_grid_width(chunk_tiles)
			
			var chunk_tile = chunk_tiles[# wx, yy];
			if(chunk_tile == noone or chunk_tile.object_index != Block){
				fits = false;	
			}
		}
		
		if(not fits){
			failures_till_stop -= 1;
			continue;	
		}
		
		pois_added_count += 1;
		var poi_instance_info_array = poi_room_struct.instances;
		for(var i = 0; i < array_length(poi_instance_info_array); ++i){
			var instance_info = poi_instance_info_array[i];
			
			var igx = gx + instance_info.x div 64;
			if(igx >= ds_grid_width(chunk_tiles)) igx -= ds_grid_width(chunk_tiles);
			var igy = gy + instance_info.y div 64;
			var i_object_index = asset_get_index(instance_info.object_index);
			
			if(i_object_index == PoiEmptyBlock){
				chunk_tiles[# igx, igy] = noone;		
			} else {
				chunk_tiles[# igx, igy] = {object_index: i_object_index}; 	
			}
		}
	}
	
	show_debug_message($"Added {pois_added_count} POI to surface {surface.name}");
}

function game_generate_surface_chunks_ground_clear(surface, star, cm, chunk_tiles){
	var ground_level = array_create(ds_grid_width(chunk_tiles), floor(cm.chunk_height_tiles * 2.5));
	
	// initial noise
	for(var xx = 0; xx < array_length(ground_level); ++xx){		
		ground_level[xx] += irandom_range(-5,5)+irandom_range(-5,5);
	}
	
	// smooth all
	for(var xx = 0; xx < array_length(ground_level); ++xx){		
		var px = xx - 1;
		if(px < 0) px = array_length(ground_level)-1;
		var nx = xx + 1; 
		if(nx >= array_length(ground_level)) nx = 0;
		
		ground_level[xx] = (ground_level[px] + ground_level[xx] + ground_level[nx]) div 3;
	}
	
	// smooth sin	
	for(var filter = -0.8; filter < 1; filter += 0.4) {
		for(var xx = 0; xx < array_length(ground_level); ++xx){		
			if(sin(xx/21) > filter) continue;
			var px = xx - 1;
			if(px < 0) px = array_length(ground_level)-1;
			var nx = xx + 1; 
			if(nx >= array_length(ground_level)) nx = 0;
		
			ground_level[xx] = (ground_level[px] + ground_level[xx] + ground_level[nx]) div 3;
		}
	}
	
	// apply
	for(var xx = 0; xx < array_length(ground_level); ++xx){		
		ds_grid_set_region(chunk_tiles, xx, ground_level[xx], xx, ds_grid_height(chunk_tiles)-1, {object_index: Block});
	}

}