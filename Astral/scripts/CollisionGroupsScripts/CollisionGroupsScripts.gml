// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
global.collision_groups = ds_map_create();

function collision_group_add(group_, obj){
	var group = global.collision_groups[? group_];
	if(is_undefined(group)){
		show_error($"Unknown Collision Group [{group}]", true);	
	}
	array_push(group, obj);
	global.collision_groups[? group_] = array_unique(group);
}

function collision_group_create(group_){
	global.collision_groups[? group_] = [];
	return group_;
}

function collision_group_destroy(group_){
	ds_map_delete(global.collision_groups, group_);
}

function collision_group_get(group_) {
	return global.collision_groups[? group_];	
}

enum COLLISION_GROUP {
	SOLIDS,
	LIGHT_BLOCKING,
	CLIMBABLE,
}

collision_group_create(COLLISION_GROUP.SOLIDS)
collision_group_create(COLLISION_GROUP.LIGHT_BLOCKING)
collision_group_create(COLLISION_GROUP.CLIMBABLE)