// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function LightRay(xx, yy, dir, dist, col, emitter_) constructor {
	x = xx;
	y = yy;
	distance = dist;
	direction = dir;
	x2 = x + lengthdir_x(dist, dir);
	y2 = y + lengthdir_y(dist, dir);
	color = col;
	emitter = emitter_
}