// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function Star() constructor {
	name = "S";
	type = STAR_TYPE.A
	radius_paramater = 0;
	surfaces = []

	function get_radius(){
		var m = global.star_properties[? type][? STAR_PROPERTY.RADIUS_MINIMUM];
		var M = global.star_properties[? type][? STAR_PROPERTY.RADIUS_MAXIMUM];
		return lerp(m,M, radius_paramater);
	}
	
	function get_habitable_zone_minimum(){
		var m = global.star_properties[? type][? STAR_PROPERTY.HABITABLE_MINIMUM];
		var M = global.star_properties[? type][? STAR_PROPERTY.HABITABLE_MAXIMUM];
		return lerp(m,M, radius_paramater/4);
	}
	
	function get_habitable_zone_maximum(){
		var m = global.star_properties[? type][? STAR_PROPERTY.HABITABLE_MINIMUM];
		var M = global.star_properties[? type][? STAR_PROPERTY.HABITABLE_MAXIMUM];
		return lerp(m,M, radius_paramater/4+0.75);
	}
	
	function get_orbital_minimum(){
		var m = global.star_properties[? type][? STAR_PROPERTY.ORBITAL_MINIMUM];
		var M = global.star_properties[? type][? STAR_PROPERTY.ORBITAL_MAXIMUM];
		return lerp(m,M, radius_paramater/4);
	}
	
	function get_orbital_maximum(){
		var m = global.star_properties[? type][? STAR_PROPERTY.ORBITAL_MINIMUM];
		var M = global.star_properties[? type][? STAR_PROPERTY.ORBITAL_MAXIMUM];
		return lerp(m,M, radius_paramater/4+0.75);
	}
	
	function get_light(){
		var base_color = global.star_properties[? type][? STAR_PROPERTY.LIGHT];
		var h = color_get_hue(base_color);
		var s = color_get_saturation(base_color);
		var v = color_get_value(base_color);
		
		h = h + (radius_paramater-0.5)*5;
		s = 128 + radius_paramater * 10
		v = v + (-radius_paramater)*5;
		
		h = clamp(h, 0, 255);
		s = clamp(s, 0, 255);
		v = clamp(v, 0, 255);
		
		return make_color_hsv(h,s,v);
	}
	
	function get_filename(){
		return $"{name}.star";	
	}
	
	function save(){
		var file = file_text_open_write(get_filename());
		file_text_write_string(file, name);
		file_text_writeln(file)
		file_text_write_real(file, type);
		file_text_writeln(file)
		file_text_write_real(file, radius_paramater);
		file_text_writeln(file)
		file_text_write_real(file, array_length(surfaces));
		file_text_writeln(file)
		for(var i = 0; i < array_length(surfaces); ++i) {
			var surface = surfaces[i];
			file_text_write_string(file, surface.name);
			file_text_writeln(file)
		}
		file_text_close(file);
		
		for(var i = 0; i < array_length(surfaces); ++i) {
			var surface = surfaces[i];
			surface.save();
		}
	}
	
	function load(){
		var filename = get_filename();
		if(not file_exists(filename)){
			return false;	
		}
		
		var file = file_text_open_read(filename);
		name = file_text_read_string(file);
		file_text_readln(file)
		type = file_text_read_real(file);
		file_text_readln(file)
		radius_paramater = file_text_read_real(file);
		file_text_readln(file)
		
		surfaces = [];
		var surface_count = file_text_read_real(file);
		file_text_readln(file)
		repeat(surface_count) {
			var surface_name = file_text_read_string(file);
			file_text_readln(file)
			var surface = new Surface();
			surface.star = self;
			surface.name = surface_name;
			if(not surface.load()){
				return false;	
			}
			surface.star = self;
			surface.name = surface_name;
			
			array_push(surfaces, surface);
		}
		
		file_text_close(file);
		return true;
	}
}

function game_generate_random_star(name=undefined) {
	var star = new Star();
	star.type = round(random_range(STAR_TYPE.O, STAR_TYPE.BH));
	star.radius_paramater = random(1);
	star.name = choose("S","T", "Ch", "B", "D", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Qu", "R");
	star.name += choose("a", "e", "i", "o", "u", "y", "ae", "ia","oi", "uo", "ey", "oy");
	star.name += choose("l", "ck", "d", "f", "g", "h", "k", "m", "n", "p", "r", "w", "z", "v");
	
	if(not is_undefined(name)) {
		star.name = name;	
	}
	
	repeat(irandom_range(2, 8)){
		array_push(star.surfaces, game_generate_random_surface(star));
	}
	
	return star;
}
// Enum for star types
enum STAR_TYPE {
	O = 0,
	B,
	A,
	F,
	G,
	K,
	M,
	BH 
}

// Enum for star properties
enum STAR_PROPERTY {
	LIGHT = 0,
	HABITABLE_MINIMUM,
	HABITABLE_MAXIMUM,
	RADIUS_MINIMUM,
	RADIUS_MAXIMUM,
	ORBITAL_MINIMUM,
	ORBITAL_MAXIMUM
}

// Create maps for star properties
global.star_properties = ds_map_create();

// Properties for Star Type O
global.star_properties[? STAR_TYPE.O] = ds_map_create();
global.star_properties[? STAR_TYPE.O][? STAR_PROPERTY.LIGHT] = #E0ECF8;
global.star_properties[? STAR_TYPE.O][? STAR_PROPERTY.HABITABLE_MINIMUM] = 100;
global.star_properties[? STAR_TYPE.O][? STAR_PROPERTY.HABITABLE_MAXIMUM] = 500;
global.star_properties[? STAR_TYPE.O][? STAR_PROPERTY.RADIUS_MINIMUM] = 6.6;
global.star_properties[? STAR_TYPE.O][? STAR_PROPERTY.RADIUS_MAXIMUM] = 10;
global.star_properties[? STAR_TYPE.O][? STAR_PROPERTY.ORBITAL_MINIMUM] = 0.1;
global.star_properties[? STAR_TYPE.O][? STAR_PROPERTY.ORBITAL_MAXIMUM] = 750;

// Properties for Star Type B
global.star_properties[? STAR_TYPE.B] = ds_map_create();
global.star_properties[? STAR_TYPE.B][? STAR_PROPERTY.LIGHT] = #E8F2FB;
global.star_properties[? STAR_TYPE.B][? STAR_PROPERTY.HABITABLE_MINIMUM] = 10;
global.star_properties[? STAR_TYPE.B][? STAR_PROPERTY.HABITABLE_MAXIMUM] = 100;
global.star_properties[? STAR_TYPE.B][? STAR_PROPERTY.RADIUS_MINIMUM] = 1.8;
global.star_properties[? STAR_TYPE.B][? STAR_PROPERTY.RADIUS_MAXIMUM] = 6.6;
global.star_properties[? STAR_TYPE.B][? STAR_PROPERTY.ORBITAL_MINIMUM] = 0.1;
global.star_properties[? STAR_TYPE.B][? STAR_PROPERTY.ORBITAL_MAXIMUM] = 250;

// Properties for Star Type A
global.star_properties[? STAR_TYPE.A] = ds_map_create();
global.star_properties[? STAR_TYPE.A][? STAR_PROPERTY.LIGHT] = #F4F4FF;
global.star_properties[? STAR_TYPE.A][? STAR_PROPERTY.HABITABLE_MINIMUM] = 3;
global.star_properties[? STAR_TYPE.A][? STAR_PROPERTY.HABITABLE_MAXIMUM] = 10;
global.star_properties[? STAR_TYPE.A][? STAR_PROPERTY.RADIUS_MINIMUM] = 1.4;
global.star_properties[? STAR_TYPE.A][? STAR_PROPERTY.RADIUS_MAXIMUM] = 1.8;
global.star_properties[? STAR_TYPE.A][? STAR_PROPERTY.ORBITAL_MINIMUM] = 0.1;
global.star_properties[? STAR_TYPE.A][? STAR_PROPERTY.ORBITAL_MAXIMUM] = 50;

// Properties for Star Type F
global.star_properties[? STAR_TYPE.F] = ds_map_create();
global.star_properties[? STAR_TYPE.F][? STAR_PROPERTY.LIGHT] = #FFFFF8;
global.star_properties[? STAR_TYPE.F][? STAR_PROPERTY.HABITABLE_MINIMUM] = 1.5;
global.star_properties[? STAR_TYPE.F][? STAR_PROPERTY.HABITABLE_MAXIMUM] = 3;
global.star_properties[? STAR_TYPE.F][? STAR_PROPERTY.RADIUS_MINIMUM] = 1.15;
global.star_properties[? STAR_TYPE.F][? STAR_PROPERTY.RADIUS_MAXIMUM] = 1.4;
global.star_properties[? STAR_TYPE.F][? STAR_PROPERTY.ORBITAL_MINIMUM] = 0.1;
global.star_properties[? STAR_TYPE.F][? STAR_PROPERTY.ORBITAL_MAXIMUM] = 25;

// Properties for Star Type G
global.star_properties[? STAR_TYPE.G] = ds_map_create();
global.star_properties[? STAR_TYPE.G][? STAR_PROPERTY.LIGHT] = #FFFDF6;
global.star_properties[? STAR_TYPE.G][? STAR_PROPERTY.HABITABLE_MINIMUM] = 0.8;
global.star_properties[? STAR_TYPE.G][? STAR_PROPERTY.HABITABLE_MAXIMUM] = 1.4;
global.star_properties[? STAR_TYPE.G][? STAR_PROPERTY.RADIUS_MINIMUM] = 0.9;
global.star_properties[? STAR_TYPE.G][? STAR_PROPERTY.RADIUS_MAXIMUM] = 1.15;
global.star_properties[? STAR_TYPE.G][? STAR_PROPERTY.ORBITAL_MINIMUM] = 0.1;
global.star_properties[? STAR_TYPE.G][? STAR_PROPERTY.ORBITAL_MAXIMUM] = 15;

// Properties for Star Type K
global.star_properties[? STAR_TYPE.K] = ds_map_create();
global.star_properties[? STAR_TYPE.K][? STAR_PROPERTY.LIGHT] = #FFFAF4;
global.star_properties[? STAR_TYPE.K][? STAR_PROPERTY.HABITABLE_MINIMUM] = 0.4;
global.star_properties[? STAR_TYPE.K][? STAR_PROPERTY.HABITABLE_MAXIMUM] = 0.8;
global.star_properties[? STAR_TYPE.K][? STAR_PROPERTY.RADIUS_MINIMUM] = 0.7;
global.star_properties[? STAR_TYPE.K][? STAR_PROPERTY.RADIUS_MAXIMUM] = 0.96;
global.star_properties[? STAR_TYPE.K][? STAR_PROPERTY.ORBITAL_MINIMUM] = 0.1;
global.star_properties[? STAR_TYPE.K][? STAR_PROPERTY.ORBITAL_MAXIMUM] = 10;

// Properties for Star Type M
global.star_properties[? STAR_TYPE.M] = ds_map_create();
global.star_properties[? STAR_TYPE.M][? STAR_PROPERTY.LIGHT] = #FFF2EC;
global.star_properties[? STAR_TYPE.M][? STAR_PROPERTY.HABITABLE_MINIMUM] = 0.1;
global.star_properties[? STAR_TYPE.M][? STAR_PROPERTY.HABITABLE_MAXIMUM] = 0.4;
global.star_properties[? STAR_TYPE.M][? STAR_PROPERTY.RADIUS_MINIMUM] = 0.3;
global.star_properties[? STAR_TYPE.M][? STAR_PROPERTY.RADIUS_MAXIMUM] = 0.7;
global.star_properties[? STAR_TYPE.M][? STAR_PROPERTY.ORBITAL_MINIMUM] = 0.1;
global.star_properties[? STAR_TYPE.M][? STAR_PROPERTY.ORBITAL_MAXIMUM] = 5;

// Properties for Black Hole (BH)
global.star_properties[? STAR_TYPE.BH] = ds_map_create();
global.star_properties[? STAR_TYPE.BH][? STAR_PROPERTY.LIGHT] = #602270;
global.star_properties[? STAR_TYPE.BH][? STAR_PROPERTY.HABITABLE_MINIMUM] = 3000000;
global.star_properties[? STAR_TYPE.BH][? STAR_PROPERTY.HABITABLE_MAXIMUM] = 30000000;
global.star_properties[? STAR_TYPE.BH][? STAR_PROPERTY.RADIUS_MINIMUM] = 15; 
global.star_properties[? STAR_TYPE.BH][? STAR_PROPERTY.RADIUS_MAXIMUM] = 3000000; 
global.star_properties[? STAR_TYPE.BH][? STAR_PROPERTY.ORBITAL_MINIMUM] = 15;
global.star_properties[? STAR_TYPE.BH][? STAR_PROPERTY.ORBITAL_MAXIMUM] = 30000000;
