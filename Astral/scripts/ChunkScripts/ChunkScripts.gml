// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function Chunk(manager_, cx_, cy_) constructor {
	manager=manager_;
	
	chunk_tiles = ds_grid_create(manager.chunk_width_tiles,manager.chunk_height_tiles)
	ds_grid_clear(chunk_tiles, noone);
	
	owned_instances = [];
	cx = cx_;
	cy = cy_;
	px = 0;
	py = 0;
	
	function get_filename(star_name=undefined, surface_name=undefined){
		if(is_undefined(star_name)) star_name = Game.star.name;
		if(is_undefined(surface_name)) surface_name = Game.surface.name;
		return $"{star_name}/{surface_name}/{cx}_{cy}.chunk";
	}
	
	function save(filename=undefined, skip_refresh=false){
		if(is_undefined(filename)) filename = get_filename();
		
		if(not skip_refresh){
			ds_grid_clear(chunk_tiles, noone);
		
			// refresh chunk_tiles
			for(var i = 0; i < array_length(owned_instances); ++i){
				var ins = owned_instances[i];
				var ctx = (ins.x - px) div 64;
				var cty = (ins.y - py) div 64
				var previous_tile = chunk_tiles[# ctx, cty];
				if(is_undefined(previous_tile)){
					show_error($"Chunk save found owned object outside of chunk {cx} {cy}, {object_get_name(ins.object_index)} @ {ins.x} {ins.y}", true);	
				}
				if(previous_tile != noone){
					show_error($"Chunk save found overlapping objects to save to to {ctx} {cty}. Object types {object_get_name(ins.object_index)} & {object_get_name(previous_tile.object_index)}", true)	
				}
				chunk_tiles[# ctx, cty] = {object_index: ins.object_index};
			}
		}
		
		var save_grid = ds_grid_create(ds_grid_width(chunk_tiles), ds_grid_height(chunk_tiles));
		//convert chunk_tiles to savable strings/noone
		for(var gx = 0; gx < ds_grid_width(save_grid); ++gx) for(var gy = 0; gy < ds_grid_height(save_grid); ++gy){
			save_grid[# gx, gy] = noone;
			var chunk_tile = chunk_tiles[# gx, gy];
			if(chunk_tile != noone){
				save_grid[# gx, gy] = object_get_name(chunk_tile.object_index);	
			}
		}
		
		var file = file_text_open_write(filename);
		file_text_write_string(file, ds_grid_write(save_grid));
		file_text_close(file);
		
		ds_grid_destroy(save_grid);
	}
	
	function load() {
		var filename = get_filename();
		if(not file_exists(filename)) return false;
		ds_grid_clear(chunk_tiles, noone);
		
		var file = file_text_open_read(filename);
		var grid_string = file_text_read_string(file);
		file_text_close(file);
		
		var load_grid = ds_grid_create(ds_grid_width(chunk_tiles), ds_grid_height(chunk_tiles));
		ds_grid_read(load_grid, grid_string);
		for(var gx = 0; gx < ds_grid_width(load_grid); ++gx) for(var gy = 0; gy < ds_grid_height(load_grid); ++gy){
			var value = load_grid[# gx, gy];
			chunk_tiles[# gx, gy] = value;
			if(is_string(value)){
				chunk_tiles[# gx, gy] = {object_index:asset_get_index(value)};
			}
		}
		ds_grid_destroy(load_grid);
		
		return true;
	}

	function spawn(xx, yy){
		px = xx;
		py = yy;
		for(var gx=0; gx < manager.chunk_width_tiles; ++gx) for(var gy=0; gy < manager.chunk_height_tiles; ++gy){
			var chunk_tile= chunk_tiles[# gx, gy];
			if(chunk_tile == noone) continue;
			if(chunk_tile.object_index != noone) {
				var n = instance_create_layer(xx+gx*64, yy+gy*64, "Instances", chunk_tile.object_index);
				array_push(owned_instances, n);
			}
		}
	}
}