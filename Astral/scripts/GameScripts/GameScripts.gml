// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function game_surface_goto(name){
	ChunkManager.unload_all();
	Game.surface = name;
}

function draw_icon_with_text(sprite, text, xx, yy, font = UiFont, max_text = text, subimage = 0, margin = 5 ) {
	draw_set_font(font);
	var font_height = string_height("|")
	var icon_scale = font_height / sprite_get_height(sprite)
	var icon_width = sprite_get_width(sprite) * icon_scale;
	var icon_height = sprite_get_height(sprite) * icon_scale;
	draw_set_halign(fa_right);
	draw_set_valign(fa_middle);
	var right = xx + margin*2 + string_width(max_text) + icon_width;
	draw_rectangle_color(xx, yy,right, yy +icon_height, c_black, c_black, c_black, c_black, false);
	draw_sprite_stretched(sprite, subimage, xx, yy, icon_width, icon_height);
	draw_text_color(right-margin, yy+0.5*icon_height, text, c_white, c_white, c_white, c_white, 1.0);
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
	
	return icon_height;
}

/// @param {Id.DsGrid} dst
/// @param {Id.DsGrid} src
/// @param {Real} gx
/// @param {Real} gy
function ds_grid_copy_from_position(dst, src, gx, gy){
	for(var xx = 0; xx < ds_grid_width(dst); ++xx) for(var yy = 0; yy < ds_grid_height(dst); ++yy){
		dst[# xx, yy] = src[# xx + gx, yy + gy];
	}
}