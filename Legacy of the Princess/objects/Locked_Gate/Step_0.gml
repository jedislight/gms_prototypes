/// @description Insert description here
// You can write your code in this editor
if(not game_state_get(GameStateFlags.CELL_KEY)) exit;

var pb = instance_nearest(x,y, Player_Base);
if( not instance_exists(pb)) exit;
if(distance_to_object(pb) < 2) {
	instance_destroy(id);
	game_state_set(GameStateFlags.CELL_OPEN, 1);
}