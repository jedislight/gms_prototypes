/// @description Insert description here
// You can write your code in this editor


event_inherited();


// GUI
if (not is_active) exit;

draw_sprite(sprite_index, 0, 0, 0);
for(var i = 1; i <=hp; ++i){
	draw_sprite(Sprite214, 0, i*8, 0);
}

if(sprite_exists(magic_sprite)) draw_sprite(magic_sprite, 0, 64, 0);
for(var i = 1; i <=mana; ++i){
	draw_sprite(Sprite21415, 0, 64+i*8, 0);
}