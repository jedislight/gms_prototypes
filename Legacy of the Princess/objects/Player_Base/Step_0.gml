/// @description Insert description here
// You can write your code in this editor

if(not is_active) exit;
event_inherited();

var fire_input = keyboard_check_pressed(ord("X")) or gamepad_button_check_pressed(0, gp_face1);
var h_input = keyboard_check(vk_right) - keyboard_check(vk_left) + gamepad_button_check(0, gp_padr) - gamepad_button_check(0, gp_padl);
var v_input = keyboard_check(vk_down) - keyboard_check(vk_up) + gamepad_button_check(0, gp_padd) - gamepad_button_check(0, gp_padu);
var jump_input = keyboard_check(ord("Z")) or gamepad_button_check(0, gp_face2);
var jump_input_pressed = keyboard_check_pressed(ord("Z")) or gamepad_button_check_pressed(0, gp_face2);

// H Move
var h_movement = h_input
var v_movement = v_input;

if(h_movement != 0) facing = sign(h_movement);
move_and_collide(h_movement, 0, solids);


// V Move
if(jump_input_pressed){
	jump_buffer_time = jump_input_pressed * jump_buffer_time_max;
}
jump_buffer_time--;
var on_solid = collision_line(x+1, y+sprite_height+1, x+sprite_width-1, y+sprite_height+1, solids, true, true)
if(on_solid) cyotee_time = cyotee_time_max; else --cyotee_time;
if(cyotee_time and jump_buffer_time > 0){
	jump_apex = y - ((jump_height_blocks + 0.5) * 8)
	jump_phase = 1;
	cyotee_time = 0;
} else if (not on_solid and not jump_input){
	jump_apex = y;
}

if(jump_apex >= y){
	jump_phase = -1;
}

// Ladder Move
var ladder_collision = instance_place(x,y, Ladder);
if(not instance_exists(ladder_collision)) on_ladder = false;
if(instance_exists(ladder_collision) and v_movement != 0){
	x = ladder_collision.x;
	move_and_collide(0, v_movement, solids);
	jump_apex = y;
	on_ladder = true;
} else if (not on_ladder) { // gravity/jump
	var amount = -jump_phase*round(sqrt(jump_height_blocks));
	amount = min(amount, 1); // uniform fall speed
	var collisions = move_and_collide(0, amount, solids);
	if(array_length(collisions) > 0) jump_apex = y;
}

// Projectiles
if(mana_to_fire and mana = 0) fire_input = false;
if(projectile_index != noone and fire_input){
	var p = instance_create_layer(x,y, "Sprites", projectile_index);
	p.direction = facing == 1 ? 0 : 180;
	if(mana_to_fire) mana -= 1;
}

// Room Move
var margin = 1;
var x_min = -margin;
var x_max = room_width+margin-sprite_width;
var y_min = -margin+sprite_height
var y_max = room_height+margin-sprite_height;
var room_target = room;
if(x < x_min){
	room_target = room_get(room_get_x()-1, room_get_y());
	x = x_max;
} else if (x > x_max){
	room_target = room_get(room_get_x()+1, room_get_y());
	x = x_min;
} else if (y > y_max){
	room_target = room_get(room_get_x(), room_get_y()+1);
	y = y_min;
}else if (y < y_min){
	room_target = room_get(room_get_x(), room_get_y()-1);
	jump_apex += room_height;
	y = y_max;
}

persistent = false;
if(room_target != room) {
	if(not room_exists(room_target)){
		x = xprevious;
		y = yprevious;
		exit;
	}
	
	persistent = true;
	room_goto(room_target);	
}