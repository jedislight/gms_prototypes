/// @description Insert description here
// You can write your code in this editor
if(self.id > other.id){
	var active = is_active ? self.id : other.id;
	var inactive = is_active ? other.id : self.id;
	
	active.x = inactive.x;
	active.y = inactive.y;
	inactive.x += sprite_width*2;
	
	inactive.is_active = true;
	active.is_active = false;
}