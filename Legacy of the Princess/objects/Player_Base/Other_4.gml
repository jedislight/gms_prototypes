/// @description Insert description here
// You can write your code in this editor
if(not is_active) {
	if(instance_number(object_index) > 1) instance_destroy(id);	
} else {
	var active_count = 0;
	with(Player_Base) active_count += is_active;
	if(active_count > 1) instance_destroy(id);	
}
