/// @description Insert description here
// You can write your code in this editor

var pb = instance_nearest(x,y, Player_Base);
if(not instance_exists(pb)) exit;

if(pb.object_index == Knight and game_state_get(GameStateFlags.LEVITATION_BOOTS)) exit;

if(distance_to_object(pb) <= 1){
	hurt(pb, 1);	
}