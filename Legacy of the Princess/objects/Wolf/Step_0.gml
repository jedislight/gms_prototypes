/// @description Insert description here
// You can write your code in this editor
event_inherited();

var pb = instance_nearest(x,y, Player_Base);
if(not instance_exists(pb)) exit;

facing = sign(pb.x - x);
var distance = distance_to_object(pb);
if(distance < 24 and frame % 2 == 0) {
	move_and_collide(facing, 0, Wall);	
}
move_and_collide(0, 1, Wall);