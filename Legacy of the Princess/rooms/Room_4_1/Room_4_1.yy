{
  "$GMRoom":"",
  "%Name":"Room_4_1",
  "creationCodeFile":"",
  "inheritCode":true,
  "inheritCreationOrder":true,
  "inheritLayers":true,
  "instanceCreationOrder":[
    {"name":"inst_40F0F15D_1","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_3A6888B3_1","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_5469854A_1","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_68AE7573","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_439D7448","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_23A5EDBA","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_32C14401","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_7FA93431","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_54A3466D","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_66085221","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_24F5B8D9","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_63E31F53","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_2C20EFDB","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_4E0B79E4","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_11A3CF64","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_48512BE","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_51BAAA73","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_3FFE0DCC","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_34FD3B48","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_7A50AE4A","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_63B25C71","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_14FCEDCF","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_297B070C","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_67BF6EF9","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_42FF715B","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_C177678","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_5351407","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_360970CB","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_7FF37EEE","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_5763F6A1","path":"rooms/Room_4_1/Room_4_1.yy",},
    {"name":"inst_634F6EC3","path":"rooms/Room_4_1/Room_4_1.yy",},
  ],
  "isDnd":false,
  "layers":[
    {"$GMRInstanceLayer":"","%Name":"Sprites","depth":0,"effectEnabled":true,"effectType":"_filter_outline","gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":true,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"$GMRInstance":"","%Name":"inst_24F5B8D9","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_24F5B8D9","objectId":{"name":"Vine_Growth","path":"objects/Vine_Growth/Vine_Growth.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":2.0,"x":64.0,"y":16.0,},
        {"$GMRInstance":"","%Name":"inst_63E31F53","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_63E31F53","objectId":{"name":"Vine_Growth","path":"objects/Vine_Growth/Vine_Growth.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":88.0,"y":16.0,},
        {"$GMRInstance":"","%Name":"inst_2C20EFDB","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_2C20EFDB","objectId":{"name":"Vine_Growth","path":"objects/Vine_Growth/Vine_Growth.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":2.0,"x":40.0,"y":56.0,},
        {"$GMRInstance":"","%Name":"inst_4E0B79E4","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_4E0B79E4","objectId":{"name":"Vine_Growth","path":"objects/Vine_Growth/Vine_Growth.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":3.0,"x":80.0,"y":64.0,},
        {"$GMRInstance":"","%Name":"inst_11A3CF64","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_11A3CF64","objectId":{"name":"Vine_Growth","path":"objects/Vine_Growth/Vine_Growth.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":4.0,"x":8.0,"y":56.0,},
        {"$GMRInstance":"","%Name":"inst_48512BE","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_48512BE","objectId":{"name":"Vine_Growth","path":"objects/Vine_Growth/Vine_Growth.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":8.0,"y":16.0,},
        {"$GMRInstance":"","%Name":"inst_51BAAA73","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_51BAAA73","objectId":{"name":"Vine_Growth","path":"objects/Vine_Growth/Vine_Growth.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":3.0,"x":104.0,"y":56.0,},
        {"$GMRInstance":"","%Name":"inst_5763F6A1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_5763F6A1","objectId":{"name":"Wolf","path":"objects/Wolf/Wolf.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":40.0,"y":112.0,},
        {"$GMRInstance":"","%Name":"inst_634F6EC3","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_634F6EC3","objectId":{"name":"Wolf","path":"objects/Wolf/Wolf.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":88.0,"y":112.0,},
      ],"layers":[],"name":"Sprites","properties":[
        {"name":"g_OutlineColour","type":1,"value":"#FF000000",},
        {"name":"g_OutlineRadius","type":0,"value":"1",},
        {"name":"g_OutlinePixelScale","type":0,"value":"4",},
      ],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"World","depth":100,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":true,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"$GMRInstance":"","%Name":"inst_40F0F15D_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_40F0F15D_1","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":2.0,"scaleY":1.0,"x":0.0,"y":96.0,},
        {"$GMRInstance":"","%Name":"inst_3A6888B3_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_3A6888B3_1","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":180.0,"scaleX":3.0,"scaleY":1.0,"x":128.0,"y":56.0,},
        {"$GMRInstance":"","%Name":"inst_5469854A_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_5469854A_1","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":360.0,"scaleX":3.0,"scaleY":1.0,"x":104.0,"y":40.0,},
        {"$GMRInstance":"","%Name":"inst_68AE7573","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_68AE7573","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":180.0,"scaleX":3.0,"scaleY":1.0,"x":96.0,"y":64.0,},
        {"$GMRInstance":"","%Name":"inst_439D7448","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_439D7448","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":360.0,"scaleX":3.0,"scaleY":1.0,"x":72.0,"y":48.0,},
        {"$GMRInstance":"","%Name":"inst_23A5EDBA","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_23A5EDBA","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":180.0,"scaleX":16.0,"scaleY":1.0,"x":128.0,"y":16.0,},
        {"$GMRInstance":"","%Name":"inst_32C14401","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_32C14401","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":180.0,"scaleX":3.0,"scaleY":1.0,"x":56.0,"y":56.0,},
        {"$GMRInstance":"","%Name":"inst_7FA93431","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_7FA93431","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":360.0,"scaleX":3.0,"scaleY":1.0,"x":32.0,"y":40.0,},
        {"$GMRInstance":"","%Name":"inst_54A3466D","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_54A3466D","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":180.0,"scaleX":2.0000002,"scaleY":1.0,"x":16.0,"y":56.0,},
        {"$GMRInstance":"","%Name":"inst_66085221","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_66085221","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":360.0,"scaleX":2.0000002,"scaleY":1.0,"x":0.0,"y":40.0,},
        {"$GMRInstance":"","%Name":"inst_3FFE0DCC","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_3FFE0DCC","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":2.0,"scaleY":1.0,"x":24.0,"y":96.0,},
        {"$GMRInstance":"","%Name":"inst_34FD3B48","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_34FD3B48","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":5.0,"scaleY":1.0,"x":56.0,"y":96.0,},
        {"$GMRInstance":"","%Name":"inst_7A50AE4A","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_7A50AE4A","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":3.0,"scaleY":1.0,"x":104.0,"y":96.0,},
        {"$GMRInstance":"","%Name":"inst_63B25C71","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_63B25C71","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":-360.0,"scaleX":16.0,"scaleY":2.0,"x":0.0,"y":120.0,},
        {"$GMRInstance":"","%Name":"inst_14FCEDCF","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_14FCEDCF","objectId":{"name":"Spikes","path":"objects/Spikes/Spikes.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":96.0,"y":104.0,},
        {"$GMRInstance":"","%Name":"inst_297B070C","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_297B070C","objectId":{"name":"Spikes","path":"objects/Spikes/Spikes.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":48.0,"y":104.0,},
        {"$GMRInstance":"","%Name":"inst_67BF6EF9","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_67BF6EF9","objectId":{"name":"Spikes","path":"objects/Spikes/Spikes.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":40.0,"y":104.0,},
        {"$GMRInstance":"","%Name":"inst_42FF715B","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_42FF715B","objectId":{"name":"Spikes","path":"objects/Spikes/Spikes.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":16.0,"y":104.0,},
        {"$GMRInstance":"","%Name":"inst_C177678","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_C177678","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":180.0,"scaleX":5.0,"scaleY":1.0,"x":96.0,"y":112.0,},
        {"$GMRInstance":"","%Name":"inst_5351407","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_5351407","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":180.0,"scaleX":2.0,"scaleY":1.0,"x":40.0,"y":112.0,},
        {"$GMRInstance":"","%Name":"inst_360970CB","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_360970CB","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":180.0,"scaleX":2.0,"scaleY":1.0,"x":16.0,"y":112.0,},
        {"$GMRInstance":"","%Name":"inst_7FF37EEE","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_7FF37EEE","objectId":{"name":"Deep_Soil","path":"objects/Deep_Soil/Deep_Soil.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":180.0,"scaleX":3.0,"scaleY":1.0,"x":128.0,"y":112.0,},
      ],"layers":[],"name":"World","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRAssetLayer":"","%Name":"Background_Assets","assets":[
        {"$GMRSpriteGraphic":"","%Name":"graphic_73933FC9_1_1_1","animationSpeed":1.0,"colour":4294967295,"frozen":false,"headPosition":0.0,"ignore":false,"inheritedItemId":null,"inheritItemSettings":false,"name":"graphic_73933FC9_1_1_1","resourceType":"GMRSpriteGraphic","resourceVersion":"2.0","rotation":0.0,"scaleX":16.0,"scaleY":1.0,"spriteId":{"name":"Sprite110","path":"sprites/Sprite110/Sprite110.yy",},"x":0.0,"y":0.0,},
      ],"depth":200,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":true,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Background_Assets","properties":[],"resourceType":"GMRAssetLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRBackgroundLayer":"","%Name":"Background","animationFPS":15.0,"animationSpeedType":0,"colour":4278190080,"depth":300,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"hspeed":0.0,"htiled":false,"inheritLayerDepth":true,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Background","properties":[],"resourceType":"GMRBackgroundLayer","resourceVersion":"2.0","spriteId":null,"stretch":false,"userdefinedAnimFPS":false,"userdefinedDepth":false,"visible":true,"vspeed":0.0,"vtiled":false,"x":0,"y":0,},
  ],
  "name":"Room_4_1",
  "parent":{
    "name":"Rooms",
    "path":"folders/Rooms.yy",
  },
  "parentRoom":{
    "name":"Room__Template",
    "path":"rooms/Room__Template/Room__Template.yy",
  },
  "physicsSettings":{
    "inheritPhysicsSettings":true,
    "PhysicsWorld":false,
    "PhysicsWorldGravityX":0.0,
    "PhysicsWorldGravityY":10.0,
    "PhysicsWorldPixToMetres":0.1,
  },
  "resourceType":"GMRoom",
  "resourceVersion":"2.0",
  "roomSettings":{
    "Height":128,
    "inheritRoomSettings":true,
    "persistent":false,
    "Width":128,
  },
  "sequenceId":null,
  "views":[
    {"hborder":32,"hport":512,"hspeed":-1,"hview":128,"inherit":false,"objectId":null,"vborder":32,"visible":true,"vspeed":-1,"wport":512,"wview":128,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
  ],
  "viewSettings":{
    "clearDisplayBuffer":true,
    "clearViewBackground":true,
    "enableViews":true,
    "inheritViewSettings":true,
  },
  "volume":1.0,
}