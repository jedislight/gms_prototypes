// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function room_get(xx, yy){
	return asset_get_index($"Room_{xx}_{yy}");
}

function room_get_x(){
	var room_name = room_get_name(room);
	var parts = string_split(room_name, "_");
	return real(parts[1]);
}

function room_get_y(){
	var room_name = room_get_name(room);
	var parts = string_split(room_name, "_");
	return real(parts[2]);
}