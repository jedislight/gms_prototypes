// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
enum GameStateFlags {
	CELL_KEY = 0,
	CELL_OPEN = 1,
	UNDER_BRIDGE_DESTROYED = 2,
	LEVITATION_BOOTS = 3,
	UNDER_WELL_DESTROYED = 4,
	DRAW_BRIDGE_OPEN = 5,
}

global.game_state = [];

function game_state_set(flag, value) {
	_game_state_ensure_array_allocation(flag);
	global.game_state[flag] = value;
}

function game_state_get(flag) {
	_game_state_ensure_array_allocation(flag);
	return global.game_state[flag];
}

function _game_state_ensure_array_allocation(flag) {
	for(var i = array_length(global.game_state); i <= flag; ++i) global.game_state[i] = 0;	
}

function game_state_destroy_if(flag) {
	if(game_state_get(flag)) {
		instance_destroy(id);
		exit;
	}
}

function save_array_to_file(array, filename) {
    var file = file_text_open_write(filename);
    if (file != -1) {
        for (var i = 0; i < array_length(array); ++i) {
            file_text_write_string(file, string(array[i]));
            file_text_writeln(file);
        }
        file_text_close(file);
    } else {
        show_error("Failed to open file for writing: " + filename, false);
    }
}

function load_array_from_file(filename) {
    var file = file_text_open_read(filename);
    var array = [];
    if (file != -1) {
        while (!file_text_eof(file)) {
            var line = file_text_read_string(file);
            file_text_readln(file);
            array[array_length(array)] = real(line);
        }
        file_text_close(file);
    } else {
        show_error("Failed to open file for reading: " + filename, false);
    }
    return array;
}
