// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function hurt(obj, amount){
	if(obj.invulnerable > 0) return;
	
	obj.hp -= amount;
	while(obj.mana_tap and obj.hp <= 0 and obj.mana > 0){
		obj.mana -= 1;
		obj.hp += obj.defense;
	}
	obj.invulnerable = obj.invulnerable_frames;
	if(obj.hp <=  0) {
		instance_destroy(obj);
		var ts = time_source_create(time_source_game, 2.5, time_source_units_seconds, game_restart);
		time_source_start(ts);
	}
}