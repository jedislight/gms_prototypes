{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 9,
  "bbox_right": 53,
  "bbox_top": 9,
  "bbox_bottom": 53,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"c2b5a1f5-776f-441c-8440-30e8ee5f4613","path":"sprites/Sprite35/Sprite35.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c2b5a1f5-776f-441c-8440-30e8ee5f4613","path":"sprites/Sprite35/Sprite35.yy",},"LayerId":{"name":"45ca4cc9-a586-4999-afb6-7a316b4c3c39","path":"sprites/Sprite35/Sprite35.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite35","path":"sprites/Sprite35/Sprite35.yy",},"resourceVersion":"1.0","name":"c2b5a1f5-776f-441c-8440-30e8ee5f4613","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"82921dfa-55e8-41c5-b0fa-d4a020d57342","path":"sprites/Sprite35/Sprite35.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"82921dfa-55e8-41c5-b0fa-d4a020d57342","path":"sprites/Sprite35/Sprite35.yy",},"LayerId":{"name":"45ca4cc9-a586-4999-afb6-7a316b4c3c39","path":"sprites/Sprite35/Sprite35.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite35","path":"sprites/Sprite35/Sprite35.yy",},"resourceVersion":"1.0","name":"82921dfa-55e8-41c5-b0fa-d4a020d57342","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c54307f2-70f4-45d3-95ea-5a3f35dc1a68","path":"sprites/Sprite35/Sprite35.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c54307f2-70f4-45d3-95ea-5a3f35dc1a68","path":"sprites/Sprite35/Sprite35.yy",},"LayerId":{"name":"45ca4cc9-a586-4999-afb6-7a316b4c3c39","path":"sprites/Sprite35/Sprite35.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite35","path":"sprites/Sprite35/Sprite35.yy",},"resourceVersion":"1.0","name":"c54307f2-70f4-45d3-95ea-5a3f35dc1a68","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"efb3b1a6-162d-4566-bda3-2b08d1120f72","path":"sprites/Sprite35/Sprite35.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"efb3b1a6-162d-4566-bda3-2b08d1120f72","path":"sprites/Sprite35/Sprite35.yy",},"LayerId":{"name":"45ca4cc9-a586-4999-afb6-7a316b4c3c39","path":"sprites/Sprite35/Sprite35.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite35","path":"sprites/Sprite35/Sprite35.yy",},"resourceVersion":"1.0","name":"efb3b1a6-162d-4566-bda3-2b08d1120f72","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2190b281-b4c0-4976-9212-f237e2b362b6","path":"sprites/Sprite35/Sprite35.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2190b281-b4c0-4976-9212-f237e2b362b6","path":"sprites/Sprite35/Sprite35.yy",},"LayerId":{"name":"45ca4cc9-a586-4999-afb6-7a316b4c3c39","path":"sprites/Sprite35/Sprite35.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite35","path":"sprites/Sprite35/Sprite35.yy",},"resourceVersion":"1.0","name":"2190b281-b4c0-4976-9212-f237e2b362b6","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"Sprite35","path":"sprites/Sprite35/Sprite35.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore`1","elementType":"MessageEventKeyframe",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore`1","elementType":"MomentsEventKeyframe",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"22619bd6-3350-48f4-ba30-be2a549898a7","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c2b5a1f5-776f-441c-8440-30e8ee5f4613","path":"sprites/Sprite35/Sprite35.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
            {"id":"98dd3b11-40a7-41f7-9331-6db1c91e03fb","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"82921dfa-55e8-41c5-b0fa-d4a020d57342","path":"sprites/Sprite35/Sprite35.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
            {"id":"a84937f6-8bc1-4c89-b17f-6f02c5d9dba4","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c54307f2-70f4-45d3-95ea-5a3f35dc1a68","path":"sprites/Sprite35/Sprite35.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
            {"id":"a37d0f07-a0b0-40e6-9d6f-deb5b23a17e0","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"efb3b1a6-162d-4566-bda3-2b08d1120f72","path":"sprites/Sprite35/Sprite35.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
            {"id":"33c79b5f-993c-4c9f-a64f-7d3bfcf16f52","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2190b281-b4c0-4976-9212-f237e2b362b6","path":"sprites/Sprite35/Sprite35.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore`1","elementType":"SpriteFrameKeyframe",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"Sprite35","path":"sprites/Sprite35/Sprite35.yy",},
    "resourceVersion": "1.4",
    "name": "Sprite35",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"45ca4cc9-a586-4999-afb6-7a316b4c3c39","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "LanderP",
    "path": "LanderP.yyp",
  },
  "resourceVersion": "1.0",
  "name": "Sprite35",
  "tags": [],
  "resourceType": "GMSprite",
}