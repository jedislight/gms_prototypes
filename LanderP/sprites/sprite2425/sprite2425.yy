{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 63,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"d7508b95-9b09-4d02-8af8-e85714894ba6","path":"sprites/Sprite2425/Sprite2425.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d7508b95-9b09-4d02-8af8-e85714894ba6","path":"sprites/Sprite2425/Sprite2425.yy",},"LayerId":{"name":"4c092d0a-9a8a-4afe-b32f-e60653faa0ac","path":"sprites/Sprite2425/Sprite2425.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite2425","path":"sprites/Sprite2425/Sprite2425.yy",},"resourceVersion":"1.0","name":"d7508b95-9b09-4d02-8af8-e85714894ba6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"beabc327-a42a-4d9f-afea-8e302d971424","path":"sprites/Sprite2425/Sprite2425.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"beabc327-a42a-4d9f-afea-8e302d971424","path":"sprites/Sprite2425/Sprite2425.yy",},"LayerId":{"name":"4c092d0a-9a8a-4afe-b32f-e60653faa0ac","path":"sprites/Sprite2425/Sprite2425.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite2425","path":"sprites/Sprite2425/Sprite2425.yy",},"resourceVersion":"1.0","name":"beabc327-a42a-4d9f-afea-8e302d971424","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"Sprite2425","path":"sprites/Sprite2425/Sprite2425.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore`1","elementType":"MessageEventKeyframe",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore`1","elementType":"MomentsEventKeyframe",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"900553c0-ae5d-4a29-b485-01d8c4d34358","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d7508b95-9b09-4d02-8af8-e85714894ba6","path":"sprites/Sprite2425/Sprite2425.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
            {"id":"0c586948-7d00-4940-899b-43a6b753000b","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"beabc327-a42a-4d9f-afea-8e302d971424","path":"sprites/Sprite2425/Sprite2425.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore`1","elementType":"SpriteFrameKeyframe",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"Sprite2425","path":"sprites/Sprite2425/Sprite2425.yy",},
    "resourceVersion": "1.4",
    "name": "Sprite24",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"4c092d0a-9a8a-4afe-b32f-e60653faa0ac","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "LanderP",
    "path": "LanderP.yyp",
  },
  "resourceVersion": "1.0",
  "name": "Sprite2425",
  "tags": [],
  "resourceType": "GMSprite",
}