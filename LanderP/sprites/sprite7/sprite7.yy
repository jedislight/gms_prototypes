{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 15,
  "bbox_top": 0,
  "bbox_bottom": 17,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 16,
  "height": 18,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"8c1842da-5528-41ef-bc5f-ef3622d17dd7","path":"sprites/Sprite7/Sprite7.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8c1842da-5528-41ef-bc5f-ef3622d17dd7","path":"sprites/Sprite7/Sprite7.yy",},"LayerId":{"name":"f77bbba9-697b-4dbe-b18c-06553f291b1e","path":"sprites/Sprite7/Sprite7.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite7","path":"sprites/Sprite7/Sprite7.yy",},"resourceVersion":"1.0","name":"8c1842da-5528-41ef-bc5f-ef3622d17dd7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"41f8318f-2e32-4c10-86b4-fa3200e6fc3e","path":"sprites/Sprite7/Sprite7.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"41f8318f-2e32-4c10-86b4-fa3200e6fc3e","path":"sprites/Sprite7/Sprite7.yy",},"LayerId":{"name":"f77bbba9-697b-4dbe-b18c-06553f291b1e","path":"sprites/Sprite7/Sprite7.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite7","path":"sprites/Sprite7/Sprite7.yy",},"resourceVersion":"1.0","name":"41f8318f-2e32-4c10-86b4-fa3200e6fc3e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9e38b794-6d92-4373-8dca-6976fb777e3f","path":"sprites/Sprite7/Sprite7.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9e38b794-6d92-4373-8dca-6976fb777e3f","path":"sprites/Sprite7/Sprite7.yy",},"LayerId":{"name":"f77bbba9-697b-4dbe-b18c-06553f291b1e","path":"sprites/Sprite7/Sprite7.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite7","path":"sprites/Sprite7/Sprite7.yy",},"resourceVersion":"1.0","name":"9e38b794-6d92-4373-8dca-6976fb777e3f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bd609d6b-e885-4179-8c32-f30c799bb0bd","path":"sprites/Sprite7/Sprite7.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bd609d6b-e885-4179-8c32-f30c799bb0bd","path":"sprites/Sprite7/Sprite7.yy",},"LayerId":{"name":"f77bbba9-697b-4dbe-b18c-06553f291b1e","path":"sprites/Sprite7/Sprite7.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite7","path":"sprites/Sprite7/Sprite7.yy",},"resourceVersion":"1.0","name":"bd609d6b-e885-4179-8c32-f30c799bb0bd","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"Sprite7","path":"sprites/Sprite7/Sprite7.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore`1","elementType":"MessageEventKeyframe",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore`1","elementType":"MomentsEventKeyframe",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"fa334ec7-da5d-4d01-8d1a-924c4e7b9ad1","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8c1842da-5528-41ef-bc5f-ef3622d17dd7","path":"sprites/Sprite7/Sprite7.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
            {"id":"05f555ae-aed6-4528-bbc3-f019d304073f","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"41f8318f-2e32-4c10-86b4-fa3200e6fc3e","path":"sprites/Sprite7/Sprite7.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
            {"id":"45eb3bb9-1a7e-45f4-ad84-2e11fb0e70d0","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9e38b794-6d92-4373-8dca-6976fb777e3f","path":"sprites/Sprite7/Sprite7.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
            {"id":"7f5654de-1c63-437c-9312-3414fc2343a3","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bd609d6b-e885-4179-8c32-f30c799bb0bd","path":"sprites/Sprite7/Sprite7.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore`1","elementType":"SpriteFrameKeyframe",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"Sprite7","path":"sprites/Sprite7/Sprite7.yy",},
    "resourceVersion": "1.4",
    "name": "Sprite7",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"f77bbba9-697b-4dbe-b18c-06553f291b1e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": {
    "left": 0,
    "top": 0,
    "right": 0,
    "bottom": 0,
    "guideColour": [
      4294902015,
      4294902015,
      4294902015,
      4294902015,
    ],
    "highlightColour": 1728023040,
    "highlightStyle": 0,
    "enabled": false,
    "tileMode": [
      0,
      0,
      0,
      0,
      0,
    ],
    "resourceVersion": "1.0",
    "loadedVersion": null,
    "resourceType": "GMNineSliceData",
  },
  "parent": {
    "name": "LanderP",
    "path": "LanderP.yyp",
  },
  "resourceVersion": "1.0",
  "name": "Sprite7",
  "tags": [],
  "resourceType": "GMSprite",
}