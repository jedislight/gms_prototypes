{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 63,
  "bbox_top": 32,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"97ed2290-4882-40e4-8d08-de42e63414a5","path":"sprites/Sprite57/Sprite57.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"97ed2290-4882-40e4-8d08-de42e63414a5","path":"sprites/Sprite57/Sprite57.yy",},"LayerId":{"name":"0a79a965-1f4d-425b-8279-6a8bd024c20b","path":"sprites/Sprite57/Sprite57.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite57","path":"sprites/Sprite57/Sprite57.yy",},"resourceVersion":"1.0","name":"97ed2290-4882-40e4-8d08-de42e63414a5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ed047e07-9ef5-4b91-8398-acaacf96e309","path":"sprites/Sprite57/Sprite57.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ed047e07-9ef5-4b91-8398-acaacf96e309","path":"sprites/Sprite57/Sprite57.yy",},"LayerId":{"name":"0a79a965-1f4d-425b-8279-6a8bd024c20b","path":"sprites/Sprite57/Sprite57.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite57","path":"sprites/Sprite57/Sprite57.yy",},"resourceVersion":"1.0","name":"ed047e07-9ef5-4b91-8398-acaacf96e309","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e9bb06ce-5ceb-4919-9984-a789560d8bd8","path":"sprites/Sprite57/Sprite57.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e9bb06ce-5ceb-4919-9984-a789560d8bd8","path":"sprites/Sprite57/Sprite57.yy",},"LayerId":{"name":"0a79a965-1f4d-425b-8279-6a8bd024c20b","path":"sprites/Sprite57/Sprite57.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite57","path":"sprites/Sprite57/Sprite57.yy",},"resourceVersion":"1.0","name":"e9bb06ce-5ceb-4919-9984-a789560d8bd8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"85a39faa-2f97-4d2a-bd46-a72c595ed1cc","path":"sprites/Sprite57/Sprite57.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"85a39faa-2f97-4d2a-bd46-a72c595ed1cc","path":"sprites/Sprite57/Sprite57.yy",},"LayerId":{"name":"0a79a965-1f4d-425b-8279-6a8bd024c20b","path":"sprites/Sprite57/Sprite57.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite57","path":"sprites/Sprite57/Sprite57.yy",},"resourceVersion":"1.0","name":"85a39faa-2f97-4d2a-bd46-a72c595ed1cc","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"Sprite57","path":"sprites/Sprite57/Sprite57.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore`1","elementType":"MessageEventKeyframe",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore`1","elementType":"MomentsEventKeyframe",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"2ff9c2f4-6f33-4742-96bb-909f5478792e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"97ed2290-4882-40e4-8d08-de42e63414a5","path":"sprites/Sprite57/Sprite57.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
            {"id":"b61ce3d0-bd0e-4ca8-824d-629c66a50ae8","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ed047e07-9ef5-4b91-8398-acaacf96e309","path":"sprites/Sprite57/Sprite57.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
            {"id":"5a9f2798-c402-44a3-93c2-e198c9507426","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e9bb06ce-5ceb-4919-9984-a789560d8bd8","path":"sprites/Sprite57/Sprite57.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
            {"id":"cbe811bf-b394-40e1-9be6-e59373c50917","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"85a39faa-2f97-4d2a-bd46-a72c595ed1cc","path":"sprites/Sprite57/Sprite57.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore`1","elementType":"SpriteFrameKeyframe",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"Sprite57","path":"sprites/Sprite57/Sprite57.yy",},
    "resourceVersion": "1.4",
    "name": "Sprite57",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"0a79a965-1f4d-425b-8279-6a8bd024c20b","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "LanderP",
    "path": "LanderP.yyp",
  },
  "resourceVersion": "1.0",
  "name": "Sprite57",
  "tags": [],
  "resourceType": "GMSprite",
}