// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function draw_ship_health(ship, xx) {
	var margin = 16
	var e_hp  =ship.hp-1;
	var e_hp_max = ship.hp_max -1;
	var hp_x = margin + xx;
	var hp_bot = margin + sprite_get_height(SpriteHp) * 16;
	var hp_top = hp_bot - sprite_get_height(SpriteHp) * e_hp_max;
	draw_set_color(c_black);
	draw_roundrect_ext(hp_x-0.25*margin, hp_top-margin/4, hp_x+0.25*margin+sprite_get_width(SpriteHp), hp_bot+margin/4, 3,3, false)
	draw_set_color(c_white);
	draw_roundrect_ext(hp_x-0.25*margin, hp_top-margin/4, hp_x+0.25*margin+sprite_get_width(SpriteHp), hp_bot+margin/4, 3,3, true)

	var sprite = SpriteHp;
	if (ship.object_index == Ship and Game.get_flag("ArmorUpgrade")) {
		sprite = SpriteArmorHp;	
	}
	for(var i = 1; i <= e_hp_max; ++i) {
		var hp_y = hp_bot - sprite_get_height(SpriteHp) * i;
		if(e_hp >= i) {
			draw_sprite(sprite, 0, hp_x, hp_y);
		}
	}
}

function create_scrap(type, minimum, mass_mod, scale) {
	if (is_undefined(type)){
		type = Scrap;	
	}
	if(is_undefined(minimum)) {
		minimum = 1;	
	}
	if(is_undefined(mass_mod)) {
		mass_mod = 5;
	}
	if (is_undefined(scale)){
		scale = 1;	
	}
	var emass = phy_mass;
	if (phy_mass == 0) {
		var height_m = sprite_height * 0.03125;
		var width_m = sprite_width * 0.03125;
		emass = height_m * width_m * 0.05;
	}
	var shards = minimum + emass * 3 * mass_mod;
	var xs = sprite_width / 4;
	var ys = sprite_height / 4;
	var n = noone;
	while(shards-- > 0){
		n = instance_create_depth(phy_com_x+random_range(-xs,xs),phy_com_y+random_range(-ys,ys),depth, type);
		n.image_xscale = scale;
		n.image_yscale = scale;
		n.phy_angular_velocity = phy_angular_velocity * random_range(0.98, 1.02);
		n.phy_linear_velocity_x = phy_linear_velocity_x * random_range(0.98, 1.02);
		n.phy_linear_velocity_y = phy_linear_velocity_y * random_range(0.98, 1.02);
		/*if (not physics_test_overlap(phy_position_x,phy_position_y ,phy_rotation, n)) {
			//n.phy_position_x = x + random_range(-15,15);
			//n.phy_position_y = y + random_range(-15,15);
			instance_destroy(n);
			++shards;
		}*/
	}
	
	return n;
}

function draw_self_tiled() {
	for(var xx = 0; xx < image_xscale*64; xx+=64) for(var yy = 0; yy < image_yscale*64; yy+=64) {
		draw_sprite_ext(sprite_index, image_index, x + xx, y + yy, 1, 1, 0, image_blend, image_alpha);
	}
}

function damage_table_lookup(source, target) {
	if ((source == Ship || source == GravityBomb || source == EnemyProjectile || source == EnemyGravityBomb) 
		and target == MoltenPlanatoid) {
		return 0.0;
	}
	
	if ((source == EnemyGravityBomb) 
		and target == Turret) {
		return 0.0;
	}
	
	if ((target == TorpedoBox)) {
		return 2.0;	
	}
	return 1.0	
}

function collision_with_enemy(enemy) {
	var nope = Ship;
	if (variable_instance_exists(id, "ignore")){
		nope = ignore;	
	}
	if(enemy.object_index == nope or enemy.id == nope) return;
	var destroy_on_contact = true;
	if (variable_instance_exists(id, "pierce")) {
		destroy_on_contact = false;
	}
	if (variable_instance_exists(enemy, "hp")) {
		var d = 1;
		if (variable_instance_exists(id, "damage")) {
			d = damage;	
		}
		d *= damage_table_lookup(object_index, enemy.object_index);
		show_debug_message("DEAL DAMAGE " + string(d) + " TO " + object_get_name(enemy.object_index));
		deal_damage(enemy, d);
		
		if(destroy_on_contact) instance_destroy(id);
	} else {
		if(destroy_on_contact) {
			instance_destroy(id);
		}
	}
}

function deal_damage(to, amount) {
	show_debug_message("deal_damage(" + string(to) + ", " + string(amount)+")")
	var ifr = undefined;
	if (variable_instance_exists(to, "iframes")) {
		ifr = variable_instance_get(to, "iframes")
	}
	if (not is_undefined(ifr) and ifr.is_active()) return;
	
	to.hp = to.hp - amount;
	to.hp = max(0, to.hp);
	
	if (not is_undefined(ifr)) {
		ifr.activate();	
	}
}

function item_drop() {
	if(instance_number(Ship) == 0) return noone;
	var xx = x;
	var yy = y;
	if (phy_active) {
		xx = phy_com_x;
		yy = phy_com_y;
	}
	var target_hp = irandom_range(0,Ship.hp_max);
	var target_energy = irandom_range(0, 16);
	if (Ship.hp < target_hp) {
		return instance_create_depth(xx,yy, Ship.depth-1, choose(HpPickupLarge, HpPickupSmall));	
	} else if (target_energy > Ship.weapons[Ship.weapons_index].energy and Ship.weapons[Ship.weapons_index].get_cost() > 0){
		return instance_create_depth(xx,yy, Ship.depth-1, choose(EnergyPickupLarge, EnergyPickupSmall));	
	}
}

function gravity_step(mass_override, ignore) {
	var G = 9800;
	var attractor_mass = phy_mass;
	if(not is_undefined(mass_override)){
		attractor_mass = mass_override;
	}
	if (is_undefined(ignore)) {
		ignore = noone	
	}
	with(Dynamic) {
		if (variable_instance_exists(id, "disturbed")) disturbed = true;
		if (object_index == GravityBomb) continue;
		if (id == ignore || (object_exists(ignore) and object_is_ancestor(object_index, ignore)) || object_index == ignore) continue;
		if (id == other.id) continue;
		var to = point_direction(phy_com_x, phy_com_y, other.phy_com_x, other.phy_com_y);
		var dist = point_distance(phy_com_x, phy_com_y, other.phy_com_x, other.phy_com_y);
		var force = G * ((attractor_mass * phy_mass) / (dist * dist));
		
		physics_apply_force(phy_com_x,phy_com_y, lengthdir_x(force, to), lengthdir_y(force, to))
	}
}

function splash() {
	if (visible){
		var n = instance_create_depth(phy_com_x, phy_com_y, depth-1, SplashFX);
		n.image_xscale = phy_mass * 4;
		n.image_yscale = n.image_xscale;
	}
}