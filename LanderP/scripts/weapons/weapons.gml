// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Weapon() constructor{
	function fire(from){}
	function sustain(from){return false}
	function get_cost() { return 1 }
	function can_fire() {return true }
	function get_unlocked() {return true};
	function get_icon() {return Sprite1}
	function get_icon_subimage() {return 0};
	function get_icon_position_offset_x() { return 0}
	function get_icon_position_offset_y() { return 0}
	function get_icon_scale() {return 1.0};
	energy = 16;
}

function DefaultWeapon() : Weapon() constructor {
	fire_time = 0;
	function _fire(from, arc) {
		fire_time = current_time;
		with(from){
			var n = instance_create_depth(x,y, depth, ShipProjectile);
			var force = 1;
			var x_imp = lengthdir_x(force, -phy_rotation+arc);
			var y_imp = lengthdir_y(force, -phy_rotation+arc);
			n.phy_linear_velocity_x = phy_linear_velocity_x;
			n.phy_linear_velocity_y = phy_linear_velocity_y;
			with(n) {
				physics_apply_impulse(0, 0, x_imp, y_imp);	
				phy_bullet = true;
			}
		}
	}
	function fire(from) {
		_fire(from, 90);
		if (Game.get_flag("WeaponUpgrade")) {
			_fire(from,100);	
			_fire(from,80);
		}
	}
	
	function can_fire() {
		var limit = 3;
		if(Game.get_flag("WeaponUpgrade")){
			limit = 9;	
		}
		return instance_number(ShipProjectile) < limit	;
	}
	
	function sustain(from){
		if(can_fire() and current_time - fire_time > 250) {
			fire(from);
			return true;
		}
	}
	
	function get_cost() {return 0;}
	function get_icon() {return Sprite1}
	energy = 0;
}

function LaserWeapon() : Weapon() constructor {
	function fire(from) {
		with(from){
			var n = instance_create_depth(x,y, depth-1, ShipLaserLeadFX);
			n.image_yscale = 20;
			if(Game.get_flag("WeaponUpgrade")){
				n.image_index += 4;	
			}
		}
	}
	
	function sustain(from){
		if(can_fire()) {
			fire(from);
			return true;
		}
	}
	
	function can_fire() {
		return instance_number(ShipLaserLeadFX) == 0	;
	}
	
	function get_unlocked() {
		return Game.get_flag(room_get_name(Level2));
	};
	function get_icon() {return Sprite12}
	function get_icon_subimage() {return 5};
	function get_icon_position_offset_y() { return -16}

	
	function get_cost() {return 2};
}

function GravityWeapon() : Weapon() constructor {
	function fire(from) {
		with(from){
			var spawn_offset = 32;
			var spawn_offset_x = lengthdir_x(spawn_offset, -phy_rotation+90);
			var spawn_offset_y = lengthdir_y(spawn_offset, -phy_rotation+90);
			var n = instance_create_depth(x+spawn_offset_x,y+spawn_offset_y, depth-1, GravityBomb);
			n.ignore = id;
			var force = 1000000;
			var x_imp = lengthdir_x(force, -phy_rotation+90);
			var y_imp = lengthdir_y(force, -phy_rotation+90);
			n.phy_linear_velocity_x = phy_linear_velocity_x;
			n.phy_linear_velocity_y = phy_linear_velocity_y;
			with(n) {
				physics_apply_impulse(0, 0, x_imp, y_imp);	
			}
		}
	}
	
	function can_fire() {
		instance_deactivate_object(EnemyGravityBomb)
		var can = instance_number(GravityBomb) == 0;
		instance_activate_all();
		return can;
	}
	
	function sustain(from){
		if(can_fire()) {
			fire(from);
			return true;
		}
	}
	
	function get_unlocked() {
		return Game.get_flag(room_get_name(Level3));
	};
	
	function get_icon() {return Sprite37}

	
	function get_cost() {return 1};
}

function FlameWeapon() : Weapon() constructor {
	from_previous = noone;
	function fire(from) {
		from_previous = from;
		with(from){
			var spawn_offset = 32;
			var spawn_offset_x = lengthdir_x(spawn_offset, -phy_rotation+90);
			var spawn_offset_y = lengthdir_y(spawn_offset, -phy_rotation+90);
			var n = instance_create_depth(x+spawn_offset_x,y+spawn_offset_y, depth-1, FlameJet);
			n.ignore = id;
			n.phy_rotation = phy_rotation;
			if (Game.get_flag("WeaponUpgrade")) {
				n.damage *= 2	
			}
		}
	}
	
	function sustain(from) {
		if (current_time mod 12 == 0) {
			fire(from);	
		}
		return true;
	}
	
	function can_fire() {
		return true;
	}
	
	function get_unlocked() {
		return Game.get_flag(room_get_name(Level7));
	};
	
	function get_icon() {return Sprite74}
	function get_icon_position_offset_y() { return 24}
	function get_icon_scale() {return 2.0};
	
	function get_cost() {
		var sustain = false;
		var f = from_previous;
		with(FlameJet) {
			if (ignore == f) sustain = true;
		}
		
		if (sustain) return 1/room_speed;
		
		return 4;
	};
}

function TorpedoWeapon() : Weapon() constructor {
	fire_time = 0;
	
	function fire(from) {
		fire_time = current_time;
		with(from){
			var spawn_offset = 32;
			var spawn_offset_x = lengthdir_x(spawn_offset, -phy_rotation+90);
			var spawn_offset_y = lengthdir_y(spawn_offset, -phy_rotation+90);
			var n = instance_create_depth(x+spawn_offset_x,y+spawn_offset_y, depth-1, Torpedo);
			n.ignore = id;
			n.phy_rotation = phy_rotation;
			n.phy_angular_velocity = phy_angular_velocity;
			n.phy_linear_velocity_x = phy_linear_velocity_x;
			n.phy_linear_velocity_y = phy_linear_velocity_y;
			n.find_target();
		}
	}
	
	function sustain(from){
		if(can_fire() and current_time - fire_time > 250) {
			fire(from);
			return true;
		}
	}
	
	function can_fire() {
		return true;
	}
	
	function get_unlocked() {
		return Game.get_flag(room_get_name(Level6));
	};
	
	function get_icon() {return Sprite49}
	function get_icon_position_offset_y() { return 24}
	function get_icon_scale() {return 2.0};
	
	function get_cost() {return (Game.get_flag("WeaponUpgrade")?1:3)};
}

function UnimplementedWeapon() : Weapon() constructor {
	function get_unlocked() {return false;}	
}