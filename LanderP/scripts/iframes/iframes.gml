// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function IFrames(blinks, owner_) constructor{
	alpha_blink = false;
	duration = 0;
	duration_base = blinks;
	owner = owner_;
	
	function is_active() {
		return duration > 0;	
	}
	function get_alpha_mod() {
		if (alpha_blink) {
			return 0.5;	
		} else {
			return 1.0;
		}
	}
	
	function activate() {
		duration = duration_base;
		time_source_start(ts);
		update(owner);
	}
	
	function update(parent) {
		parent.iframes.alpha_blink = ! parent.iframes.alpha_blink;
		parent.iframes.duration -= 1;
		if (parent.iframes.duration == 0) {
			parent.iframes.alpha_blink = false;
			time_source_stop(parent.iframes.ts);
		}
	}	
	
	function destroy() {
		time_source_destroy(ts);	
	}
	
	ts = time_source_create(time_source_game, 0.25, time_source_units_seconds, update,[owner],-1, time_source_expire_after)
}