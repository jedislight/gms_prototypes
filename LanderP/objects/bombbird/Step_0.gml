/// @description Insert description here
// You can write your code in this editor
var y_error = ygoal - y
y_error=clamp(y_error, -12,12);
y_force = y_error;
physics_apply_force(phy_com_x, phy_com_y, 0, y_force);
image_speed = sqrt(abs(y_force));

if(facing == 1) {
	sprite_index = Sprite69
}

if (facing == -1) {
	sprite_index = Sprite6970
}

physics_apply_force(phy_com_x, phy_com_y, facing,  0);

physics_apply_torque(-angle_difference(phy_rotation, 0))

if (hp <= 0) {
	instance_destroy(id);	
}