/// @description Insert description here
// You can write your code in this editor



phy_linear_velocity_x = 0;
phy_linear_velocity_y = 0;
phy_angular_velocity = 0;
phy_position_x = phy_position_xprevious;
phy_position_y = phy_position_yprevious;

with(Dynamic) {
	var inside = point_distance(phy_com_x, phy_com_y, other.phy_com_x, other.phy_com_y) < other.sprite_width/2;
	if (inside and not ds_map_exists(other.in, id)) {
		// enter	
		splash()
		if(object_index == WaterScrap) {
			instance_destroy(id);
			exit;
		}
		phy_angular_damping *= other.viscosity;
		phy_linear_damping *= other.viscosity;
		phy_linear_velocity_x *= 0.25;
		phy_linear_velocity_y *= 0.25;
		other.in[? id] = true;
		
	} else if (not inside and ds_map_exists(other.in, id)){
		//exit
		phy_angular_damping /= other.viscosity;
		phy_linear_damping /= other.viscosity;
		ds_map_delete(other.in, id)
		
		splash()
	}
	
}