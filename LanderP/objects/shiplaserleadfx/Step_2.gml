/// @description Insert description here
// You can write your code in this editor


if (instance_number(Ship) == 0 ) {
	instance_destroy(id);
	exit;
}

x = Ship.x
y = Ship.y
image_angle = -Ship.phy_rotation+180;

x += lengthdir_x(10, image_angle-90);
y += lengthdir_y(10, image_angle-90);

if (floor(image_index) == 6 and not fired) {
	fired = true;
	var n = instance_create_depth(x,y, depth, ShipLaserSensor);
	n.phy_rotation = Ship.phy_rotation-180;
	n.image_xscale = image_xscale;
	n.image_yscale = image_yscale;
}