/// @description Insert description here
// You can write your code in this editor


with(WaterZone){
	if (point_in_rectangle(other.phy_com_x, other.phy_com_y, x,y, x+sprite_width, y+sprite_height)) {
		var water_depth = abs(other.phy_com_y - y);
		with(other)
		{
			physics_apply_force(phy_com_x, phy_com_y, 0, -water_depth);	
		}	
	}
}
