/// @description Insert description here
// You can write your code in this editor


shards = 8
var arc = 360 div shards;
p_force = .5;
for(var i = 0; i < 360; i += arc) {
	var to = arc*i+phy_rotation;
	var out_d = 64;
	var offset_x = lengthdir_x(out_d, to);
	var offset_y = lengthdir_y(out_d, to);
	var n = instance_create_depth(phy_com_x+offset_x, phy_com_y+offset_y, depth-1, DamageScrap);
	n.ignore = id;
	
	with(n){
		physics_apply_impulse(x, y, lengthdir_x(other.p_force, to), lengthdir_y(other.p_force, to))			
	}

}