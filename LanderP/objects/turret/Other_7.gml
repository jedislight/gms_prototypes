/// @description Insert description here
// You can write your code in this editor

if (instance_number(Ship) == 0) exit;

var n = instance_create_depth(x+p_offset_x, y+p_offset_y, depth-1, EnemyProjectile);
var to = point_direction(n.x, n.y, Ship.x, Ship.y);
with(n){
	physics_apply_impulse(x, y, lengthdir_x(other.p_force, to), lengthdir_y(other.p_force, to))	
}
