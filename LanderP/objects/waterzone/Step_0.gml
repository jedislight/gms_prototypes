/// @description Insert description here
// You can write your code in this editor

function splash() {
	if (visible){
		var n = instance_create_depth(phy_com_x, phy_com_y, depth-1, SplashFX);
		n.image_xscale = phy_mass * 4;
		n.image_yscale = n.image_xscale;
	}
}

with(Dynamic) {
	var inside = collision_point(phy_com_x, phy_com_y, WaterZone, false, false) == other.id;
	if (inside and not ds_map_exists(other.in, id)) {
		// enter	
		splash()
		if(object_index == WaterScrap or object_index == FlameJet) {
			instance_destroy(id);
			exit;
		}
		phy_angular_damping *= other.viscosity;
		phy_linear_damping *= other.viscosity;
		phy_linear_velocity_x *= 0.25;
		phy_linear_velocity_y *= 0.25;
		other.in[? id] = true;
		
	} else if (not inside and ds_map_exists(other.in, id)){
		//exit
		phy_angular_damping /= other.viscosity;
		phy_linear_damping /= other.viscosity;
		ds_map_delete(other.in, id)
		
		splash()
	}
	
}