/// @description Insert description here
// You can write your code in this editor


state_interrupt_time = room_speed * 2.5;
state_interrupt_counter = 0;

aim = 0;

function is_on_ground() {
	return physics_test_overlap(phy_com_x, phy_com_y+5, phy_rotation, Static)	
}

function drive(f) {
	if (abs(phy_linear_velocity_x) < abs(f/2))
		physics_apply_force(phy_com_x, phy_com_y, f, 0);
}

function right() {
	sprite_index = Sprite61;
	if(!is_on_ground()) return;
	
	if (!physics_test_overlap(phy_com_x+2*drive_force, phy_com_y+25, phy_rotation, Static))
	{
		state = left;	
	}
	drive(drive_force)
}

function left() {
	sprite_index = Sprite6162;
	if(!is_on_ground()) return;
	
	if (!physics_test_overlap(phy_com_x-2*drive_force, phy_com_y+25, phy_rotation, Static))
	{
		state = right;
	}
	drive(-drive_force)
}

aim_delta = 2.5;
function aim_and_fire() {
	if(!is_on_ground()) return;
	
	phy_linear_velocity_x *= 0.9;
	
	aim += aim_delta;
	if (aim >= 180) {
		aim_delta = -aim_delta;
	}
	
	if (aim <= 0) {
		state = previous_state	
		aim_delta = -aim_delta;
		aim = 0;
	}
	
	if (aim mod 45 == 0) {
		var to = aim;
		if (sprite_index != object_get_sprite(object_index)) {
			to = 180 - to;	
		}
		p_force = 0.5;
		offset_x = 5;
		offset_y = -10;
		var n = instance_create_depth(phy_com_x+offset_x, phy_com_y+offset_y, depth-1, DamageScrap);
		n.ignore = id;
	
		with(n){
			physics_apply_impulse(x, y, lengthdir_x(other.p_force, to), lengthdir_y(other.p_force, to))			
		}
	}
}

state = right
previous_state = right;
drive_force = 50
image_speed = 0.125

hp = 1;

