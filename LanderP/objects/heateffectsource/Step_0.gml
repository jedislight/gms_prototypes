/// @description Insert description here
// You can write your code in this editor



visible = Debug.enabled;
if(random(1) < 0.01){
	
	var dx = abs(Camera.x - x);
	var dy = abs(Camera.y - y);
	
	var less = false;
	if (instance_number(Ship)>0){
		with(WaterZone){
			if(in[? Ship.id]) {
				less = true;	
			}
		}
		
		with(WaterPlanetoid){
			if(in[? Ship.id]) {
				less = true;	
			}
		}
	}
	
	if(less) {
		dx *= 3;
		dy *= 3;
	}
	
	dx = max(1, dx);
	dy = max(1, dy);

	var s1 = cap - (dx / dist_damp);
	var s2 = cap - (dx / dist_damp);

	var fx = layer_get_fx("HeatEffect");

	fx_set_parameter(fx, "g_Distort1Amount", s1);
	fx_set_parameter(fx, "g_Distort2Amount", s2);
}