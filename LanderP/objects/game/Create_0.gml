/// @description Insert description here
// You can write your code in this editor


flags = ds_map_create();

function get_flag(flag){
	if (Debug.all_flags) return true;
	var f = flags[? flag];
	if (is_undefined(f)) {
		f = false;	
	}
	
	return f;
}

function set_flag(flag, value){
	flags[? flag] = value;
}