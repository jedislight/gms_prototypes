{
  "spriteId": {
    "name": "Sprite16",
    "path": "sprites/Sprite16/Sprite16.yy",
  },
  "solid": true,
  "visible": false,
  "managed": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "CameraConstraint",
    "path": "objects/CameraConstraint/CameraConstraint.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 1,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Level Assets",
    "path": "folders/Objects/Level Assets.yy",
  },
  "resourceVersion": "1.0",
  "name": "CameraBlock",
  "tags": [],
  "resourceType": "GMObject",
}