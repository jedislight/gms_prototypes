/// @description Insert description here
// You can write your code in this editor

var v = animcurve_channel_evaluate(animcurve_get_channel(curve, 0), pos);
image_speed = (v+0.125) * -sign(force_mod);
var force = v * force_mod;
pos += 1 / room_speed / period;
if (pos > 1){
	pos -= 1;
}

with(Dynamic) {
	var inside = collision_point(phy_com_x, phy_com_y, other.object_index, false, false) == other.id;

	if (inside) {
		physics_apply_force(phy_com_x, phy_com_y, 0, force)	
	}
}
