/// @description Insert description here
// You can write your code in this editor
image_index = clamp (image_index, 0, image_number-1);

switch (state) {
	case(OWD_STATES.OWD_OPENING):
		physics_pause_enable(true)	
		image_speed = 1;
		if (image_index == image_number - 1) {
			state = OWD_STATES.OWD_SLIDING;	
		}
		
		with(Turret) image_speed = 0;
	break;
	
	case(OWD_STATES.OWD_SLIDING):
		physics_pause_enable(true);
		Ship.phy_position_x += force_mod;
		Ship.x += force_mod;
		if(Ship.x > x + sprite_width +Ship.sprite_width) {
			state = OWD_STATES.OWD_CLOSING;	
		}
	break;
	
	case(OWD_STATES.OWD_CLOSING):
		image_speed = -1;
		if (image_index == 0) {
			state = OWD_STATES.OWD_FINISHED;	
		}
		physics_pause_enable(true)	
	break;
	
	case(OWD_STATES.OWD_FINISHED):
		physics_pause_enable(false)
		var orig = Ship.id;
		var clone = instance_create_depth(orig.x, orig.y, orig.depth, orig.object_index);
		clone.phy_rotation = orig.phy_rotation;
		clone.hp = orig.hp;
		clone.hp_prev = orig.hp_prev;
		instance_destroy(orig, false);
		
		with(Turret) image_speed = 0.125;
	
	
		instance_change(Block, true);
	break;
}