/// @description Insert description here
// You can write your code in this editor





image_alpha -= image_xscale * .025;

if (image_alpha < 0.1) {
	instance_destroy(id);	
}

var dyns = [];
with(Dynamic){
	array_push(dyns, id);	
}
with(Static){
	if(variable_instance_exists(id, "hp") or object_index == LaserBlock){
		array_push(dyns, id);	
	}
}
for(var i = 0; i < array_length(dyns); ++i) {
	var dyn = dyns[i];	
	if (physics_test_overlap(phy_position_x, phy_position_y, phy_rotation, dyn)) {
		collision_with_enemy(dyn);
		if(dyn.object_index == LaserBlock){
			instance_destroy(dyn)	;
		}
	}
}
