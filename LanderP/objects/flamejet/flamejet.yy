{
  "spriteId": {
    "name": "Sprite74",
    "path": "sprites/Sprite74/Sprite74.yy",
  },
  "solid": false,
  "visible": true,
  "managed": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "Dynamic",
    "path": "objects/Dynamic/Dynamic.yy",
  },
  "physicsObject": true,
  "physicsSensor": true,
  "physicsShape": 1,
  "physicsGroup": 1,
  "physicsDensity": 0.0,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": false,
  "physicsKinematic": false,
  "physicsShapePoints": [
    {"x":12.0,"y":1.0,},
    {"x":51.0,"y":1.0,},
    {"x":51.0,"y":65.0,},
    {"x":12.0,"y":65.0,},
  ],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":3,"collisionObjectId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":2,"collisionObjectId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Objects",
    "path": "folders/Objects.yy",
  },
  "resourceVersion": "1.0",
  "name": "FlameJet",
  "tags": [],
  "resourceType": "GMObject",
}