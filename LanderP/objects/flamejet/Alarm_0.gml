/// @description Insert description here
// You can write your code in this editor



if( spawn > 0) {
	var offset = -sprite_height * 0.875;
	var xx = x + lengthdir_x(offset, -phy_rotation-90);
	var yy = y + lengthdir_y(offset, -phy_rotation-90);
	var n = instance_create_depth(xx,yy, depth, object_index);	
	n.spawn = spawn -1;
	n.phy_rotation = phy_rotation;
	n.image_xscale = image_xscale * 1.3;
	n.image_yscale = image_yscale * 0.8;
	n.ignore = ignore;
	spawn = 0;
	
}