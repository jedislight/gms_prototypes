/// @description Insert description here
// You can write your code in this editor
var scale_dampening = 200;
image_xscale = point_distance(phy_linear_velocity_x, phy_linear_velocity_y, 0, 0) / scale_dampening;
image_xscale = clamp(image_xscale, 0, 2.0);
image_yscale = image_xscale

if (image_xscale < 0.1) {
	instance_destroy(id)	
}