/// @description Insert description here
// You can write your code in this editor

vprevious=v;
v = animcurve_channel_evaluate(animcurve_get_channel(curve, 0), pos);

pos += 1 / room_speed / period;
if (pos > 1){
	pos -= 1;
}

var on = v > threshold;
var spew = on and ( vprevious < threshold or random_range(0,1) < 0.1);

image_speed = 0.125 + v;

if(spew) {
	var offset_dir = 270+35;
	var offset_length = 55 * ((image_yscale + image_xscale) / 2);
	var xx = x + lengthdir_x(offset_length, offset_dir - phy_rotation);
	var yy = y + lengthdir_y(offset_length, offset_dir - phy_rotation)
	var n = instance_create_depth(xx, yy, depth+1, FlameJet);	
	n.phy_rotation = phy_rotation;
	n.image_xscale = image_xscale;
	n.image_yscale = image_yscale;
	n.ignore = id;
}

if (hp <= 0) instance_destroy(id);