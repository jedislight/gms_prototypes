/// @description Insert description here
// You can write your code in this editor


hp_max = 9;
for(var i = 1; i <= 8; ++i){
	var flag = "Level"+string(i)+"HpMax";
	if(Game.get_flag(flag)) {
		++hp_max;	
	}
}
hp = hp_max;
hp_prev = hp;

vel_last_x = 0;
vel_last_y = 0;

vel_delta = 0;
vel_delta_max = 0;

instance_create_depth(x,y,depth, Camera);
weapons_index = 0;

weapons = [
	new DefaultWeapon(), // 0
	new UnimplementedWeapon(), // 1
	new LaserWeapon(), // 2
	new GravityWeapon(), // 3
	new UnimplementedWeapon(), // 4
	new UnimplementedWeapon(), // 5
	new TorpedoWeapon(), // 6
	new FlameWeapon(), // 7
	new UnimplementedWeapon(), // 8
];

function cycle_weapons(amount) {
	do { 
		weapons_index += amount;
		if (weapons_index < 0 ) {
			weapons_index = 8;	
		}
	
		if (weapons_index > 8 ) {
			weapons_index = 0;	
		}
	} until (weapons[weapons_index].get_unlocked())
}

turn_accel = 1

iframes = new IFrames(8, id);

fire_pressed = 0;