/// @description Insert description here
// You can write your code in this editor

draw_ship_health(id, 0);
//draw_text(x+32,y-32, string(vel_delta_max))

var weapon = weapons[weapons_index];
if( weapon.get_cost() > 0) {
	var margin = 16
	var e = weapon.energy;
	var xx = 32 + sprite_get_width(SpriteEnergy);
	var e_max = 16;
	var hp_x = margin + xx;
	var hp_bot = margin + sprite_get_height(SpriteEnergy) * 16;
	var hp_top = hp_bot - sprite_get_height(SpriteEnergy) * e_max;
	draw_set_color(c_black);
	draw_roundrect_ext(hp_x-0.25*margin, hp_top-margin/4, hp_x+0.25*margin+sprite_get_width(SpriteEnergy), hp_bot+margin/4, 3,3, false)
	draw_set_color(c_white);
	draw_roundrect_ext(hp_x-0.25*margin, hp_top-margin/4, hp_x+0.25*margin+sprite_get_width(SpriteEnergy), hp_bot+margin/4, 3,3, true)

	for(var i = 1; i <= e_max; ++i) {
		var hp_y = hp_bot - sprite_get_height(SpriteEnergy) * i;
		if(e >= i) {
			draw_sprite(SpriteEnergy, 0, hp_x, hp_y);
		}
	}
	
	var icon = weapon.get_icon();
	var icon_x = hp_x+0.25*margin+sprite_get_width(SpriteEnergy) + margin;
	var icon_y = hp_top;
	draw_set_color(c_black);
	draw_roundrect(icon_x, icon_y, icon_x+32+margin*2, icon_y+32+margin*2, false)
	draw_set_color(c_white);
	draw_roundrect(icon_x, icon_y, icon_x+32+margin*2, icon_y+32+margin*2, true)
	draw_sprite_ext(icon, weapon.get_icon_subimage(), icon_x+margin+16+weapon.get_icon_position_offset_x(), icon_y + margin + 16 + weapon.get_icon_position_offset_y(), 0.5*weapon.get_icon_scale(), 0.5*weapon.get_icon_scale(), 0, c_white, 1.0);
}