
vel_delta = point_distance(phy_linear_velocity_x, phy_linear_velocity_y, vel_last_x, vel_last_y);
vel_delta_max = max(vel_delta_max, vel_delta);

vel_last_x = phy_linear_velocity_x;
vel_last_y = phy_linear_velocity_y;

var impact = vel_delta div impact_resistance;
if (impact > 0) {
	deal_damage(id, impact);
}

var hp_lost = hp_prev - hp;
hp_lost = max(0, hp_lost);
if (Game.get_flag("ArmorUpgrade") and hp > 1) {
	hp += hp_lost * 0.5;	
}
if(hp_lost > 0) {
	create_scrap(ShipScrap, hp_lost, 0);
}
hp_prev = hp;


if (hp == 0) { 
	instance_destroy(id);	
}

if(not keyboard_check_direct(ord("A")) and not keyboard_check_direct(ord("D"))) {
	turn_accel = -5;
}
if (abs(phy_angular_velocity) > 200) {
	phy_angular_velocity=200 * sign(phy_angular_velocity);	
}