/// @description Insert description here
// You can write your code in this editor

draw_sprite_ext(sprite_index, image_index, x,y, image_xscale, image_yscale, image_angle, image_blend, image_alpha * iframes.get_alpha_mod())

if (Game.get_flag("ArmorUpgrade")) {
	draw_sprite_ext(SpriteShipArmor, image_index, x,y, image_xscale, image_yscale, image_angle, image_blend, image_alpha * iframes.get_alpha_mod())
}

if(Game.get_flag("ThrustUpgrade")) {
	draw_sprite_ext(SpriteShipThrustBoost, image_index, x,y, image_xscale, image_yscale, image_angle, image_blend, image_alpha * iframes.get_alpha_mod())
}

if(Game.get_flag("SpikesUpgrade")) {
	draw_sprite_ext(SpriteShipSpikes, image_index, x,y, image_xscale, image_yscale, image_angle, image_blend, image_alpha * iframes.get_alpha_mod())
}

if(Game.get_flag("WeaponUpgrade")) {
	draw_sprite_ext(SpriteShipWeapon, image_index, x,y, image_xscale, image_yscale, image_angle, image_blend, image_alpha * iframes.get_alpha_mod())
}