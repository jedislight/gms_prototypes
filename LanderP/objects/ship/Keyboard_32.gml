/// @description Fire/Sustain
// You can write your code in this editor

var weapon = weapons[weapons_index];

fire_pressed+=1;

if (fire_pressed = 1) {
	if (weapon.get_cost() <= weapon.energy and weapon.can_fire()) {
		if (not Debug.enabled) weapon.energy -= weapon.get_cost();
		weapon.fire(id);
	}
}

else {
	if (weapon.get_cost() <= weapon.energy) {
		if (weapon.sustain(id)) {
			if (not Debug.enabled) weapon.energy -= weapon.get_cost();		
		}
	}
}