/// @description Insert description here
// You can write your code in this editor

if (hp <= 0) {
	instance_destroy(id);
	exit;
}

if(instance_number(Camera) > 0){
	if (point_distance(phy_com_x, phy_com_y, Camera.x, Camera.y) < 512){
		show_hp_rate = 0.25
		image_speed = 0.125 * (17-hp)
		if (instance_number(Planetoid) > 0) with(Planetoid) instance_destroy(id);
		if (instance_number(WaterPlanetoid) > 0) with(WaterPlanetoid) instance_destroy(id);
	}
}

show_hp += show_hp_rate

if(random(1) < 0.01){
	
	var dx = abs(Camera.x - x);
	var dy = abs(Camera.y - y);
	
	var less = false;
	if (instance_number(Ship)>0){
		with(WaterZone){
			if(in[? Ship.id]) {
				less = true;	
			}
		}
		
		with(WaterPlanetoid){
			if(in[? Ship.id]) {
				less = true;	
			}
		}
	}
	
	if(less) {
		dx *= 3;
		dy *= 3;
	}
	
	dx = max(1, dx);
	dy = max(1, dy);

	var s1 = cap - (dx / dist_damp);
	var s2 = cap - (dx / dist_damp);

	var fx = layer_get_fx("HeatEffect");
	var t = max(0.1, tempuature);
	fx_set_parameter(fx, "g_Distort1Amount", s1 * t);
	fx_set_parameter(fx, "g_Distort2Amount", s2 * t);
}