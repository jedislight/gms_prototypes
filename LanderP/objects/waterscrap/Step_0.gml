/// @description Insert description here
// You can write your code in this editor

var fade =0.94 + 0.5*(abs(phy_linear_velocity_x) + abs(phy_linear_velocity_y) + abs(phy_angular_velocity))
fade = min(fade, 1.0);
image_alpha *=fade;

if (image_alpha < 0.2) {
	instance_destroy(id);	
}

if (hp <= 0) instance_destroy(id);