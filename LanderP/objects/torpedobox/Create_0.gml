/// @description Insert description here
// You can write your code in this editor



pos = 0;

target_x = phy_position_x;
target_y = phy_position_y;
idle_counter = 0;

move_x  = 0;
move_y = 0;

function idle_state() {
	phy_position_x = phy_position_xprevious;
	phy_position_y = phy_position_yprevious;
	if(++idle_counter > idle_duration or (iframes.is_active() and idle_counter > iframes_idle_duration)) {
		idle_counter = 0;
		var l = ds_list_create();
		with(BoxWaypoints) {
			if (x == other.target_x or y == other.target_y) {
				ds_list_add(l, id);		
			}
		}
		
		ds_list_shuffle(l);
		var waypoint = l[|0];
		ds_list_destroy(l);
		
		target_x = waypoint.x;
		target_y = waypoint.y;
		
		state = motion_state;
	}
}

function motion_state() {
	var dist = point_distance(target_x, target_y, phy_position_x, phy_position_y);
	dist_catchup = 1.0 + (dist div 100)
	move_x = sign(target_x - phy_position_x) * move_speed * dist_catchup;
	move_y = sign(target_y - phy_position_y) * move_speed * dist_catchup;
	move_x = min(abs(move_x), abs(target_x-phy_position_x)) * sign(move_x);
	move_y = min(abs(move_y), abs(target_y-phy_position_y)) * sign(move_y);
	if ( dist < move_speed) {
		phy_position_x = target_x;
		phy_position_y = target_y;
		move_x = 0;
		move_y = 0;
		state = fire_state;
	} else {
		phy_position_x = phy_position_xprevious + move_x;
		phy_position_y = phy_position_yprevious + move_y;	
	}
}

function fire_state() {
	state = idle_state;	
	if (show_hp == 0 ) return;
	phy_position_x = phy_position_xprevious;
	phy_position_y = phy_position_yprevious;
	if (instance_number(Ship) == 0) return;
	var spawn_offset = 32;
	var dir = point_direction(x,y, Ship.x, Ship.y)
	var spawn_offset_x = lengthdir_x(spawn_offset, dir);
	var spawn_offset_y = lengthdir_y(spawn_offset, dir);
	var n = instance_create_depth(x+spawn_offset_x,y+spawn_offset_y, depth-1, Torpedo);
	n.ignore = id;
	n.phy_rotation = -dir+90;
	n.find_target();
	n.acceleration *= 0.25;
	n.acceleration_decay *= 0.07;
}

state = idle_state

x_log = ds_list_create();
y_log = ds_list_create();

trails = [1, 3, 7, 15, 31];


hp = 16
show_hp_rate = 0;
show_hp = 0
hp_max = 16;
iframes = new IFrames(4, id);