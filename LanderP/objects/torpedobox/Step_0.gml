/// @description Insert description here
// You can write your code in this editor


state();

iframes_idle_duration = hp * 2;

var v = animcurve_channel_evaluate(animcurve_get_channel(curve, 0), pos);
v = (v-0.5) * 2;
image_speed = (v+0.125) * -sign(force_mod);
var force = v * force_mod;
pos += 1 / room_speed / period;
if (pos > 1){
	pos -= 1;
}

phy_rotation += force;
phy_linear_velocity_y = 0;
phy_linear_velocity_x = 0;

ds_list_insert(x_log, 0, phy_position_x)
ds_list_insert(y_log, 0, phy_position_y)
if(ds_list_size(x_log) > trails[array_length(trails)-1]){
	ds_list_delete(x_log, ds_list_size(x_log)-1)	
	ds_list_delete(y_log, ds_list_size(y_log)-1)
}

if(instance_number(Camera) > 0){
	if (point_distance(phy_com_x, phy_com_y, Camera.x, Camera.y) < 512){
		show_hp_rate = 0.25
		image_speed = 0.125 * (17-hp)
		if (instance_number(Planetoid) > 0) with(Planetoid) instance_destroy(id);
		if (instance_number(WaterPlanetoid) > 0) with(WaterPlanetoid) instance_destroy(id);
	}
}

show_hp += show_hp_rate

if (hp <= 0) {
	instance_destroy(id);
	exit;
}