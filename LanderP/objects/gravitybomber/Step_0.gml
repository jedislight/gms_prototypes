/// @description Insert description here
// You can write your code in this editor

image_index = clamp(image_index, 0, image_number-1);

state();

if (phy_active) {
	var stab_mod = 1.2;
	physics_apply_force(phy_com_x, phy_com_y, -phy_linear_velocity_x*stab_mod, -phy_linear_velocity_y*stab_mod)
	phy_rotation = 0;
	phy_angular_velocity = 0;
}

if (hp <= 0) instance_destroy(id);