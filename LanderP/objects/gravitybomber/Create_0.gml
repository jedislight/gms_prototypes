/// @description Insert description here
// You can write your code in this editor

function gravity_bomber_fire() {
	var rots = [0, 90, 180, 270];
	for(var i = 0; i < array_length(rots); ++i){
		var rot = rots[i];
		var spawn_offset = 92;
		var spawn_offset_x = lengthdir_x(spawn_offset, -phy_rotation+rot);
		var spawn_offset_y = lengthdir_y(spawn_offset, -phy_rotation+rot);
		var n = instance_create_depth(x+spawn_offset_x,y+spawn_offset_y, depth-1, EnemyGravityBomb);
		n.ignore = GravityBomber;
		var force = 75;
		var x_imp = lengthdir_x(force, -phy_rotation+rot);
		var y_imp = lengthdir_y(force, -phy_rotation+rot);
		n.phy_linear_velocity_x = phy_linear_velocity_x;
		n.phy_linear_velocity_y = phy_linear_velocity_y;
		with(n) {
			physics_apply_impulse(0, 0, x_imp, y_imp);	
		}
	}
}


function gravity_bomber_spin_up() {
	image_speed = 0.05;
	if (image_index >= image_number-1) {
		state = gravity_bomber_spin_down;
		gravity_bomber_fire();
		
	}
}

function gravity_bomber_spin_down() {
	image_speed = -0.5;
	if (image_index <= 1) {
		image_index = 1;
		if(instance_number(Ship) > 0 and point_distance(x,y, Ship.x, Ship.y) < 800){
			state = gravity_bomber_spin_up	
		}
	}
}

state = gravity_bomber_spin_down