/// @description Insert description here
// You can write your code in this editor



target = noone;

image_speed = 0.125

ignore = noone;

function find_target(override) {
	if (not is_undefined(override)) {
		target = override;	
	}else {
		instance_deactivate_object(Scrap);
		var l = ds_list_create();
		collision_circle_list(x,y, 2048, Dynamic, false, true, l, true);
		for(var i = 0; i < ds_list_size(l); ++i) {
			var n = l[|i];
			if(variable_instance_exists(n, "hp") and ignore != n) {
				target = n;
				break;
			}
		}
		instance_activate_all();
	}
}