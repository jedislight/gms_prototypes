/// @description Insert description here
// You can write your code in this editor


var fwd = phy_rotation+90;
if(!instance_exists(target)){
	find_target();	
}
if (acceleration > 0) {
	if (instance_exists(target)) {
		var to = -point_direction(phy_com_x, phy_com_y, target.phy_com_x, target.phy_com_y);
		var adjust = angle_difference(fwd, to);
		physics_apply_torque(min(abs(adjust), turn_rate)*sign(adjust))
	}

	physics_apply_force(phy_com_x, phy_com_y, lengthdir_x(-acceleration, -fwd), lengthdir_y(-acceleration, -fwd));	
	
	instance_create_depth(phy_com_x,phy_com_y,depth, SmokeFX)
} else {
	physics_apply_force(phy_com_x, phy_com_y, 0, -acceleration);			
}

acceleration -= 1 / 2 * (acceleration > 0 ? acceleration_decay: 1);
