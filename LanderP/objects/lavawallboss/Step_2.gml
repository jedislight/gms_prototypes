/// @description Insert description here
// You can write your code in this editor



if (hp <= 0) {
	instance_destroy(id);
	exit;
}

if(instance_exists(child)){
	var c = hp / hp_max * 255;
	child.image_blend = make_color_rgb(c,c,c);
	child.image_alpha *= iframes.get_alpha_mod();

}

image_alpha = iframes.get_alpha_mod();