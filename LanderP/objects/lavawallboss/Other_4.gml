/// @description Insert description here
// You can write your code in this editor



child = instance_create_depth(x,y, depth+1, LavaBlock);
child.image_xscale = image_xscale;
child.image_yscale = image_yscale;
child.phy_active = false;

with(FlameSpewer) {
	if (distance_to_point(other.x,other.y) < 1000) {
		array_push(other.spewers, id);
	}
}

array_sort(spewers, function(elm1, elm2)
{
    return elm1.y - elm2.y;
});