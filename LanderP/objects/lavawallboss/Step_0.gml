/// @description Insert description here
// You can write your code in this editor

var v = animcurve_channel_evaluate(animcurve_get_channel(curve, 0), pos);

pos += 1 / room_speed / period;
if (pos > 1){
	pos -= 1;
}

v = v * (array_length(spewers)-1);
v = round(v);
v = v mod array_length(spewers);
for(var i = 0; i < array_length(spewers); ++i) {
	var spewer = spewers[i];
	if (instance_exists(spewer)){
		spewer.curve = CurveNever;
		if (v == i) spewer.curve = CurveAlways;
	}
}


if(instance_number(Camera) > 0){
	if (point_distance(phy_com_x, 0, Camera.x, 0) < 512){
		show_hp_rate = 0.25
	}
}

show_hp += show_hp_rate