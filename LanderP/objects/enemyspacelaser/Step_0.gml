/// @description Insert description here
// You can write your code in this editor
if(not instance_number(Ship) > 0) exit;

if(hit_ship) exit;

if (floor(image_index) != 6 or floor(image_index - image_speed) != 5) exit;

var xd = abs(Ship.x - x);
if (xd <= sprite_width/2){
	hit_ship = true;
	deal_damage(Ship.id, 2);
}

with(LaserBlock) {
	var xd = abs(other.x - x-sprite_width/2);
	if (xd <= other.sprite_width) {
		create_scrap();
		instance_destroy(id);
	}
}

with(Scrap) {
	var xd = abs(other.x - x-sprite_width/2);
	if (xd <= other.sprite_width) {
		if(image_alpha != 1.0) {
			
			instance_destroy(id)
		}
	}
}