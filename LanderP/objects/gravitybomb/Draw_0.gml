/// @description Insert description here
// You can write your code in this editor


if ((pulse_step == 5 or pulse_step == 6)) {
	if (Game.get_flag("WeaponUpgrade") and object_index == GravityBomb) {
		draw_sprite_ext(sprite_index, image_index, x, y, image_xscale*8, image_yscale*8, 0, c_dkgray, 1.0)	
	}
	draw_sprite_ext(sprite_index, image_index, x, y, image_xscale*4, image_yscale*4, 0, c_gray, 1.0)	
	draw_sprite_ext(sprite_index, image_index, x, y, image_xscale*2, image_yscale*2, 0, c_ltgray, 1.0)	
	
}

draw_self();
