/// @description Insert description here
// You can write your code in this editor



var v = animcurve_channel_evaluate(animcurve_get_channel(curve, 0), pos);
var pulse_step_previous = floor(pos * 8);
pos += 1 / room_speed / period;
pulse_step = floor(pos * 8);
do_pulse = pulse_step_previous != pulse_step and (pulse_step == 5 or pulse_step == 6)

var upgraded = Game.get_flag("WeaponUpgrade") and object_index == GravityBomb
if (do_pulse) {
	with(PhysicsObject) {
		if (not variable_instance_exists(id, "hp")) continue;
		var d = id;
		var dist = point_distance(phy_com_x, phy_com_y, other.phy_com_x, other.phy_com_y);
		if (dist < 64) {
			with(other) {damage = (upgraded ? 3 : 2); collision_with_enemy(d);}
		}
		else if (dist < 128) {
			with(other){damage = (upgraded ? 2 : 1); collision_with_enemy(d);}
		}
		else if (upgraded and dist < 256) {
			with(other) {damage = 1; collision_with_enemy(d);}
		}
		
	}
}

image_xscale = v
image_yscale = image_xscale;
if(pos > 1) {
	instance_destroy(id);
}


