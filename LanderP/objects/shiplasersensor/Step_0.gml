/// @description Insert description here
// You can write your code in this editor



with(PhysicsObject) {
	if (phy_active and id != other.id){
		if (physics_test_overlap(phy_position_x, phy_position_y, phy_rotation, other.id)) {
			if (object_index == LaserBlock) {
				instance_destroy(id);
			} else {
				var obj = id;
				with(other.id) {
					collision_with_enemy(obj);	
				}
			}
		}
	}
}

instance_destroy(id);