/// @description Insert description here
// You can write your code in this editor

if (instance_number(object_index) > 1) {
	instance_destroy(id);
	exit;
}
room_camera = view_get_camera(0);
camera_set_view_target(room_camera, id);
camera_set_view_border(room_camera, 600,300);
camera_set_view_speed(room_camera, -1, -1);

target = Ship
trap_override = noone;