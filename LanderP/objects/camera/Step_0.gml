/// @description Insert description here
// You can write your code in this editor

speed = 0;
trap = collision_point(x,y, CameraTrap, false, true);

var final_target = target;
if(instance_exists(trap_override)) {
	final_target = trap_override;
	if (not instance_exists((trap))) {
		var wtrap = collision_circle(x,y, 3, CameraTrap, false, true);
		if (instance_exists(wtrap)) {
			var tc = wtrap.x + 64 * wtrap.image_xscale/2;
			var tm = wtrap.y + 64 *wtrap.image_yscale/2;
			while (collision_point(x,y, CameraTrap, false, true) == noone) {
				x = lerp(x, tc, 0.1);
				y = lerp(y, tm, 0.1);
			}
			trap_override = noone;
			exit;
		}
	}
}



var target_instance = noone;
if (object_exists(final_target) and instance_number(final_target) > 0 ) {
		target_instance = instance_nearest(x,y, final_target);
} else if (instance_exists(final_target)) {
	target_instance = final_target;
}

function camera_constrained() {
	if (collision_point(x,y, CameraBlock, false, true) != noone) return true;
	if (instance_exists(trap) and collision_point(x,y, CameraTrap, false, true) == noone) return true
	return false;
}

if(instance_exists(target_instance)) {
	//lock
	// x = target_instance.x;
	// y = target_instance.y;
	
	// lerp
	x = lerp(x, target_instance.x, 0.1);
	if(camera_constrained()) {
		x = xprevious;
	}
	
	y = lerp(y, target_instance.y, 0.1);
	if(camera_constrained()) {
		y = yprevious;
	}
}