/// @description Insert description here
// You can write your code in this editor

if (keyboard_check_pressed(vk_f1)){
	set_debug_mode(!enabled);
}

if (!enabled) exit;

if (keyboard_check_pressed(vk_f2)) {
	room_goto(Hub);
}

if (keyboard_check_pressed(vk_f3)) {
	all_flags = true;	
}

if (keyboard_check_pressed(vk_f4)){
	if (instance_number(Game) > 0){
		ds_map_clear(Game.flags);
	}
}

if (mouse_check_button_pressed(mb_right)) {
	x = mouse_x
	y = mouse_y
	item_drop()
}

if (keyboard_check_direct(vk_f5)) {
	game_set_speed(7.5, gamespeed_fps) 	
}

if (keyboard_check_direct(vk_f6)) {
	game_set_speed(30, gamespeed_fps) 	
}

if (keyboard_check_direct(vk_f7)) {
	game_set_speed(60, gamespeed_fps) 	
}

if (keyboard_check_direct(vk_f8)) {
	game_set_speed(gamespeed_fps, gamespeed_fps) 	
}