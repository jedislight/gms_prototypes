/// @description Insert description here
// You can write your code in this editor

if (instance_number(object_index) > 1) {
	instance_destroy(id);
	exit;
}

function set_debug_mode(value) {
	enabled = value;	
	show_debug_overlay(enabled);
}

set_debug_mode(os_get_config() == "Debug");

all_flags = false;
