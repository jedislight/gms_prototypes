/// @description Insert description here
// You can write your code in this editor



image_speed = 0.125

function apply_convery_force(f) {
	with(other) {
		if(sign(f) != sign(phy_linear_velocity_x) or abs(phy_linear_velocity_x) < other.max_vel) {
			physics_apply_force(phy_com_x, phy_com_y, f*phy_mass, 0)	
		}
	}	
}