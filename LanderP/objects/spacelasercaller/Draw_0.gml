/// @description Insert description here
// You can write your code in this editor

var fire_frame = frames_until_next_fire div 8;
if( fire_frame <= 4 ) {
	var offset_distance = 17 * image_yscale;
	var offset_dir = 90;
	var sprite_scale_mod = 0.35;
	var sprite_x_offset = -5;
	var sprite_y_offset = -3;
	var x_offset = lengthdir_x(offset_distance, offset_dir);
	var y_offset = lengthdir_y(offset_distance, offset_dir);
	draw_sprite_ext(Sprite11, 0, x+sprite_x_offset+x_offset, y+y_offset+sprite_y_offset, image_xscale*sprite_scale_mod, image_yscale*sprite_scale_mod, 0, c_white, 1.0);	
}

draw_sprite_ext(sprite_index, image_index, x,y, image_xscale, image_yscale, image_angle, image_blend, image_alpha * iframes.get_alpha_mod())
