/// @description Insert description here
// You can write your code in this editor


physics_apply_force(0, 0, 0, -phy_linear_velocity_y*.25);

var target_x = xstart;
var target_y = ystart;
var target = instance_nearest(x,y, PathObserver);
if (instance_exists(target)){
	target_x = target.x;
	target_y = target.y;
}

var to_target = point_direction(x, y, target_x, target_y);
var d_target = (point_distance(x, y, target_x, target_y) + 250) / 2;
var dx = lengthdir_x(d_target, to_target);
var dy = lengthdir_y(d_target, to_target);

physics_apply_force(0,0, dx, dy);

phy_rotation = phy_rotation + angle_difference(0, phy_rotation) / 100

frames_until_next_fire-=17-hp;
if (frames_until_next_fire <= 0) {
	frames_until_next_fire = fire_delay;
	if (instance_number(Ship) > 0) {
		var n = instance_create_depth(Ship.x,0, depth, EnemySpaceLaser);
		n.image_yscale = room_height / 32;
	}
}

if(instance_number(Camera) > 0){
	if (distance_to_object(Camera.id) < 512){
		show_hp_rate = 0.25
		image_speed = 0.125 * (17-hp)
	}
}

show_hp += show_hp_rate

var m = 128;
phy_linear_velocity_x = clamp(phy_linear_velocity_x, -m, m)
phy_linear_velocity_y = clamp(phy_linear_velocity_y, -m, m)

if (hp <= 0) instance_destroy(id);