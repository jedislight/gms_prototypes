var all_done = true;
with(HubPortal){
	all_done = all_done and Game.get_flag(room_get_name(target_room));
}

if (not all_done) {
	instance_destroy(id);
} else {	
	target_room = LevelF1;
	if (Game.get_flag(room_get_name(LevelF1))) {
		target_room = LevelF2;	
	}
	if (Game.get_flag(room_get_name(LevelF2))) {
		target_room = LevelF3;	
	}
	if (Game.get_flag(room_get_name(LevelF3))) {
		target_room = LevelF4;
	}
	if (Game.get_flag(room_get_name(LevelF4))) {
		target_room = Credits;
		image_blend = c_white;
	}
}