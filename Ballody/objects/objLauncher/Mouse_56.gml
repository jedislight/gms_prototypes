/// @description Insert description here
// You can write your code in this editor

if(launching){
	launching = false;
	var b = instance_create_depth(x,y, depth, objBall);
	with(b) {
		var to_mouse = point_direction(x,y, mouse_x, mouse_y);
		var impulse = 3;
		var ix = lengthdir_x(impulse, to_mouse)
		var iy = lengthdir_y(impulse, to_mouse)
		physics_apply_impulse(phy_com_x, phy_com_y, ix, iy)	
	}
}