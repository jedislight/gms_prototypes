// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Rect() constructor{
	x1=0
	x2=0
	y1=0
	y2=0
	any_set = false;
	
	function grow_to_contain(xx, yy) {
		if(any_set == false) {
			x1=xx;
			x2=xx;
			y1=yy;
			y2=yy;
		} else {
			x1 = min(xx, x1);
			x2 = max(xx, x2);
			y1 = min(yy, y1);
			y2 = max(yy, y2);
		}
		
		any_set = true;
	}
}