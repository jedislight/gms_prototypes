// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function InputState() constructor{
	down = false;
	pressed = false;
	released = false;
	
	function add_key(k) {
		down = down || keyboard_check(k);
		pressed = pressed || keyboard_check_pressed(k);
		released = released || keyboard_check_released(k);
	}
	
	function add_button(b) {
		down = down || gamepad_button_check(0, b);	
		pressed = pressed || gamepad_button_check_pressed(0, b);
		released = released || gamepad_button_check_released(0, b);
	}
	
	function add_mouse(m) {
		down = down || mouse_check_button(m);
		pressed = pressed || mouse_check_button_pressed(m);
		released = released || mouse_check_button_released(m);		
	}
	
	function merge(o) {
		down = down || o.down;
		pressed = pressed || o.pressed;
		released = released && o.released;
	}
	
	function clear() {
		down = false;
		pressed = false;
		released = false;
	}
}