// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function ArmorNullDamageListener(): MutableEventListener() constructor {
	function listen(context){
		var armor_element = context.target.items[? "armor"];
		if( armor_element == "null" and context.value > 0){
			context.add -= 2;
		}
	}
	function needs_cleanup() { return false };
}
mutable_events_add_listener(new ArmorNullDamageListener(), "damage");


function ArmorFireDamageListener(): MutableEventListener() constructor {
	function listen(context){
		var armor_element = context.target.items[? "armor"];
		if( armor_element == "fire" and context.value > 0){
			context.add -= 1;
			if(array_contains(context.tags, "fire")) {
				context.add -= 2;	
			}
			if(array_contains(context.tags, "water")) {
				context.add += 2;	
			}
		}
	}
	function needs_cleanup() { return false };
}
mutable_events_add_listener(new ArmorFireDamageListener(), "damage");


function ArmorWaterDamageListener(): MutableEventListener() constructor {
	function listen(context){
		var armor_element = context.target.items[? "armor"];
		if( armor_element == "water" and context.value > 0){
			context.add -= 1;
			if(array_contains(context.tags, "water")) {
				context.add -= 2;	
			}
			if(array_contains(context.tags, "earth")) {
				context.add += 2;	
			}
		}
	}
	function needs_cleanup() { return false };
}
mutable_events_add_listener(new ArmorWaterDamageListener(), "damage");

function ArmorEarthDamageListener(): MutableEventListener() constructor {
	function listen(context){
		var armor_element = context.target.items[? "armor"];
		if( armor_element == "earth" and context.value > 0){
			context.add -= 1;
			if(array_contains(context.tags, "earth")) {
				context.add -= 2;	
			}
			if(array_contains(context.tags, "fire")) {
				context.add += 2;	
			}
		}
	}
	function needs_cleanup() { return false };
}
mutable_events_add_listener(new ArmorEarthDamageListener(), "damage");


function ArmorGhostDamageListener(): MutableEventListener() constructor {
	function listen(context){
		var armor_element = context.target.items[? "armor"];
		if( armor_element == "ghost" and context.value > 0){
			if(not array_contains(context.tags, "dark")) {
				context.zero = true;
				context.target.x += choose(64,128,-64,-128);
				context.target.y += choose(64,128,-64,-128);
			}
			else {
				context.zero = true;
				context.target.transform_to_wisp();
			}
		}
	}
	function needs_cleanup() { return false };
}
mutable_events_add_listener(new ArmorGhostDamageListener(), "damage");

function ArmorDarkDamageListener(): MutableEventListener() constructor {
	function listen(context){
		var armor_element = context.target.items[? "armor"];
		if( armor_element == "dark" and context.value > 0){
			if(array_contains(context.tags, "dark")) {
				// pass
			} else if (array_contains(context.tags, "light")){
				contex.mul *= 2;
			} else {
				context.zero = true;
			}
		}
	}
	function needs_cleanup() { return false };
}
mutable_events_add_listener(new ArmorDarkDamageListener(), "damage");