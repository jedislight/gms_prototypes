// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
 // map < string, array >

function mutable_events_initalize() {
	if (not variable_global_exists("mutable_event_listeners")) {
		global.mutable_event_listeners = ds_map_create();		
	}
}

function MutableEventListener() constructor {
	function listen(context){}
	function needs_cleanup() { return false };
}

function mutable_events_add_listener(listener, event) {
	mutable_events_initalize()
	var event_listener_array = global.mutable_event_listeners[? event];
	log(string("LISTENER ADD {0} {1}", event, listener))

	if(is_undefined(event_listener_array)) {
		event_listener_array = array_create(1, listener);
		global.mutable_event_listeners[? event] = event_listener_array;
		log(string("New mutable event type registered: " + event));
		return;
	} else {
		array_push(event_listener_array, listener);
		return;
	}
}

function mutable_events_cleanup() {
	mutable_events_initalize()
	for(var event = ds_map_find_first(global.mutable_event_listeners); not is_undefined(event); event = ds_map_find_next(global.mutable_event_listeners, event)) {
		var event_listener_array = global.mutable_event_listeners[? event];
		for (var i = 0; i < array_length(event_listener_array); ++i) {
			var listener = event_listener_array[i];
			if( listener.needs_cleanup() ) {
				array_delete(event_listener_array, i, 1);
				--i;
				log(string("LISTENER CLEAN {0} {1}", event, listener))
			}
		}	
	}
}

function mutable_events_broadcast(event, context) {
	mutable_events_initalize()
	log(string("LISTENER EVENT {0}", event))
	var event_listener_array = global.mutable_event_listeners[? event];
	var len = array_length(event_listener_array); // don't follow new adds
	for (var i = 0; i < len; ++i) {
			var listener = event_listener_array[i];
			if(not listener.needs_cleanup()){
				log(string("LISTENER TRIGGER {0} {1}", event, listener))
				listener.listen(context);
			}
		}
}