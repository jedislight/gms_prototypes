// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function placeholder_find(label_){
	with(Placeholder){
		if(label == label_) return id;
	}
	
	return noone;
}
function transform_tiles(xx, yy, r, transform_tile_metadata_key, transform_callback, randomize_v, randomize_h, randomize_r) {
	if(is_undefined(randomize_h)) randomize_h = false;
	if(is_undefined(randomize_v)) randomize_v = false;
	if(is_undefined(randomize_r)) randomize_r = false;
	var ctx = (xx div 64) * 64;
	var cty = (yy div 64) * 64;
	var tr = (r div 64) * 64 + 64;
	var layers = layer_get_all();
	var any = false;
	var need_mp_update = false;
	var invalidation_rect = new Rect();
	invalidation_rect.grow_to_contain(xx,yy);
	for (var i = 0; i < array_length(layers); ++i) {
	    var i_layer = layers[i];
		var tile_map = layer_tilemap_get_id(i_layer);
		if(tile_map == -1) continue;
		var tile_set = tilemap_get_tileset(tile_map);
		
		for(var itx = ctx - tr; itx <= ctx+tr; itx += 64) for(var ity = cty - tr; ity <= cty+tr; ity += 64) {
			var d = point_distance(itx, ity, ctx, cty);
			if(d < r){
				var tile_data = tilemap_get_at_pixel(tile_map, itx, ity);
				var i_tile_index = tile_get_index(tile_data);
				var transform_to = MotionPlanning.get_tile_metadata(tile_set, i_tile_index, transform_tile_metadata_key, -1);
				if(is_array(transform_to)){
					transform_to = transform_to[irandom_range(0, array_length(transform_to)-1)];
				}
				if(transform_to != -1) {
					var was_solid = MotionPlanning.get_tile_metadata(tile_set, i_tile_index, "solid", false);
					var is_now_solid = MotionPlanning.get_tile_metadata(tile_set, transform_to, "solid", false);
					var was_impassible = MotionPlanning.get_tile_metadata(tile_set, i_tile_index, "impassible", false);
					var is_now_impassible = MotionPlanning.get_tile_metadata(tile_set, transform_to, "impassible", false);
					tile_data = tile_set_index(tile_data, transform_to);
					tile_data = tile_set_flip(tile_data, choose(randomize_v, false));
					tile_data = tile_set_mirror(tile_data, choose(randomize_h, false));
					tile_data = tile_set_rotate(tile_data, choose(randomize_r, false));
					tilemap_set_at_pixel(tile_map,tile_data, itx, ity)	
					any = true;
					invalidation_rect.grow_to_contain(itx, ity);
					if(was_solid != is_now_solid) need_mp_update = true;
					if(was_impassible != is_now_impassible) need_mp_update = true;
					if(not is_undefined(transform_callback)) {
						transform_callback(itx, ity, tile_data)
					}
				}
			}
		}
	}
	
	if(need_mp_update){		
		MotionPlanning.invalidate_radius(xx, yy, r);	
		MotionPlanning.invalidate_rect(invalidation_rect.x1, invalidation_rect.y1, invalidation_rect.x2, invalidation_rect.y2);	
	}
	
	return any;
}

function burn_tiles(xx, yy, r) {
	var any_transformed = transform_tiles(xx, yy ,r, "burns_to", function(tx, ty, td){
		instance_create_layer(tx, ty, "Instances", FxAnimateOnce, {sprite_index:SpriteFlames});
	})
	if(any_transformed and !audio_is_playing(SoundFireWoosh)) {
		audio_play_sound(SoundFireWoosh, 10, false);	
	}
}

function grow_tiles(xx, yy, r) { 
	transform_tiles(xx , yy , r, "grow_to", undefined, false, true, false)	
}

function wet_tiles(xx, yy, r) { 
	transform_tiles(xx , yy , r, "wet_to", undefined, true, true, true)	
}

function draw_health(xx, yy, amount, capacity, scale) {
	if(is_undefined(scale)) scale = 1.0;
	var cursor = 0;
	while(cursor < capacity){
		var fill = clamp(amount, 0,4);
		amount -= 4;
		var sub = 4-fill;
		draw_sprite_ext(SpriteHealthUI, sub, xx, yy, scale, scale, 0, c_white, 1.0);
	    xx += 64;
		cursor += 4
	}
}

function draw_mana(xx, yy, amount, capacity, recharge_percent_int, scale) {
	if(is_undefined(scale)) scale = 1.0;
	var cursor = 0;
	var left = xx;
	var xx_last_full = xx;
	while(cursor < capacity){
		var sub = 0;
		if(cursor >= amount){
			sub = 1;
		} else {
			xx_last_full = xx + sprite_get_width(SpriteManaUI)*1.25;
		}
		
		draw_sprite_ext(SpriteManaUI, sub, xx, yy, scale, scale, 0, c_white, 1.0);
	    xx += sprite_get_width(SpriteManaUI) * 1.25;
		cursor += 1
	}
	draw_healthbar(xx_last_full, yy, xx, yy+2, recharge_percent_int, c_black, c_blue, c_blue, 1, false, false);
}

function draw_stamina(xx, yy, amount, capacity, recharge_percent_int, scale) {
	if(is_undefined(scale)) scale = 1.0;
	var cursor = 0;
	var left = xx;
	var xx_last_full = xx+(sprite_get_width(SpriteStaminaUI)*1.25);
	while(cursor < capacity){
		var sub = 0;
		if(cursor >= amount){
			sub = 1;
		} else {
			xx_last_full = xx + (sprite_get_width(SpriteStaminaUI)*1.25*2);
		}
		
		draw_sprite_ext(SpriteStaminaUI, sub, xx, yy, scale, scale, 0, c_white, 1.0);
	    xx += sprite_get_width(SpriteStaminaUI) * 1.25;
		cursor += 1
	}
	draw_healthbar(left, yy, xx_last_full, yy+2, recharge_percent_int, c_black, c_yellow, c_yellow, 0, false, false);
}