// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function array_contains(array, item) {
	return array_index_of(array, item) != -1;
}

function array_index_of(array, item) {
	for (var i = 0; i < array_length(array); ++i) {
	    if(array[i] == item) return i;
	}
	return -1;
}

function instance_is_primary(inst){
	return instance_find(id.object_index, 0) == inst.id;
}


function log(msg) {
	if(not variable_global_exists("log_buffer")) {
		global.log_buffer = [];
		global.log_buffer_capacity = 500;		
	}
	show_debug_message(msg);
	array_push(global.log_buffer, msg);
	if(array_length(global.log_buffer) > global.log_buffer_capacity * 2){
		array_delete(global.log_buffer, 0, global.log_buffer_capacity);	
	}
}