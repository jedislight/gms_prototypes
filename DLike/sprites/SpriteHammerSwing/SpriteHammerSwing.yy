{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "SpriteHammerSwing",
  "bbox_bottom": 330,
  "bbox_left": -50,
  "bbox_right": 387,
  "bbox_top": 2,
  "bboxMode": 2,
  "collisionKind": 2,
  "collisionTolerance": 0,
  "DynamicTexturePage": false,
  "edgeFiltering": false,
  "For3D": false,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"bb4f14c7-a5b4-4826-922f-493652e3bb2a",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ecdb9b84-fbbc-4f8a-8256-a33f77f86046",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"08fc7033-20cb-48ce-a094-f52dc4b11a36",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"1364ac45-67d2-4d3e-b7b8-6647a3af0d10",},
  ],
  "gridX": 0,
  "gridY": 0,
  "height": 256,
  "HTile": false,
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"65213b86-0cf3-4f65-ba26-f14727ca6992","blendMode":0,"displayName":"default","isLocked":false,"opacity":100.0,"visible":true,},
  ],
  "nineSlice": null,
  "origin": 9,
  "parent": {
    "name": "Items",
    "path": "folders/Sprites/Items.yy",
  },
  "preMultiplyAlpha": false,
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "SpriteHammerSwing",
    "autoRecord": true,
    "backdropHeight": 768,
    "backdropImageOpacity": 0.5,
    "backdropImagePath": "",
    "backdropWidth": 1366,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "events": {"resourceType":"KeyframeStore<MessageEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "eventStubScript": null,
    "eventToFunction": {},
    "length": 4.0,
    "lockOrigin": false,
    "moments": {"resourceType":"KeyframeStore<MomentsEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "playback": 1,
    "playbackSpeed": 7.0,
    "playbackSpeedType": 0,
    "showBackdrop": true,
    "showBackdropImage": false,
    "timeUnits": 1,
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"1.0","Keyframes":[
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"bb4f14c7-a5b4-4826-922f-493652e3bb2a","path":"sprites/SpriteHammerSwing/SpriteHammerSwing.yy",},},},"Disabled":false,"id":"0bf3e5f8-7bcf-44a6-b7de-67ff106e923c","IsCreationKey":false,"Key":0.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"ecdb9b84-fbbc-4f8a-8256-a33f77f86046","path":"sprites/SpriteHammerSwing/SpriteHammerSwing.yy",},},},"Disabled":false,"id":"29bc8dda-fff1-4be7-8ad4-a86beba5d394","IsCreationKey":false,"Key":1.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"08fc7033-20cb-48ce-a094-f52dc4b11a36","path":"sprites/SpriteHammerSwing/SpriteHammerSwing.yy",},},},"Disabled":false,"id":"b26f55b9-0ace-44ff-b3e9-6fd86f231dbb","IsCreationKey":false,"Key":2.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"1364ac45-67d2-4d3e-b7b8-6647a3af0d10","path":"sprites/SpriteHammerSwing/SpriteHammerSwing.yy",},},},"Disabled":false,"id":"52242ea3-703a-41b5-943b-1d5e33b8a6b6","IsCreationKey":false,"Key":3.0,"Length":1.0,"Stretch":false,},
          ],},"modifiers":[],"spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange": null,
    "volume": 1.0,
    "xorigin": 105,
    "yorigin": 131,
  },
  "swatchColours": null,
  "swfPrecision": 2.525,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "type": 0,
  "VTile": false,
  "width": 256,
}