{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "SpriteSlimeAttackLeft",
  "bbox_bottom": 55,
  "bbox_left": 0,
  "bbox_right": 53,
  "bbox_top": 0,
  "bboxMode": 0,
  "collisionKind": 1,
  "collisionTolerance": 0,
  "DynamicTexturePage": false,
  "edgeFiltering": false,
  "For3D": false,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"860db8a8-618d-4c7d-94a5-509734610883",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"8e432f0f-333d-45f9-8344-9464f3ee2dd1",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"7a1f87a9-8f14-46d9-8ac1-e73eb0921a26",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"7ed2f1a3-9857-45e6-b6cf-fdd707e43444",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"9599b9f4-5e76-4848-8c5e-1712f129cb63",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"5826674d-70c0-4c26-8150-6b2125ad60e8",},
  ],
  "gridX": 0,
  "gridY": 0,
  "height": 56,
  "HTile": false,
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"e9abc3e9-6f56-4dd5-a15a-91b839b2a382","blendMode":0,"displayName":"default","isLocked":false,"opacity":100.0,"visible":true,},
  ],
  "nineSlice": null,
  "origin": 9,
  "parent": {
    "name": "Characters",
    "path": "folders/Sprites/Characters.yy",
  },
  "preMultiplyAlpha": false,
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "SpriteSlimeAttackLeft",
    "autoRecord": true,
    "backdropHeight": 768,
    "backdropImageOpacity": 0.5,
    "backdropImagePath": "",
    "backdropWidth": 1366,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "events": {"resourceType":"KeyframeStore<MessageEventKeyframe>","resourceVersion":"1.0","Keyframes":[
        {"resourceType":"Keyframe<MessageEventKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"MessageEventKeyframe","resourceVersion":"1.0","Events":[
                "impact",
              ],},},"Disabled":false,"id":"3a5e7c87-dfe8-442d-8ac6-2db1a1e3677b","IsCreationKey":false,"Key":4.0,"Length":1.0,"Stretch":false,},
      ],},
    "eventStubScript": null,
    "eventToFunction": {},
    "length": 6.0,
    "lockOrigin": false,
    "moments": {"resourceType":"KeyframeStore<MomentsEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "playback": 1,
    "playbackSpeed": 6.0,
    "playbackSpeedType": 0,
    "showBackdrop": true,
    "showBackdropImage": false,
    "timeUnits": 1,
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"1.0","Keyframes":[
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"860db8a8-618d-4c7d-94a5-509734610883","path":"sprites/SpriteSlimeAttackLeft/SpriteSlimeAttackLeft.yy",},},},"Disabled":false,"id":"7c7d1478-0a01-48a9-8c89-6bd91d06b1c9","IsCreationKey":false,"Key":0.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"8e432f0f-333d-45f9-8344-9464f3ee2dd1","path":"sprites/SpriteSlimeAttackLeft/SpriteSlimeAttackLeft.yy",},},},"Disabled":false,"id":"01cf5e2c-4bc9-4c35-a08b-c7935d9a2845","IsCreationKey":false,"Key":1.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"7a1f87a9-8f14-46d9-8ac1-e73eb0921a26","path":"sprites/SpriteSlimeAttackLeft/SpriteSlimeAttackLeft.yy",},},},"Disabled":false,"id":"d6215339-9586-47b7-937b-38a590d8761a","IsCreationKey":false,"Key":2.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"7ed2f1a3-9857-45e6-b6cf-fdd707e43444","path":"sprites/SpriteSlimeAttackLeft/SpriteSlimeAttackLeft.yy",},},},"Disabled":false,"id":"7c0dc08c-c079-4085-b2f2-00667be7b1e6","IsCreationKey":false,"Key":3.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"9599b9f4-5e76-4848-8c5e-1712f129cb63","path":"sprites/SpriteSlimeAttackLeft/SpriteSlimeAttackLeft.yy",},},},"Disabled":false,"id":"632b5556-8253-4067-9480-d7024849110e","IsCreationKey":false,"Key":4.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"5826674d-70c0-4c26-8150-6b2125ad60e8","path":"sprites/SpriteSlimeAttackLeft/SpriteSlimeAttackLeft.yy",},},},"Disabled":false,"id":"5971b149-0a6b-4e89-8059-d38f9cd086a7","IsCreationKey":false,"Key":5.0,"Length":1.0,"Stretch":false,},
          ],},"modifiers":[],"spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange": null,
    "volume": 1.0,
    "xorigin": 29,
    "yorigin": 32,
  },
  "swatchColours": null,
  "swfPrecision": 2.525,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "type": 0,
  "VTile": false,
  "width": 54,
}