{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "SpriteDropShadow64",
  "bbox_bottom": 63,
  "bbox_left": 0,
  "bbox_right": 63,
  "bbox_top": 0,
  "bboxMode": 0,
  "collisionKind": 1,
  "collisionTolerance": 0,
  "DynamicTexturePage": false,
  "edgeFiltering": false,
  "For3D": false,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"f21df33d-936f-4a87-979a-699232fa7703",},
  ],
  "gridX": 0,
  "gridY": 0,
  "height": 64,
  "HTile": false,
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"cc794a47-3c8f-4714-a204-4f61ee897aa7","blendMode":0,"displayName":"default","isLocked":false,"opacity":100.0,"visible":true,},
  ],
  "nineSlice": null,
  "origin": 4,
  "parent": {
    "name": "Characters",
    "path": "folders/Sprites/Characters.yy",
  },
  "preMultiplyAlpha": false,
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "SpriteDropShadow64",
    "autoRecord": true,
    "backdropHeight": 768,
    "backdropImageOpacity": 0.5,
    "backdropImagePath": "",
    "backdropWidth": 1366,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "events": {"resourceType":"KeyframeStore<MessageEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "eventStubScript": null,
    "eventToFunction": {},
    "length": 1.0,
    "lockOrigin": false,
    "moments": {"resourceType":"KeyframeStore<MomentsEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "showBackdrop": true,
    "showBackdropImage": false,
    "timeUnits": 1,
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"1.0","Keyframes":[
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"f21df33d-936f-4a87-979a-699232fa7703","path":"sprites/SpriteDropShadow64/SpriteDropShadow64.yy",},},},"Disabled":false,"id":"81d2119f-22cb-4e2b-b676-55220e051c1d","IsCreationKey":false,"Key":0.0,"Length":1.0,"Stretch":false,},
          ],},"modifiers":[],"spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange": null,
    "volume": 1.0,
    "xorigin": 32,
    "yorigin": 32,
  },
  "swatchColours": null,
  "swfPrecision": 2.525,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "type": 0,
  "VTile": false,
  "width": 64,
}