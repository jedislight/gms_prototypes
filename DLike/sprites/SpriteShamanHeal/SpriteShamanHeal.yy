{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "SpriteShamanHeal",
  "bbox_bottom": 55,
  "bbox_left": 0,
  "bbox_right": 61,
  "bbox_top": 0,
  "bboxMode": 0,
  "collisionKind": 1,
  "collisionTolerance": 0,
  "DynamicTexturePage": false,
  "edgeFiltering": false,
  "For3D": false,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"267bad4b-23d2-4dcb-82d9-e083372ca03e",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"d55fc5c7-e426-48de-9972-d5efb51ae6a5",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"9381073f-074f-49e7-8e68-a0bdb7607621",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"5bdced95-4f82-4bd1-9ec3-c701a011209d",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"e2ef1cc7-5495-45e0-b7ee-1810c06e3aec",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c0a83848-caf5-4f98-b5a2-48fe6556b8d3",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"a0e65fe8-dcb0-40e5-9fb2-db8ecddce7d1",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ae7abe8e-6667-420b-9ef2-c17883c42a90",},
  ],
  "gridX": 0,
  "gridY": 0,
  "height": 64,
  "HTile": false,
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"a2742358-70fe-4eac-a258-e6f8581a861a","blendMode":0,"displayName":"default","isLocked":false,"opacity":100.0,"visible":true,},
  ],
  "nineSlice": null,
  "origin": 9,
  "parent": {
    "name": "FX",
    "path": "folders/Sprites/FX.yy",
  },
  "preMultiplyAlpha": false,
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "SpriteShamanHeal",
    "autoRecord": true,
    "backdropHeight": 768,
    "backdropImageOpacity": 0.5,
    "backdropImagePath": "",
    "backdropWidth": 1366,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "events": {"resourceType":"KeyframeStore<MessageEventKeyframe>","resourceVersion":"1.0","Keyframes":[
        {"resourceType":"Keyframe<MessageEventKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"MessageEventKeyframe","resourceVersion":"1.0","Events":[
                "trigger",
              ],},},"Disabled":false,"id":"2f2bc4b5-39cd-4826-8be6-de82ca5a9d21","IsCreationKey":false,"Key":7.0,"Length":1.0,"Stretch":false,},
      ],},
    "eventStubScript": null,
    "eventToFunction": {},
    "length": 8.0,
    "lockOrigin": false,
    "moments": {"resourceType":"KeyframeStore<MomentsEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "playback": 1,
    "playbackSpeed": 20.0,
    "playbackSpeedType": 0,
    "showBackdrop": true,
    "showBackdropImage": false,
    "timeUnits": 1,
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"1.0","Keyframes":[
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"267bad4b-23d2-4dcb-82d9-e083372ca03e","path":"sprites/SpriteShamanHeal/SpriteShamanHeal.yy",},},},"Disabled":false,"id":"b87f3974-5faf-47a8-b974-fa0a47dde723","IsCreationKey":false,"Key":0.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"d55fc5c7-e426-48de-9972-d5efb51ae6a5","path":"sprites/SpriteShamanHeal/SpriteShamanHeal.yy",},},},"Disabled":false,"id":"5df183ba-2380-4cb7-8dfe-cd64bb20f9bc","IsCreationKey":false,"Key":1.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"9381073f-074f-49e7-8e68-a0bdb7607621","path":"sprites/SpriteShamanHeal/SpriteShamanHeal.yy",},},},"Disabled":false,"id":"fc82f037-a794-4694-b1af-44039fcfd360","IsCreationKey":false,"Key":2.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"5bdced95-4f82-4bd1-9ec3-c701a011209d","path":"sprites/SpriteShamanHeal/SpriteShamanHeal.yy",},},},"Disabled":false,"id":"05c1064e-47d0-4f4c-b7e9-4378fffafece","IsCreationKey":false,"Key":3.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"e2ef1cc7-5495-45e0-b7ee-1810c06e3aec","path":"sprites/SpriteShamanHeal/SpriteShamanHeal.yy",},},},"Disabled":false,"id":"7bfa1657-1fb6-45b3-84c3-0d259a164d29","IsCreationKey":false,"Key":4.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"c0a83848-caf5-4f98-b5a2-48fe6556b8d3","path":"sprites/SpriteShamanHeal/SpriteShamanHeal.yy",},},},"Disabled":false,"id":"03d642ae-1647-47f6-a70d-dbc977884855","IsCreationKey":false,"Key":5.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"a0e65fe8-dcb0-40e5-9fb2-db8ecddce7d1","path":"sprites/SpriteShamanHeal/SpriteShamanHeal.yy",},},},"Disabled":false,"id":"a6b54a4c-8128-4925-a2d0-3a7f53bdf665","IsCreationKey":false,"Key":6.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"ae7abe8e-6667-420b-9ef2-c17883c42a90","path":"sprites/SpriteShamanHeal/SpriteShamanHeal.yy",},},},"Disabled":false,"id":"2b176694-6774-478f-8825-62985298f32c","IsCreationKey":false,"Key":7.0,"Length":1.0,"Stretch":false,},
          ],},"modifiers":[],"spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange": null,
    "volume": 1.0,
    "xorigin": 36,
    "yorigin": 58,
  },
  "swatchColours": null,
  "swfPrecision": 2.525,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "type": 0,
  "VTile": false,
  "width": 64,
}