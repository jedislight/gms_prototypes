{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "SpriteSlimeLeft",
  "bbox_bottom": 39,
  "bbox_left": 0,
  "bbox_right": 54,
  "bbox_top": 0,
  "bboxMode": 0,
  "collisionKind": 1,
  "collisionTolerance": 0,
  "DynamicTexturePage": false,
  "edgeFiltering": false,
  "For3D": false,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"26f5055b-84de-4006-b822-f0321a0f2006",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"a14d894c-0290-4b14-8a02-36fd5a8c5598",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"38501366-f6f3-45f4-b18f-6bfef4db8b37",},
  ],
  "gridX": 0,
  "gridY": 0,
  "height": 40,
  "HTile": false,
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"e9abc3e9-6f56-4dd5-a15a-91b839b2a382","blendMode":0,"displayName":"default","isLocked":false,"opacity":100.0,"visible":true,},
  ],
  "nineSlice": null,
  "origin": 9,
  "parent": {
    "name": "Characters",
    "path": "folders/Sprites/Characters.yy",
  },
  "preMultiplyAlpha": false,
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "SpriteSlimeLeft",
    "autoRecord": true,
    "backdropHeight": 768,
    "backdropImageOpacity": 0.5,
    "backdropImagePath": "",
    "backdropWidth": 1366,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "events": {"resourceType":"KeyframeStore<MessageEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "eventStubScript": null,
    "eventToFunction": {},
    "length": 3.0,
    "lockOrigin": false,
    "moments": {"resourceType":"KeyframeStore<MomentsEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "showBackdrop": true,
    "showBackdropImage": false,
    "timeUnits": 1,
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"1.0","Keyframes":[
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"26f5055b-84de-4006-b822-f0321a0f2006","path":"sprites/SpriteSlimeLeft/SpriteSlimeLeft.yy",},},},"Disabled":false,"id":"0dabc958-b82b-41dc-b38a-57792d24ee70","IsCreationKey":false,"Key":0.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"a14d894c-0290-4b14-8a02-36fd5a8c5598","path":"sprites/SpriteSlimeLeft/SpriteSlimeLeft.yy",},},},"Disabled":false,"id":"95d5cac8-3516-4502-8915-7b359d0a73cc","IsCreationKey":false,"Key":1.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"38501366-f6f3-45f4-b18f-6bfef4db8b37","path":"sprites/SpriteSlimeLeft/SpriteSlimeLeft.yy",},},},"Disabled":false,"id":"04e6cd8b-b52f-4342-891b-4f9f311deab6","IsCreationKey":false,"Key":2.0,"Length":1.0,"Stretch":false,},
          ],},"modifiers":[],"spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange": null,
    "volume": 1.0,
    "xorigin": 30,
    "yorigin": 16,
  },
  "swatchColours": null,
  "swfPrecision": 2.525,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "type": 0,
  "VTile": false,
  "width": 55,
}