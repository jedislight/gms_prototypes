{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "SpriteGhostDown",
  "bbox_bottom": 74,
  "bbox_left": 17,
  "bbox_right": 72,
  "bbox_top": 1,
  "bboxMode": 0,
  "collisionKind": 1,
  "collisionTolerance": 0,
  "DynamicTexturePage": false,
  "edgeFiltering": false,
  "For3D": false,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"e3f3324d-8e4e-426d-9409-6c15f3e82ff2",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"51e82f78-9b62-4c06-80fe-6eb33d40a913",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"25cf087f-d460-4f0d-944b-be0b68e5c719",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"c124a3e5-7aef-41b6-915c-c39519d5ea04",},
  ],
  "gridX": 0,
  "gridY": 0,
  "height": 92,
  "HTile": false,
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"28c43cb0-430c-4f4f-92cd-3cbf6a34f4f8","blendMode":0,"displayName":"default","isLocked":false,"opacity":100.0,"visible":true,},
  ],
  "nineSlice": null,
  "origin": 7,
  "parent": {
    "name": "Characters",
    "path": "folders/Sprites/Characters.yy",
  },
  "preMultiplyAlpha": false,
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "SpriteGhostDown",
    "autoRecord": true,
    "backdropHeight": 768,
    "backdropImageOpacity": 0.5,
    "backdropImagePath": "",
    "backdropWidth": 1366,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "events": {"resourceType":"KeyframeStore<MessageEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "eventStubScript": null,
    "eventToFunction": {},
    "length": 4.0,
    "lockOrigin": false,
    "moments": {"resourceType":"KeyframeStore<MomentsEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "playback": 1,
    "playbackSpeed": 4.0,
    "playbackSpeedType": 0,
    "showBackdrop": true,
    "showBackdropImage": false,
    "timeUnits": 1,
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"1.0","Keyframes":[
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"e3f3324d-8e4e-426d-9409-6c15f3e82ff2","path":"sprites/SpriteGhostDown/SpriteGhostDown.yy",},},},"Disabled":false,"id":"6c8360ff-949f-4e48-b79b-4a0893604df2","IsCreationKey":false,"Key":0.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"51e82f78-9b62-4c06-80fe-6eb33d40a913","path":"sprites/SpriteGhostDown/SpriteGhostDown.yy",},},},"Disabled":false,"id":"20b8c9d0-8c93-44fe-9a3c-d69c673dca84","IsCreationKey":false,"Key":1.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"25cf087f-d460-4f0d-944b-be0b68e5c719","path":"sprites/SpriteGhostDown/SpriteGhostDown.yy",},},},"Disabled":false,"id":"e0bfafc7-d32d-4e72-a862-55ba9570e051","IsCreationKey":false,"Key":2.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"c124a3e5-7aef-41b6-915c-c39519d5ea04","path":"sprites/SpriteGhostDown/SpriteGhostDown.yy",},},},"Disabled":false,"id":"ac9c2327-0b37-458c-a2e8-5786d72e6981","IsCreationKey":false,"Key":3.0,"Length":1.0,"Stretch":false,},
          ],},"modifiers":[],"spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange": null,
    "volume": 1.0,
    "xorigin": 46,
    "yorigin": 92,
  },
  "swatchColours": null,
  "swfPrecision": 2.525,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "type": 0,
  "VTile": false,
  "width": 92,
}