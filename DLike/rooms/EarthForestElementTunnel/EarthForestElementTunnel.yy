{
  "resourceType": "GMRoom",
  "resourceVersion": "1.0",
  "name": "EarthForestElementTunnel",
  "creationCodeFile": "",
  "inheritCode": false,
  "inheritCreationOrder": false,
  "inheritLayers": false,
  "instanceCreationOrder": [
    {"name":"inst_F0CED96","path":"rooms/EarthForestElementTunnel/EarthForestElementTunnel.yy",},
    {"name":"inst_49A16CD5","path":"rooms/EarthForestElementTunnel/EarthForestElementTunnel.yy",},
    {"name":"inst_4049920F","path":"rooms/EarthForestElementTunnel/EarthForestElementTunnel.yy",},
    {"name":"inst_6F5A363A","path":"rooms/EarthForestElementTunnel/EarthForestElementTunnel.yy",},
  ],
  "isDnd": false,
  "layers": [
    {"resourceType":"GMREffectLayer","resourceVersion":"1.0","name":"Effect_2","depth":0,"effectEnabled":true,"effectType":"_filter_vignette","gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_VignetteEdges","type":0,"value":"0.75",},
        {"name":"g_VignetteEdges","type":0,"value":"0.9",},
        {"name":"g_VignetteSharpness","type":0,"value":"2",},
        {"name":"g_VignetteTexture","type":2,"value":"_filter_vignette_texture",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMREffectLayer","resourceVersion":"1.0","name":"Effect_1","depth":100,"effectEnabled":true,"effectType":"_filter_tintfilter","gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_TintCol","type":1,"value":"#FFFF6D6D",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMRInstanceLayer","resourceVersion":"1.0","name":"Instances","depth":200,"effectEnabled":true,"effectType":"_filter_outline","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_F0CED96","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_anchor","path":"objects/RoomTransition/RoomTransition.yy",},"value":"2",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_room","path":"objects/RoomTransition/RoomTransition.yy",},"value":"EarthForestElementRoom",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1728.0,"y":1536.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_49A16CD5","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_room","path":"objects/RoomTransition/RoomTransition.yy",},"value":"EarthForestHub",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_anchor","path":"objects/RoomTransition/RoomTransition.yy",},"value":"0",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":960.0,"y":1536.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_4049920F","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"propertyId":{"name":"anchor_index","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"value":"1",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1088.0,"y":1536.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_6F5A363A","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"propertyId":{"name":"anchor_index","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"value":"0",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1600.0,"y":1536.0,},
      ],"layers":[],"properties":[
        {"name":"g_OutlineColour","type":1,"value":"#FF000000",},
        {"name":"g_OutlineRadius","type":0,"value":"2",},
        {"name":"g_OutlinePixelScale","type":0,"value":"1",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_2","depth":300,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-818,-2147483648,1,268435492,-4,-2147483648,3,36,-2147483648,1342177316,-35,-2147483648,3,268435494,-2147483648,1073741860,-38,-2147483648,3,268435492,-2147483648,1073741862,-37,-2147483648,1,0,-31,-2147483648,1,1879048570,-4,-2147483648,1,268435492,-4,-2147483648,1,36,-3,-2147483648,
1,1073742202,-32,-2147483648,1,38,-3,-2147483648,1,1073741860,-34,-2147483648,1,36,-4,-2147483648,1,0,-38,-2147483648,1,36,-77,-2147483648,1,0,-39,-2147483648,1,0,-38,-2147483648,1,0,-338,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_1","depth":400,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-733,2,-16,1073741826,-24,2,-5,1073741826,1,1073742276,-7,1073742244,3,1073742212,1073741826,1073741826,-24,2,-4,1073741826,2,1073742276,1073742211,-7,1073742245,3,1073742213,1073741826,1073741826,-24,2,-3,1073741826,2,1073742276,1073742211,-8,1073742245,3,1073742213,
1073741826,1073741826,-25,2,-2,1073741826,1,1073742277,-9,1073742245,3,1073742213,1073741826,1073741826,-26,2,2,1073742244,1073742211,-9,1073742245,2,1073742243,1073742244,-27,2,-13,1073742245,-27,2,2,1073742246,1073742210,-9,1073742245,2,1073742242,1073742246,-25,2,
4,1073741826,2,1073741826,1073742277,-9,1073742245,2,1073742213,1073741826,-25,2,-3,1073741826,2,1073742278,1073742210,-8,1073742245,3,1073742213,1073741826,1073741826,-24,2,-4,1073741826,1,1073742278,-8,1073742246,3,1073742214,1073741826,1073741826,-24,2,-16,1073741826,-411,
2,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRBackgroundLayer","resourceVersion":"1.0","name":"Background","animationFPS":30.0,"animationSpeedType":0,"colour":4294967295,"depth":500,"effectEnabled":true,"effectType":"_filter_pixelate","gridX":32,"gridY":32,"hierarchyFrozen":false,"hspeed":0.0,"htiled":true,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_CellSize","type":0,"value":"8",},
      ],"spriteId":{"name":"download","path":"sprites/download/download.yy",},"stretch":true,"userdefinedAnimFPS":false,"userdefinedDepth":false,"visible":true,"vspeed":0.0,"vtiled":true,"x":0,"y":0,},
  ],
  "parent": {
    "name": "EarthForest",
    "path": "folders/Rooms/EarthForest.yy",
  },
  "parentRoom": null,
  "physicsSettings": {
    "inheritPhysicsSettings": false,
    "PhysicsWorld": false,
    "PhysicsWorldGravityX": 0.0,
    "PhysicsWorldGravityY": 10.0,
    "PhysicsWorldPixToMetres": 0.1,
  },
  "roomSettings": {
    "Height": 2500,
    "inheritRoomSettings": false,
    "persistent": false,
    "Width": 2500,
  },
  "sequenceId": null,
  "tags": [
    "ROOM_BUILDING",
  ],
  "views": [
    {"hborder":888,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":{"name":"Player","path":"objects/Player/Player.yy",},"vborder":888,"visible":true,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
  ],
  "viewSettings": {
    "clearDisplayBuffer": true,
    "clearViewBackground": false,
    "enableViews": true,
    "inheritViewSettings": false,
  },
  "volume": 1.0,
}