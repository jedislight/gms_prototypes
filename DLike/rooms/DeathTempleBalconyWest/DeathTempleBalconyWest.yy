{
  "resourceType": "GMRoom",
  "resourceVersion": "1.0",
  "name": "DeathTempleBalconyWest",
  "creationCodeFile": "",
  "inheritCode": false,
  "inheritCreationOrder": false,
  "inheritLayers": false,
  "instanceCreationOrder": [
    {"name":"inst_4FA612AF_1_1","path":"rooms/DeathTempleBalconyWest/DeathTempleBalconyWest.yy",},
    {"name":"inst_3211B5AC_1","path":"rooms/DeathTempleBalconyWest/DeathTempleBalconyWest.yy",},
    {"name":"inst_430DA93_1","path":"rooms/DeathTempleBalconyWest/DeathTempleBalconyWest.yy",},
    {"name":"inst_6451604D_1","path":"rooms/DeathTempleBalconyWest/DeathTempleBalconyWest.yy",},
    {"name":"inst_1456978E","path":"rooms/DeathTempleBalconyWest/DeathTempleBalconyWest.yy",},
    {"name":"inst_7383DE4F","path":"rooms/DeathTempleBalconyWest/DeathTempleBalconyWest.yy",},
    {"name":"inst_30C3D143","path":"rooms/DeathTempleBalconyWest/DeathTempleBalconyWest.yy",},
  ],
  "isDnd": false,
  "layers": [
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Wall_Decorations_3","depth":0,"effectEnabled":true,"effectType":"_filter_tintfilter","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_TintCol","type":1,"value":"#FF7F6559",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-326,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-19,-2147483648,-20,0,
-20,-2147483648,-17,0,3,86,0,0,-20,-2147483648,-20,0,-20,-2147483648,-20,0,-349,-2147483648,-3,0,-37,-2147483648,3,0,-2147483648,0,-379,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Canopy","depth":100,"effectEnabled":true,"effectType":"_filter_tintfilter","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_TintCol","type":1,"value":"#FF9C877C",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-492,-2147483648,-11,0,-29,-2147483648,-11,0,-29,-2147483648,-11,0,-29,-2147483648,-11,0,-29,-2147483648,-11,0,-29,-2147483648,-11,0,-29,-2147483648,-11,0,-29,-2147483648,-11,0,-29,-2147483648,-5,0,1,289,-3,0,
2,289,0,-29,-2147483648,-11,0,-29,-2147483648,-11,0,-29,-2147483648,-11,0,-29,-2147483648,-5,0,1,256,-5,0,-29,-2147483648,-5,0,1,288,-3,0,2,289,0,-29,-2147483648,-11,0,-29,-2147483648,
-11,0,-29,-2147483648,-11,0,-457,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Canopy_Walls_Decorations","depth":200,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF33190F",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-1178,-2147483648,3,123,124,125,-37,-2147483648,3,136,137,138,-379,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Canopy_Walls","depth":300,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF33190F",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-453,-2147483648,-15,0,-25,-2147483648,-15,0,-25,-2147483648,-15,0,-25,-2147483648,-15,0,-25,-2147483648,-15,0,-25,-2147483648,-12,0,1,26,-14,27,-13,-2147483648,-12,0,3,52,53,53,-12,40,-25,-2147483648,
3,65,147,75,-12,40,-28,-2147483648,-12,40,-28,-2147483648,-12,40,-28,-2147483648,-12,40,-28,-2147483648,-12,40,-25,-2147483648,3,26,27,27,-12,40,-25,-2147483648,1,39,-14,40,-12,-2147483648,2,26,
28,-11,-2147483648,1,39,-14,40,-12,-2147483648,2,39,41,-11,-2147483648,1,39,-14,40,-12,-2147483648,2,39,41,-11,-2147483648,1,39,-14,40,-12,-2147483648,2,39,41,-11,-2147483648,1,39,-14,40,
-12,-2147483648,2,39,42,-4,27,3,28,0,26,-4,27,1,43,-14,40,-12,-2147483648,1,39,-5,40,3,41,137,39,-19,40,-12,-2147483648,1,39,-27,40,-12,-2147483648,1,39,-27,
40,-12,-2147483648,1,39,-27,40,-12,-2147483648,1,39,-27,40,-12,-2147483648,1,39,-27,40,-12,-2147483648,1,39,-27,40,-12,-2147483648,1,39,-27,40,-12,-2147483648,1,39,-27,40,-12,-2147483648,1,
39,-27,40,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMREffectLayer","resourceVersion":"1.0","name":"Effect_Fog_Floor","depth":400,"effectEnabled":true,"effectType":"_filter_fractal_noise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_FractalNoiseScale","type":0,"value":"512",},
        {"name":"g_FractalNoisePersistence","type":0,"value":"0.3",},
        {"name":"g_FractalNoiseOffset","type":0,"value":"0",},
        {"name":"g_FractalNoiseOffset","type":0,"value":"0",},
        {"name":"g_FractalNoiseSpeed","type":0,"value":"0.25",},
        {"name":"g_FractalNoiseTintColour","type":1,"value":"#FFCCCCCC",},
        {"name":"g_FractalNoiseTexture","type":2,"value":"_filter_fractal_noise_texture",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMRInstanceLayer","resourceVersion":"1.0","name":"Instances","depth":500,"effectEnabled":true,"effectType":"_filter_outline","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_4FA612AF_1_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"MusicRequester","path":"objects/MusicRequester/MusicRequester.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"MusicRequester","path":"objects/MusicRequester/MusicRequester.yy",},"propertyId":{"name":"music","path":"objects/MusicRequester/MusicRequester.yy",},"value":"MusicDeathTemple",},
          ],"rotation":0.0,"scaleX":3.0,"scaleY":3.0,"x":256.0,"y":256.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_3211B5AC_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"propertyId":{"name":"anchor_index","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"value":"3",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1216.0,"y":1792.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_430DA93_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"Sign","path":"objects/Sign/Sign.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1728.0,"y":1472.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_6451604D_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_anchor","path":"objects/RoomTransition/RoomTransition.yy",},"value":"0",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_room","path":"objects/RoomTransition/RoomTransition.yy",},"value":"Room1",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1216.0,"y":1920.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_1456978E","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1600.0,"y":1344.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_7383DE4F","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_anchor","path":"objects/RoomTransition/RoomTransition.yy",},"value":"0",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_room","path":"objects/RoomTransition/RoomTransition.yy",},"value":"Room1",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":3.0,"x":1728.0,"y":1280.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_30C3D143","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"Sign","path":"objects/Sign/Sign.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1152.0,"y":1856.0,},
      ],"layers":[],"properties":[
        {"name":"g_OutlineColour","type":1,"value":"#FF000000",},
        {"name":"g_OutlineRadius","type":0,"value":"2",},
        {"name":"g_OutlinePixelScale","type":0,"value":"1",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Wall_Decorations_2","depth":600,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF332A0F",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-326,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-19,-2147483648,-5,0,
-35,-2147483648,-5,0,-35,-2147483648,-5,0,-35,-2147483648,-5,0,-786,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Wall_Decoarations","depth":700,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF33190F",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-326,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-16,-2147483648,-24,0,-19,-2147483648,-6,0,
-34,-2147483648,-6,0,-10,-2147483648,3,33,143,75,-21,-2147483648,-6,0,-10,-2147483648,2,46,156,-22,-2147483648,-6,0,-34,-2147483648,-6,0,-323,-2147483648,-3,0,-37,-2147483648,-3,0,-379,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Walls","depth":800,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF33190F",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-439,0,1,40,-39,0,1,40,-39,0,1,40,-39,0,1,40,-39,0,1,40,-39,0,1,40,-25,0,1,26,-14,27,-14,0,-11,-2147483648,3,52,53,53,-12,40,
-14,0,-11,-2147483648,3,65,66,66,-12,40,-14,0,-11,-2147483648,3,78,82,82,-12,40,-14,0,-13,-2147483648,1,0,-12,40,-14,0,-13,-2147483648,1,0,-12,40,-14,0,-13,-2147483648,
1,0,-12,40,-14,0,-11,-2147483648,3,26,27,27,-12,40,-14,0,-11,-2147483648,1,39,-14,40,-12,0,2,26,28,-11,-2147483648,1,39,-14,40,-12,0,2,39,41,-11,-2147483648,
1,39,-14,40,-12,0,2,39,41,-11,-2147483648,1,39,-14,40,-12,0,2,39,41,-11,-2147483648,1,39,-14,40,-12,0,2,39,42,-4,27,3,28,0,26,-4,27,1,
43,-14,40,-12,0,1,39,-5,40,3,41,137,39,-19,40,-12,0,1,39,-27,40,-12,0,1,39,-27,40,-12,0,1,39,-27,40,-12,0,1,39,-27,40,-12,
0,1,39,-27,40,-12,0,1,39,-27,40,-12,0,1,39,-27,40,-12,0,1,39,-27,40,-12,0,1,39,-27,40,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Floor_Decorations_Ext","depth":900,"effectEnabled":true,"effectType":"_filter_tintfilter","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_TintCol","type":1,"value":"#FF9C877C",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-327,-2147483648,-26,0,-14,-2147483648,-26,0,-14,-2147483648,-26,0,-14,-2147483648,-26,0,-14,-2147483648,-26,0,-14,-2147483648,-26,0,-14,-2147483648,-26,0,-14,-2147483648,-26,0,-14,-2147483648,-26,0,-14,-2147483648,-26,0,
-14,-2147483648,-5,0,1,304,-12,305,-8,0,-14,-2147483648,-5,0,2,336,64,-11,129,-8,0,-16,-2147483648,-3,0,3,336,99,0,-34,-2147483648,-3,0,6,336,99,0,-2147483648,-2147483648,320,
-3,-2147483648,1,320,-27,-2147483648,-3,0,5,336,99,0,-2147483648,-2147483648,-5,0,-27,-2147483648,-3,0,3,336,99,0,-36,-2147483648,3,304,431,99,-37,-2147483648,1,336,-39,-2147483648,1,336,-5,-2147483648,
1,320,-3,-2147483648,1,320,-29,-2147483648,6,336,-2147483648,-2147483648,307,308,309,-5,-2147483648,3,307,308,309,-26,-2147483648,6,336,-2147483648,-2147483648,339,340,341,-5,-2147483648,3,339,340,341,-26,-2147483648,6,336,
-2147483648,-2147483648,371,372,373,-5,-2147483648,3,371,372,373,-26,-2147483648,1,336,-39,-2147483648,1,336,-39,-2147483648,1,336,-39,-2147483648,1,336,-39,-2147483648,1,336,-39,-2147483648,1,336,-39,-2147483648,1,336,-39,
-2147483648,1,336,-39,-2147483648,1,336,-68,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Floor_Decorations","depth":1000,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF7F325D",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-814,0,1,155,-11,0,1,16,-27,0,3,155,0,16,-37,0,1,155,-4,0,1,16,-5,0,1,13,-28,0,1,16,-6,0,1,15,-73,0,1,2,-8,0,
1,1879048282,-38,0,1,16,-37,0,1,15,-35,0,1,2,-4,0,1,13,-76,0,2,168,15,-379,0,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Floor","depth":1100,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF7F325D",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-669,0,-11,155,-29,0,-11,155,-29,0,-11,155,-13,0,-12,155,-4,0,-11,155,-13,0,-27,155,-13,0,-27,155,-13,0,-27,155,-13,0,-27,155,-13,0,-27,155,
-12,0,-28,155,-12,0,-28,155,-12,0,-2,155,-3,40,-5,155,-3,40,-15,155,-12,0,-2,155,3,40,155,40,-5,155,3,40,155,40,-15,155,-12,0,-28,155,
-11,0,-29,155,-11,0,-29,155,-11,0,-29,155,-11,0,-29,155,-11,0,-29,155,-11,0,-29,155,-11,0,-29,155,-11,0,-29,155,-11,0,-69,155,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMREffectLayer","resourceVersion":"1.0","name":"Effect_Fog_Distance","depth":1200,"effectEnabled":true,"effectType":"_filter_fractal_noise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_FractalNoiseScale","type":0,"value":"512",},
        {"name":"g_FractalNoisePersistence","type":0,"value":"0.3",},
        {"name":"g_FractalNoiseOffset","type":0,"value":"256",},
        {"name":"g_FractalNoiseOffset","type":0,"value":"344",},
        {"name":"g_FractalNoiseSpeed","type":0,"value":"0.25",},
        {"name":"g_FractalNoiseTintColour","type":1,"value":"#FFCCCCCC",},
        {"name":"g_FractalNoiseTexture","type":2,"value":"_filter_fractal_noise_texture",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Sky_Decorations","depth":1300,"effectEnabled":true,"effectType":null,"gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-532,-2147483648,1,464,-1067,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetExtras","path":"tilesets/TileSetExtras/TileSetExtras.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Sky","depth":1400,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FFFF0000",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-39,340,1,-2147483648,-39,340,1,-2147483648,-39,340,1,-2147483648,-39,340,1,-2147483648,-39,340,1,-2147483648,-39,340,1,-2147483648,-39,340,1,-2147483648,-39,340,1,-2147483648,-39,340,1,-2147483648,-39,340,1,-2147483648,
-39,340,1,-2147483648,-39,340,1,-2147483648,-39,340,1,-2147483648,-39,340,1,-2147483648,-39,340,-1001,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Distance_Decorations","depth":1500,"effectEnabled":true,"effectType":"_filter_gradient","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_GradientColour1","type":1,"value":"#6100FFFF",},
        {"name":"g_GradientColour2","type":1,"value":"#00FF0000",},
        {"name":"g_GradientPosition1","type":0,"value":"0.45",},
        {"name":"g_GradientPosition1","type":0,"value":"0.5",},
        {"name":"g_GradientPosition2","type":0,"value":"1.025",},
        {"name":"g_GradientPosition2","type":0,"value":"0.9",},
        {"name":"g_GradientMode","type":0,"value":"1",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-600,-2147483648,6,268435465,219,219,268435465,219,9,-3,219,15,215,536870913,805306369,536870913,215,215,536870913,215,215,1,215,215,536870913,215,805306369,-3,215,3,536870913,215,805306369,-9,215,11,-2147483648,219,9,219,
805306377,219,219,268435465,805306377,219,268435465,-30,-2147483648,-3,219,7,9,268435465,9,219,268435465,219,215,-30,-2147483648,4,219,9,219,268435465,-4,219,-32,-2147483648,6,268435465,219,268435465,219,268435465,268435465,-34,-2147483648,4,
219,268435465,219,219,-36,-2147483648,3,9,219,219,-37,-2147483648,2,219,805306377,-38,-2147483648,2,268435465,219,-38,-2147483648,1,219,-39,-2147483648,1,805306377,-599,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetExtras","path":"tilesets/TileSetExtras/TileSetExtras.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Distance","depth":1600,"effectEnabled":true,"effectType":"_filter_greyscale","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"0.6",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-600,-2147483648,-50,340,-20,421,-10,-2147483648,-9,340,-2,421,-16,1,-3,421,-10,-2147483648,-8,340,-2,421,-2,1,-14,-2147483648,-3,1,1,421,-10,-2147483648,-6,340,-3,421,-3,1,-16,-2147483648,
-2,1,-10,-2147483648,-4,340,-3,421,-5,1,-17,-2147483648,1,1,-10,-2147483648,-3,340,-2,421,-7,1,-17,-2147483648,1,1,-10,-2147483648,-2,340,-2,421,-8,1,-17,-2147483648,1,1,-10,-2147483648,
-2,340,1,421,-9,1,-17,-2147483648,1,1,-10,-2147483648,3,340,421,421,-8,1,-29,-2147483648,2,340,421,-9,1,-29,-2147483648,-2,421,-9,1,-29,-2147483648,1,421,-10,1,-29,-2147483648,-11,
1,-29,-2147483648,-11,1,-29,-2147483648,-11,1,-29,-2147483648,-11,1,-29,-2147483648,-11,1,-29,-2147483648,-11,1,-29,-2147483648,-11,1,-29,-2147483648,-11,1,-29,-2147483648,-11,1,-29,-2147483648,-11,1,-29,-2147483648,-11,
1,-69,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRBackgroundLayer","resourceVersion":"1.0","name":"Background","animationFPS":4.0,"animationSpeedType":0,"colour":4294967295,"depth":1700,"effectEnabled":true,"effectType":"none","gridX":32,"gridY":32,"hierarchyFrozen":false,"hspeed":0.0,"htiled":true,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"spriteId":{"name":"fire_trap_dungeon_idea","path":"sprites/fire_trap_dungeon_idea/fire_trap_dungeon_idea.yy",},"stretch":false,"userdefinedAnimFPS":false,"userdefinedDepth":false,"visible":true,"vspeed":0.0,"vtiled":true,"x":0,"y":0,},
  ],
  "parent": {
    "name": "DeathTemple",
    "path": "folders/Rooms/DeathTemple.yy",
  },
  "parentRoom": null,
  "physicsSettings": {
    "inheritPhysicsSettings": false,
    "PhysicsWorld": false,
    "PhysicsWorldGravityX": 0.0,
    "PhysicsWorldGravityY": 10.0,
    "PhysicsWorldPixToMetres": 0.1,
  },
  "roomSettings": {
    "Height": 2500,
    "inheritRoomSettings": false,
    "persistent": false,
    "Width": 2500,
  },
  "sequenceId": null,
  "tags": [
    "ROOM_BUILDING",
  ],
  "views": [
    {"hborder":888,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":{"name":"Player","path":"objects/Player/Player.yy",},"vborder":888,"visible":true,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
  ],
  "viewSettings": {
    "clearDisplayBuffer": true,
    "clearViewBackground": false,
    "enableViews": true,
    "inheritViewSettings": false,
  },
  "volume": 1.0,
}