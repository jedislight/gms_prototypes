{
  "resourceType": "GMRoom",
  "resourceVersion": "1.0",
  "name": "DeathTempleExteriror",
  "creationCodeFile": "",
  "inheritCode": false,
  "inheritCreationOrder": false,
  "inheritLayers": false,
  "instanceCreationOrder": [
    {"name":"inst_21E602CE","path":"rooms/DeathTempleExteriror/DeathTempleExteriror.yy",},
    {"name":"inst_7FEB39D3","path":"rooms/DeathTempleExteriror/DeathTempleExteriror.yy",},
    {"name":"inst_7BD2521C","path":"rooms/DeathTempleExteriror/DeathTempleExteriror.yy",},
    {"name":"inst_4334BA91","path":"rooms/DeathTempleExteriror/DeathTempleExteriror.yy",},
    {"name":"inst_26B62F1C","path":"rooms/DeathTempleExteriror/DeathTempleExteriror.yy",},
    {"name":"inst_E301E6E","path":"rooms/DeathTempleExteriror/DeathTempleExteriror.yy",},
  ],
  "isDnd": false,
  "layers": [
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Canopy","depth":0,"effectEnabled":true,"effectType":null,"gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"tiles":{"SerialiseHeight":18,"SerialiseWidth":22,"TileCompressedData":[
-272,0,1,256,-21,0,1,288,-3,0,1,289,-97,0,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRInstanceLayer","resourceVersion":"1.0","name":"Instances","depth":100,"effectEnabled":true,"effectType":"_filter_outline","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_21E602CE","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"BossDoor","path":"objects/BossDoor/BossDoor.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"BossDoor","path":"objects/BossDoor/BossDoor.yy",},"propertyId":{"name":"blocked_sprite","path":"objects/BossDoor/BossDoor.yy",},"value":"SpriteDeathKey",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"BossDoor","path":"objects/BossDoor/BossDoor.yy",},"propertyId":{"name":"progression_flag","path":"objects/BossDoor/BossDoor.yy",},"value":"death_key",},
          ],"rotation":0.0,"scaleX":3.0,"scaleY":3.0,"x":576.0,"y":768.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_7FEB39D3","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":636.0,"y":1037.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_7BD2521C","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[],"rotation":0.0,"scaleX":19.0,"scaleY":0.359375,"x":64.0,"y":1129.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_4334BA91","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"MusicRequester","path":"objects/MusicRequester/MusicRequester.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"MusicRequester","path":"objects/MusicRequester/MusicRequester.yy",},"propertyId":{"name":"music","path":"objects/MusicRequester/MusicRequester.yy",},"value":"MusicDeathTemple",},
          ],"rotation":0.0,"scaleX":4.0,"scaleY":3.0,"x":0.0,"y":-192.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_26B62F1C","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"propertyId":{"name":"anchor_index","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"value":"1",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":640.0,"y":704.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_E301E6E","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_anchor","path":"objects/RoomTransition/RoomTransition.yy",},"value":"3",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_room","path":"objects/RoomTransition/RoomTransition.yy",},"value":"DeathTempleEntrance",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":640.0,"y":576.0,},
      ],"layers":[],"properties":[
        {"name":"g_OutlineColour","type":1,"value":"#FF000000",},
        {"name":"g_OutlineRadius","type":0,"value":"2",},
        {"name":"g_OutlinePixelScale","type":0,"value":"1",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Ground_Decorations","depth":200,"effectEnabled":true,"effectType":null,"gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"tiles":{"SerialiseHeight":18,"SerialiseWidth":22,"TileCompressedData":[
-179,0,1,35,-12,0,2,268435778,35,-5,0,4,64,0,0,321,-13,0,1,67,-4,0,
3,99,35,35,-3,0,1,322,-3,0,1,321,-5,0,2,35,99,-4,0,1,99,-13,
0,4,322,0,0,99,-4,0,1,99,-7,35,-3,0,-6,35,1,99,-4,0,2,99,
322,-10,0,1,322,-4,0,2,99,268435777,-3,0,1,128,-6,129,1,320,-3,0,1,320,
-5,129,1,97,-4,0,1,322,-23,0,1,321,-11,0,1,321,-23,0,1,322,-4,0,
],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles__Ground","depth":300,"effectEnabled":true,"effectType":null,"gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"tiles":{"SerialiseHeight":18,"SerialiseWidth":22,"TileCompressedData":[
-74,0,5,68,69,70,71,72,-14,0,11,68,69,70,101,101,102,103,103,102,71,72,
-11,0,11,100,101,102,101,101,102,103,103,102,103,104,-9,0,14,304,305,100,101,102,
101,101,102,103,103,102,103,104,306,-7,0,17,304,431,105,100,101,102,101,101,102,103,
103,102,103,104,430,305,306,-3,0,7,304,305,431,105,105,132,133,-7,134,9,135,136,
105,105,430,306,0,0,336,-4,105,1,164,-4,165,1,166,-4,167,1,168,-3,105,4,
338,0,0,336,-4,105,1,196,-4,197,1,198,-4,199,1,200,-3,105,4,338,0,0,
336,-18,105,4,338,0,0,336,-18,105,4,430,306,0,336,-19,105,3,338,0,336,-19,
105,3,338,0,336,-19,105,3,338,0,336,-19,105,3,338,0,336,-19,105,2,338,0,
],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Sky_Decorations","depth":400,"effectEnabled":true,"effectType":null,"gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"tiles":{"SerialiseHeight":18,"SerialiseWidth":22,"TileCompressedData":[
-25,-2147483648,1,464,-6,-2147483648,1,0,-363,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetExtras","path":"tilesets/TileSetExtras/TileSetExtras.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Sky","depth":500,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FFFF0000",},
      ],"tiles":{"SerialiseHeight":18,"SerialiseWidth":22,"TileCompressedData":[
-66,340,-59,-2147483648,1,2,-11,-2147483648,1,2,-9,-2147483648,1,2,-11,-2147483648,1,2,-9,-2147483648,1,2,
-11,-2147483648,1,2,-9,-2147483648,1,2,-11,-2147483648,1,2,-9,-2147483648,1,2,-11,-2147483648,-5,2,1,-2147483648,
-5,2,-160,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMREffectLayer","resourceVersion":"1.0","name":"Effect_1","depth":600,"effectEnabled":true,"effectType":"_filter_fractal_noise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_FractalNoiseScale","type":0,"value":"512",},
        {"name":"g_FractalNoisePersistence","type":0,"value":"0.3",},
        {"name":"g_FractalNoiseOffset","type":0,"value":"0",},
        {"name":"g_FractalNoiseOffset","type":0,"value":"0",},
        {"name":"g_FractalNoiseSpeed","type":0,"value":"0.25",},
        {"name":"g_FractalNoiseTintColour","type":1,"value":"#FFCCCCCC",},
        {"name":"g_FractalNoiseTexture","type":2,"value":"_filter_fractal_noise_texture",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Distance_Decorations_2","depth":700,"effectEnabled":true,"effectType":"_filter_gradient","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_GradientColour1","type":1,"value":"#6100FFFF",},
        {"name":"g_GradientColour2","type":1,"value":"#00FF0000",},
        {"name":"g_GradientPosition1","type":0,"value":"0.18",},
        {"name":"g_GradientPosition1","type":0,"value":"0.25",},
        {"name":"g_GradientPosition2","type":0,"value":"0.45",},
        {"name":"g_GradientPosition2","type":0,"value":"0.475",},
        {"name":"g_GradientMode","type":0,"value":"1",},
      ],"tiles":{"SerialiseHeight":18,"SerialiseWidth":22,"TileCompressedData":[
-32,-2147483648,1,0,-33,-2147483648,15,215,536870913,805306369,536870913,215,215,536870913,215,215,1,215,215,536870913,215,805306369,
-3,215,5,536870913,215,805306369,536870913,211,-20,-2147483648,-2,215,-285,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetExtras","path":"tilesets/TileSetExtras/TileSetExtras.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Distance","depth":800,"effectEnabled":true,"effectType":"_filter_greyscale","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"0.6",},
      ],"tiles":{"SerialiseHeight":18,"SerialiseWidth":22,"TileCompressedData":[
-66,-2147483648,-13,340,-10,0,-20,421,3,0,-2147483648,421,-6,1,-2,-2147483648,-8,1,-5,421,-2,1,
-14,-2147483648,-3,1,-2,421,-2,1,-17,-2147483648,-3,1,-20,-2147483648,-2,1,-20,-2147483648,-2,1,-20,-2147483648,
-2,1,-20,-2147483648,-2,1,-21,-2147483648,1,1,-21,-2147483648,1,1,-21,-2147483648,1,1,-21,-2147483648,1,1,
-21,-2147483648,1,1,-21,-2147483648,1,1,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRBackgroundLayer","resourceVersion":"1.0","name":"Background","animationFPS":4.0,"animationSpeedType":0,"colour":4294967295,"depth":900,"effectEnabled":true,"effectType":"_filter_pixelate","gridX":32,"gridY":32,"hierarchyFrozen":false,"hspeed":0.0,"htiled":true,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_CellSize","type":0,"value":"8",},
      ],"spriteId":{"name":"rough_map","path":"sprites/rough_map/rough_map.yy",},"stretch":false,"userdefinedAnimFPS":false,"userdefinedDepth":false,"visible":true,"vspeed":0.0,"vtiled":true,"x":-100,"y":0,},
  ],
  "parent": {
    "name": "DeathTemple",
    "path": "folders/Rooms/DeathTemple.yy",
  },
  "parentRoom": null,
  "physicsSettings": {
    "inheritPhysicsSettings": false,
    "PhysicsWorld": false,
    "PhysicsWorldGravityX": 0.0,
    "PhysicsWorldGravityY": 10.0,
    "PhysicsWorldPixToMetres": 0.1,
  },
  "roomSettings": {
    "Height": 1152,
    "inheritRoomSettings": false,
    "persistent": false,
    "Width": 1366,
  },
  "sequenceId": null,
  "tags": [
    "ROOM_BUILDING",
  ],
  "views": [
    {"hborder":888,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":{"name":"Player","path":"objects/Player/Player.yy",},"vborder":888,"visible":true,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
  ],
  "viewSettings": {
    "clearDisplayBuffer": true,
    "clearViewBackground": false,
    "enableViews": true,
    "inheritViewSettings": false,
  },
  "volume": 1.0,
}