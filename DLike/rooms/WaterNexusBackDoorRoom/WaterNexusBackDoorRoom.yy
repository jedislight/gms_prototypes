{
  "resourceType": "GMRoom",
  "resourceVersion": "1.0",
  "name": "WaterNexusBackDoorRoom",
  "creationCodeFile": "",
  "inheritCode": false,
  "inheritCreationOrder": false,
  "inheritLayers": false,
  "instanceCreationOrder": [
    {"name":"inst_7E07533A_1","path":"rooms/WaterNexusBackDoorRoom/WaterNexusBackDoorRoom.yy",},
    {"name":"inst_712A0FF5","path":"rooms/WaterNexusBackDoorRoom/WaterNexusBackDoorRoom.yy",},
    {"name":"inst_4787B12","path":"rooms/WaterNexusBackDoorRoom/WaterNexusBackDoorRoom.yy",},
    {"name":"inst_23782425","path":"rooms/WaterNexusBackDoorRoom/WaterNexusBackDoorRoom.yy",},
    {"name":"inst_6D32893A","path":"rooms/WaterNexusBackDoorRoom/WaterNexusBackDoorRoom.yy",},
    {"name":"inst_550411F9","path":"rooms/WaterNexusBackDoorRoom/WaterNexusBackDoorRoom.yy",},
    {"name":"inst_FD48608","path":"rooms/WaterNexusBackDoorRoom/WaterNexusBackDoorRoom.yy",},
    {"name":"inst_2144B9AF","path":"rooms/WaterNexusBackDoorRoom/WaterNexusBackDoorRoom.yy",},
  ],
  "isDnd": false,
  "layers": [
    {"resourceType":"GMRInstanceLayer","resourceVersion":"1.0","name":"Instances","depth":0,"effectEnabled":true,"effectType":"_filter_outline","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_7E07533A_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"MusicRequester","path":"objects/MusicRequester/MusicRequester.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"MusicRequester","path":"objects/MusicRequester/MusicRequester.yy",},"propertyId":{"name":"music","path":"objects/MusicRequester/MusicRequester.yy",},"value":"MusicMystical",},
          ],"rotation":0.0,"scaleX":4.75,"scaleY":4.75,"x":0.0,"y":-448.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_712A0FF5","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"Checkpoint","path":"objects/Checkpoint/Checkpoint.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":128.0,"y":320.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_4787B12","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_anchor","path":"objects/RoomTransition/RoomTransition.yy",},"value":"10",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_room","path":"objects/RoomTransition/RoomTransition.yy",},"value":"LakeRoom",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":640.0,"y":0.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_23782425","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_room","path":"objects/RoomTransition/RoomTransition.yy",},"value":"WaterNexusMainRoom",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_anchor","path":"objects/RoomTransition/RoomTransition.yy",},"value":"1",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":448.0,"y":0.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_6D32893A","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_room","path":"objects/RoomTransition/RoomTransition.yy",},"value":"WaterNexusBossRoom",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_anchor","path":"objects/RoomTransition/RoomTransition.yy",},"value":"0",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":320.0,"y":576.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_550411F9","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":640.0,"y":128.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_FD48608","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"propertyId":{"name":"anchor_index","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"value":"1",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":448.0,"y":128.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_2144B9AF","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"propertyId":{"name":"anchor_index","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"value":"2",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":320.0,"y":448.0,},
      ],"layers":[],"properties":[
        {"name":"g_OutlineColour","type":1,"value":"#FF000000",},
        {"name":"g_OutlineRadius","type":0,"value":"2",},
        {"name":"g_OutlinePixelScale","type":0,"value":"1",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Walls","depth":100,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FFFF0400",},
      ],"tiles":{"SerialiseHeight":10,"SerialiseWidth":12,"TileCompressedData":[
1,29,-6,53,6,0,53,30,-2147483648,40,41,-6,
66,6,0,66,39,-2147483648,40,41,-6,66,6,0,
66,39,-2147483648,40,41,-8,0,4,39,-2147483648,40,41,
-8,0,4,52,-2147483648,30,41,-8,0,4,65,-2147483648,
39,41,-8,0,4,78,-2147483648,39,41,-9,0,3,
-2147483648,39,41,-9,0,3,-2147483648,39,42,-4,27,1,
0,-5,27,1,43,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Floor_Decoratoins","depth":200,"effectEnabled":true,"effectType":null,"gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"tiles":{"SerialiseHeight":10,"SerialiseWidth":12,"TileCompressedData":[
-7,-2147483648,4,172,-2147483648,-2147483648,291,-8,-2147483648,4,172,-2147483648,
-2147483648,291,-8,-2147483648,4,172,-2147483648,-2147483648,291,-11,-2147483648,1,
291,-11,-2147483648,1,291,-11,-2147483648,1,291,-10,-2147483648,2,
40,291,-10,-2147483648,1,0,-19,-2147483648,1,172,-6,-2147483648,
],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Floor","depth":300,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF4C3426",},
      ],"tiles":{"SerialiseHeight":10,"SerialiseWidth":12,"TileCompressedData":[
-10,0,-2,-2147483648,-10,0,-2,-2147483648,-10,0,-2,-2147483648,
1,0,-8,155,4,0,-2147483648,-2147483648,0,-8,155,4,
0,-2147483648,-2147483648,0,-8,155,4,0,-2147483648,-2147483648,0,-8,
155,4,0,-2147483648,-2147483648,0,-10,155,2,-2147483648,0,-10,
155,1,-2147483648,-10,0,-2,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRBackgroundLayer","resourceVersion":"1.0","name":"Background","animationFPS":4.0,"animationSpeedType":0,"colour":4278190080,"depth":400,"effectEnabled":true,"effectType":"none","gridX":32,"gridY":32,"hierarchyFrozen":false,"hspeed":0.0,"htiled":true,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"spriteId":null,"stretch":false,"userdefinedAnimFPS":false,"userdefinedDepth":false,"visible":true,"vspeed":0.0,"vtiled":true,"x":0,"y":0,},
  ],
  "parent": {
    "name": "WaterNexus",
    "path": "folders/Rooms/WaterNexus.yy",
  },
  "parentRoom": null,
  "physicsSettings": {
    "inheritPhysicsSettings": false,
    "PhysicsWorld": false,
    "PhysicsWorldGravityX": 0.0,
    "PhysicsWorldGravityY": 10.0,
    "PhysicsWorldPixToMetres": 0.1,
  },
  "roomSettings": {
    "Height": 640,
    "inheritRoomSettings": false,
    "persistent": false,
    "Width": 768,
  },
  "sequenceId": null,
  "tags": [
    "ROOM_BUILDING",
  ],
  "views": [
    {"hborder":888,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":{"name":"Player","path":"objects/Player/Player.yy",},"vborder":888,"visible":true,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
  ],
  "viewSettings": {
    "clearDisplayBuffer": true,
    "clearViewBackground": false,
    "enableViews": true,
    "inheritViewSettings": false,
  },
  "volume": 1.0,
}