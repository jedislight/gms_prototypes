{
  "resourceType": "GMRoom",
  "resourceVersion": "1.0",
  "name": "DeathTempleEntrance",
  "creationCodeFile": "",
  "inheritCode": false,
  "inheritCreationOrder": false,
  "inheritLayers": false,
  "instanceCreationOrder": [
    {"name":"inst_4FA612AF_1","path":"rooms/DeathTempleEntrance/DeathTempleEntrance.yy",},
    {"name":"inst_3211B5AC","path":"rooms/DeathTempleEntrance/DeathTempleEntrance.yy",},
    {"name":"inst_A78E1F6","path":"rooms/DeathTempleEntrance/DeathTempleEntrance.yy",},
    {"name":"inst_A79C7E3","path":"rooms/DeathTempleEntrance/DeathTempleEntrance.yy",},
    {"name":"inst_430DA93","path":"rooms/DeathTempleEntrance/DeathTempleEntrance.yy",},
    {"name":"inst_6451604D","path":"rooms/DeathTempleEntrance/DeathTempleEntrance.yy",},
  ],
  "isDnd": false,
  "layers": [
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Falls","depth":0,"effectEnabled":true,"effectType":"_filter_large_blur","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Radius","type":0,"value":"10",},
        {"name":"g_NoiseTexture","type":2,"value":"_filter_large_blur_noise",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-575,-2147483648,1,14,-7,-2147483648,1,14,-31,-2147483648,1,322,-7,-2147483648,1,322,-511,-2147483648,1,339,-7,-2147483648,1,339,-456,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetExtras","path":"tilesets/TileSetExtras/TileSetExtras.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Canopy_Walls","depth":100,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF33190F",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-458,-2147483648,3,100,101,102,-37,-2147483648,3,113,114,115,-37,-2147483648,3,126,127,128,-637,-2147483648,3,123,124,125,-37,-2147483648,3,136,137,138,-379,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Canopy","depth":200,"effectEnabled":true,"effectType":"_filter_tintfilter","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_TintCol","type":1,"value":"#FF9C877C",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-537,-2147483648,-5,0,-35,-2147483648,-5,0,-35,-2147483648,1,256,-4,0,-35,-2147483648,1,288,-3,-2147483648,1,289,-35,-2147483648,-6,0,-34,-2147483648,-6,0,-34,-2147483648,-4,0,1,256,-35,-2147483648,1,289,
-3,0,1,288,-35,-2147483648,-5,0,-155,-2147483648,1,289,-3,-2147483648,1,289,-578,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRInstanceLayer","resourceVersion":"1.0","name":"Instances","depth":300,"effectEnabled":true,"effectType":"_filter_outline","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_4FA612AF_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"MusicRequester","path":"objects/MusicRequester/MusicRequester.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"MusicRequester","path":"objects/MusicRequester/MusicRequester.yy",},"propertyId":{"name":"music","path":"objects/MusicRequester/MusicRequester.yy",},"value":"MusicDeathTemple",},
          ],"rotation":0.0,"scaleX":3.0,"scaleY":3.0,"x":576.0,"y":576.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_3211B5AC","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"propertyId":{"name":"anchor_index","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"value":"3",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1216.0,"y":1792.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_A78E1F6","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"Sign","path":"objects/Sign/Sign.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":768.0,"y":1344.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_A79C7E3","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"Sign","path":"objects/Sign/Sign.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1216.0,"y":1024.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_430DA93","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"Sign","path":"objects/Sign/Sign.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1728.0,"y":1472.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_6451604D","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_anchor","path":"objects/RoomTransition/RoomTransition.yy",},"value":"1",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_room","path":"objects/RoomTransition/RoomTransition.yy",},"value":"DeathTempleExteriror",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1216.0,"y":1920.0,},
      ],"layers":[],"properties":[
        {"name":"g_OutlineColour","type":1,"value":"#FF000000",},
        {"name":"g_OutlineRadius","type":0,"value":"2",},
        {"name":"g_OutlinePixelScale","type":0,"value":"1",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Wall_Decorations_3","depth":400,"effectEnabled":true,"effectType":"_filter_tintfilter","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_TintCol","type":1,"value":"#FF7F6559",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-493,-2147483648,-16,0,-24,-2147483648,-16,0,-24,-2147483648,-16,0,-24,-2147483648,-16,0,-24,-2147483648,-16,0,-24,-2147483648,-16,0,-23,-2147483648,1,86,-13,0,3,86,0,0,-24,-2147483648,-16,0,-24,-2147483648,
-16,0,-771,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Wall_Decorations_2","depth":500,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF332A0F",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-534,-2147483648,3,32,-2147483648,32,-5,-2147483648,3,32,-2147483648,32,-29,-2147483648,3,45,-2147483648,45,-5,-2147483648,3,45,-2147483648,45,-1015,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Wall_Decoarations","depth":600,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF33190F",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-534,-2147483648,2,1,75,-7,-2147483648,2,75,2,-29,-2147483648,4,15,16,3,47,-3,-2147483648,4,33,15,-2147483648,15,-146,-2147483648,2,75,147,-12,-2147483648,3,33,143,75,-24,-2147483648,2,160,34,
-11,-2147483648,2,46,156,-391,-2147483648,-3,0,-37,-2147483648,-3,0,-379,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Walls","depth":700,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF33190F",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-493,40,1,29,-3,53,5,54,126,0,128,52,-3,53,1,30,-27,40,1,41,-3,66,1,67,-3,0,1,65,-3,66,1,39,-27,40,1,41,-3,66,5,80,
139,140,141,78,-3,66,1,39,-27,40,1,41,-11,-2147483648,1,39,-27,40,1,41,-11,-2147483648,1,39,-25,40,-2,53,1,54,-11,-2147483648,3,52,53,53,-23,40,-2,66,
1,67,-11,-2147483648,3,65,66,66,-23,40,-2,69,1,80,-11,-2147483648,3,78,82,82,-23,40,1,0,-15,-2147483648,1,0,-23,40,1,0,-15,-2147483648,1,0,-23,40,1,0,
-15,-2147483648,1,0,-23,40,-2,27,1,28,-11,-2147483648,3,26,27,27,-25,40,1,41,-11,-2147483648,1,39,-27,40,1,41,-11,-2147483648,1,39,-27,40,1,41,-11,-2147483648,1,39,
-27,40,1,41,-11,-2147483648,1,39,-27,40,1,41,-11,-2147483648,1,39,-27,40,1,42,-4,27,3,28,0,26,-4,27,1,43,-32,40,3,41,137,39,-379,40,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Floor_Decorations_Ext","depth":800,"effectEnabled":true,"effectType":"_filter_tintfilter","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_TintCol","type":1,"value":"#FF9C877C",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-574,-2147483648,3,307,308,309,-5,-2147483648,3,307,308,309,-29,-2147483648,3,339,340,341,-5,0,3,339,340,341,-29,-2147483648,3,371,372,373,-5,-2147483648,3,371,372,373,-32,-2147483648,1,320,
-3,-2147483648,1,320,-35,-2147483648,-5,0,3,-2147483648,0,0,-112,-2147483648,1,320,-3,-2147483648,1,320,-35,-2147483648,-5,0,-155,-2147483648,1,320,-3,-2147483648,1,320,-32,-2147483648,3,307,308,309,-5,-2147483648,
3,307,308,309,-29,-2147483648,3,339,340,341,-5,-2147483648,3,339,340,341,-29,-2147483648,3,371,372,373,-5,-2147483648,3,371,372,373,-415,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Floor_Decorations","depth":900,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF7F325D",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-613,155,6,0,2,1,3,0,0,-5,155,1,14,-29,155,1,2,-40,155,-4,0,1,14,-4,0,-31,155,1,14,-5,0,3,2,0,0,-31,155,-3,0,1,
1,-5,0,-29,155,2,16,155,-9,0,-2,155,1,16,-25,155,5,13,155,155,0,16,-7,0,-31,155,-4,0,1,16,-4,0,2,155,13,-28,155,1,16,-6,
0,3,15,0,0,-31,155,-9,0,-31,155,1,2,-8,0,1,1879048282,-30,155,-8,0,1,16,-37,155,1,15,-35,155,1,2,-4,155,1,13,-76,155,2,168,15,
-379,155,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Floor","depth":1000,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF7F325D",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-578,155,3,40,155,40,-33,155,3,40,155,40,-5,155,3,40,155,40,-29,155,-3,40,-5,155,-3,40,-429,155,-3,40,-5,155,-3,40,-29,155,3,40,155,40,
-5,155,3,40,155,40,-455,155,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRBackgroundLayer","resourceVersion":"1.0","name":"Background","animationFPS":4.0,"animationSpeedType":0,"colour":4294967295,"depth":1100,"effectEnabled":true,"effectType":"none","gridX":32,"gridY":32,"hierarchyFrozen":false,"hspeed":0.0,"htiled":true,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"spriteId":{"name":"fire_trap_dungeon_idea","path":"sprites/fire_trap_dungeon_idea/fire_trap_dungeon_idea.yy",},"stretch":false,"userdefinedAnimFPS":false,"userdefinedDepth":false,"visible":true,"vspeed":0.0,"vtiled":true,"x":0,"y":0,},
  ],
  "parent": {
    "name": "DeathTemple",
    "path": "folders/Rooms/DeathTemple.yy",
  },
  "parentRoom": null,
  "physicsSettings": {
    "inheritPhysicsSettings": false,
    "PhysicsWorld": false,
    "PhysicsWorldGravityX": 0.0,
    "PhysicsWorldGravityY": 10.0,
    "PhysicsWorldPixToMetres": 0.1,
  },
  "roomSettings": {
    "Height": 2500,
    "inheritRoomSettings": false,
    "persistent": false,
    "Width": 2500,
  },
  "sequenceId": null,
  "tags": [
    "ROOM_BUILDING",
  ],
  "views": [
    {"hborder":888,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":{"name":"Player","path":"objects/Player/Player.yy",},"vborder":888,"visible":true,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
  ],
  "viewSettings": {
    "clearDisplayBuffer": true,
    "clearViewBackground": false,
    "enableViews": true,
    "inheritViewSettings": false,
  },
  "volume": 1.0,
}