{
  "resourceType": "GMRoom",
  "resourceVersion": "1.0",
  "name": "FireNexusBossRoom",
  "creationCodeFile": "",
  "inheritCode": false,
  "inheritCreationOrder": false,
  "inheritLayers": false,
  "instanceCreationOrder": [
    {"name":"inst_46433D2E_4","path":"rooms/FireNexusBossRoom/FireNexusBossRoom.yy",},
    {"name":"inst_341EE084_4","path":"rooms/FireNexusBossRoom/FireNexusBossRoom.yy",},
    {"name":"inst_1E91D65","path":"rooms/FireNexusBossRoom/FireNexusBossRoom.yy",},
    {"name":"inst_29CE04E4","path":"rooms/FireNexusBossRoom/FireNexusBossRoom.yy",},
    {"name":"inst_7C09E162","path":"rooms/FireNexusBossRoom/FireNexusBossRoom.yy",},
    {"name":"inst_74136D97","path":"rooms/FireNexusBossRoom/FireNexusBossRoom.yy",},
  ],
  "isDnd": false,
  "layers": [
    {"resourceType":"GMRInstanceLayer","resourceVersion":"1.0","name":"Instances","depth":0,"effectEnabled":true,"effectType":"_filter_outline","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_46433D2E_4","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"propertyId":{"name":"anchor_index","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"value":"0",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1216.0,"y":1856.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_341EE084_4","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_anchor","path":"objects/RoomTransition/RoomTransition.yy",},"value":"1",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_room","path":"objects/RoomTransition/RoomTransition.yy",},"value":"FireNexusBossEntranceRoom",},
          ],"rotation":0.0,"scaleX":3.0,"scaleY":1.0,"x":1152.0,"y":1984.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_1E91D65","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"MusicRequester","path":"objects/MusicRequester/MusicRequester.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"MusicRequester","path":"objects/MusicRequester/MusicRequester.yy",},"propertyId":{"name":"music","path":"objects/MusicRequester/MusicRequester.yy",},"value":"MusicBoss",},
          ],"rotation":0.0,"scaleX":3.0,"scaleY":3.0,"x":576.0,"y":576.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_29CE04E4","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PickupHeartLarge","path":"objects/PickupHeartLarge/PickupHeartLarge.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":960.0,"y":1152.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_7C09E162","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"Sign","path":"objects/Sign/Sign.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1344.0,"y":256.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_74136D97","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"FlameSkull","path":"objects/FlameSkull/FlameSkull.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"Combatant","path":"objects/Combatant/Combatant.yy",},"propertyId":{"name":"combatant_hp","path":"objects/Combatant/Combatant.yy",},"value":"30",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"MagicSkull","path":"objects/MagicSkull/MagicSkull.yy",},"propertyId":{"name":"damage","path":"objects/MagicSkull/MagicSkull.yy",},"value":"10",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"Combatant","path":"objects/Combatant/Combatant.yy",},"propertyId":{"name":"combatant_attack_speed_mod","path":"objects/Combatant/Combatant.yy",},"value":"1.8",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"Mobile","path":"objects/Mobile/Mobile.yy",},"propertyId":{"name":"movement_speed","path":"objects/Mobile/Mobile.yy",},"value":"4",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"MagicSkull","path":"objects/MagicSkull/MagicSkull.yy",},"propertyId":{"name":"spawns","path":"objects/MagicSkull/MagicSkull.yy",},"value":"[{x:0,y:0}, {x:32,y:32}, {x:-32,y:-32},{x:-32,y:32},{x:32,y:-32}]",},
          ],"rotation":0.0,"scaleX":2.0,"scaleY":2.0,"x":1216.0,"y":1216.0,},
      ],"layers":[],"properties":[
        {"name":"g_OutlineColour","type":1,"value":"#FF000000",},
        {"name":"g_OutlineRadius","type":0,"value":"2",},
        {"name":"g_OutlinePixelScale","type":0,"value":"1",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Walls","depth":100,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF1919FF",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-13,40,1,29,-11,53,1,30,-27,40,8,41,66,66,69,66,94,95,96,-4,66,1,39,-27,40,1,41,-4,66,8,107,71,109,66,66,79,66,39,-27,40,
1,41,-11,0,1,39,-27,40,1,41,-11,0,1,39,-27,40,1,41,-11,0,1,39,-27,40,1,41,-11,0,1,39,-27,40,1,41,-11,0,1,39,-27,40,
1,41,-11,0,1,39,-27,40,1,41,-11,0,1,39,-27,40,1,41,-11,0,1,39,-27,40,1,41,-11,0,1,39,-27,40,1,41,-11,0,1,39,-27,40,
1,41,-11,0,1,39,-27,40,1,41,-11,0,1,39,-27,40,1,41,-11,-2147483648,1,39,-27,40,1,41,-11,-2147483648,1,39,-27,40,1,41,-11,-2147483648,1,39,-27,40,
1,41,-11,-2147483648,1,39,-27,40,1,41,-11,-2147483648,1,39,-27,40,1,41,-11,-2147483648,1,39,-27,40,1,41,-11,-2147483648,1,39,-27,40,1,41,-11,-2147483648,1,39,-27,40,
1,41,-11,-2147483648,1,39,-27,40,1,41,-11,-2147483648,1,39,-27,40,1,41,-11,-2147483648,1,39,-27,40,1,41,-11,-2147483648,1,39,-27,40,1,41,-11,-2147483648,1,39,-27,40,
1,41,-11,-2147483648,1,39,-27,40,1,42,-3,27,1,28,-3,-2147483648,1,26,-3,27,1,43,-31,40,1,41,-3,0,1,39,-35,40,1,41,-3,0,1,39,-338,40,
],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Lava","depth":200,"effectEnabled":true,"effectType":null,"gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-133,-2147483648,-2,268435916,10,460,536871372,460,1879048652,1342177740,268435916,805306828,536871372,460,1879048652,-28,-2147483648,12,805306828,268435916,805306828,460,268435916,268435916,460,268435916,805306828,536871372,1879048652,1342177740,-29,-2147483648,11,805306828,268435916,460,805306828,268435916,805306828,460,
1879048652,1879048652,1342177740,1342177740,-28,-2147483648,12,1073742284,805306828,268435916,1879048652,1342177740,805306828,268435916,460,1342177740,1342177740,1879048652,1073742284,-29,-2147483648,-2,1879048652,9,1342177740,1073742284,805306828,268435916,1342177740,1879048652,1073742284,1073742284,1879048652,-29,-2147483648,11,805306828,1342177740,1879048652,1342177740,
1610613196,1610613196,1879048652,1073742284,1610613196,1610613196,1879048652,-29,-2147483648,-2,1879048652,-2,1342177740,-2,1073742284,-2,1610613196,-2,1073742284,1,460,-29,-2147483648,-2,1342177740,9,1879048652,1073742284,1073742284,460,1610613196,1073742284,805306828,460,536871372,-29,-2147483648,3,460,1073742284,
1073742284,-8,460,-1135,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Floor_Holes","depth":300,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF5487FF",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-407,155,-24,0,-16,155,-24,0,-16,155,-24,0,-16,155,-24,0,-16,155,-24,0,-16,155,-24,0,-16,155,-24,0,-16,155,-24,0,-16,155,-24,0,-16,155,-24,0,
-16,155,-24,0,-16,155,-24,0,-16,155,-24,0,-16,155,-24,0,-16,155,-24,0,-16,155,-24,0,-16,155,-24,0,-16,155,-24,0,-16,155,-24,0,-69,155,1,15,
-379,155,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Floor","depth":400,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF5487FF",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-1600,155,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRBackgroundLayer","resourceVersion":"1.0","name":"Background","animationFPS":4.0,"animationSpeedType":0,"colour":4294967295,"depth":500,"effectEnabled":true,"effectType":"none","gridX":32,"gridY":32,"hierarchyFrozen":false,"hspeed":0.0,"htiled":true,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"spriteId":{"name":"fire_trap_dungeon_idea","path":"sprites/fire_trap_dungeon_idea/fire_trap_dungeon_idea.yy",},"stretch":false,"userdefinedAnimFPS":false,"userdefinedDepth":false,"visible":true,"vspeed":0.0,"vtiled":true,"x":0,"y":0,},
  ],
  "parent": {
    "name": "FireNexus",
    "path": "folders/Rooms/FireNexus.yy",
  },
  "parentRoom": null,
  "physicsSettings": {
    "inheritPhysicsSettings": false,
    "PhysicsWorld": false,
    "PhysicsWorldGravityX": 0.0,
    "PhysicsWorldGravityY": 10.0,
    "PhysicsWorldPixToMetres": 0.1,
  },
  "roomSettings": {
    "Height": 2500,
    "inheritRoomSettings": false,
    "persistent": false,
    "Width": 2500,
  },
  "sequenceId": null,
  "tags": [
    "ROOM_BUILDING",
  ],
  "views": [
    {"hborder":888,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":{"name":"Player","path":"objects/Player/Player.yy",},"vborder":888,"visible":true,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
  ],
  "viewSettings": {
    "clearDisplayBuffer": true,
    "clearViewBackground": false,
    "enableViews": true,
    "inheritViewSettings": false,
  },
  "volume": 1.0,
}