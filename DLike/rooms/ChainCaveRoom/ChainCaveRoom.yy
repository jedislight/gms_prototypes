{
  "resourceType": "GMRoom",
  "resourceVersion": "1.0",
  "name": "ChainCaveRoom",
  "creationCodeFile": "",
  "inheritCode": false,
  "inheritCreationOrder": false,
  "inheritLayers": false,
  "instanceCreationOrder": [
    {"name":"inst_28010C98_1_1_1","path":"rooms/ChainCaveRoom/ChainCaveRoom.yy",},
    {"name":"inst_5003BED0_1_1_1","path":"rooms/ChainCaveRoom/ChainCaveRoom.yy",},
    {"name":"inst_E5FEE27","path":"rooms/ChainCaveRoom/ChainCaveRoom.yy",},
    {"name":"inst_23970870","path":"rooms/ChainCaveRoom/ChainCaveRoom.yy",},
    {"name":"inst_171090B8","path":"rooms/ChainCaveRoom/ChainCaveRoom.yy",},
    {"name":"inst_19C23DA4","path":"rooms/ChainCaveRoom/ChainCaveRoom.yy",},
  ],
  "isDnd": false,
  "layers": [
    {"resourceType":"GMREffectLayer","resourceVersion":"1.0","name":"Effect_2","depth":0,"effectEnabled":true,"effectType":"_filter_vignette","gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_VignetteEdges","type":0,"value":"0.2",},
        {"name":"g_VignetteEdges","type":0,"value":"1",},
        {"name":"g_VignetteSharpness","type":0,"value":"1.26",},
        {"name":"g_VignetteTexture","type":2,"value":"_filter_vignette_texture",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMREffectLayer","resourceVersion":"1.0","name":"Effect_1","depth":100,"effectEnabled":true,"effectType":"_filter_tintfilter","gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_TintCol","type":1,"value":"#FFFFE5E5",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMRInstanceLayer","resourceVersion":"1.0","name":"Instances","depth":200,"effectEnabled":true,"effectType":"_filter_tintfilter","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_28010C98_1_1_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_room","path":"objects/RoomTransition/RoomTransition.yy",},"value":"Room1",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_anchor","path":"objects/RoomTransition/RoomTransition.yy",},"value":"1",},
          ],"rotation":-90.0,"scaleX":1.0,"scaleY":3.0,"x":640.0,"y":1024.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_5003BED0_1_1_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":512.0,"y":896.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_E5FEE27","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"Sign","path":"objects/Sign/Sign.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":640.0,"y":960.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_23970870","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"DecorSkeleton","path":"objects/DecorSkeleton/DecorSkeleton.yy",},"properties":[],"rotation":0.0,"scaleX":1.5000001,"scaleY":1.5000001,"x":384.0,"y":128.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_171090B8","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PickupChain","path":"objects/PickupChain/PickupChain.yy",},"properties":[],"rotation":0.0,"scaleX":-6.0,"scaleY":5.3243246,"x":800.0,"y":896.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_19C23DA4","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"Checkpoint","path":"objects/Checkpoint/Checkpoint.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":192.0,"y":448.0,},
      ],"layers":[],"properties":[
        {"name":"g_TintCol","type":1,"value":"#FF4772B2",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_2","depth":300,"effectEnabled":true,"effectType":"_filter_tintfilter","gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_TintCol","type":1,"value":"#FF4C6999",},
      ],"tiles":{"SerialiseHeight":19,"SerialiseWidth":19,"TileCompressedData":[
-50,-2147483648,1,536870948,-13,-2147483648,1,805306404,-15,-2147483648,1,1610612772,-23,-2147483648,1,36,-15,-2147483648,6,
38,-2147483648,1879048228,-2147483648,-2147483648,1879048228,-4,-2147483648,1,38,-7,-2147483648,1,36,-14,-2147483648,1,36,-4,
-2147483648,1,1610612772,-8,-2147483648,1,805306404,-8,-2147483648,1,1610612774,-3,-2147483648,1,1879048230,-17,-2147483648,-2,0,
4,1879048228,0,0,536870948,-5,0,-8,-2147483648,1,1610612772,-10,0,-8,-2147483648,-11,0,-8,-2147483648,
-5,0,1,66,-5,0,-8,-2147483648,-11,0,-8,-2147483648,-11,0,-8,-2147483648,-11,0,-8,
-2147483648,-11,0,-8,-2147483648,-11,0,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_1","depth":400,"effectEnabled":true,"effectType":"_filter_tintfilter","gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_TintCol","type":1,"value":"#FF19537F",},
      ],"tiles":{"SerialiseHeight":19,"SerialiseWidth":19,"TileCompressedData":[
-23,2,1,388,-9,389,1,390,-7,2,2,388,419,-9,421,2,418,390,-5,
2,2,388,419,-11,421,1,422,-5,2,1,420,-12,421,2,418,390,-3,2,
2,388,419,-12,421,6,307,308,2,2,388,419,-13,421,5,339,105,2,2,
420,-14,421,5,371,372,2,2,420,-15,421,5,422,2,2,452,387,-13,421,
2,386,454,-3,2,1,420,-13,421,1,422,-4,2,2,452,387,-12,421,1,
422,-5,2,2,452,387,-8,421,4,386,453,453,454,-6,2,4,452,453,453,
387,-3,421,3,386,453,454,-12,2,1,420,-3,421,1,422,-14,2,1,420,
-3,421,1,422,-14,2,1,420,-3,421,1,422,-46,2,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"HiddenCollision","depth":500,"effectEnabled":true,"effectType":"_filter_tintfilter","gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_TintCol","type":1,"value":"#FF19537F",},
      ],"tiles":{"SerialiseHeight":19,"SerialiseWidth":19,"TileCompressedData":[
-65,0,-2,2,4,0,2,0,2,-14,0,-5,2,-16,0,-3,2,1,0,
-3,2,-10,0,8,2,0,2,2,0,0,2,2,-10,0,-2,2,7,0,
2,2,0,0,2,2,-9,0,-2,2,-2,0,1,2,-15,0,-4,2,-178,
0,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRBackgroundLayer","resourceVersion":"1.0","name":"Background","animationFPS":30.0,"animationSpeedType":0,"colour":4278190080,"depth":600,"effectEnabled":true,"effectType":"_filter_pixelate","gridX":32,"gridY":32,"hierarchyFrozen":false,"hspeed":0.0,"htiled":true,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_CellSize","type":0,"value":"8",},
      ],"spriteId":{"name":"download","path":"sprites/download/download.yy",},"stretch":true,"userdefinedAnimFPS":false,"userdefinedDepth":false,"visible":true,"vspeed":0.0,"vtiled":true,"x":0,"y":0,},
  ],
  "parent": {
    "name": "ShieldCave",
    "path": "folders/Rooms/ShieldCave.yy",
  },
  "parentRoom": null,
  "physicsSettings": {
    "inheritPhysicsSettings": false,
    "PhysicsWorld": false,
    "PhysicsWorldGravityX": 0.0,
    "PhysicsWorldGravityY": 10.0,
    "PhysicsWorldPixToMetres": 0.1,
  },
  "roomSettings": {
    "Height": 1162,
    "inheritRoomSettings": false,
    "persistent": false,
    "Width": 1162,
  },
  "sequenceId": null,
  "tags": [
    "ROOM_BUILDING",
  ],
  "views": [
    {"hborder":888,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":{"name":"Player","path":"objects/Player/Player.yy",},"vborder":888,"visible":true,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
  ],
  "viewSettings": {
    "clearDisplayBuffer": true,
    "clearViewBackground": false,
    "enableViews": true,
    "inheritViewSettings": false,
  },
  "volume": 1.0,
}