{
  "resourceType": "GMRoom",
  "resourceVersion": "1.0",
  "name": "FireNexusSWRoom",
  "creationCodeFile": "",
  "inheritCode": false,
  "inheritCreationOrder": false,
  "inheritLayers": false,
  "instanceCreationOrder": [
    {"name":"inst_FEB64FE_1","path":"rooms/FireNexusSWRoom/FireNexusSWRoom.yy",},
    {"name":"inst_134A63C8_1","path":"rooms/FireNexusSWRoom/FireNexusSWRoom.yy",},
    {"name":"inst_31F041D_1","path":"rooms/FireNexusSWRoom/FireNexusSWRoom.yy",},
    {"name":"inst_59E891AD_1","path":"rooms/FireNexusSWRoom/FireNexusSWRoom.yy",},
    {"name":"inst_72FB124A","path":"rooms/FireNexusSWRoom/FireNexusSWRoom.yy",},
    {"name":"inst_31BE914A","path":"rooms/FireNexusSWRoom/FireNexusSWRoom.yy",},
    {"name":"inst_7E950907","path":"rooms/FireNexusSWRoom/FireNexusSWRoom.yy",},
    {"name":"inst_2CEBD639","path":"rooms/FireNexusSWRoom/FireNexusSWRoom.yy",},
    {"name":"inst_575BB135","path":"rooms/FireNexusSWRoom/FireNexusSWRoom.yy",},
    {"name":"inst_58EA8C2B","path":"rooms/FireNexusSWRoom/FireNexusSWRoom.yy",},
    {"name":"inst_195DD5A4","path":"rooms/FireNexusSWRoom/FireNexusSWRoom.yy",},
  ],
  "isDnd": false,
  "layers": [
    {"resourceType":"GMRInstanceLayer","resourceVersion":"1.0","name":"Instances","depth":0,"effectEnabled":true,"effectType":"_filter_outline","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_FEB64FE_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1600.0,"y":1152.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_134A63C8_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"propertyId":{"name":"anchor_index","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"value":"1",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1216.0,"y":896.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_31F041D_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_room","path":"objects/RoomTransition/RoomTransition.yy",},"value":"FireNexusEntranceRoom",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_anchor","path":"objects/RoomTransition/RoomTransition.yy",},"value":"2",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":3.0,"x":1728.0,"y":1088.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_59E891AD_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_room","path":"objects/RoomTransition/RoomTransition.yy",},"value":"FireNexusAmorRoom",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_anchor","path":"objects/RoomTransition/RoomTransition.yy",},"value":"3",},
          ],"rotation":0.0,"scaleX":3.0,"scaleY":1.0,"x":1152.0,"y":768.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_72FB124A","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TileDamageTrap","path":"objects/TileDamageTrap/TileDamageTrap.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1216.0,"y":1792.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_31BE914A","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TileDamageTrap","path":"objects/TileDamageTrap/TileDamageTrap.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1216.0,"y":1664.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_7E950907","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PickupHeartSmall","path":"objects/PickupHeartSmall/PickupHeartSmall.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1440.0,"y":1280.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_2CEBD639","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"MusicRequester","path":"objects/MusicRequester/MusicRequester.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"MusicRequester","path":"objects/MusicRequester/MusicRequester.yy",},"propertyId":{"name":"music","path":"objects/MusicRequester/MusicRequester.yy",},"value":"MusicDungeon",},
          ],"rotation":0.0,"scaleX":3.0,"scaleY":3.0,"x":576.0,"y":576.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_575BB135","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"MoltenSlime","path":"objects/MoltenSlime/MoltenSlime.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1408.0,"y":1408.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_58EA8C2B","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"MoltenSlime","path":"objects/MoltenSlime/MoltenSlime.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":960.0,"y":1280.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_195DD5A4","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"MoltenSlime","path":"objects/MoltenSlime/MoltenSlime.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1088.0,"y":1024.0,},
      ],"layers":[],"properties":[
        {"name":"g_OutlineColour","type":1,"value":"#FF000000",},
        {"name":"g_OutlineRadius","type":0,"value":"2",},
        {"name":"g_OutlinePixelScale","type":0,"value":"1",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Walls","depth":100,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF1919FF",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-493,40,1,29,-3,53,1,54,-3,0,1,39,-31,40,1,41,-3,66,1,67,-3,0,1,39,-31,40,10,41,91,95,96,80,0,0,26,43,29,-5,53,-25,
40,3,41,-2147483648,0,-4,-2147483648,3,39,40,41,-4,66,1,70,-25,40,3,41,-2147483648,0,-3,-2147483648,6,26,43,29,54,95,93,-3,66,-25,40,12,41,-2147483648,0,-2147483648,26,
27,43,40,41,67,0,-2147483648,-3,0,-25,40,12,41,-2147483648,0,-2147483648,39,40,40,29,54,80,0,-2147483648,-3,0,-25,40,12,41,-2147483648,0,-2147483648,39,40,40,41,67,-2147483648,0,
-2147483648,-3,0,-25,40,15,41,-2147483648,0,-2147483648,39,40,40,41,80,-2147483648,0,-2147483648,26,27,27,-25,40,13,41,-2147483648,0,-2147483648,39,40,40,41,-2147483648,-2147483648,0,-2147483648,39,-27,40,13,
41,-2147483648,0,-2147483648,39,40,40,41,-2147483648,-2147483648,0,-2147483648,39,-27,40,13,41,-2147483648,0,-2147483648,52,53,53,54,-2147483648,-2147483648,0,-2147483648,39,-27,40,13,41,-2147483648,0,-2147483648,65,66,66,67,
-2147483648,-2147483648,0,-2147483648,39,-27,40,13,41,-2147483648,0,-2147483648,78,66,66,80,-2147483648,-2147483648,0,-2147483648,39,-27,40,3,41,-2147483648,0,-7,-2147483648,3,0,-2147483648,39,-27,40,2,41,-2147483648,-9,0,
2,-2147483648,39,-27,40,1,41,-11,-2147483648,1,39,-27,40,1,42,-11,27,1,43,-414,40,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Bridges","depth":200,"effectEnabled":true,"effectType":null,"gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-775,-2147483648,1,77,-39,-2147483648,1,77,-39,-2147483648,1,77,-285,-2147483648,1,77,-458,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Floor_Holes","depth":300,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF5487FF",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-540,155,1,15,-72,155,-2,0,2,51,1,-8,0,-30,155,1,536870963,-8,155,1,0,-30,155,1,805306419,-7,0,2,51,0,-30,155,4,51,155,0,0,-4,155,2,
51,0,-30,155,2,64,155,-5,0,6,2,51,0,155,155,14,-25,155,-2,0,1,51,-7,0,1,805306419,-3,0,-25,155,-3,0,2,48,155,-5,0,2,155,51,
-3,0,-27,155,3,0,64,155,-5,0,4,155,1073741862,0,0,-28,155,3,0,805306419,155,-5,0,3,155,805306419,0,-30,155,2,268435507,155,-5,0,3,155,805306419,0,-30,155,
1,536870963,-3,155,-2,0,3,155,14,48,-31,155,2,48,15,-5,0,2,16,51,-31,155,9,61,38,1073741875,38,805306406,38,36,38,63,-32,155,-8,0,1,13,-34,155,
-2,0,-39,155,1,0,-379,155,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Lava","depth":400,"effectEnabled":true,"effectType":null,"gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-615,-2147483648,1,460,-39,-2147483648,1,1073742284,-39,-2147483648,1,805306828,-7,-2147483648,1,805306828,-31,-2147483648,1,536871372,-7,-2147483648,1,536871372,-39,-2147483648,1,1342177740,-39,-2147483648,1,268435916,-39,-2147483648,1,460,-31,-2147483648,1,1610613196,
-7,-2147483648,1,1879048652,-31,-2147483648,1,1342177740,-7,-2147483648,1,1610613196,-31,-2147483648,1,536871372,-7,-2147483648,1,1073742284,-31,-2147483648,1,805306828,-7,-2147483648,1,460,-31,-2147483648,1,268435916,-7,-2147483648,1,268435916,-31,-2147483648,9,1342177740,
1879048652,1610613196,268435916,805306828,1879048652,1073742284,1073742284,805306828,-37,-2147483648,1,460,-458,-2147483648,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Floor","depth":500,"effectEnabled":true,"effectType":"_filter_colourise","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_Intensity","type":0,"value":"1",},
        {"name":"g_TintCol","type":1,"value":"#FF5487FF",},
      ],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-1141,155,1,0,-458,155,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetUnderworld","path":"tilesets/TileSetUnderworld/TileSetUnderworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRBackgroundLayer","resourceVersion":"1.0","name":"Background","animationFPS":4.0,"animationSpeedType":0,"colour":4294967295,"depth":600,"effectEnabled":true,"effectType":"none","gridX":32,"gridY":32,"hierarchyFrozen":false,"hspeed":0.0,"htiled":true,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"spriteId":{"name":"fire_trap_dungeon_idea","path":"sprites/fire_trap_dungeon_idea/fire_trap_dungeon_idea.yy",},"stretch":false,"userdefinedAnimFPS":false,"userdefinedDepth":false,"visible":true,"vspeed":0.0,"vtiled":true,"x":0,"y":0,},
  ],
  "parent": {
    "name": "FireNexus",
    "path": "folders/Rooms/FireNexus.yy",
  },
  "parentRoom": null,
  "physicsSettings": {
    "inheritPhysicsSettings": false,
    "PhysicsWorld": false,
    "PhysicsWorldGravityX": 0.0,
    "PhysicsWorldGravityY": 10.0,
    "PhysicsWorldPixToMetres": 0.1,
  },
  "roomSettings": {
    "Height": 2500,
    "inheritRoomSettings": false,
    "persistent": false,
    "Width": 2500,
  },
  "sequenceId": null,
  "tags": [
    "ROOM_BUILDING",
  ],
  "views": [
    {"hborder":888,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":{"name":"Player","path":"objects/Player/Player.yy",},"vborder":888,"visible":true,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
  ],
  "viewSettings": {
    "clearDisplayBuffer": true,
    "clearViewBackground": false,
    "enableViews": true,
    "inheritViewSettings": false,
  },
  "volume": 1.0,
}