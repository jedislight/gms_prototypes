{
  "resourceType": "GMRoom",
  "resourceVersion": "1.0",
  "name": "EarthForestElementRoom",
  "creationCodeFile": "",
  "inheritCode": false,
  "inheritCreationOrder": false,
  "inheritLayers": false,
  "instanceCreationOrder": [
    {"name":"inst_3BDD0852_1_1_1_2_1_1","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_490E84DE","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_1A544151","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_114D9784","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_2CF866A","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_337DFBF7","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_35DC7692","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_1B63A6DB","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_49D96D75","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_643E9B10","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_5B798F8A","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_6B69C93E","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_3730B885","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_53DBDDB6","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_3C94C824","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_3DB93A4A","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_557FE2D5","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_24EC2BA4","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_732D3FCE","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_3D471019","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_78608D9F","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_439FE9DC","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_1BC38FE6","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_1C91E8D5","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_76E0795D","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_3D753BA6","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_778E19E5","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_38283E43","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_48E358C9","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_3E72F6A6","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_1308F972","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_15DDDCBF","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_586DCB90","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_4AC85F31","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_2FE3B8BF","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
    {"name":"inst_7DA6AD65","path":"rooms/EarthForestElementRoom/EarthForestElementRoom.yy",},
  ],
  "isDnd": false,
  "layers": [
    {"resourceType":"GMREffectLayer","resourceVersion":"1.0","name":"Effect_2","depth":0,"effectEnabled":true,"effectType":"_filter_vignette","gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_VignetteEdges","type":0,"value":"0.5",},
        {"name":"g_VignetteEdges","type":0,"value":"1.45",},
        {"name":"g_VignetteSharpness","type":0,"value":"2",},
        {"name":"g_VignetteTexture","type":2,"value":"_filter_vignette_texture",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMREffectLayer","resourceVersion":"1.0","name":"Effect_1","depth":100,"effectEnabled":true,"effectType":"_filter_tintfilter","gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[
        {"name":"g_TintCol","type":1,"value":"#FFCCA3A3",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Canopy","depth":200,"effectEnabled":true,"effectType":null,"gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-643,0,3,108,107,108,-149,0,4,107,108,107,108,-34,0,2,107,108,-36,0,2,107,108,-11,0,11,107,108,0,107,108,107,108,107,108,107,108,-40,0,4,
107,108,107,108,-27,0,1,108,-12,0,2,107,108,-8,0,2,107,108,-70,0,2,107,108,-4,0,2,107,108,-156,0,2,107,108,-373,0,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRInstanceLayer","resourceVersion":"1.0","name":"Instances","depth":300,"effectEnabled":true,"effectType":"_filter_outline","gridX":64,"gridY":64,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_3BDD0852_1_1_1_2_1_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"MusicRequester","path":"objects/MusicRequester/MusicRequester.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"MusicRequester","path":"objects/MusicRequester/MusicRequester.yy",},"propertyId":{"name":"music","path":"objects/MusicRequester/MusicRequester.yy",},"value":"MusicDarkForest",},
          ],"rotation":0.0,"scaleX":3.0,"scaleY":2.0,"x":0.0,"y":-128.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_490E84DE","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"Sign","path":"objects/Sign/Sign.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":2176.0,"y":1216.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_1A544151","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PickupEarthElement","path":"objects/PickupEarthElement/PickupEarthElement.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":512.0,"y":1152.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_114D9784","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PickupHeartSmall","path":"objects/PickupHeartSmall/PickupHeartSmall.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1792.0,"y":1216.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_2CF866A","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1600.0,"y":1152.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_337DFBF7","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1600.0,"y":1216.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_35DC7692","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1536.0,"y":1152.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_1B63A6DB","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1536.0,"y":1280.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_49D96D75","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1472.0,"y":1216.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_643E9B10","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1408.0,"y":1216.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_5B798F8A","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1472.0,"y":1088.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_6B69C93E","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1408.0,"y":1088.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_3730B885","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1472.0,"y":1344.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_53DBDDB6","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1408.0,"y":1344.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_3C94C824","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1344.0,"y":1280.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_3DB93A4A","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1280.0,"y":1216.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_557FE2D5","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1216.0,"y":1216.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_24EC2BA4","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1216.0,"y":1344.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_732D3FCE","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1152.0,"y":1280.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_3D471019","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1344.0,"y":1472.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_78608D9F","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1408.0,"y":1472.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_439FE9DC","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1984.0,"y":1024.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_1BC38FE6","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1536.0,"y":896.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_1C91E8D5","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1664.0,"y":1472.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_76E0795D","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"PushBlockAny","path":"objects/PushBlockAny/PushBlockAny.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1088.0,"y":960.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_3D753BA6","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"propertyId":{"name":"anchor_index","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"value":"0",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":2304.0,"y":1152.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_778E19E5","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"propertyId":{"name":"anchor_index","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"value":"1",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1408.0,"y":128.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_38283E43","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"propertyId":{"name":"anchor_index","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"value":"2",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":512.0,"y":1280.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_48E358C9","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"TransitionAnchor","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"propertyId":{"name":"anchor_index","path":"objects/TransitionAnchor/TransitionAnchor.yy",},"value":"3",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1536.0,"y":2304.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_3E72F6A6","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_anchor","path":"objects/RoomTransition/RoomTransition.yy",},"value":"1",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_room","path":"objects/RoomTransition/RoomTransition.yy",},"value":"EarthForestBootsRoom",},
          ],"rotation":0.0,"scaleX":2.0,"scaleY":1.0,"x":1472.0,"y":2432.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_1308F972","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":2.0,"x":2432.0,"y":1152.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_15DDDCBF","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_anchor","path":"objects/RoomTransition/RoomTransition.yy",},"value":"2",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_room","path":"objects/RoomTransition/RoomTransition.yy",},"value":"EarthForestJavelinRoom",},
          ],"rotation":0.0,"scaleX":3.0,"scaleY":1.0,"x":1344.0,"y":0.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_586DCB90","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"properties":[
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_room","path":"objects/RoomTransition/RoomTransition.yy",},"value":"EarthForestElementTunnel",},
            {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"RoomTransition","path":"objects/RoomTransition/RoomTransition.yy",},"propertyId":{"name":"target_anchor","path":"objects/RoomTransition/RoomTransition.yy",},"value":"0",},
          ],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":384.0,"y":1280.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_4AC85F31","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"Shaman","path":"objects/Shaman/Shaman.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":832.0,"y":1152.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_2FE3B8BF","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"Shaman","path":"objects/Shaman/Shaman.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":960.0,"y":1408.0,},
        {"resourceType":"GMRInstance","resourceVersion":"1.0","name":"inst_7DA6AD65","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"objectId":{"name":"Shaman","path":"objects/Shaman/Shaman.yy",},"properties":[],"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":1088.0,"y":1088.0,},
      ],"layers":[],"properties":[
        {"name":"g_OutlineColour","type":1,"value":"#FF000000",},
        {"name":"g_OutlineRadius","type":0,"value":"2",},
        {"name":"g_OutlinePixelScale","type":0,"value":"1",},
      ],"userdefinedDepth":false,"visible":true,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Ground_Decorations","depth":400,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
61,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,0,0,37,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,
76,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,-3,0,37,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,
76,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,-3,0,37,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,
76,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,-3,0,37,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,
76,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,-3,0,37,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,
76,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,-3,0,37,139,140,75,76,75,76,75,76,75,76,75,76,75,76,75,
76,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,139,140,-5,0,33,75,76,75,76,75,76,75,76,75,76,75,76,75,76,76,
75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,-3,0,1,37,-3,0,33,75,76,75,76,75,76,75,76,75,76,75,76,75,76,76,
75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,-7,0,73,75,76,75,76,75,76,75,76,75,76,75,76,75,76,76,75,76,75,76,
75,76,75,76,75,76,75,76,75,76,75,76,75,76,0,37,0,39,0,0,39,75,76,75,76,75,76,75,76,75,76,75,76,75,76,76,75,76,75,76,
75,76,75,76,75,76,75,76,75,76,75,76,75,76,-7,0,35,75,76,75,76,75,76,75,76,75,76,75,76,75,76,76,75,76,75,76,75,76,75,76,
75,76,75,76,75,76,75,76,139,140,37,37,-5,0,34,139,140,75,76,75,76,75,76,75,76,75,76,75,76,76,75,76,75,76,75,76,75,76,75,76,
75,76,75,76,139,140,0,0,37,-3,0,1,37,-4,0,27,75,76,75,76,75,76,75,76,75,76,75,76,76,75,76,75,76,75,76,75,76,75,76,75,
76,75,76,-6,0,1,39,-6,0,27,139,140,75,76,75,76,75,76,75,76,75,76,76,75,76,75,76,75,76,75,76,75,76,75,76,139,140,-4,0,1,
37,-5,0,-2,39,-3,0,23,139,140,75,76,75,76,75,76,75,76,76,75,76,139,140,139,140,139,140,139,140,139,140,-12,0,-6,39,12,0,139,140,75,
76,75,76,75,76,75,76,75,-4,0,-2,37,2,0,40,-10,0,7,39,0,0,39,39,0,0,-5,39,13,0,139,140,75,76,75,76,75,76,75,76,75,
76,-3,37,1,40,-8,0,4,39,0,0,39,-5,0,2,37,0,-6,39,13,0,139,140,139,140,75,76,75,76,75,76,37,37,-5,0,2,37,39,-14,0,
-9,39,-2,0,10,75,76,75,76,75,76,0,0,37,37,-5,0,1,37,-9,0,-2,39,-2,0,-8,39,-3,0,10,75,76,75,76,75,76,268435501,0,37,37,
-10,0,1,39,-4,0,-10,39,14,75,76,75,76,75,75,76,75,76,139,140,0,37,37,-3,0,1,40,-7,0,1,39,-3,0,-9,39,14,75,76,75,76,
75,76,75,75,76,75,76,0,0,37,-16,0,1,39,-5,0,-2,39,24,75,76,75,76,75,76,75,76,75,75,76,75,76,75,76,0,75,76,75,76,75,
76,75,76,-14,0,30,39,0,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,0,75,76,75,76,75,76,75,76,75,76,75,76,-5,0,1,37,
-6,0,30,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,-5,0,35,39,0,0,75,
76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,-3,0,1,39,-4,0,34,75,76,
75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,-4,0,76,75,76,75,76,75,
76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,0,37,0,0,75,76,75,76,75,
76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,-4,0,36,75,76,75,76,75,76,
75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,-4,0,373,75,76,75,76,75,76,75,
76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,0,0,75,76,75,76,75,76,75,76,75,
76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,0,0,75,76,75,76,75,76,75,76,75,
76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,0,0,75,76,75,76,75,76,75,76,75,
76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,0,0,75,76,75,76,75,76,75,76,75,
76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,0,0,75,76,75,76,75,76,75,76,75,
76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,37,0,75,76,75,76,75,76,75,76,75,
76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,0,0,75,76,75,76,75,76,75,76,75,
76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,0,0,75,76,75,76,75,76,75,76,75,
76,75,76,75,76,0,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,75,76,0,0,75,76,75,76,75,76,75,76,75,
76,75,76,75,76,0,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRTileLayer","resourceVersion":"1.1","name":"Tiles_Ground","depth":500,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"tiles":{"SerialiseHeight":40,"SerialiseWidth":40,"TileCompressedData":[
-568,1,1,340,-39,1,-2,340,-39,1,-2,340,-38,1,-2,340,-38,1,-2,340,-38,1,-2,340,-38,1,-3,340,-38,1,-3,340,-38,1,-2,340,-707,1,],"TileDataFormat":1,},"tilesetId":{"name":"TileSetOverworld","path":"tilesets/TileSetOverworld/TileSetOverworld.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"resourceType":"GMRBackgroundLayer","resourceVersion":"1.0","name":"Background","animationFPS":4.0,"animationSpeedType":0,"colour":4294967295,"depth":600,"effectEnabled":true,"effectType":"none","gridX":32,"gridY":32,"hierarchyFrozen":false,"hspeed":0.0,"htiled":true,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"properties":[],"spriteId":{"name":"earth_forest_dd","path":"sprites/earth_forest_dd/earth_forest_dd.yy",},"stretch":false,"userdefinedAnimFPS":false,"userdefinedDepth":false,"visible":true,"vspeed":0.0,"vtiled":true,"x":0,"y":0,},
  ],
  "parent": {
    "name": "EarthForest",
    "path": "folders/Rooms/EarthForest.yy",
  },
  "parentRoom": null,
  "physicsSettings": {
    "inheritPhysicsSettings": false,
    "PhysicsWorld": false,
    "PhysicsWorldGravityX": 0.0,
    "PhysicsWorldGravityY": 10.0,
    "PhysicsWorldPixToMetres": 0.1,
  },
  "roomSettings": {
    "Height": 2500,
    "inheritRoomSettings": false,
    "persistent": false,
    "Width": 2500,
  },
  "sequenceId": null,
  "tags": [
    "ROOM_BUILDING",
  ],
  "views": [
    {"hborder":888,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":{"name":"Player","path":"objects/Player/Player.yy",},"vborder":888,"visible":true,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1366,"wview":1366,"xport":0,"xview":0,"yport":0,"yview":0,},
  ],
  "viewSettings": {
    "clearDisplayBuffer": true,
    "clearViewBackground": false,
    "enableViews": true,
    "inheritViewSettings": false,
  },
  "volume": 1.0,
}