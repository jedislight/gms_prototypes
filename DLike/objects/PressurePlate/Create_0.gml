/// @description Insert description here
// You can write your code in this editor




// Inherit the parent event
event_inherited();

image_speed = 0;
pressed_this_frame = false;
pressed = false;

function on_pressed(){
	pressed_this_frame = true;
	if(pressed) return;
	pressed = true;
	image_index = 1;
	mutable_events_broadcast("signal_"+string(signal_pressed), {by:id}); 
	var s = audio_play_sound(SoundToggleThud, 10, false)
	audio_sound_pitch(s, 0.8)
}

function on_released() {
	if(not pressed) return;
	pressed = false;
	image_index = 0;
	mutable_events_broadcast("signal_"+string(signal_released), {by:id}); 
	var s = audio_play_sound(SoundToggleThud, 10, false)
	audio_sound_pitch(s, 1.2)
}