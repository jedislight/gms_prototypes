/// @description Insert description here
// You can write your code in this editor

event_inherited();

function SignalLampListener(owner_) : MutableEventListener()constructor {
	owner=owner_;
	function listen(context){
		if(owner.sprite_index == SpriteSignalLampOff) {
			owner.sprite_index = SpriteSignalLampOn;	
		} else {
			owner.sprite_index = SpriteSignalLampOff;
		}
	}
	function needs_cleanup() { return not instance_exists(owner) };
}
mutable_events_add_listener(new SignalLampListener(id), "signal_"+string(signal));