/// @description Insert description here
// You can write your code in this editor

if (yy == -1) exit;
++counter;
if(counter <= rate) exit;
counter = 0;

var wipe_effect_layer_id = layer_get_id("Wipe_Rumble_Effect");
var layer_fx = layer_get_fx(wipe_effect_layer_id);
fx_set_parameter(layer_fx, "g_ShakeSpeed", .1 );
if(not audio_is_playing(loop_sound)){
	audio_play_sound(loop_sound, 0, true)	
}

var player = instance_find(Player, 0);
if(instance_exists(player)) {
	var locks = player.input_locking_child_objects;
	if(not array_contains(locks, id)) {
		array_push(locks, id);	
	}
}
for (var i = 0; i < array_length(layers); ++i) {
	var layer_id = layer_get_id(layers[i]);
	if(layer_id == -1) exit;
	var tilemap = layer_tilemap_get_id(layer_id);
	if(tilemap = -1) exit;
	for (var xx = 0; xx < room_width; xx+=64) {
		var tile_data = tilemap_get_at_pixel(tilemap, xx, yy);
		tile_data = tile_set_empty(tile_data);
		tilemap_set_at_pixel(tilemap, tile_data, xx, yy);
	}
}
if(wipe_sound != noone) {
	audio_play_sound(wipe_sound, 5, false);
}
yy += 64;
if (yy >= room_height) {
	yy = -1;	
	if(audio_is_playing(loop_sound)){
		audio_stop_sound(loop_sound)	
	}
	fx_set_parameter(layer_fx, "g_ShakeSpeed", 0 );
	MotionPlanning.invalidate_all();
	if(instance_exists(player)) {
		var locks = player.input_locking_child_objects;
		if(array_contains(locks, id)) {
			var index = array_find_index(locks,function(e){return e == id});
			if(index >= 0) {
				array_delete(locks, index, 1);	
			}
		}
	}
	instance_destroy(id);
}
