/// @description Insert description here
// You can write your code in this editor




// Inherit the parent event
event_inherited();
audio_play_sound(SoundAirSwish, 10, false)
speed = 10;

if(array_contains(tags, "water")){
	image_blend = c_aqua;	
}

if(array_contains(tags, "fire")){
	image_blend = #F03441;	
}

if(array_contains(tags, "earth")){
	image_blend = #21F032;	
}