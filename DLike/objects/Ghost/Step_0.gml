/// @description Insert description here
// You can write your code in this editor




// Inherit the parent event
event_inherited();
stuck_damage_accumulator = 0;
var t = current_time + instance_seed;
image_blend = make_color_hsv(255,0, sin(t/1000)*100+150);
if (instance_exists(combat_target)){
	path_end();
	path_clear_points(path);
	tx = x;
	ty = y;
	var orbit_d = abs(sin(t/1000)*200)+50
	var off_x = sin(t/1000)*orbit_d;
	var off_y = cos(t/1000)*orbit_d;
	motion_add(point_direction(x,y, combat_target.x+off_x, combat_target.y+off_y), movement_speed/15);
}
speed *= 59/60;
speed = min(speed, movement_speed);

if(pacified and frame_counter mod 30 == 0 and is_wisp) {
	var tmp = items[? "armor"];
	items[? "armor"] = "";
	deal_damage(id, id, 1, []);
	items[? "armor"] = tmp;
}