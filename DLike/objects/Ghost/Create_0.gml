/// @description Insert description here
// You can write your code in this editor

is_wisp = false;
function transform_to_wisp(){
	is_wisp = true;
	top_sprite = {
		up: SpriteWisp,
		down: SpriteWisp,
		left: SpriteWisp,
		right: SpriteWisp
	}
	top_sprite_down = SpriteWisp
	top_sprite_left = SpriteWisp
	top_sprite_right = SpriteWisp
	top_sprite_up = SpriteWisp
	
	unpacify = function(){};
}

// Inherit the parent event
event_inherited();

behaviors = [
	behavior_target_near_combatant,
	behavior_idle,
]

function unstuck() {
	// never unstuck	
}