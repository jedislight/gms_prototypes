/// @description Insert description here
// You can write your code in this editor



// Inherit the parent event
event_inherited();

progression_flag = object_get_name(object_index) + room_get_name(room);

function on_pickup(player){
	array_push(player.progression_flags, progression_flag);
	if(!array_contains(player.progression_flags, "bag1")) {
		array_push(player.progression_flags, "bag1");	
	} else if (!array_contains(player.progression_flags, "bag2")) {
		array_push(player.progression_flags, "bag2");		
	} else if(!array_contains(player.progression_flags, "bag3")) {
		array_push(player.progression_flags, "bag3");		
	}
}

function void_if(player){
	return array_contains(player.progression_flags, progression_flag) or
		array_contains(player.progression_flags, "bag3");
}