/// @description Insert description here
// You can write your code in this editor

event_inherited()
var p = instance_find(Player,0);
var s = source.id;
var t = other.id;
if(array_contains(tags, "earth")) {
	s = t;
}
var n = instance_create_layer(s.x,s.y, "Instances", FxChain, {tags:tags, source:s, target:t, offset_y:sprite_get_height(t.get_top_sprite())/-2});

if(array_contains(tags, "earth")) {
	n.taught_distance = 0;
}

instance_destroy(id);