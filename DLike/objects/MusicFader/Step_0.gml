/// @description Insert description here
// You can write your code in this editor


if(not audio_is_playing(track)){
	instance_destroy(id);
}

if(audio_sound_get_gain(track) == 0) {
	audio_stop_sound(track);
}
