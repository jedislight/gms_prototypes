/// @description Insert description here
// You can write your code in this editor




// Inherit the parent event
event_inherited();

function behavior_shaman_heal_if_target_not_player() {
	if(instance_exists(combat_target) and combat_target.object_index != Player){
		var n = instance_create_layer(combat_target.x, combat_target.y, "Instances", FxHeal, {heal:heal, target:combat_target});
		instance_create_layer(x,y, "Instances", FxChanneling, {target:n, source:id, offset_y:-32, image_blend: #08E53A});
		array_push(wait_for_instances_array, n);
		direction = point_direction(x,y, combat_target.x, combat_target.y);
	}
	return false;
}

pet = noone;
function behavior_shaman_summon_earth_slime_if_target_player() {
	if(not instance_exists(pet) and instance_exists(combat_target) and combat_target.object_index == Player){
		pet = instance_create_layer(x,y, "Instances", EarthSlime);
	}
	return false;
}

behaviors = [
	behavior_target_near_combatant,
	behavior_shaman_heal_if_target_not_player,
	behavior_wait_for_instances,
	behavior_shaman_summon_earth_slime_if_target_player,
	behavior_leashed_wander_once,
	behavior_idle_timed_idle,
	behavior_loop,
];
