/// @description Insert description here
// You can write your code in this editor

if(!variable_global_exists("transition_anchor_index")){
	global.transition_anchor_index = 0;	
}

if (global.transition_anchor_index == anchor_index) {
	if(instance_number(Player) == 0 ) {
		instance_create_layer(x,y, "Instances", Player);	
	} else {
		with(Player) {
			if(not loading){
				x = other.x + other.sprite_width /2;
				y = other.y + other.sprite_height /2; 
			}
			persistent = false;
			layer = layer_get_id("Instances")
		}
	}
}
instance_destroy(id);	

