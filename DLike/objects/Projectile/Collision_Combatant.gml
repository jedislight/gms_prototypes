/// @description Insert description here
// You can write your code in this editor

event_inherited();
if (other.id == target or (target == noone and other.id != source)) {
	other.deal_damage(other.id, id, damage, tags);
	instance_destroy(id);
	on_impact();
}