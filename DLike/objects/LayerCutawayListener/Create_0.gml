/// @description Insert description here
// You can write your code in this editor


/// @description Insert description here
// You can write your code in this editor

event_inherited();
layer_visibility = true;

function CutawayListener(owner_) : MutableEventListener()constructor {
	owner=owner_;
	function listen(context){
		owner.layer_visibility = false;
	}
	function needs_cleanup() { return not instance_exists(owner) };
}
mutable_events_add_listener(new CutawayListener(id), "signal_"+string(signal));