/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

// target x,y (final target)
tx = x;
ty = y;

path = path_add();
last_free = {x:x, y:y};
pixels_t = 0;

draw_debug_motion = false;

behavior_cooldown = irandom(new_behavior_rate_frames);

current_behavior = 0;

frame_counter = 0;

function behavior_wander() {
	if (almost_equal(tx, x) and almost_equal(ty,y)){
		tx = random(room_width);
		ty = random(room_height);
	}
	
	return path_get_length(path) > 0;
}
function behavior_progressive_wander() {
	if (almost_equal(tx, x) and almost_equal(ty,y)){
		tx = x + random_range(-leash_distance, leash_distance);
		ty = y + random_range(-leash_distance, leash_distance)
	}
	
	return path_get_length(path) > 0;
}

function behavior_wait_if_player_nearby() {
	var player = instance_find(Player,0);
	if(instance_exists(player) and point_distance(x,y, player.x, player.y) < leash_distance ) {
		tx = x;
		ty = y;
		path_end();
		path_clear_points(path);
		return true;
	}
	return false;
}

function behavior_idle() {
	if(not travel_sprites_enabled) {
		set_travel_top_sprites();
	}
	
	return true;
}

timed_idle_behavior_duration_frames = 120;
timed_idle_behavior_counter = 0;
function behavior_idle_timed_idle() {
	timed_idle_behavior_counter += 1;
	if(timed_idle_behavior_counter >= timed_idle_behavior_duration_frames){
		timed_idle_behavior_counter = 0;
		return false;
	}
	
	return true;
}

function behavior_idle_sometimes() {
	if(not travel_sprites_enabled) {
		set_travel_top_sprites();
	}
	
	return random_range(0,100) < 99;
}

wait_for_instances_array = [];
function behavior_wait_for_instances() {
	for (var i = 0; i < array_length(wait_for_instances_array); ++i) {
	    var n = wait_for_instances_array[i];
		if(instance_exists(n)){
			stop();
			return true;	
		} else {
			stop();
			array_delete(wait_for_instances_array, i, 1);
			return true;
		}
	}
	
	return false;
}

function behavior_leashed_wander_once(){
	set_travel_top_sprites();
	if (almost_equal(tx, x) and almost_equal(ty,y)){
		tx = xstart + random_range(-leash_distance, leash_distance);
		ty = ystart + random_range(-leash_distance, leash_distance)
	}	
	return false;
}

function behavior_leashed_wander(){
	behavior_leashed_wander_once();
	return path_get_length(path) > 0;
}

function behavior_loop(){
	current_behavior = -1	
	return false;
}

sing_bubble = noone;
function behavior_sing() {
	
	if(random_range(0,100) > 50 and not instance_exists(sing_bubble)){
		sing_bubble = create_dialog_bubble([[SpriteMusicNote]], 120);	
	}
	
	return instance_exists(sing_bubble);
}

behaviors = [behavior_idle];
pacified = false;
unpacified_behaviors = behaviors;
pacified_behaviors = [behavior_sing, behavior_leashed_wander, behavior_idle_sometimes, behavior_loop];

function pacify() {
	if(not pacified) {
		pacified = true;
		unpacified_behaviors = behaviors;
		behaviors = pacified_behaviors;
	}
}

function unpacify() {
	if (pacified){
		behaviors = unpacified_behaviors	
		pacified = false;
		if(instance_exists(sing_bubble)) instance_destroy(sing_bubble);
		create_dialog_bubble([[SpriteAngry]], 45);	
	}
}

function unstuck(hard) {
	if(place_meeting(xprevious, y, Impassibles)){
		y = yprevious;	
	}
	if(place_meeting(x, yprevious, Impassibles)){
		x = xprevious;	
	}
	if(place_meeting(x, y, Impassibles))
	{
		var out = point_direction(x,y, last_free.x, last_free.y);
		var distance_to_out = point_distance(x,y, last_free.x, last_free.y);
		var eject_force = min(4, distance_to_out);
		x += lengthdir_x(eject_force, out);
		y += lengthdir_y(eject_force, out);
	
		if(hard){
			var n = instance_place(x,y, Impassibles);
			if (instance_exists(n)) do {
				x += lengthdir_x(1, out);
				y += lengthdir_y(1, out);
			} until(not place_meeting(x,y, n));
		}
		
		xprevious = x;
		yprevious = y;
	}
}

function stop(){
	tx = x;
	ty = y;
	top_sprite_change_delay = 0;
	path_clear_points(path);
	path_end();
}