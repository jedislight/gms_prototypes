/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
++frame_counter;

if(instance_number(MotionPlanning) == 0) {
	instance_create_depth(0,0,0, MotionPlanning);	
}
if(!almost_equal(x, tx) or !almost_equal(y, ty)) {
	var found = true;
	if(not path_exists(path) 
		or not almost_equal(path_get_x(path, 1), tx, 16) 
		or not almost_equal(path_get_y(path, 1), ty, 16)
		or path_get_length(path) < 64
	) {
		var mp_cell_x = x div 64;
		var mp_cell_y = y div 64;
		var mp_cell_state = mp_grid_get_cell(MotionPlanning.mp_grid, mp_cell_x, mp_cell_y);
		mp_grid_clear_cell(MotionPlanning.mp_grid, mp_cell_x, mp_cell_y);
		found = mp_grid_path(MotionPlanning.mp_grid, path, x, y, tx, ty, true);
		if(mp_cell_state == -1){
			mp_grid_add_cell(MotionPlanning.mp_grid, mp_cell_x, mp_cell_y);	
		}
		path_start(path, movement_speed, path_action_stop, false);
	}
	if (not found) {
		tx = x;
		ty = y;
	}
}

var n = instance_place(x,y, Impassibles);
if(not instance_exists(n) and point_in_rectangle(x,y, 16, 16, room_width-16, room_height-16)){
	last_free = {x:x, y:y};	
} else if(instance_exists(n)) {
	unstuck(n.hard_eject);
}


if(array_length(behaviors) > current_behavior){
	var behavior_function = behaviors[current_behavior];
	var active = behavior_function();
	if(not active) {
		++current_behavior;
	}
}

if(behavior_cooldown-- <= 0){
	current_behavior = 0;
	behavior_cooldown = new_behavior_rate_frames;
}

if(not travel_sprites_enabled) {
	path_clear_points(path)	;
	tx = x;
	ty = y;
	path_end();
}


