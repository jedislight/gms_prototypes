/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if( travel_sprites_enabled ) {
	var distance_moved = point_distance(x,y, xprevious, yprevious);
	shared_image_speed = distance_moved / 60 * shared_image_speed_mod;
	if (distance_moved < .25)  {
		shared_image_index = 0;
		shared_image_speed = 0;
	}	
} else {
	x = xprevious;
	y = yprevious;
	path_clear_points(path);
	
	shared_image_speed = 1 / 60 * shared_image_speed_mod;
}