/// @description Insert description here
// You can write your code in this editor




// Inherit the parent event
event_inherited();

if(instance_exists(target) and instance_exists(source)){
	x = source.x;
	y = source.y;
	var dist = point_distance(x,y, target.x, target.y);
	var dir = point_direction(x,y, target.x, target.y);
	y += offset_y;
	
	image_xscale = dist / 64;
	image_angle = dir;
} else {
	instance_destroy(id);	
}