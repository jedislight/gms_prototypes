/// @description Insert description here
// You can write your code in this editor


if(instance_number(MusicPlayer) >0) {
	var now_playing = audio_get_name(MusicPlayer.playing_music);
	var request_name = audio_get_name(music);
	var is_new_track = now_playing != request_name;
	
	log(string("MUSIC REQUESTED: {0}", request_name))
	
	if(is_new_track){
		log(string("MUSIC STOP: {0}", now_playing))
		instance_create_layer(0,0,"Instances", MusicFader, {track: MusicPlayer.playing_music});
		
		log(string("MUSIC PLAY: {0}", request_name))
		MusicPlayer.playing_music = audio_play_sound(music, 100, true);	
		audio_sound_gain(MusicPlayer.playing_music, 0, 0);
		audio_sound_gain(MusicPlayer.playing_music, 1, 1000);
		
	} else {
		log(string("MUSIC CONTINUE: {0}", request_name));
	}
	
	instance_destroy(id);
}