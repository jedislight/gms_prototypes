/// @description Insert description here
// You can write your code in this editor




// Inherit the parent event
event_inherited();

push_dirs = [0];
error_tolerance = 30;
push_cooldown = 25;
push_counter = 0;

function push(push_dir){
	var nx = x + lengthdir_x(64, push_dir)
	var ny = y + lengthdir_y(64, push_dir) 
	if(not place_meeting(nx, ny, Impassibles)) {
		x = nx;
		y = ny;
		MotionPlanning.update();	
		push_counter = push_cooldown;
		return true;
	}
	return false;
}