/// @description Insert description here
// You can write your code in this editor

var center_x = x + 32;
var center_y = y - 32;

if(other.collision_frame != -1 and floor(other.image_index) != other.collision_frame){
	exit;
}

var player = instance_find(Player, 0);
if(not instance_exists(player)) exit;

var p_dist = point_distance(center_x, center_y, player.x, player.y)
if(p_dist > 64) exit;
var extra_tolerance = 64 - p_dist;
if(push_counter > 0) exit;

for (var i = 0; i < array_length(push_dirs); ++i) {
	var push_dir = push_dirs[i];
	
	var pushing_dir = point_direction(other.x, other.y, center_x, center_y);
	var error = abs(angle_difference(pushing_dir, push_dir));
	if(error < error_tolerance + extra_tolerance){
		push(push_dir);
		break;
	}
}
