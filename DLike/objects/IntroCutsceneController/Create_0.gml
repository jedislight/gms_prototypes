/// @description Insert description here
// You can write your code in this editor
grave = noone;
right_gem = noone;
left_gem = noone;
priest = noone;
fade_speed = 0;
skipper = noone;
function spawn_priest(){
	priest = instance_create_layer(737,450, "Instances", NpcPriest);
	priest.behaviors = [priest.behavior_idle]
	priest.direction = 180;
	next();
}

function spawn_ghost() {
	var n = instance_create_layer(grave.x, grave.y, "Instances", Ghost);
	n.combat_target = grave.id;
	n.behaviors = [n.behavior_idle];
	next();
}

function delete_priest(){
	instance_destroy(priest);	
	next();
}

function delete_gems(){
	if(instance_exists(left_gem)) instance_destroy(left_gem)
	if(instance_exists(right_gem)) instance_destroy(right_gem)
	next();
}

function spawn_right_gem(){
	right_gem = instance_create_layer(occultist.x + 64, occultist.y, "Instances", PickupHeartLarge);
	next();
}

function spawn_left_gem(){
	left_gem = instance_create_layer(occultist.x -64, occultist.y, "Instances", PickupHeartLarge);
	next();
}

function next() {
	array_delete(states, 0, 1);
	log(string("CUTSCENE STEPS REMAINING: {0}", array_length(states)));
}

var occultist = noone;
function spawn_occultist(){
	occultist = instance_create_layer(350,696, "Instances", NpcVillagerBrightOccultist);
	occultist.behaviors = [occultist.behavior_idle]
	occultist.direction = 90;
	timer = 180;
	next();
}

timer = 0;
function timed_wait() {
	timer -= 1;
	if(timer <= 0){
		next();	
	}
}
function transition() {
	room_goto(Room1);
}

function wait_for_dialog() {
	if(instance_number(EmojiDialogBubble) == 0){
		next();	
	}
}

fade = 0;
function fade_out() {
	fade += 1/240;
	if(fade >=1.25) {
		next();	
	}
}

function fade_in() {
	fade -= 1/240;
	if(fade <= 0) {
		next();	
	}
}
function wait_for_paths() {
	var done = true;
	with(Mobile) {
		if(point_distance(tx,ty, x, y) > 1){
			done = false;	
		}
	}
	
	if(done) next();
}

function wait_for_channeling(){
	if(instance_number(FxChanneling) == 0) next();	
}

states = [
	//scene 1: Sad
	spawn_occultist,
	function(){timer = 300; next();},
	timed_wait,
	function(){occultist.create_dialog_bubble([[SpriteSad],[SpriteGrave,SpriteHero]], 300); next();},
	wait_for_dialog,
	function(){timer = 300; next();},
	timed_wait,
	fade_out,
	
	// scene 2: attempt failure
	function(){with(occultist){x = 1309; y = 741; tx = 350; ty = 700}next();},
	fade_in,	
	wait_for_paths,
	function(){occultist.stop(); occultist.direction =90;next();},
	function(){
		var n = instance_create_layer(grave.x, grave.y, "Instances", FxHeal, {target:noone});
		n.shared_image_speed_mod = 0.5;
		instance_create_layer(grave.x, grave.y, "Instances", FxChanneling, {source: occultist, target:n});
		next();
	},	
	wait_for_channeling,
	function(){timer = 60; next();},
	timed_wait,
	function(){occultist.create_dialog_bubble([[SpriteAngry]], 300); next();},
	wait_for_dialog,
	fade_out,
	
	// scene 3: observed
	function(){with(occultist){x = 1309; y = 741; tx = 350; ty = 700}next();},
	fade_in,	
	wait_for_paths,
	function(){occultist.stop(); occultist.direction =90;next();},
	function(){timer = 60; next();},
	timed_wait,
	
	// place gem R
	function(){occultist.stop(); occultist.direction =0;next();},
	function(){timer = 60; next();},
	spawn_right_gem,
	timed_wait,
	
	//face forward
	function(){occultist.stop(); occultist.direction =90;next();},
	function(){timer = 60; next();},
	timed_wait,
	
	// cast
	function(){
		var n = instance_create_layer(grave.x, grave.y, "Instances", FxHeal, {target:noone});
		n.shared_image_speed_mod = 0.40;//slightly slower than before
		
		instance_create_layer(grave.x, grave.y, "Instances", FxChanneling, {source: occultist, target:n});
		instance_create_layer(grave.x, grave.y, "Instances", FxChanneling, {source: right_gem, target:n, image_blend:#EF3040});
		next();
	},	
	spawn_priest,
	function(){priest.direction = 180; next();},
	function(){priest.create_dialog_bubble([[SpriteSurprise]], 300); next();},
	wait_for_channeling,
	delete_gems,
	wait_for_dialog,
	function(){timer = 60; next();},
	timed_wait,
	function(){occultist.create_dialog_bubble([[SpriteAngry]], 300); next();},
	delete_priest,
	wait_for_dialog,
	fade_out,
	
	// scene 4: interupted
	function(){with(occultist){x = 1309; y = 741; tx = 350; ty = 700}next();},
	fade_in,	
	wait_for_paths,
	function(){occultist.stop(); occultist.direction =90;next();},
	function(){timer = 60; next();},
	timed_wait,
	
	// place gem R
	function(){occultist.stop(); occultist.direction =0;next();},
	function(){timer = 60; next();},
	spawn_right_gem,
	timed_wait,
	
	// place gem L
	function(){occultist.stop(); occultist.direction =180;next();},
	function(){timer = 60; next();},
	spawn_left_gem,
	timed_wait,
	
	//face forward
	function(){occultist.stop(); occultist.direction =90;next();},
	function(){timer = 60; next();},
	timed_wait,
	
	// cast
	function(){
		var n = instance_create_layer(grave.x, grave.y, "Instances", FxHeal, {target:noone});
		n.shared_image_speed_mod = 0.25;//very slow
		
		instance_create_layer(grave.x, grave.y, "Instances", FxChanneling, {source: occultist, target:n});
		instance_create_layer(grave.x, grave.y, "Instances", FxChanneling, {source: right_gem, target:n, image_blend:#EF3040});
		instance_create_layer(grave.x, grave.y, "Instances", FxChanneling, {source: left_gem, target:n, image_blend:#EF3040});
		next();
	},	
	
	// interrupt
	spawn_priest,
	function(){priest.direction = 180; priest.tx = occultist.x; priest.ty = occultist.y+64; next();},
	wait_for_paths,
	function(){priest.direction = 90; occultist.direction = 270; next();},
	function(){occultist.create_dialog_bubble([[SpriteSurprise]], 300); next();},
	delete_gems,
	
	wait_for_channeling,
	delete_gems,
	wait_for_dialog,
	function(){timer = 60; next();},
	timed_wait,
	function(){occultist.create_dialog_bubble([[SpriteAngry]], 300); next();},
	function(){priest.create_dialog_bubble([[SpriteAngry]], 300); next();},
	
	
	// async pre-fade
	function(){fade_speed = 1/240; next();},
		
	//ghosts
	spawn_ghost,
	function(){timer = 30; next();},
	timed_wait,
	spawn_ghost,
	function(){timer = 30; next();},
	timed_wait,
	spawn_ghost,
	function(){timer = 30; next();},
	timed_wait,
	spawn_ghost,
	function(){timer = 30; next();},
	timed_wait,
	spawn_ghost,
	function(){timer = 30; next();},
	timed_wait,
	spawn_ghost,
	function(){timer = 30; next();},
	timed_wait,
	spawn_ghost,
	function(){timer = 30; next();},
	timed_wait,
	spawn_ghost,
	function(){timer = 30; next();},
	timed_wait,
	spawn_ghost,
	function(){timer = 30; next();},
	timed_wait,
	spawn_ghost,
	function(){timer = 30; next();},
	wait_for_dialog,
	fade_out,

	// end scene
	transition,
];

