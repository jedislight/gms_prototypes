/// @description Insert description here
// You can write your code in this editor


grave = placeholder_find("grave")
states[0]();

fade += fade_speed

if(
	keyboard_check_direct(vk_anykey) 
	or gamepad_button_check(0, gp_face1)
	or gamepad_button_check(0, gp_face2)
	or gamepad_button_check(0, gp_face3)
	or gamepad_button_check(0, gp_face4)
) {
	if(not instance_exists(skipper)){
		skipper = instance_create_layer(room_width/2,room_height/2,"Instances", Skipper);
	}	
} else {
	if(instance_exists(skipper)){
		instance_destroy(skipper);
	}	
}

if(instance_exists(skipper) and skipper.confirmed){
	transition();
}	