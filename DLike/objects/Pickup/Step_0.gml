/// @description Insert description here
// You can write your code in this editor
event_inherited();

var player = instance_find(Player, 0);
if(!void_checked and instance_exists(player)) {
	void_checked = true;
	if(void_if(player)){
		instance_destroy(id);
		exit;	
	}
}

if(triggered) {
	
	var peak = 128; 
	
	anchor_speed += acceleration;
	anchor_offset += anchor_speed;
	if(anchor_offset > peak){
		anchor_offset = peak;
		anchor_speed *= -1;
		acceleration *= -1;
	}
	if(anchor_offset < 0) {
		if(instance_exists(player)){
			on_pickup(player);
		}
		instance_destroy(id);	
	}
	
	if(instance_exists(player)){
		x = player.x;
		y = player.y - anchor_offset;
	}
	
	var scale = 1;
	if(anchor_speed > 0) {
		scale = 1+anchor_offset/peak;	
	} else {
		scale = 2 - (1-(anchor_offset/peak))*2
	}
	image_xscale = scale;
	image_yscale = scale;
	
}