/// @description Insert description here
// You can write your code in this editor




// Inherit the parent event
event_inherited();
image_speed = 0;
hit_by = [];

function hit() {
	if(not array_contains(hit_by, other.id)) {
		for (var i = 0; i < array_length(hit_by); ++i) {
		    var h = hit_by[i];
			if(not instance_exists(h)) {
				array_delete(hit_by, i, 1);
				--i;
			}
		}
		state = not state;
		var signals = [ signal_off, signal_on];
		mutable_events_broadcast("signal_"+string(signals[state]), {by:id}); 
		array_push(hit_by, other.id);
	}	
}