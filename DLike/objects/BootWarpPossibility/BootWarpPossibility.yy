{
  "resourceType": "GMObject",
  "resourceVersion": "1.0",
  "name": "BootWarpPossibility",
  "eventList": [
    {"resourceType":"GMEvent","resourceVersion":"1.0","name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,},
    {"resourceType":"GMEvent","resourceVersion":"1.0","name":"","collisionObjectId":null,"eventNum":0,"eventType":3,"isDnD":false,},
    {"resourceType":"GMEvent","resourceVersion":"1.0","name":"","collisionObjectId":null,"eventNum":64,"eventType":8,"isDnD":false,},
  ],
  "managed": true,
  "overriddenProperties": [
    {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"Isometricish","path":"objects/Isometricish/Isometricish.yy",},"propertyId":{"name":"draw_top_during_draw_end","path":"objects/Isometricish/Isometricish.yy",},"value":"True",},
    {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"Isometricish","path":"objects/Isometricish/Isometricish.yy",},"propertyId":{"name":"top_blendmode","path":"objects/Isometricish/Isometricish.yy",},"value":"bm_add",},
    {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"Isometricish","path":"objects/Isometricish/Isometricish.yy",},"propertyId":{"name":"top_sprite_up","path":"objects/Isometricish/Isometricish.yy",},"value":"SpriteBootWarpPossibility",},
    {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"Isometricish","path":"objects/Isometricish/Isometricish.yy",},"propertyId":{"name":"top_sprite_down","path":"objects/Isometricish/Isometricish.yy",},"value":"SpriteBootWarpPossibility",},
    {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"Isometricish","path":"objects/Isometricish/Isometricish.yy",},"propertyId":{"name":"top_sprite_left","path":"objects/Isometricish/Isometricish.yy",},"value":"SpriteBootWarpPossibility",},
    {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"Isometricish","path":"objects/Isometricish/Isometricish.yy",},"propertyId":{"name":"top_sprite_right","path":"objects/Isometricish/Isometricish.yy",},"value":"SpriteBootWarpPossibility",},
    {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"Isometricish","path":"objects/Isometricish/Isometricish.yy",},"propertyId":{"name":"base_sprite","path":"objects/Isometricish/Isometricish.yy",},"value":"SpriteBlank",},
  ],
  "parent": {
    "name": "Items",
    "path": "folders/Objects/Items.yy",
  },
  "parentObjectId": {
    "name": "Isometricish",
    "path": "objects/Isometricish/Isometricish.yy",
  },
  "persistent": false,
  "physicsAngularDamping": 0.1,
  "physicsDensity": 0.5,
  "physicsFriction": 0.2,
  "physicsGroup": 1,
  "physicsKinematic": false,
  "physicsLinearDamping": 0.1,
  "physicsObject": false,
  "physicsRestitution": 0.1,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsShapePoints": [],
  "physicsStartAwake": true,
  "properties": [
    {"resourceType":"GMObjectProperty","resourceVersion":"1.0","name":"tx","filters":[],"listItems":[],"multiselect":false,"rangeEnabled":false,"rangeMax":10.0,"rangeMin":0.0,"value":"0","varType":0,},
    {"resourceType":"GMObjectProperty","resourceVersion":"1.0","name":"ty","filters":[],"listItems":[],"multiselect":false,"rangeEnabled":false,"rangeMax":10.0,"rangeMin":0.0,"value":"0","varType":0,},
    {"resourceType":"GMObjectProperty","resourceVersion":"1.0","name":"ready","filters":[],"listItems":[],"multiselect":false,"rangeEnabled":false,"rangeMax":10.0,"rangeMin":0.0,"value":"0","varType":3,},
    {"resourceType":"GMObjectProperty","resourceVersion":"1.0","name":"selected","filters":[],"listItems":[],"multiselect":false,"rangeEnabled":false,"rangeMax":10.0,"rangeMin":0.0,"value":"0","varType":3,},
  ],
  "solid": false,
  "spriteId": {
    "name": "SpriteBootWarpPossibility",
    "path": "sprites/SpriteBootWarpPossibility/SpriteBootWarpPossibility.yy",
  },
  "spriteMaskId": null,
  "visible": true,
}