/// @description Insert description here
// You can write your code in this editor




// Inherit the parent event
event_inherited();

shared_image_index += .5;

var travel = min(point_distance(x,y, tx, ty), 5)
var dir = point_direction(x,y, tx, ty);
x += lengthdir_x(travel, dir)
y += lengthdir_y(travel, dir)

ready = travel < 1
