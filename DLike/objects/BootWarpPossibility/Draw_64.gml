/// @description Insert description here
// You can write your code in this editor

var camx = camera_get_view_x(view_get_camera(0));
var camy = camera_get_view_y(view_get_camera(0));	

var player = instance_find(Player, 0);
if(not instance_exists(player)) exit;


var width = 3;
if(ready) { 
	width += 3;
}
var scatter = 64;
if(selected) {
	scatter = 0;
	width *= 2;
}

gpu_set_blendmode(bm_add)
var scatter_x = random_range(-scatter,scatter);
var scatter_y = random_range(-scatter,scatter);
draw_circle_color(player.x-camx, player.y-camy, width/2, image_blend, image_blend, false);
draw_line_width_color(player.x-camx, player.y-camy, x+scatter_x-camx, y+scatter_y-camy, width, image_blend, image_blend);
draw_circle_color(x+scatter_x-camx, y+scatter_y-camy, width/2, image_blend, image_blend, false);

gpu_set_blendmode(bm_normal)