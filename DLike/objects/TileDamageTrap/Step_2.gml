/// @description Insert description here
// You can write your code in this editor
event_inherited()

if(triggered) {
	trap_image_index = 0;
	trap_speed = 1 / reset_frames * (image_number-1);
	triggered = false;
	audio_play_sound(SoundFireWoosh, 50, false);
}

trap_image_index += trap_speed;
if (trap_image_index > image_number-1) {
	trap_image_index = image_number-1;
	trap_speed = 0;
}

image_index = trap_image_index;
draw_top_sprite = trap_image_index < 1;