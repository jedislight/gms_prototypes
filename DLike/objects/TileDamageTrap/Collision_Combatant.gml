/// @description Insert description here
// You can write your code in this editor
event_inherited()

if(trap_speed != 0) exit;

other.deal_damage(other.id, id, damage_amount, [damage_tag, "trap"]);
triggered = true;