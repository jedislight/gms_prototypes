/// @description Insert description here
// You can write your code in this editor


var s = anchor.get_top_sprite(anchor.direction);
var camx = camera_get_view_x(view_get_camera(0));
var camy = camera_get_view_y(view_get_camera(0));

var phrase = phrases[phrase_cursor];

var top = anchor.y - sprite_get_height(s) - 128 - camy;
var left = anchor.x - 32 - (array_length(phrase)-1) * 32 - camx;

draw_sprite_ext(sprite_index, 0, left-16, top-16, 0.5 + array_length(phrase), 1.5, 0, c_white, 0.85);
for (var i = 0; i < array_length(phrase); ++i) {
    draw_sprite(phrase[i], 0, left + i * 64, top);
}
