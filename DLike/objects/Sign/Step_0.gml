/// @description Insert description here
// You can write your code in this editor

event_inherited();

var player = instance_find(Player, 0);
var show = (instance_exists(player) and point_distance(player.x, player.y, x, y) < activation_distance)

if(show and not instance_exists(bubble)){
	var duration = 120;
	if(array_length(message_sprites) == 1) duration = 999999;
	bubble = create_dialog_bubble(message_sprites, duration);	
} else if(not show and instance_exists(bubble)){
	instance_destroy(bubble);	
}