/// @description Insert description here
// You can write your code in this editor



// Inherit the parent event
event_inherited();

function on_pickup(player){
	array_push(player.owned_items, "chain");
	if(player.items[? "chain"] == "") {
		player.items[? "chain"] = "null";
	}
}

function void_if(player){
	return array_contains(player.owned_items, "chain")
}