/// @description Insert description here
// You can write your code in this editor



// Inherit the parent event
event_inherited();

draw_blocked = false;
sound = noone;
image_speed = 0;

function interact(player) {
	if(array_contains(player.progression_flags, progression_flag)) {
		image_speed = 60/60;
		draw_blocked = true;
		if(sound == noone){
			sound = audio_play_sound(SoundStoneRolling, 0, false);
		}

	} else {
		draw_blocked = true;	
	}
}
