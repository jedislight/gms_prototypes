/// @description Insert description here
// You can write your code in this editor


if(draw_blocked) {
	var camx = camera_get_view_x(view_get_camera(0));
	var camy = camera_get_view_y(view_get_camera(0));
	var scale = image_index/image_number+1;
	draw_sprite_ext(blocked_sprite, 0, x + 0.5*sprite_width-camx, y-camy, scale, scale, 0, c_black, 1.0);
	var alpha = 0.85 + sin(current_time*.01)/4;
	if(image_speed > 0) {
		alpha = 1-image_index/image_number;	
	}
	draw_sprite_ext(blocked_sprite, 0, x + 0.5*sprite_width-camx, y-camy, scale, scale, 0, c_white, alpha);
	draw_set_alpha(1.0)
}