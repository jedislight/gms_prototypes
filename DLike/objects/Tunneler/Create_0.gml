/// @description Insert description here
// You can write your code in this editor




// Inherit the parent event
event_inherited();

behaviors_offense = [
	behavior_target_near_player,
	behavior_attack_combat_target_no_interupt,
	behavior_leashed_wander,
	behavior_idle,
]

behaviors_defense = [
	behavior_target_near_player,
	behavior_avoid_combat_target,
	behavior_leashed_wander,
	behavior_idle,
]

behaviors = behaviors_offense;
defense_ttl = 0;

function on_damaged_during_attack(){
	behaviors = behaviors_defense;
	defense_ttl = (2 + combatant_hp_max - combatant_hp) * 15;
	set_travel_top_sprites();		
	top_sprite_change_delay = 0
}

function TunnelerDamageListener(owner_) : MutableEventListener()constructor {
	owner=owner_;
	function listen(context){
		if(context.target == owner.id) {
			if(not owner.travel_sprites_enabled) {
				owner.on_damaged_during_attack()
			} else {
				context.zero = true;
			}
		}
	}
	function needs_cleanup() { return not instance_exists(owner) };
}
mutable_events_add_listener(new TunnelerDamageListener(id), "damage");