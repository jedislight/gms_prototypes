/// @description Insert description here
// You can write your code in this editor



// Inherit the parent event
event_inherited();

progression_flag = object_get_name(object_index) + room_get_name(room);

function on_pickup(player){
	array_push(player.progression_flags, progression_flag);
	player.combatant_hp += 1;
	player.combatant_hp_max += 1;
}

function void_if(player){
	return array_contains(player.progression_flags, progression_flag)
}