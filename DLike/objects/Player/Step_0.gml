/// @description Insert description here
// You can write your code in this editor
event_inherited();
if(not instance_exists(id)) exit;
#region Gather Input 
ds_map_clear(input);
input[? "utility_4"] = new InputState();
input[? "utility_4"].add_mouse(mb_right);
input[? "utility_4"].add_button(gp_face1);

input[? "utility_1"] = new InputState();
input[? "utility_1"].add_mouse(mb_middle);
input[? "utility_1"].add_button(gp_face2);

input[? "utility_2"] = new InputState();
input[? "utility_2"].add_key(vk_space);
input[? "utility_2"].add_button(gp_face3);

input[? "utility_3"] = new InputState();
input[? "utility_3"].add_key(ord("W"));
input[? "utility_3"].add_button(gp_face4);

// limit input based on owned bags
if(! array_contains(progression_flags, "bag1")){
	input[? "utility_1"].clear();	
}

if(! array_contains(progression_flags, "bag2")){
	input[? "utility_2"].clear();	
}

if(! array_contains(progression_flags, "bag3")){
	input[? "utility_3"].clear();	
}

input[? "boots"] = new InputState();
input[? "boots"].released = true;
input[? "shield"] = new InputState();
input[? "javelin"] = new InputState();
input[? "vortex"] = new InputState();
input[? "sword"] = new InputState();
input[? "chain"] = new InputState();

for (var u = 1; u <= 4; ++u) {
    var utility_key = "utility_"+string(u);
	var utility_index = u-1;
	var equipped_utility = equipped_utilities[utility_index];
	if (equipped_utility == "") continue;
	if( is_undefined(input[? equipped_utility])) input[? equipped_utility] = new InputState();
	input[? equipped_utility].merge(input[? utility_key]); 
}

input[? "toggle_modifier"] = new InputState();
input[? "toggle_modifier"].add_key(ord("Q"));
input[? "toggle_modifier"].add_button(gp_shoulderl);

input[? "toggle_element"] = new InputState();
input[? "toggle_element"].add_key(ord("E"));
input[? "toggle_element"].add_button(gp_shoulderr);

if(!equip_enabled){
	input[? "toggle_element"].clear();
	input[? "toggle_modifier"].clear();
}

var modifier_down = input[? "toggle_modifier"].down || input[? "toggle_element"].down

var move_x = 0;
var move_y = 0;
if(mouse_check_button(mb_any)) {
	var md = point_direction(x,y, mouse_x, mouse_y);
	move_x = lengthdir_x(1, md);
	move_y = lengthdir_y(1, md);
	direction = point_direction(0,0, move_x, move_y);
}

if(abs(gamepad_axis_value(0, gp_axislh)) + abs(gamepad_axis_value(0, gp_axislv)) > 0.1) {
	move_x = gamepad_axis_value(0, gp_axislh);
	move_y = gamepad_axis_value(0, gp_axislv);
}

#endregion

#region Input State
if (input[? "toggle_modifier"].down) {
	for (var u = 1; u <=4 ; ++u) {
	    var utility_key = "utility_"+string(u);
		if(input[? utility_key].pressed) {
			var utility_index = u-1;
			select_next_utility(utility_index);
		}
	}	
}

for (var i = 0; i < array_length(input_locking_child_objects); ++i) {
    if(not instance_exists(input_locking_child_objects[i])){
		array_delete(input_locking_child_objects, i--, 1);
	}
}

if(not accept_input){
	if (move_x == 0 and move_y == 0)  {
		accept_input = true;	
		log("Inital input clear, starting to accept input")
	}
}

var handle_input = accept_input and array_length(input_locking_child_objects) == 0;
#endregion

#region Input Response
if (handle_input and (travel_sprites_enabled and input[? "shield"].down and items[? "shield"] != "" and sword_cooldown <= 0)) {
	if (not modifier_down) {
		sword_cooldown = sword_cooldown_reset;
		set_attack_top_sprites();
		shared_image_index = 0;
		var dir_offset = 2;
		var offset_y = -32;
		var swing_dir = direction;
		if(abs(gamepad_axis_value(0, gp_axislh)) + abs(gamepad_axis_value(0, gp_axislv)) >= 0.1) {
			swing_dir = point_direction(0,0, gamepad_axis_value(0, gp_axislh), gamepad_axis_value(0, gp_axislv))	
		}
		var xx = x + lengthdir_x(dir_offset, swing_dir)
		var yy = y + lengthdir_y(dir_offset, swing_dir) + offset_y;
		var n = instance_create_layer(xx, yy, "Instances", FxShield, {source:id, image_angle:swing_dir, depth_offset: -offset_y, tag:items[? "armor"]});
		if(!spend_stamina(1)) {
			n.image_speed *= 3;	
		}
		array_push(input_locking_child_objects, n);
		stop();
		direction = swing_dir;
	}
}

if (handle_input and (travel_sprites_enabled and input[? "vortex"].down and items[? "vortex"] != "" and sword_cooldown <= 0)) {
	if (not modifier_down and instance_number(FxVortex) == 0 and spend_mana(2)) {
		sword_cooldown = sword_cooldown_reset;
		var swing_dir = direction;
		if(abs(gamepad_axis_value(0, gp_axislh)) + abs(gamepad_axis_value(0, gp_axislv)) >= 0.1) {
			swing_dir = point_direction(0,0, gamepad_axis_value(0, gp_axislh), gamepad_axis_value(0, gp_axislv))	
		}
		instance_create_layer(x,y, "Instances", FxVortex, {tags:[items[? "vortex"]], direction:swing_dir});
	}
}

if (handle_input and (travel_sprites_enabled and input[? "sword"].down and sword_cooldown <= 0)) {
	if(not modifier_down){
		sword_cooldown = sword_cooldown_reset;
		set_attack_top_sprites();
		shared_image_index = 0;
		var dir_offset = 2;
		var offset_y = -32;
		var swing_dir = direction;
		if(abs(gamepad_axis_value(0, gp_axislh) + gamepad_axis_value(0, gp_axislv)) >= 0.1) {
			swing_dir = point_direction(0,0, gamepad_axis_value(0, gp_axislh), gamepad_axis_value(0, gp_axislv))	
		}
		var xx = x + lengthdir_x(dir_offset, swing_dir)
		var yy = y + lengthdir_y(dir_offset, swing_dir) + offset_y;
		var n = instance_create_layer(xx, yy, "Instances", FxSwordSwing, {source:id, tags:[items[? "sword"]], value: 5, image_angle:swing_dir, depth_offset: -offset_y});
		var cd_mod = 1.0;
		if(!spend_stamina(1)){
			n.value = round(n.value/ 2);
			n.image_speed *= 0.5;
			cd_mod = 2;
		}
		
		sword_cooldown = sword_cooldown_reset * n.cooldown_mod * cd_mod;
		array_push(input_locking_child_objects, n);
		stop();
		direction = swing_dir;
	}
}

if (handle_input and (travel_sprites_enabled and input[? "chain"].down and sword_cooldown <= 0)) {
	if(not modifier_down){
		sword_cooldown = sword_cooldown_reset;
		set_attack_top_sprites();
		shared_image_index = 0;
		var dir_offset = 2;
		var offset_y = -32;
		var swing_dir = direction;
		if(abs(gamepad_axis_value(0, gp_axislh) + gamepad_axis_value(0, gp_axislv)) >= 0.1) {
			swing_dir = point_direction(0,0, gamepad_axis_value(0, gp_axislh), gamepad_axis_value(0, gp_axislv))	
		}
		var xx = x + lengthdir_x(dir_offset, swing_dir)
		var yy = y + lengthdir_y(dir_offset, swing_dir) + offset_y;
		var old = instance_find(FxChain, 0);
		if(instance_exists(old)){
			instance_destroy(old.id);
		}
		var n = instance_create_layer(xx, yy, "Instances", FxChainSwing, {source:id, tags:[items[? "chain"]], image_angle:swing_dir, depth_offset: -offset_y});
		var cd_mod = 1.0;
		if(!spend_stamina(1)){
			n.image_speed *= 0.5;
			cd_mod = 2;
		}
		
		sword_cooldown = sword_cooldown_reset * n.cooldown_mod * cd_mod;
		array_push(input_locking_child_objects, n);
		stop();
		direction = swing_dir;
	}
}


if (handle_input and (travel_sprites_enabled and input[? "javelin"].pressed and sword_cooldown <= 0)) {
	if(not modifier_down and spend_stamina(1)){
		sword_cooldown = sword_cooldown_reset;
		set_attack_top_sprites();
		shared_image_index = 0;
		var j = instance_create_layer(x,y-32, "Instances", FxJavelinProjectile, {direction: direction, tags:[items[?"javelin"]], source:id, damage:3});
	}
}


if (handle_input and (move_x != 0 or move_y != 0)) {
	var can_move = travel_sprites_enabled;
	if(not can_move){
		var ts = get_top_sprite(direction);
		can_move = shared_image_index > sprite_get_number(ts);
	}
	
	if(can_move) {
		tx = x + move_x*24;	
		ty = y + move_y*24;
	}
}

if (handle_input and input[? "boots"].down and instance_number(BootWarpPossibility) == 0 and not modifier_down and mana > 0) {
	var element = items[? "boots"];
	var blend = #444444;
	switch(element){
		case "fire": blend = #442200; break	
		case "water": blend = #222244; break
		case "earth": blend = #204408; break
	}
	if(element == "null" and instance_exists(null_boot_anchor)) {
		var bwp = instance_create_layer(x,y, "Instances", BootWarpPossibility, {tx: null_boot_anchor.x, ty: null_boot_anchor.y, image_blend: blend});
		array_push(input_locking_child_objects,bwp);
	} else {
		var target_points = [];
		with(Combatant) {
			if(other.id == id) continue;
			if(point_distance(other.x, other.y, x, y) > 700) continue;
			if(array_contains(combatant_damage_tags, element) or items[?"armor"] == element) {
				array_push(target_points, {x:x, y:y});
			}
		}
		
		with(BootAnchor) {
			if(point_distance(other.x, other.y, x, y) > 700) continue;
			if(tag == element) {
				array_push(target_points, {x:x, y:y});
			}
		}
		
		with(FxJavelinProjectile) {
			if(point_distance(other.x, other.y, x, y) > 700) continue;
			if(speed > 0) continue;
			if(array_contains(tags, element)) {
				var back_point ={x:x, y:y};
				back_point.x += lengthdir_x(32, -image_angle)
				back_point.y += lengthdir_y(32, -image_angle)
				array_push(target_points, back_point);
			}
		}
		
		for (var i = 0; i < array_length(target_points); ++i) {
		    var target_point = target_points[i];
			var bwp = instance_create_layer(x,y, "Instances", BootWarpPossibility, {tx: target_point.x, ty: target_point.y, image_blend: blend});
			array_push(input_locking_child_objects,bwp);	
		}
	}
}

var closest_angle = 180;
var closest = noone;
var move_dir = point_direction(0,0, move_x, move_y)
with(BootWarpPossibility) {
	var angle = point_direction(other.x, other.y, x, y);
	var error = abs(angle_difference(angle, move_dir));
	selected = false;
	if(error < closest_angle){
		closest_angle = error;
		closest = id;
	}
}

with(closest) selected = true;

if( input[? "boots"].released ) {
	var bwp = noone;
	with(BootWarpPossibility) {
		if(selected and ready) bwp = id;	
	}
	
	if(instance_exists(bwp)) {
		x = bwp.x;
		y = bwp.y;
		path_clear_points(path)
		path_end();
		tx = x;
		ty = y;
		spend_mana(1);
	}
	
	instance_destroy(BootWarpPossibility)
}

if(input[? "toggle_element"].down) {
	if(input[? "utility_4"].pressed) {
		select_next_element(equipped_utilities[3]);
	}
	
	if(input[? "utility_1"].pressed) {
		select_next_element(equipped_utilities[0]);
	}
	
	if(input[? "utility_2"].pressed) {
		select_next_element(equipped_utilities[1]);
	}
	
	if(input[? "utility_3"].pressed) {
		select_next_element(equipped_utilities[2]);
	}
}

var list = ds_list_create();
var hits = collision_line_list(x,y, tx,ty, Impassibles, true, true, list, true);
if (hits > 0) {
	var closest = list[| 0];
	var cast_length = point_distance(x,y, closest.x, closest.y);
	var cast_dir = point_direction(x,y, tx, ty);
	while(hits > 0 and --cast_length > 0) {
		hits = collision_line_list(x,y, x+lengthdir_x(cast_length, cast_dir), y+lengthdir_y(cast_length,cast_dir), closest, true, true, list, false);
	}
		
	if (hits == 0) {
		cast_length = max(0, cast_length-16);
		tx = x+lengthdir_x(cast_length, cast_dir)
		ty = y+lengthdir_y(cast_length,cast_dir)
	}
}
	
ds_list_destroy(list);

#endregion

#region Tick stuff
item_selection_ttl -= 1;
if(item_selection_ttl <= 0){
	item_selection_ttl = 0;
	item_selection = undefined;
}

sword_cooldown -= 1;

if(mana != mana_max) {
	++mana_recharge_frame_counter;
} else {
	mana_recharge_frame_counter = 0;	
}

if(stamina != stamina_max) {
	++stamina_recharge_frame_counter;
} else {
	stamina_recharge_frame_counter = 0;
}

if(stamina_recharge_frame_counter > get_stamina_recharge_target()) {
	stamina += 1;
	stamina_recharge_frame_counter = 0;
}

if(mana_recharge_frame_counter > get_mana_recharge_target()) {
	mana += 1;
	mana_recharge_frame_counter = 0;
}


if(stamina == 0) {
	movement_speed = 2.5;
} else {
	movement_speed = 5;	
}

var save_point = instance_nearest(x,y, Checkpoint);
equip_enabled = false;
if(instance_exists(save_point) and distance_to_object(save_point) < 128 and array_length(owned_items)!=0){
	equip_enabled = true;	
}

#endregion