/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
var nearest_transition_anchor = instance_nearest(x,y, TransitionAnchor);
if(instance_exists(nearest_transition_anchor)){
	direction = point_direction(x,y, nearest_transition_anchor.x, nearest_transition_anchor.y)	
}
accept_input = false;
mouse_targeting = true;
draw_gamepad_target_guide = 0;
input_locking_child_objects = [];
sword_cooldown = 0;
sword_cooldown_reset = 60;

behaviors = [];
null_boot_anchor = noone;

tx1 = x
ty1 = y
tx2 = x
ty2 = y
tx0 = x;
ty0 = y;

combatant_floating_damage_text_colors.harm = c_red;

owned_items = [];
owned_elements = ["null"];
equipped_utilities = ["", "", "",""];
equip_enabled = false;

item_selection_ttl = 0;
item_selection = undefined;

progression_flags = [];

loading = false;

mana = 1
mana_max = 1
mana_recharge_frame_counter = 0;
function get_mana_recharge_target() {
	return 240 * (mana_max - mana);
}

function spend_mana(amount){
	if(amount > mana) {
		return false;
	}
	
	pct = mana_recharge_frame_counter / get_mana_recharge_target();
	mana -= amount;
	mana_recharge_frame_counter = pct * get_mana_recharge_target();
	if(is_nan(mana_recharge_frame_counter)) {
		mana_recharge_frame_counter = 0;
	}
	return true;
}

stamina = 1;
stamina_max = 1;
stamina_recharge_frame_counter = 0;
function get_stamina_recharge_target() {
	return 120 * (stamina+1);
}
function spend_stamina(amount){
	if(amount > stamina) {
		return false;	
	}
	
	pct = stamina_recharge_frame_counter / get_stamina_recharge_target();
	stamina -= amount;
	stamina_recharge_frame_counter = pct * get_stamina_recharge_target();
	
	return true;
}

input = ds_map_create();

function save() {
	var save_map = ds_map_create();
	save_map[? "owned_items"] = owned_items;
	save_map[? "equipped_utilities"] = equipped_utilities;
	save_map[? "owned_elements"] = owned_elements;
	save_map[? "progression_flags"] = progression_flags;
	var items_copy = ds_map_create();
	ds_map_copy(items_copy, items);
	ds_map_add_map(save_map, "items", items_copy);
	save_map[? "combatant_hp"] = combatant_hp;
	save_map[? "combatant_hp_max"] = combatant_hp_max;
	save_map[? "mana_max"] = mana_max;
	save_map[? "stamina_max"] = stamina_max;
	save_map[? "room"] = room_get_name(room);
	save_map[? "x"] = string(x);
	save_map[? "y"] = string(y);
	var writer = file_text_open_write("save");
	file_text_write_string(writer, json_encode(save_map));
	file_text_close(writer);
	ds_map_destroy(save_map);
	create_dialog_bubble([[SpriteSaveDisk]]);
	
}

function load() {
	if(not file_exists("save")) return;
	
	var reader = file_text_open_read("save");
	var load_struct = json_parse(file_text_read_string(reader));
	file_text_close(reader);
	var n = instance_create_layer(0,0, "Instances", Player);
	
	n.owned_items = load_struct.owned_items;
	n.equipped_utilities = load_struct.equipped_utilities;
	n.owned_elements = load_struct.owned_elements;
	n.progression_flags = load_struct.progression_flags;
	ds_map_clear(n.items);
	var items_struct = load_struct.items;
	var item_keys = variable_struct_get_names(items_struct);
	for (var i = 0; i < array_length(item_keys); ++i) {
		var key = item_keys[i];
	    var value = variable_struct_get(items_struct, key);
		n.items[? key] = value;
	}
	n.combatant_hp = load_struct.combatant_hp
	n.combatant_hp_max = load_struct.combatant_hp_max
	n.mana_max = load_struct.mana_max
	n.stamina_max = load_struct.stamina_max
	n.mana = n.mana_max;
	n.stamina = n.stamina_max;
	
	n.x = real(load_struct.x);
	n.y = real(load_struct.y);
	n.loading = true;
	n.persistent = true;
	
	instance_destroy(id);
	
	var load_room = asset_get_index(load_struct.room);
	if(load_room == room) {
		room_restart();
	} else if room_exists(load_room){
		room_goto(load_room);
	} else {
		room_goto(BellStarRoom);
		n.loading = false;
	}
}

function on_death(){
	instance_create_depth(0,0,0, DeathFadeer);	
}
function select_next_element(item_index) {
	if(array_length(owned_elements) > 0) {
		if(not audio_is_playing(SoundSubtleMenuBeep)) audio_play_sound(SoundSubtleMenuBeep, 0, false);
		item_selection_ttl = 60;
		var element = items[? item_index];
		var element_index = array_index_of(owned_elements, element);
		var next_index = element_index + 1;
		if(next_index == array_length(owned_elements)) {
			next_index = 0;	
		}
		
		items[? item_index] = owned_elements[next_index];
	}	
}

function select_next_utility(utility_index) {
	if(not audio_is_playing(SoundSubtleMenuBeep)) audio_play_sound(SoundSubtleMenuBeep, 0, false);
	var current = equipped_utilities[utility_index];
	var item_index = array_index_of(owned_items, current);
	var start = item_index;
	if(start == -1) {
		start = array_length(owned_items)-1;	
	}
	var done = false;
	var skip = [];
	skip = array_concat(skip, []);
	do {
		item_index += 1;
		if(item_index >= array_length(owned_items)) item_index = 0;
		var next = owned_items[item_index];
		done = not array_contains(skip, next);
	} until (done or item_index == start);
	if(done){
		var next = owned_items[item_index];
		
		equipped_utilities[utility_index] = next;
		if(current != "" and items[? next] == ""){
			items[? next] = items[? current];
			if(not array_contains(equipped_utilities, current)) {
				items[? current] = "";
			}
		}
		if(items[? next] == "") items[? next] = "null";
		log(string("EQUIP SET utility_{0} : {1}", utility_index, next));
	}
}

function has_any_items() {
	for(var item = ds_map_find_first(items); not is_undefined(item); item = ds_map_find_next(items, item)){
		var element = items[? item];
		if(element != "") return true;	
	}
	return false;
}

function get_utility_element(utility_key) {
	return items[? utility_key];
}