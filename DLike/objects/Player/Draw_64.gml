/// @description Insert description here
// You can write your code in this editor

event_inherited();

function draw_player_health() {
	var ax = 16;
	var ay = 768-16;

	var height = -100;
	var width = 100;
	var percent = combatant_hp / combatant_hp_max;

	var lx = ax;
	var ly = ay;

	var rx = ax+width;
	var ry = ay;

	var lux = ax + width/2 - width*(1-percent)/2;
	var luy = ay + height * (percent);

	var rux = ax + width/2 + width*(1-percent)/2;
	var ruy = luy;

	var center = ax + width/2;
	var top = ay + height;

	var base_color = c_maroon;
	var tip_color = c_red;
	var outline_width = 10;

	draw_triangle_color(lx, ly, lux, luy, rx ,ry, base_color, tip_color, base_color, false);
	draw_triangle_color(lux, luy, rux, ruy, rx ,ry, tip_color, tip_color, base_color, false);
	draw_line_width_color(lx, ly, lux, luy, outline_width, c_black, c_black);
	draw_line_width_color(rx, ry, rux, ruy,outline_width, c_black, c_black);
	draw_line_width_color(lx, ly, rx, ry,outline_width, c_black, c_black);
	draw_line_width_color(lux, luy, rux, ruy,outline_width, c_black, c_black);
	draw_line_width_color(lx, ly, center, top,outline_width, c_black, c_black);
	draw_line_width_color(rx, ry, center, top,outline_width, c_black, c_black);


	draw_set_halign(fa_center);
	draw_set_valign(fa_bottom);
	draw_set_font(FontHpUI);
	draw_set_color(c_white);

	draw_text(center, ay, string(ceil(combatant_hp)))
}

function draw_player_targeting_guide() {
	var cx = camera_get_view_x(view_get_camera(0));
	var cy = camera_get_view_y(view_get_camera(0));
	var color = #000070 
	gpu_set_blendmode(bm_add)
	draw_triangle_color(tx0-cx,ty0-cy, tx1-cx, ty1-cy, tx2-cx, ty2-cy, color, c_black, c_black, false);
	gpu_set_blendmode(bm_normal)
}

function get_item_sprite(item){
	switch(item){
		case "armor": return SpriteArmorUI;
		case "sword": return SpriteSwordUI;
		case "shield": return SpriteShieldUI;
		case "boots": return SpriteBootsUI;
		case "javelin": return SpriteJavelinUI;
		case "vortex": return SpriteVortexUI;
		case "chain": return SpriteChainUI;

		default: return SpriteBlank;
	}
}

function get_element_sprite(item){
	switch(item){
		case "fire": return SpriteFireElementUI;
		case "water": return SpritewaterElementUI;
		case "earth": return SpriteEarthElementUI;
		case "light": return SpriteLifeElementUI;
		case "dark": return SpriteDeathElementUI;
		case "null": return SpriteElementNull;
		default: return SpriteBlank;
	}
}

function draw_player_item_ui() {
	var bottom = 768;
	var right = 1366;
	var highlight = current_time /200;
	var highlight_alpha = 0.75 * equip_enabled;
	
	
	if(true)draw_sprite_ext(SpriteHighlightUI ,highlight, right-192, bottom-64, 1, 1, 0, #FFFFFF, highlight_alpha);
	draw_sprite(get_element_sprite(get_utility_element(equipped_utilities[3])) ,0, right-128-32, bottom-64)
	draw_sprite(get_item_sprite(equipped_utilities[3 ]) ,0, right-192, bottom-64);
	
	if(array_contains(progression_flags, "bag1"))draw_sprite_ext(SpriteHighlightUI ,highlight, right-128, bottom-128, 1, 1, 0, #FFFFFF, highlight_alpha);
	draw_sprite(get_element_sprite(get_utility_element(equipped_utilities[0])) ,0, right-128+32, bottom-128);
	draw_sprite(get_item_sprite(equipped_utilities[0]) ,0, right-128, bottom-128);
	
	if(array_contains(progression_flags, "bag2"))draw_sprite_ext(SpriteHighlightUI ,highlight,  right-192, bottom-192, 1, 1, 0, #FFFFFF, highlight_alpha);
	draw_sprite(get_element_sprite(get_utility_element(equipped_utilities[2])) ,0, right-192+32, bottom-192);
	draw_sprite(get_item_sprite(equipped_utilities[2]) ,0, right-192, bottom-192);
	
	if(array_contains(progression_flags, "bag3"))draw_sprite_ext(SpriteHighlightUI ,highlight, right-256, bottom-128, 1, 1, 0, #FFFFFF, highlight_alpha);
	draw_sprite(get_element_sprite(get_utility_element(equipped_utilities[1])) ,0, right-256+32, bottom-128);
	draw_sprite(get_item_sprite(equipped_utilities[1]) ,0, right-256, bottom-128);
	
	//
	//draw_sprite(get_element_sprite(items[?"armor"]) ,0, right-192-32, bottom-256)
	//draw_sprite(get_item_sprite("armor") ,0, right-256, bottom-256);
	
}

if(draw_gamepad_target_guide > 30) {
	draw_player_targeting_guide();
}
//draw_player_health();
draw_health(16, 16, combatant_hp, combatant_hp_max, 1.0);
draw_mana(16, 32+sprite_get_height(SpriteHealthUI), mana, mana_max, (mana_recharge_frame_counter/ get_mana_recharge_target())*100);
draw_stamina(16, 48+sprite_get_height(SpriteHealthUI)+sprite_get_height(SpriteManaUI), stamina, stamina_max, (stamina_recharge_frame_counter/ get_stamina_recharge_target())*100);
draw_player_item_ui();