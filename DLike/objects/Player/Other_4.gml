/// @description Insert description here
// You can write your code in this editor

if(instance_number(Developer) == 0) {
	instance_create_depth(0,0,0, Developer);
}
instance_create_layer(0,0, "Instances", IsoRenderer);
if(instance_number(MotionPlanning) == 0) {
	var n = instance_create_layer(0,0, "Instances", MotionPlanning);
	n.init();
	n.update();
	
}

if(instance_number(MusicPlayer) == 0) {
	instance_create_depth(0,0,0, MusicPlayer);	
}
accept_input = false;
draw_gamepad_target_guide = 0;
tx = x;
ty = y;

path_clear_points(path)

last_free = {x:x, y:y};
log("New Room:" + room_get_name(room));