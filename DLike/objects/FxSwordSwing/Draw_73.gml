/// @description Insert description here
// You can write your code in this editor



if(sprite_index == SpriteHammerSwing){
	if(image_index > collision_frame) {
		gpu_set_blendmode(bm_add);
		var t = image_index - collision_frame;
		draw_set_alpha(1-t);
		var radius = t*256;
		draw_circle_color(x + 64 * image_xscale, y + 32, radius, #235012, c_black, false)
		gpu_set_blendmode(bm_normal)
		draw_set_alpha(1);
	}
}