/// @description Insert description here
// You can write your code in this editor

event_inherited()

if(collision_frame != -1 and collision_frame != floor(image_index)) {
	exit;
}

if(array_contains(hit_already, other.id)) exit;
if(not instance_exists(source)) exit;

array_push(hit_already, other.id);
other.deal_damage(other.id, source.id, value, tags);