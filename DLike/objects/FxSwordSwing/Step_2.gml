/// @description Insert description here
// You can write your code in this editor


if(not instance_exists(source)) exit;

x += source.x - source.xprevious;
y += source.y - source.yprevious;

if(sprite_index == SpriteScythWhirlFX){
	if(round(image_index) == collision_frame){
		audio_play_sound(SoundMelee, 10, false);	
	}
}

if(windup-- > 0) {
	image_index = 0;
}

if(sprite_index == SpriteHammerSwing){
	if(image_index > collision_frame) {
		var transform_radius = 200;
		grow_tiles(x + 64 * image_xscale, y + 32, transform_radius);
		transform_tiles(x + 64 * image_xscale, y + 32, transform_radius, "hammer_to")		
		if(not audio_is_playing(SoundThud)){
			audio_play_sound(SoundThud, 10, false);	
		}
	}
}