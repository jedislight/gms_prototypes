/// @description Insert description here
// You can write your code in this editor


event_inherited();
hit_already = [source];
collision_frame = -1;
windup = 0;
cooldown_mod = 1;

if(array_contains(tags, "fire")) {
	//image_blend = #fb8b23  	
	sprite_index = SpriteAxSwing
	var tip_x = x + lengthdir_x(64, image_angle);
	var tip_y = y + lengthdir_y(64, image_angle);
	burn_tiles(tip_x, tip_y, 70)
	audio_play_sound(SoundMelee, 10, false);
	image_speed *= 2;
	cooldown_mod = 0.5;
}

else if(array_contains(tags, "water")) {
	sprite_index = SpriteSpearSwing
	for (var i = 0; i < 7; ++i) {
	    var tip_x = x + lengthdir_x(32*i, image_angle);
		var tip_y = y + lengthdir_y(32*i, image_angle);
		wet_tiles(tip_x, tip_y, 64)
	}
	audio_play_sound(SoundMelee, 10, false);
}

else if(array_contains(tags, "earth")) {
	sprite_index = SpriteHammerSwing;
	if (image_angle > 90 and image_angle < 270) {
		image_xscale = -1;
	}
	image_angle = 0;
	collision_frame = 3;
	audio_play_sound(SoundMelee, 10, false);
}

else if(array_contains(tags, "light")) {
	sprite_index = SpriteHarp;
	if (image_angle > 90 and image_angle < 270) {
		image_xscale = -1;
	}
	image_angle = 0;
	collision_frame = 38;//no collision
	with(Combatant) {
		if(object_index == Player) continue;
		pacify();
	}
	audio_play_sound(SoundHarp, 10, false);

}

else if(array_contains(tags, "dark")) {
	sprite_index = SpriteScythWhirlFX;
	collision_frame = 16;
	windup = 45;

}

else { // null
	audio_play_sound(SoundMelee, 10, false);
}