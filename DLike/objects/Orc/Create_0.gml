/// @description Insert description here
// You can write your code in this editor




// Inherit the parent event
event_inherited();

behaviors = [
	behavior_leashed_wander_once,
	behavior_target_near_player,
	behavior_launch_projectile_at_combat_target,
	behavior_loop,
];
