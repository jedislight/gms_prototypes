/// @description Insert description here
// You can write your code in this editor

event_inherited();

taught_distance = 256;

function scoot(mobile_obj, dist, dir){
	if(array_contains(tags, "water")) {
		mobile_obj.stop();
	}
	with(mobile_obj){
		var dx = lengthdir_x(dist, dir);
		var dy = lengthdir_y(dist, dir);
		if(place_empty(x+dx,y+dy, Impassibles) and place_empty(x,y, Impassibles)){
			move_and_collide(dx, dy, Impassibles,4);

		} else {
			return false;
		}
	}
	
	return true;
}

on_taught = function(dist, dir) {
	if(array_contains(tags, "water")) {
		instance_destroy(id);
		exit;
	}
	if(object_is_ancestor(target.object_index, Mobile) and object_is_ancestor(source.object_index, Mobile)){
		if(array_contains(tags, "earth")) {
			source.x = xstart;
			source.y = ystart;
			target.x = xstart;
			target.y = ystart;
			source.stop();
			target.stop();
			return;
		}
		
		var dist_to_snap = (dist-taught_distance) div 2;
		var source_scooted = scoot(source, dist_to_snap, dir);
		var target_scooted = scoot(target, dist_to_snap, dir+180);
		if(source_scooted == false or target_scooted == false){
			instance_destroy(id);
			exit;	
		}
	}
	
	if(object_is_ancestor(target.object_index, Lever) or target.object_index == Lever){
		var is_pull_side = (target.image_index == 0) != (abs(angle_difference(dir,180)) < 90);
		if(is_pull_side){
			target.hit();
			instance_destroy(id);
		} else {
			scoot(source, (dist-taught_distance), dir);
		}
		exit;
	}
	
	if(object_is_ancestor(target.object_index, PushBlockR) or target.object_index == PushBlockR){
		var normdir = dir;
		var pull_dir = abs(dir+180) mod 360;
		pull_dir = round(pull_dir /90);
		pull_dir *= 90;
		if(pull_dir == 360) pull_dir = 0;
		
		if(array_contains(target.push_dirs, pull_dir) and target.push(pull_dir)){
		} else {
			scoot(source, (dist-taught_distance), dir);
		}
	}
}

on_slack = function(dist, dir){
	if(array_contains(tags, "water")){
		var dist_to_snap = 10;
		if(object_is_ancestor(target.object_index, Mobile) and object_is_ancestor(source.object_index, Mobile)){
			var source_scooted = scoot(source, dist_to_snap, dir+180);
			var target_scooted = scoot(target, dist_to_snap, dir);
			if(source_scooted == false or target_scooted == false){
				instance_destroy(id);
				exit;	
			}
		}
		
		if(object_is_ancestor(target.object_index, PushBlockR) or target.object_index == PushBlockR){
			var pull_dir = abs(dir) mod 360;
			pull_dir = round(pull_dir /90);
			pull_dir *= 90;
			if(pull_dir == 360) pull_dir = 0;
		
			if(array_contains(target.push_dirs, pull_dir) and target.push(pull_dir)){
			} else {
				scoot(source, (dist-taught_distance), dir);
			}
		}
	}
}

tick_counter = 90;
// Inherit the parent event
if(array_contains(tags, "earth")){
	sprite_index = SpriteEntangle;	
}
if(array_contains(tags, "fire")){
	image_blend = c_red;
}
y += offset_y