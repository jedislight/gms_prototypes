/// @description Insert description here
// You can write your code in this editor




// Inherit the parent event
event_inherited();

if(instance_exists(target) and instance_exists(source)){
	x = source.x;
	y = source.y;
	var dist = point_distance(x,y, target.x, target.y);
	var dir = point_direction(x,y, target.x, target.y);
	y += offset_y;
	
	if(!array_contains(tags, "earth")){
		image_xscale = dist / 64;
		image_angle = dir;	
	}
	
	if(taught_distance <= dist){
		on_taught(dist, dir);	
	}else {
		on_slack(dist, dir);	
	}
} else {
	instance_destroy(id);	
}
if(tick_counter-- <= 0){
	tick_counter = 90
	if(array_contains(tags, "fire")){
		if(object_is_ancestor(target.object_index, Combatant)){
			target.deal_damage(target.id, source.id, 3, tags);
		}
	}
}