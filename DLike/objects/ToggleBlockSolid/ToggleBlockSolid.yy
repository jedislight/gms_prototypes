{
  "resourceType": "GMObject",
  "resourceVersion": "1.0",
  "name": "ToggleBlockSolid",
  "eventList": [
    {"resourceType":"GMEvent","resourceVersion":"1.0","name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,},
  ],
  "managed": true,
  "overriddenProperties": [
    {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"Solids","path":"objects/Solids/Solids.yy",},"propertyId":{"name":"hard_eject","path":"objects/Solids/Solids.yy",},"value":"True",},
  ],
  "parent": {
    "name": "World",
    "path": "folders/Objects/World.yy",
  },
  "parentObjectId": {
    "name": "Solids",
    "path": "objects/Solids/Solids.yy",
  },
  "persistent": false,
  "physicsAngularDamping": 0.1,
  "physicsDensity": 0.5,
  "physicsFriction": 0.2,
  "physicsGroup": 1,
  "physicsKinematic": false,
  "physicsLinearDamping": 0.1,
  "physicsObject": false,
  "physicsRestitution": 0.1,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsShapePoints": [],
  "physicsStartAwake": true,
  "properties": [
    {"resourceType":"GMObjectProperty","resourceVersion":"1.0","name":"signal","filters":[],"listItems":[],"multiselect":false,"rangeEnabled":false,"rangeMax":10.0,"rangeMin":0.0,"value":"0","varType":1,},
  ],
  "solid": false,
  "spriteId": {
    "name": "SpriteToggleBlockSolidTop",
    "path": "sprites/SpriteToggleBlockSolidTop/SpriteToggleBlockSolidTop.yy",
  },
  "spriteMaskId": {
    "name": "Mask64x64",
    "path": "sprites/Mask64x64/Mask64x64.yy",
  },
  "tags": [
    "ROOM_BUILDING",
  ],
  "visible": true,
}