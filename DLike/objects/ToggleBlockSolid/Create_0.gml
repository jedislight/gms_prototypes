/// @description Insert description here
// You can write your code in this editor

event_inherited();

sprite_index = SpriteToggleBlockSolidBase;
top_sprite = {
	up: SpriteToggleBlockSolidTop,
	down: SpriteToggleBlockSolidTop,
	left: SpriteToggleBlockSolidTop,
	right: SpriteToggleBlockSolidTop,
}

function ToggleBlockSolidListener(owner_) : MutableEventListener()constructor {
	owner=owner_;
	function listen(context){
		var s = owner.signal;
		instance_create_layer(owner.x, owner.y-64, "Instances", ToggleBlockEmpty, {signal:s, image_blend:owner.image_blend});
		instance_destroy(owner)
		if(not variable_struct_exists(context, "sound")) {
			context.sound=audio_play_sound(SoundToggleThud, 10, false);
		}
		MotionPlanning.update();
	}
	function needs_cleanup() { return not instance_exists(owner) or owner.object_index != ToggleBlockSolid };
}

mutable_events_add_listener(new ToggleBlockSolidListener(id), "signal_"+string(signal));