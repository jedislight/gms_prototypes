{
  "resourceType": "GMObject",
  "resourceVersion": "1.0",
  "name": "ToggleBlockEmpty",
  "eventList": [
    {"resourceType":"GMEvent","resourceVersion":"1.0","name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,},
  ],
  "managed": true,
  "overriddenProperties": [
    {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"Isometricish","path":"objects/Isometricish/Isometricish.yy",},"propertyId":{"name":"base_sprite","path":"objects/Isometricish/Isometricish.yy",},"value":"SpriteToggleBlockEmpty",},
    {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"Isometricish","path":"objects/Isometricish/Isometricish.yy",},"propertyId":{"name":"top_sprite_up","path":"objects/Isometricish/Isometricish.yy",},"value":"SpriteToggleBlockEmpty",},
    {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"Isometricish","path":"objects/Isometricish/Isometricish.yy",},"propertyId":{"name":"top_sprite_down","path":"objects/Isometricish/Isometricish.yy",},"value":"SpriteToggleBlockEmpty",},
    {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"Isometricish","path":"objects/Isometricish/Isometricish.yy",},"propertyId":{"name":"top_sprite_left","path":"objects/Isometricish/Isometricish.yy",},"value":"SpriteToggleBlockEmpty",},
    {"resourceType":"GMOverriddenProperty","resourceVersion":"1.0","name":"","objectId":{"name":"Isometricish","path":"objects/Isometricish/Isometricish.yy",},"propertyId":{"name":"top_sprite_right","path":"objects/Isometricish/Isometricish.yy",},"value":"SpriteToggleBlockEmpty",},
  ],
  "parent": {
    "name": "World",
    "path": "folders/Objects/World.yy",
  },
  "parentObjectId": {
    "name": "Isometricish",
    "path": "objects/Isometricish/Isometricish.yy",
  },
  "persistent": false,
  "physicsAngularDamping": 0.1,
  "physicsDensity": 0.5,
  "physicsFriction": 0.2,
  "physicsGroup": 1,
  "physicsKinematic": false,
  "physicsLinearDamping": 0.1,
  "physicsObject": false,
  "physicsRestitution": 0.1,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsShapePoints": [],
  "physicsStartAwake": true,
  "properties": [
    {"resourceType":"GMObjectProperty","resourceVersion":"1.0","name":"signal","filters":[],"listItems":[],"multiselect":false,"rangeEnabled":false,"rangeMax":10.0,"rangeMin":0.0,"value":"0","varType":1,},
  ],
  "solid": false,
  "spriteId": {
    "name": "SpriteToggleBlockEmpty",
    "path": "sprites/SpriteToggleBlockEmpty/SpriteToggleBlockEmpty.yy",
  },
  "spriteMaskId": {
    "name": "Mask64x64TL",
    "path": "sprites/Mask64x64TL/Mask64x64TL.yy",
  },
  "tags": [
    "ROOM_BUILDING",
  ],
  "visible": true,
}