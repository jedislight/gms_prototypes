/// @description Insert description here
// You can write your code in this editor

event_inherited();

sprite_index = SpriteToggleBlockEmpty;
top_sprite = {
	up: SpriteToggleBlockEmpty,
	down: SpriteToggleBlockEmpty,
	left: SpriteToggleBlockEmpty,
	right: SpriteToggleBlockEmpty,
}

function ToggleBlockEmptyListener(owner_) : MutableEventListener()constructor {
	owner=owner_;
	function listen(context){
		var s = owner.signal;
		instance_create_layer(owner.x, owner.y+64, "Instances", ToggleBlockSolid, {signal:s, image_blend:owner.image_blend})
		instance_destroy(owner);
		if(not variable_struct_exists(context, "sound")) {
			context.sound = audio_play_sound(SoundToggleThud, 10, false);
		}
		MotionPlanning.update();
	}
	function needs_cleanup() { return not instance_exists(owner) };
}

mutable_events_add_listener(new ToggleBlockEmptyListener(id), "signal_"+string(signal));