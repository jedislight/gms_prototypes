/// @description Insert description here
// You can write your code in this editor
if(player_lock){
	var active = array_length(opening)+array_length(closing) > 0;
	var player = instance_find(Player,0);
	if(instance_exists(player)){
		var lock_index = array_index_of(player.input_locking_child_objects, id)	;
		if(lock_index < 0 and active){
			array_push(player.input_locking_child_objects, id);	
		}
		
		if (lock_index >= 0 and not active){
			array_delete(player.input_locking_child_objects, lock_index, 1);	
		}
	}
}
if (tick_counter-- <= 0){
	tick_counter = tick_delay;
	if (array_length(opening) == 0 and array_length(closing) == 0) exit;
	var layer_id = layer_get_id(layer_name);
	if(layer_id == -1) exit;
	var tilemap = layer_tilemap_get_id(layer_id);
	if(tilemap = -1) exit;
	
	var update_rect = new Rect();

	for (var i = 0; i < array_length(opening); ++i) {
	    var open_offset = opening[i];
		var end_offset = {x:open_offset.x, y:open_offset.y};
		var end_type = bridge_top;
		if(open_offset.y > 0) end_type = bridge_bottom;
		if(open_offset.x < 0) end_type = bridge_left;
		if(open_offset.x > 0) end_type = bridge_right;
		var begin_type = bridge_bottom;
		if(open_offset.y > 0) begin_type = bridge_top;
		if(open_offset.x < 0) begin_type = bridge_right;
		if(open_offset.x > 0) begin_type = bridge_left;
		var tile_mid_type = bridge_horizontal;
		if(open_offset.y != 0) tile_mid_type = bridge_vertical;
		var my_bridge_types = [tile_mid_type, end_type, begin_type];

		for(var d = 1; true; ++d){
			var edge_type = begin_type;
			if(d > 1) edge_type = end_type;
			
			var tiledata = tilemap_get_at_pixel(tilemap, x+open_offset.x*d, y+open_offset.y*d);
			if(tiledata == -1) {
				break;
			}
			if (tile_get_index(tiledata) == water_tile) {
				tiledata = tile_set_index(tiledata, edge_type);
				tilemap_set_at_pixel(tilemap, tiledata, x+open_offset.x*d, y+open_offset.y*d);
				update_rect.grow_to_contain( x+open_offset.x*d, y+open_offset.y*d);
				if( d > 2) {
					tiledata = tile_set_index(tiledata, tile_mid_type);
					tilemap_set_at_pixel(tilemap, tiledata, x+open_offset.x*(d-1), y+open_offset.y*(d-1));	
					update_rect.grow_to_contain(x+open_offset.x*(d-1), y+open_offset.y*(d-1));
				}
				break;	
			} else if (not array_contains(my_bridge_types, tile_get_index(tiledata))){
				array_delete(opening, i, 1);
				--i;
				break;	
			}
		}
	}
	
	for (var i = 0; i < array_length(closing); ++i) {
	    var open_offset = closing[i];
		var end_offset = {x:open_offset.x, y:open_offset.y};
		var end_type = bridge_top;
		if(open_offset.y > 0) end_type = bridge_bottom;
		if(open_offset.x < 0) end_type = bridge_left;
		if(open_offset.x > 0) end_type = bridge_right;
		var begin_type = bridge_bottom;
		if(open_offset.y > 0) begin_type = bridge_top;
		if(open_offset.x < 0) begin_type = bridge_right;
		if(open_offset.x > 0) begin_type = bridge_left;
		var tile_mid_type = bridge_horizontal;
		if(open_offset.y != 0) tile_mid_type = bridge_vertical;

		for(var d = 1; true; ++d){
			var edge_type = begin_type;
			if(d > 1) edge_type = end_type;
			
			var tiledata = tilemap_get_at_pixel(tilemap, x+open_offset.x*d, y+open_offset.y*d);
			if(tiledata == -1) {
				break;
			}
			if (tile_get_index(tiledata) == end_type) {
				tiledata = tile_set_index(tiledata, water_tile);
				tilemap_set_at_pixel(tilemap, tiledata, x+open_offset.x*d, y+open_offset.y*d);
				update_rect.grow_to_contain(x+open_offset.x*d, y+open_offset.y*d);
				if( d > 1) {
					tiledata = tile_set_index(tiledata, end_type);
					tilemap_set_at_pixel(tilemap, tiledata, x+open_offset.x*(d-1), y+open_offset.y*(d-1));	
					update_rect.grow_to_contain(x+open_offset.x*(d-1), y+open_offset.y*(d-1));
				}
				
				var l = ds_list_create();
				var hit = collision_rectangle_list(x+open_offset.x*d, y+open_offset.y*d, x+open_offset.x*d+64, y+open_offset.y*d+64, Mobile, false, true, l, false);
				if(hit) {
					for (var h = 0; h < ds_list_size(l); ++h) {
					    var m = l[| h];
						m.x -= open_offset.x;
						m.y -= open_offset.y;
					}	
				}
				ds_list_destroy(l)
				break;	
				
			} else if (tile_get_index(tiledata) == water_tile or not array_contains(bridge_all, tile_get_index(tiledata))){
				array_delete(closing, i, 1);
				--i;
				break;	
			}
		}
	}
	
	if(update_rect.any_set) {
		MotionPlanning.invalidate_rect(update_rect.x1, update_rect.y1, update_rect.x2, update_rect.y2);	
		audio_play_sound(SoundThud, 10, false);
	}
}