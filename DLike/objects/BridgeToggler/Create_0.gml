/// @description Insert description here
// You can write your code in this editor




event_inherited();
opening = [];
closing = [];

adjacent_offsets = [
	{x:0, y:64},
	{x:0, y:-64},
	{x:64, y:0},
	{x:-64, y:0}
]

water_tile = 340;

bridge_top = 109
bridge_bottom = 173
bridge_left = 201;
bridge_right = 203;
bridge_ends = [bridge_top, bridge_left, bridge_right, bridge_bottom]
bridge_horizontal = 202;
bridge_vertical = 141;
bridge_all = [bridge_top, bridge_left, bridge_right, bridge_bottom, bridge_horizontal,bridge_vertical ]

tick_delay = 20;
tick_counter = 0;

function BridgeTogglerListener(owner_) : MutableEventListener()constructor {
	owner=owner_;
	function listen(context){
		with(owner){
			if(array_length(opening)+array_length(closing) > 0) return;
			tick_counter = 0;
			var layer_id = layer_get_id(layer_name);
			if(layer_id == -1) return;
			var tilemap = layer_tilemap_get_id(layer_id);
			if(tilemap = -1) return;
		
			for (var i = 0; i < array_length(adjacent_offsets); ++i) {
			    var offset = adjacent_offsets[i];
				var tiledata = tilemap_get_at_pixel(tilemap, x+offset.x, y+offset.y);
				if(tiledata == -1) continue;
				if(tile_get_index(tiledata) == water_tile) {
					array_push(opening, {x:offset.x, y:offset.y});	
				} else if (array_contains(bridge_ends, tiledata)) {
					array_push(closing, {x:offset.x, y:offset.y});
				}
			}
		}
	}
	function needs_cleanup() { return not instance_exists(owner) };
}
mutable_events_add_listener(new BridgeTogglerListener(id), "signal_"+string(signal));