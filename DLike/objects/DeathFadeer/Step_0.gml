/// @description Insert description here
// You can write your code in this editor


alpha_speed += alpha_acceleration;

alpha += alpha_speed;

if(alpha > 1.6 and alpha_speed > 0) {
	persistent = true;
	if(file_exists("save")){
		var n = instance_create_depth(0,0,0, Player);
		n.load();
	} else {
		game_restart();	
	}
	alpha_speed *= -1;
	alpha_acceleration *= -1;
}

if( alpha < 0 and alpha_speed < 0) {
	instance_destroy();	
}