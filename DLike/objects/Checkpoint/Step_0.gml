/// @description Insert description here
// You can write your code in this editor


event_inherited();
var player = instance_find(Player, 0);
if(instance_exists(player) and point_distance(x,y, player.x, player.y) < 128){
	if(not used) {
		set_used()
		var healing =-1*(player.combatant_hp_max-player.combatant_hp);
		if(healing != 0){
			player.deal_damage(player, id, healing, []);
		}
		player.save();
		audio_play_sound(SoundCheckpoint, 0, false);
	}
}