/// @description Insert description here
// You can write your code in this editor




// Inherit the parent event
image_speed = shared_image_speed_mod
event_inherited();
_super_handle_sprite_event = handle_sprite_event;
function handle_sprite_event(msg) {
	_super_handle_sprite_event(msg);
	if (msg == "trigger") {
		if( instance_exists(target)) {
			target.deal_damage(target, id, -heal, []);
		}
		instance_destroy(id);
	}
}