/// @description Insert description here
// You can write your code in this editor



// Inherit the parent event
event_inherited();

iso_visible = false;

if(not force and instance_exists(collision_point(x,y, object_index, false, true))) {
	instance_destroy(id);	
}

cull = false;