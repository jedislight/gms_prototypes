/// @description Insert description here
// You can write your code in this editor


cell_size = 64;
optimize = true;
max_placeholder_size = 15;
mp_grid = mp_grid_create(
	0,0,
	room_width div cell_size, room_height div cell_size,
	cell_size, cell_size
);

tile_metadata_map = ds_map_create();
tile_metadata_map[? TileSetOverworld] = ds_map_create();
tile_metadata_map[? TileSetUnderworld] = ds_map_create();
var overworld_metadata = tile_metadata_map[? TileSetOverworld];
var underworld_metadata = tile_metadata_map[? TileSetUnderworld];
overworld_metadata[? 1__] = { grow_to: [5, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1] }
overworld_metadata[? 35_] = { solid: true }
overworld_metadata[? 137] = { solid: true }
overworld_metadata[? 138] = { solid: true }
overworld_metadata[? 64_] = { solid: true }
overworld_metadata[? 65_] = { solid: true }
overworld_metadata[? 66_] = { solid: true }
overworld_metadata[? 67_] = { solid: true }
overworld_metadata[? 128] = { solid: true }
overworld_metadata[? 129] = { solid: true }
overworld_metadata[? 130] = { solid: true }
overworld_metadata[? 131] = { solid: true }
overworld_metadata[? 96_] = { solid: true }
overworld_metadata[? 97_] = { solid: true }
overworld_metadata[? 99_] = { solid: true }
overworld_metadata[? 115] = { solid: true }
overworld_metadata[? 116] = { solid: true }
overworld_metadata[? 368] = { solid: true }
overworld_metadata[? 369] = { solid: true }
overworld_metadata[? 370] = { solid: true }
overworld_metadata[? 400] = { solid: true }
overworld_metadata[? 401] = { solid: true }
overworld_metadata[? 402] = { solid: true }
overworld_metadata[? 432] = { solid: true }
overworld_metadata[? 433] = { solid: true }
overworld_metadata[? 434] = { solid: true }
overworld_metadata[? 304] = { solid: true }
overworld_metadata[? 306] = { solid: true }
overworld_metadata[? 305] = { solid: true }
overworld_metadata[? 336] = { solid: true }
overworld_metadata[? 338] = { solid: true }
overworld_metadata[? 368] = { solid: true }
overworld_metadata[? 369] = { solid: true }
overworld_metadata[? 370] = { solid: true }
overworld_metadata[? 398] = { solid: true }
overworld_metadata[? 399] = { solid: true }
overworld_metadata[? 430] = { solid: true }
overworld_metadata[? 431] = { solid: true }
overworld_metadata[? 388] = { solid: true }
overworld_metadata[? 389] = { solid: true }
overworld_metadata[? 390] = { solid: true }
overworld_metadata[? 420] = { solid: true }
overworld_metadata[? 422] = { solid: true }
overworld_metadata[? 452] = { solid: true }
overworld_metadata[? 453] = { solid: true }
overworld_metadata[? 454] = { solid: true }
overworld_metadata[? 386] = { solid: true }
overworld_metadata[? 387] = { solid: true }
overworld_metadata[? 418] = { solid: true }
overworld_metadata[? 419] = { solid: true }
overworld_metadata[? 160] = { solid: true }
overworld_metadata[? 161] = { solid: true }
overworld_metadata[? 162] = { solid: true }
overworld_metadata[? 163] = { solid: true }
overworld_metadata[? 192] = { solid: true }
overworld_metadata[? 193] = { solid: true }
overworld_metadata[? 195] = { solid: true }
overworld_metadata[? 224] = { solid: true }
overworld_metadata[? 225] = { solid: true }
overworld_metadata[? 226] = { solid: true }
overworld_metadata[? 227] = { solid: true }
overworld_metadata[? 340] = { impassible: true }
overworld_metadata[? 2__] = { solid: true }
overworld_metadata[? 46_] = { solid: true }
overworld_metadata[? 75_] = { solid: true }
overworld_metadata[? 76_] = { solid: true }
overworld_metadata[? 139] = { solid: true }
overworld_metadata[? 140] = { solid: true }
overworld_metadata[? 230] = { solid: true }
overworld_metadata[? 231] = { solid: true }
overworld_metadata[? 240] = { solid: true }
overworld_metadata[? 241] = { solid: true }
overworld_metadata[? 242] = { solid: true }
overworld_metadata[? 243] = { solid: true }
overworld_metadata[? 244] = { solid: true }
overworld_metadata[? 245] = { solid: true }
overworld_metadata[? 278] = { solid: true }
overworld_metadata[? 177] = { solid: true }
overworld_metadata[? 180] = { solid: true }
overworld_metadata[? 181] = { solid: true }
overworld_metadata[? 9__] = { solid: true }
overworld_metadata[? 320] = { solid: true }
overworld_metadata[? 238] = { solid: true }
overworld_metadata[? 239] = { solid: true }
overworld_metadata[? 206] = { solid: true }
overworld_metadata[? 207] = { solid: true }
overworld_metadata[? 33_] = { solid: true }
overworld_metadata[? 142] = { solid: true }
overworld_metadata[? 143] = { solid: true }
overworld_metadata[? 117] = { solid: true }
overworld_metadata[? 118] = { solid: true }
overworld_metadata[? 182] = { solid: true }
overworld_metadata[? 460] = { impassible: true, wet_to: 8 }
overworld_metadata[? 461] = { impassible: true, wet_to: 8 }
overworld_metadata[? 462] = { impassible: true, wet_to: 8 }
overworld_metadata[? 463] = { impassible: true, wet_to: 8 }
overworld_metadata[? 39_] = { solid: true, burns_to: 0 }
overworld_metadata[? 77_] = { always_clear: true }
overworld_metadata[? 109] = { always_clear: true }
overworld_metadata[? 141] = { always_clear: true }
overworld_metadata[? 173] = { always_clear: true }
overworld_metadata[? 201] = { always_clear: true }
overworld_metadata[? 202] = { always_clear: true }
overworld_metadata[? 203] = { always_clear: true }
overworld_metadata[? 204] = { always_clear: true }
overworld_metadata[? 260] = { burns_to: 0 }
overworld_metadata[? 261] = { burns_to: 0 }
overworld_metadata[? 262] = { burns_to: 0 }
overworld_metadata[? 263] = { burns_to: 0 }
overworld_metadata[? 292] = { burns_to: 0 }
overworld_metadata[? 293] = { burns_to: 0 }
overworld_metadata[? 294] = { burns_to: 0 }
overworld_metadata[? 295] = { burns_to: 0 }
overworld_metadata[? 324] = { burns_to: 0 }
overworld_metadata[? 325] = { burns_to: 0 }
overworld_metadata[? 326] = { burns_to: 0 }
overworld_metadata[? 327] = { burns_to: 0 }
overworld_metadata[? 356] = { burns_to: 0 }
overworld_metadata[? 357] = { burns_to: 0 }
overworld_metadata[? 385] = { burns_to: 0 }
overworld_metadata[? 359] = { burns_to: 0 }
overworld_metadata[? 5__] = { burns_to: 1 }
overworld_metadata[? 6__] = { burns_to: 1 }
overworld_metadata[? 34_] = { solid: true, hammer_to: 352 }
overworld_metadata[? 352] = { always_clear: true }


underworld_metadata[? 26_] = { solid: true }
underworld_metadata[? 27_] = { solid: true }
underworld_metadata[? 28_] = { solid: true }
underworld_metadata[? 39_] = { solid: true }
underworld_metadata[? 40_] = { solid: true }
underworld_metadata[? 41_] = { solid: true }
underworld_metadata[? 52_] = { solid: true }
underworld_metadata[? 53_] = { solid: true }
underworld_metadata[? 54_] = { solid: true }
underworld_metadata[? 65_] = { solid: true }
underworld_metadata[? 66_] = { solid: true }
underworld_metadata[? 67_] = { solid: true }
underworld_metadata[? 78_] = { solid: true }
underworld_metadata[? 79_] = { solid: true }
underworld_metadata[? 80_] = { solid: true }
underworld_metadata[? 29_] = { solid: true }
underworld_metadata[? 30_] = { solid: true }
underworld_metadata[? 42_] = { solid: true }
underworld_metadata[? 43_] = { solid: true }
underworld_metadata[? 68_] = { solid: true }
underworld_metadata[? 69_] = { solid: true }
underworld_metadata[? 70_] = { solid: true }
underworld_metadata[? 81_] = { solid: true }
underworld_metadata[? 82_] = { solid: true }
underworld_metadata[? 83_] = { solid: true }
underworld_metadata[? 91_] = { solid: true }
underworld_metadata[? 92_] = { solid: true }
underworld_metadata[? 93_] = { solid: true }
underworld_metadata[? 94_] = { solid: true }
underworld_metadata[? 95_] = { solid: true }
underworld_metadata[? 96_] = { solid: true }


function get_tile_metadata(tile_set, tile_index, variable_name, default_value){
	var set_map = tile_metadata_map[? tile_set];
	if(is_undefined(set_map)) return default_value;
	var metadata = set_map[? tile_index];
	if(is_undefined(metadata)) return default_value;
	var value = variable_struct_get(metadata, variable_name);
	if(is_undefined(value)) return default_value;
	return value;
}

update = function() {
	mp_grid_clear_all(mp_grid);
	mp_grid_add_instances(mp_grid, Impassibles, true);
}

init = function(xlimit1, ylimit1, xlimit2, ylimit2) {
	var layers = layer_get_all();
	var always_clear_tiles = [];
	var placeholder_grid = ds_grid_create(room_width div 64, room_height div 64)
	ds_grid_clear(placeholder_grid, noone)
	for(var i = array_length(layers)-1; i >= 0; --i) {
		var layer_id = layers[i];
		var tile_map = layer_tilemap_get_id(layer_id);
		if(tile_map == -1) continue;
		var tile_set = tilemap_get_tileset(tile_map);

		var start_x = 0;
		var start_y = 0;
		var end_x = room_width/64;
		var end_y = room_height/64;
		if(not is_undefined(xlimit1)) {
			start_x = max(start_x, xlimit1 div 64);
			start_y = max(start_y, ylimit1 div 64);
			end_x = min(xlimit2/64+64, end_x);
			end_y = min(ylimit2/64+64, end_y);
		}
		for(var tile_x = start_x; tile_x < end_x; ++tile_x) for(var tile_y = start_y; tile_y < end_y; ++tile_y){
			var tile_data = tilemap_get(tile_map, tile_x, tile_y);
			var tile_index = tile_get_index(tile_data);
			if(get_tile_metadata(tile_set, tile_index, "solid", false)){
				placeholder_grid[# tile_x, tile_y] = TileSolidPlaceholder;
			}
			if(get_tile_metadata(tile_set, tile_index, "impassible", false)){
				placeholder_grid[# tile_x, tile_y] = TileImpassiblePlaceholder;
			}
			if(get_tile_metadata(tile_set, tile_index, "always_clear", false)){
				array_push(always_clear_tiles, {x:tile_x*64, y:tile_y*64});
			}
		}
	}
	
	for (var gx = 0; gx < room_width div 64; ++gx) for (var gy = 0; gy < room_height div 64; ++gy){
	    if(placeholder_grid[# gx, gy] != noone){
			instance_create_layer(gx*64, gy*64, "Instances", placeholder_grid[# gx, gy]);	
		}
	}
	ds_grid_destroy(placeholder_grid)
	
	with(TileSolidPlaceholder){
		for (var i = 0; i < array_length(always_clear_tiles); ++i) {
		    var act = always_clear_tiles[i];
			if(x == act.x and y == act.y){
				instance_destroy(id);	
			}
		}	
	}
	
	with(TileImpassiblePlaceholder){
		for (var i = 0; i < array_length(always_clear_tiles); ++i) {
		    var act = always_clear_tiles[i];
			if(x == act.x and y == act.y){
				instance_destroy(id);	
			}
		}	
	}
	
	if (optimize) {
		do_optimizations();
	}
}

function fragment(x1, y1, x2, y2) {
	bounds = new Rect();
	var l = ds_list_create()
	var hit = collision_rectangle_list(x1, y1, x2, y2, TileSolidPlaceholder, false, false, l, false);
	hit = hit or collision_rectangle_list(x1, y1, x2, y2, TileImpassiblePlaceholder, false, false, l, false);
	if(hit){
		for (var i = 0; i < ds_list_size(l); ++i) {
		    var p = l[| i];

			if(p.image_xscale == 1 and p.image_yscale == 1) continue;
			bounds.grow_to_contain(p.x, p.y)
			bounds.grow_to_contain(p.x +p.sprite_width, p.y +p.sprite_height);
			for(var px = p.x; px < p.x +p.sprite_width; px += 64) for(var py = p.y; py < p.y +p.sprite_height; py += 64) {
				instance_create_layer(px, py, "Instances", p.object_index, {force:true});
			}
			
			instance_destroy(p);			
		}	
	}
	
	ds_list_destroy(l);
	return bounds;
}

function do_optimizations_for(obj) {
	with(obj) {
		if(cull) continue;
		if(image_xscale >= other.max_placeholder_size) continue;
		var rn = collision_point(x+sprite_width, y, obj, false, true);
		if(not instance_exists(rn) or (rn.image_xscale + image_xscale>= other.max_placeholder_size)) continue;
		if(rn.x == x+sprite_width and rn.y == y and rn.image_yscale == image_yscale) {
			image_xscale += rn.image_xscale;
			rn.cull = true;
			instance_destroy(rn);
		}
	}

	with(obj) {
		if(cull) continue;
		if(image_yscale >= other.max_placeholder_size) continue;
		var dn = collision_point(x, y+sprite_height, obj, false, true);
		if(not instance_exists(dn) or (dn.image_yscale + image_yscale >= other.max_placeholder_size)) continue;
		if(dn.image_xscale == image_xscale and dn.x == x) {
			image_yscale += dn.image_yscale;
			dn.cull = true;
			instance_destroy(dn);
		}
	}		
}
function do_optimizations() {
	do_optimizations_for(TileSolidPlaceholder);
	do_optimizations_for(TileImpassiblePlaceholder);
}

function invalidate_all(){
	var start_time = current_time;
	instance_destroy(TileSolidPlaceholder);
	instance_destroy(TileImpassiblePlaceholder);
	MotionPlanning.init(0,0, room_width, room_height)
	MotionPlanning.update();
	var end_time = current_time;
	log(string("MOTION INVALIDATE ALL: {0}", end_time - start_time))
}

function invalidate_rect(x1, y1, x2, y2){
	var start_time = current_time;
	rebuild_rect = MotionPlanning.fragment(x1, y1, x2, y2);
	var l = ds_list_create();
	var hit = collision_rectangle_list(x1-64, y1-64, x2+64, y2+64, TileSolidPlaceholder, false, true, l, false);
	hit = hit or collision_rectangle_list(x1-64, y1-64, x2+64, y2+64, TileImpassiblePlaceholder, false, true, l, false);
	if(hit) {
		for (var i = 0; i < ds_list_size(l); ++i) {
			instance_destroy(l[| i]);
		}	
	}
	ds_list_destroy(l);
	MotionPlanning.init(rebuild_rect.x1, rebuild_rect.y1, rebuild_rect.x2, rebuild_rect.y2)
	MotionPlanning.update();
	var end_time = current_time;
	log(string("MOTION INVALIDATE RECT: {0}", end_time - start_time))
}

function invalidate_radius(xx, yy, r){
	var start_time = current_time;
	rebuild_rect = MotionPlanning.fragment(xx-r-64, yy-r-64, xx+r+64, yy+r+64);
	var l = ds_list_create();
	var hit = collision_circle_list(xx, yy, r*1.5, TileSolidPlaceholder, false, true, l, false);
	hit = hit and collision_circle_list(xx, yy, r*1.5, TileImpassiblePlaceholder, false, true, l, false);
	if(hit) {
		for (var i = 0; i < ds_list_size(l); ++i) {
			instance_destroy(l[| i]);
		}	
	}
	ds_list_destroy(l);
	MotionPlanning.init(rebuild_rect.x1, rebuild_rect.y1, rebuild_rect.x2, rebuild_rect.y2)
	MotionPlanning.update();	
	var end_time = current_time;
	log(string("MOTION INVALIDATE RADIUS: {0}", end_time - start_time))
}

invalidate_area = invalidate_radius;