{
  "resourceType": "GMObject",
  "resourceVersion": "1.0",
  "name": "RoomTransition",
  "eventList": [
    {"resourceType":"GMEvent","resourceVersion":"1.0","name":"","collisionObjectId":{"name":"Player","path":"objects/Player/Player.yy",},"eventNum":0,"eventType":4,"isDnD":false,},
  ],
  "managed": true,
  "overriddenProperties": [],
  "parent": {
    "name": "World",
    "path": "folders/Objects/World.yy",
  },
  "parentObjectId": null,
  "persistent": false,
  "physicsAngularDamping": 0.1,
  "physicsDensity": 0.5,
  "physicsFriction": 0.2,
  "physicsGroup": 1,
  "physicsKinematic": false,
  "physicsLinearDamping": 0.1,
  "physicsObject": false,
  "physicsRestitution": 0.1,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsShapePoints": [],
  "physicsStartAwake": true,
  "properties": [
    {"resourceType":"GMObjectProperty","resourceVersion":"1.0","name":"target_room","filters":[
        "GMRoom",
      ],"listItems":[],"multiselect":false,"rangeEnabled":false,"rangeMax":10.0,"rangeMin":0.0,"value":"Room1","varType":5,},
    {"resourceType":"GMObjectProperty","resourceVersion":"1.0","name":"target_anchor","filters":[],"listItems":[],"multiselect":false,"rangeEnabled":true,"rangeMax":25.0,"rangeMin":0.0,"value":"0","varType":1,},
  ],
  "solid": false,
  "spriteId": {
    "name": "Sprite22",
    "path": "sprites/Sprite22/Sprite22.yy",
  },
  "spriteMaskId": null,
  "tags": [
    "ROOM_BUILDING",
  ],
  "visible": false,
}