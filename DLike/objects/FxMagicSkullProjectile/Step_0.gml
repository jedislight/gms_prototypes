/// @description Insert description here
// You can write your code in this editor

event_inherited();
if (speed = 0 and instance_exists(target)){
	direction = point_direction(x, y, target.x, target.y)
}


if(array_contains(tags, "fire")){
	burn_tiles(x, y, 128);
}

if(array_contains(tags, "earth")){
	grow_tiles(x, y, 128);
}

if(array_contains(tags, "water")){
	wet_tiles(x, y, 128);
}