/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

combatant_floating_damage_text_ttl -= 1;
if(combatant_floating_damage_text_ttl <= 0) {
	combatant_floating_damage_text_ttl = 0;
	combatant_floating_damage_accumulator = 0;
}

if(!travel_sprites_enabled) {
	tx = x;
	ty = y;
}


if(combatant_hp <= 0) {
	on_death();
	instance_destroy(id);	
	exit;
}

if( combatant_hp < combatant_hp_max) {
	if(object_index != Player) {
		with(Mobile) {
			if(object_index != Player){
				unpacify();	
			}
		}
	}
}

if(tick_counter-- <= 0){
	on_tick();
	tick_counter = tick_frames;
}