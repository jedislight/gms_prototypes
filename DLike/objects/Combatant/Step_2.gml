/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(combatant_god_mode) {
		combatant_hp = combatant_hp_max;	
}

if(not travel_sprites_enabled) {
	shared_image_speed = 1 / 60 * shared_image_speed_mod * combatant_attack_speed_mod;	
	var ts = get_top_sprite(direction);
	if(shared_image_index > sprite_get_number(ts)-1) {
		set_travel_top_sprites();	
	}
}
if(not place_empty(x,y, Impassibles)){
	++stuck_damage_accumulator;
} else {
	stuck_damage_accumulator -= 5;	
}
stuck_damage_accumulator = clamp(stuck_damage_accumulator, 0, 100);
if(stuck_damage_accumulator >= 100 and frame_counter mod 10 == 0){
	combatant_hp -= 1;	
}