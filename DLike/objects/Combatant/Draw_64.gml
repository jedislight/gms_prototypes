/// @description Insert description here
// You can write your code in this editor
event_inherited();

var s = get_top_sprite(direction);

var camx = camera_get_view_x(view_get_camera(0));
var camy = camera_get_view_y(view_get_camera(0));	

if(attention != -1 ){
	var height = sprite_get_height(s)
	var width = sprite_get_width(s);
	var radius = max(height, width)/2;
	var cx = x;
	var cy = y - height/8;
	var segment_count = 64;
	var segment_arc = 360 / segment_count;
	var thickness = 5;
	var percent_hp = combatant_hp / combatant_hp_max;
	var missing_color = c_black;
	for(var i = 1; i <= segment_count; ++i) {
		var segment_color_start = attention;
		var segment_color_end = attention;
		var p = i-1;
		var percent_segment_start = i / segment_count;
		if(percent_segment_start > percent_hp) {
			segment_color_start = missing_color;
		}
			
		var percent_segment_end = p / segment_count;
		if(percent_segment_end > percent_hp) {
			segment_color_end = missing_color;
		}
			
		var px = cx+lengthdir_x(radius, segment_arc*p);
		var py = cy+lengthdir_y(radius, segment_arc*p);
			
		var ix = cx+lengthdir_x(radius, segment_arc*i);
		var iy = cy+lengthdir_y(radius, segment_arc*i);
		draw_line_width_color(ix-camx,iy-camy,px-camx,py-camy, thickness, segment_color_start, segment_color_end);
	}
	draw_set_halign(fa_center);
	draw_set_valign(fa_bottom);
	draw_set_font(FontAttentionText);
	draw_text_color(1+x-camx, 1+cy-radius+5-camy, name + " - " + string(combatant_level), c_black, c_black, c_black, c_black, 1.0);
	draw_text_color(x-camx, cy-radius+5-camy, name + " - " + string(combatant_level), attention, attention, attention, attention, 1.0);
}


if(combatant_floating_damage_text_ttl > 0) {
	var s = get_top_sprite(direction);


	var text = string(abs(combatant_floating_damage_accumulator))
	var color = combatant_floating_damage_text_colors.zero;
	if(combatant_floating_damage_accumulator > 0) {
		color = combatant_floating_damage_text_colors.harm;
		text = "- " + text;
	}else if( combatant_floating_damage_accumulator < 0) {
		color = combatant_floating_damage_text_colors.help;
		text = "+ " + text;
	}
	
	draw_set_font(FontAttentionText);
	draw_set_halign(fa_center);
	draw_set_valign(fa_bottom);
	draw_set_alpha(max(0.5, (30+combatant_floating_damage_text_ttl)/60))
	draw_set_color(c_black)
	draw_text(1+x-camx, 1+y-sprite_get_height(s)-camy, text);
	draw_set_color(color);
	draw_text(x-camx, y-sprite_get_height(s)-camy, text);
		
	draw_set_alpha(1.0)	
}