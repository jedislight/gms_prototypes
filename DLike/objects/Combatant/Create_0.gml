/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

combatant_hp_max = combatant_hp;
combat_target = noone;
combatant_level = 1;
combatant_floating_damage_accumulator = 0;
combatant_floating_damage_text_ttl = 0;
combatant_floating_damage_text_colors = {
	help: c_lime,
	harm: c_white,
	zero: c_white
}
tick_frames = 120;
tick_counter = tick_frames;
	
items = ds_map_create();
items[? "armor"] = _armor;
items[? "sword"] = "";
items[? "shield"] = "";
items[? "boots"] = "";
items[? "javelin"] = "";

	
function on_death(){}
function on_tick() {
	if(items[? "armor"] == "dark") {
		deal_damage(id, id, 1, ["dark"]);
	}
}
	
combatant_god_mode = false;

// random starting facing
tx = x + random_range(-1,1);
ty = y + random_range(-1,1);
	
attention = -1;

stuck_damage_accumulator = 0;


function set_attack_top_sprites() {
	travel_sprites_enabled = false;
	if(top_sprite.left != attack_sprite_left) {
		image_index = 0;
		shared_image_index = 0;
		top_sprite_change_delay = 0;
		//log(string(name) + " " + string(id) + " starting attack animation")
	}
	top_sprite = {
		up: attack_sprite_up,
		down: attack_sprite_down,
		left: attack_sprite_left,
		right: attack_sprite_right,
	}
}

function deal_damage(target, source, value, tags){
	var target_level = target.combatant_level;
	var source_level = target.combatant_level;
	var t = array_concat(tags, [])
	if(instance_exists(source) and variable_instance_exists(source, "combatant_level")){
		source_level = source.combatant_level;	
		t = array_concat(t, source.combatant_damage_tags)
	}
	
	var damage_event = {
		value: value,
		add: 0,
		mul: source_level / target_level,
		target: target,
		source: source,
		tags: t,
		zero: false
	}
	mutable_events_broadcast("damage", damage_event);
	var final_damage = floor((damage_event.value * damage_event.mul) + damage_event.add);	
	if(damage_event.zero) final_damage = 0;
	target.combatant_hp -= final_damage;
	target.combatant_hp = clamp(target.combatant_hp, 0, target.combatant_hp_max);
	target.combatant_floating_damage_accumulator += final_damage;
	target.combatant_floating_damage_text_ttl = 60;
}

_super_handle_sprite_event = handle_sprite_event;
function handle_sprite_event(msg) {
	_super_handle_sprite_event(msg);
	if (msg == "impact") {
		if( instance_exists(combat_target)) {
			audio_play_sound(SoundMelee, 0, false);
			if(point_distance(combat_target.x, combat_target.y, x, y) < combatant_attack_range*1.1) {
				deal_damage(combat_target, id,  default_damage, []);
			}
		}
	}
}


function behavior_attack_combat_target_no_interupt() {
	return behavior_attack_combat_target(true);
}

function behavior_attack_combat_target(wait_for_attack_animation_to_finish) {
	if ( is_undefined(wait_for_attack_animation_to_finish)) wait_for_attack_animation_to_finish = false;
	if(not instance_exists(combat_target)){
		return false;
	}
	var pd = point_distance(combat_target.x, combat_target.y, x, y);
	
	var attacking = travel_sprites_enabled == false;
	if(attacking and wait_for_attack_animation_to_finish){
		direction = point_direction(x,y, combat_target.x, combat_target.y);
		return true;
	}
	if(attacking and pd < combatant_attack_range * 1.1){
		direction = point_direction(x,y, combat_target.x, combat_target.y);
		return true;
	}
	
	if(pd < combatant_attack_range) {
		set_attack_top_sprites();
		shared_image_speed = 1/60 * self.shared_image_speed_mod;
		shared_image_index = 0;
	} else if(point_distance(tx, ty, combat_target.x, combat_target.y) > combatant_attack_range) {
		set_travel_top_sprites();
		top_sprite_change_delay=0;
		tx = combat_target.x;
		ty = combat_target.y;
	}
	
	var l = ds_list_create();
	var hit = collision_line_list(combat_target.x, combat_target.y, x, y, Combatant, true, true, l, false);
	if(hit) {
		for (var i = 0; i < ds_list_size(l); ++i) {
		    var n = l[| i];
			if(n.id == combat_target.id) continue;
			
			var player_to_n = point_direction(combat_target.x, combat_target.y, n.x, n.y);
			var encircle_dir = player_to_n + 90;
			if(instance_seed mod 2 == 0) {
				encircle_dir += 180;	
			}
			tx = combat_target.x + lengthdir_x(sprite_width*5, encircle_dir);
			ty = combat_target.y + lengthdir_y(sprite_width*5, encircle_dir);
		}	
	}
	ds_list_destroy(l);
	
	return true;
}

projectile_object_type = FxOrcJavelinProjectile;
projectile_cooldown = 60;
projectile_last_time = frame_counter;

function behavior_launch_projectile_at_combat_target(){
	if (not instance_exists(combat_target)) return false;
	if(frame_counter > projectile_last_time + 60) {
		var l = ds_list_create();
		var hit = collision_line_list(x,y, combat_target.x, combat_target.y, Solids, true, false, l, false);
		ds_list_destroy(l)
		if(not hit){
			var launch_dir =point_direction(x,y, combat_target.x, combat_target.y);
			var scatter = random_range(-14,14);
			launch_dir += scatter;
			instance_create_layer(x,y, "Instances", projectile_object_type, {source:id, direction: launch_dir, target:combat_target});
			projectile_last_time = frame_counter;
			direction = launch_dir;
		}
	}
	
	return false;
}

function behavior_target_near_player(){
	combat_target = noone;
	var player = instance_nearest(x,y, Player);
	
	if(instance_exists(player) and point_distance(x,y, player.x, player.y) < 700){
		combat_target = player;	
	}
	return false;// always continue to next behavior
}

function behavior_try_target_near_player(){
	var player = instance_nearest(x,y, Player);
	
	if(instance_exists(player) and point_distance(x,y, player.x, player.y) < 700){
		combat_target = player;	
	}
	return false;// always continue to next behavior
}

function behavior_target_near_combatant() {
	combat_target = noone;
	instance_deactivate_object(object_index);
	var n = instance_nearest(x,y, Combatant);
	instance_activate_object(object_index);
	
	if(instance_exists(n) and point_distance(x,y, n.x, n.y) < 700){
		combat_target = n;	
	}
	return false;// always continue to next behavior
}

function behavior_avoid_combat_target() {
	if(not travel_sprites_enabled) return true;
	if(instance_exists(combat_target)){
		var away = point_direction(combat_target.x, combat_target.y, x, y);
		var dist = point_distance(combat_target.x, combat_target.y, x, y);
		if (dist > combatant_attack_range*.9) {
			return false;	
		}
		
		tx = x + lengthdir_x(combatant_attack_range*.9-dist, away);
		ty = y + lengthdir_y(combatant_attack_range*.9-dist, away);
		set_travel_top_sprites();
		return true;
	}
	
	return false;
}

behaviors = [
	behavior_target_near_player,
	behavior_attack_combat_target,
	behavior_leashed_wander,
	behavior_idle,
]