/// @description Insert description here
// You can write your code in this editor
if(array_contains(hit, other.id)) exit;
if (image_index < 1 or array_contains(other.tags, tag) ) {
	other.direction += 180;
	other.source = FxShield;
	other.target = noone;
	other.damage *= 2;
	array_push(hit, other.id);
} else {
	instance_destroy(other.id);
}