
event_inherited();

image_alpha = 0.75;
image_xscale = 2;
image_yscale = image_xscale;

reverse = false;
halting = false;
spliting = false;

if(array_contains(tags, "fire")) {
	image_blend = c_red;	
	reverse = true;
	image_speed *= -1;
}

else if(array_contains(tags, "water")) {
	image_blend = c_aqua
	lifetime *= 2;
}

else if(array_contains(tags, "earth")) {
	image_blend = c_green
	speed = 3;
}

else if(array_contains(tags, "light")) {
	image_blend = c_yellow;
	lifetime *= 0.5;
	spliting = true
	speed = 5;
}

else if(array_contains(tags, "dark")) {
	image_blend = c_purple
	halting = true
}

else { // null
	
}

max_scale = image_xscale;
max_alpha = 0.75;
min_scale = max_scale*0.75;
min_alpha = 0.25;
max_lifetime = lifetime