/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

with(Mobile) {
	var to_other = point_direction(x,y, other.x, other.y);
	if(other.reverse){ 
		to_other += 180;
	}
	var dx = lengthdir_x(1, to_other);
	var dy = lengthdir_y(1, to_other);
	if(not other.halting and place_empty(x+dx,y+dy, Impassibles) and place_empty(x,y, Impassibles)){
		move_and_collide(dx, dy, Impassibles,4);
	}
	if(object_index != Player){
		if(not other.halting) direction = to_other+180;
		stop();
	}
}

with(Projectile) {
	var to_other = point_direction(x,y, other.x, other.y);
	if(other.reverse) {
		to_other += 180;	
	}
	if(speed == 0) {
		var dx = lengthdir_x(1, to_other);
		var dy = lengthdir_y(1, to_other);
		move_and_collide(dx, dy, Solids);	
	} else {
		var arc_to_other = angle_difference(to_other, direction);
		direction += sign(arc_to_other)*2;
	}
	if(other.halting) {
		x = xprevious;
		y = yprevious;
	}
}

lifetime -= 1;
if(spliting and lifetime mod 30 == 0) {
	speed -= 1;
	var n = instance_copy(false);	
	n.direction += 15;
	direction -= 15;
}
var lt = lifetime/max_lifetime;
image_xscale = lerp(min_scale, max_scale, lt);
image_yscale = image_xscale;
image_alpha = lerp(min_alpha, max_alpha, lt);


if(lifetime <= 0) instance_destroy(id);