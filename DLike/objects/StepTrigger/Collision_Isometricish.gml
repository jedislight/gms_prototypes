/// @description Insert description here
// You can write your code in this editor

var other_is_interesting = (
	other.object_index == trigger_object_index 
	or object_is_ancestor(other.object_index, trigger_object_index)
);

if(not triggered and other_is_interesting){
	triggered = once or entry_only;
	mutable_events_broadcast("signal_"+string(signal), {by:id}); 
}

if(other_is_interesting){
	nothing_this_frame = false;
}

