/// @description Insert description here
// You can write your code in this editor
event_inherited();
var player = instance_find(Player, 0 );
tag = "dark"
if(instance_exists(player)){
	if(array_contains(player.owned_elements, "dark")){
		top_sprite = {
			up: SpriteNullAnchor,
			down: SpriteNullAnchor,
			left: SpriteNullAnchor,
			right: SpriteNullAnchor
		}		
		tag = "null"
	}
	
	if(array_contains(player.owned_items, "vortex")) {
		instance_destroy(id);
		exit;
	}
}