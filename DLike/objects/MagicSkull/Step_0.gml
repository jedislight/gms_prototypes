/// @description Insert description here
// You can write your code in this editor




// Inherit the parent event
event_inherited();

var scale = (shared_image_index-2) / 4;
for (var i = 0; i < array_length(projectiles); ++i) {
	var p = projectiles[i];
	if(not instance_exists(p)) continue;
	var ps = max(p.image_xscale, scale);
	p.image_xscale = ps;
	p.image_yscale = ps;
}

