/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

behaviors = [
	behavior_target_near_player,
	behavior_avoid_combat_target,
	behavior_attack_combat_target,
	behavior_leashed_wander,
	behavior_idle,
]
projectiles = [];
function handle_sprite_event(msg){
	switch(msg) {
		case "spawn":
			if(not instance_exists(combat_target)) break;
			for (var i = 0; i < array_length(spawns); ++i) {
			    var spawn = spawns[i];
				var n = instance_create_layer(x+spawn.x,y+spawn.y+1, "Instances", FxMagicSkullProjectile, {target: combat_target, direction: point_direction(x,y, combat_target.x, combat_target.y), damage: damage, tags: combatant_damage_tags, image_xscale:0, image_yscale : 0, owner: id});
				array_push(projectiles, n);
			}

			audio_play_sound(SoundMagicGrow, 10, false);
		break
		
		case "shoot":
		var any = false;
			for (var i = 0; i < array_length(projectiles); ++i) {
				any = true;
			    var p = projectiles[i];
				if(not instance_exists(p)){ 
					array_delete(projectiles, i, 1);
					--i;	
					continue;
				}
				p.speed = 12;
			}
			if(any){
				audio_play_sound(SoundMagicLaunch, 10, false);
			}
		break
	}
	
}