/// @description Insert description here
// You can write your code in this editor



if(other.collision_frame == floor(other.image_index) or other.collision_frame == -1) {
	var new_object_index = object_index;
	if(array_contains(other.tags, "fire")){
		new_object_index = FireAnchor;
	} else if(array_contains(other.tags, "water")){
		new_object_index = WaterAnhcor;
	} else if(array_contains(other.tags, "earth")){
		new_object_index = EarthAnchor;
	}
	
	if(new_object_index != object_index){
		instance_create_layer(x,y, "Instances", new_object_index);
		instance_destroy(id);
	}
}