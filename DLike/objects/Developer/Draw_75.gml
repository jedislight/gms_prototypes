/// @description Insert description here
// You can write your code in this editor

if(debug_overlay){
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
	draw_set_font(FontDebug);

	var text = "-Instances-"
	var o = 0;
	while (object_exists(o)){
		var c = instance_number(o);
		if(c > 0){
			text += "\n " + object_get_name(o) + ": " + string(c);	
		}
		++o;
	}
	
	draw_text(8,32, text);
}


if(show_console) {
	draw_set_font(FontDebug);
	var console_height = 768 div 2;
	var pcursor = console_height;
	var console_width = 1366;
	var terminal_height = string_height("M");
	draw_set_alpha(0.75)
	draw_set_color(c_black);
	draw_rectangle(0,0, console_width, console_height+terminal_height, false)
	draw_set_color(c_white){
		draw_rectangle(0,0, console_width, console_height+terminal_height, true)
		draw_rectangle(0,0, console_width, console_height, true)
	}
	var glb = global.log_buffer;
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
	draw_set_color(c_lime);
	for(var i = array_length(glb) -1; i >= 0; --i) {
		var line_height = string_height(glb[i]);
		if(pcursor < 0) break;
		
		pcursor -= line_height;
		draw_text(16, pcursor, glb[i]);
	}
	
	var terminal_prefix = "> ";
	var terminal_suffix = "";
	if(sin(current_time*0.01) > 0) terminal_suffix = "_";
	
	draw_set_valign(fa_middle);
	draw_text(16, console_height+terminal_height/2, terminal_prefix + keyboard_string + terminal_suffix)
	
	draw_set_alpha(1.0);
}

