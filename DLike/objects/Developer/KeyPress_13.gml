/// @description Insert description here
// You can write your code in this editor


if(show_console) {
	var text = keyboard_string;
	keyboard_string = "";
	log("> "+text);
	if(string_length(text) == 0) exit;
	var words = string_split(text, " ", true);
	if(array_length(words) == 0) exit;
	
	var command = words[0];
	switch(command) {
		case "god_mode":
			with(Player) {
				combatant_god_mode = !combatant_god_mode;
				if(combatant_god_mode) {
					log("GOD MODE ENABLED");	
				} else {
					log("GOD MODE DISABLED");
				}
			}
		break
		
		case "show_mp":
			with(Mobile) draw_debug_motion = !draw_debug_motion;
			with(MotionPlanning) {
				visible = !visible;
				if(visible) {
					log("SHOW MOTION PLANNING ENABLED");	
				} else {
					log("SHOW MOTION PLANNING DISABLED");	
				}
			}
		break
		
		case "show_tiles":
		var on = false;
			with(TileSolidPlaceholder) {
				iso_visible = !iso_visible;
				on = iso_visible;
			}
			with(TileImpassiblePlaceholder) {
				iso_visible = !iso_visible;
				on = iso_visible;
			}
			if(on) {
				log("SHOW TILE PLACEHOLDERS ENABLED");	
			} else {
				log("SHOW TILE PLACEHOLDERS DISABLED");	
			}
		break
		
		case "bun":
			with(Bun){
				// Feather disable once GM2022
				instance_copy(true)
			}
		break
		
		case "goto":
			if(array_length(words) < 3) {
				log("GOTO [roomName] [anchor]");
				break;
			}
			var roomId = asset_get_index(words[1]);
			if(room_exists(roomId)){
				global.transition_anchor_index = real(words[2]);
				with(Player) persistent = true;
				room_goto(roomId);
			} else {
				log($"Could not find room {words[1]}");
				break;
			}
		break
	}
}