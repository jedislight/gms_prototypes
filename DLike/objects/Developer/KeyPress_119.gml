/// @description Insert description here
// You can write your code in this editor


var player = instance_find(Player, 0);

if (instance_exists(player)) {
	player.load();
} else {
	player = instance_create_depth(0,0,0, Player);
	player.load();
}