/// @description Insert description here
// You can write your code in this editor

if (always_visible){

	with(Isometricish){
		if(not transparent) continue;
	
		var temp = mask_index;
		mask_index = sprite_index;
		var ts = get_top_sprite(direction);
		if (sprite_exists(ts)) {
			mask_index = ts;
		}
		var hit = place_meeting(x,y, other.id);
		mask_index = temp;
		
		if (hit and y > other.y){
			collision_with_always_visible_this_frame = true;
			break;
		}
	}
}
