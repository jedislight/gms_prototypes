/// @description Insert description here
// You can write your code in this editor


var s = get_top_sprite(direction);

if(draw_top_during_draw_end == true and draw_top_sprite and sprite_exists(s)) {
	gpu_set_blendmode(top_blendmode);
	draw_sprite_ext(s, shared_image_index, x, y,image_xscale, image_yscale, image_angle, image_blend, image_alpha)	
	gpu_set_blendmode(bm_normal);
}