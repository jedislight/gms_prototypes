/// @description Insert description here
// You can write your code in this editor

collision_with_always_visible_this_frame = false;

top_sprite = {
	up: top_sprite_up,
	down: top_sprite_down,
	left: top_sprite_left,
	right: top_sprite_right
}

travel_sprites_enabled = true;

shared_image_index = 0;
shared_image_speed = 3/60;

top_sprite_change_delay = 0;

top_sprite_previous = top_sprite.right;

instance_seed = random(10000000);
draw_top_sprite = true;
iso_visible = true;
watch_variables = []

if (base_sprite != noone) {
	sprite_index = base_sprite;	
}


/**
 * epsilon comparison
 * @param {Real} a 
 * @param {Real} b 
 * @param {Real} [t] Optional equivilance values. Deafault: 1
 * @returns {bool}
 */
function almost_equal(a,b, t){
	if (is_undefined(t)){
		t = 1;
	}
	return abs(a-b) < t
};

function set_travel_top_sprites() {
	if(not travel_sprites_enabled) {	
		travel_sprites_enabled = true;
		//log(string(name) + " " + string(id) + " starting travel animation")
	}
	
	top_sprite = {
		up: top_sprite_up,
		down: top_sprite_down,
		left: top_sprite_left,
		right: top_sprite_right
	}
}

/**
 * gets the top sprite for the provided direction w/ built in de-jitter
 * @param {Real} d direction 
 * @returns {asset.GMSprite}
 */
function get_top_sprite(d){
	if(top_sprite_change_delay > 0) {
		return top_sprite_previous;	
	}
	if(is_undefined(d)) {
		d = direction;
	}
	var s = top_sprite.right;
	
	if( top_sprite.up == noone ){
		// L / R Only
		if (d > 90 and d < 270) {
			s = top_sprite.left;	
		}
	} else {
		if (d >=45 and d <= 135){
			s = top_sprite.up;	
		}else if (d >= 135 and d <= 225) {
			s = top_sprite.left;	
		} else if (d > 225 and d <= 315) {
			s = top_sprite.down;	
		}	
	}
	
	top_sprite_change_delay = 30;
	top_sprite_previous = s;
	return s;
}

function handle_sprite_event(msg) {
	
}


function create_dialog_bubble(phrases, duration) {
	if(is_undefined(duration)) duration = 180;
	return instance_create_layer(x,y, "Instances", EmojiDialogBubble, {phrases: phrases, anchor: id, phrase_duration:duration});	
}
