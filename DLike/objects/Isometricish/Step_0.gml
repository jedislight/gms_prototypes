/// @description Insert description here
// You can write your code in this editor


--top_sprite_change_delay;


var active_sprite = get_top_sprite();
if( sprite_exists(active_sprite)){
	var active_sprite_images = sprite_get_number(active_sprite);
	var previous_active_sprite_image = floor(shared_image_index) mod active_sprite_images;
	var mul = shared_image_speed_mod;
	if(variable_instance_exists(id, "travel_sprites_enabled")){
		if(!travel_sprites_enabled and variable_instance_exists(id, "combatant_attack_speed_mod")) {
			mul = combatant_attack_speed_mod;	
		}
	}
	shared_image_index += shared_image_speed * mul;
	var active_sprite_image = floor(shared_image_index) mod active_sprite_images;
	if ( active_sprite_image != previous_active_sprite_image) {
		var sprite_info = sprite_get_info(active_sprite);
		var ms = variable_struct_get(sprite_info, "messages");
		if (not is_undefined(ms)){
			for (var i = 0; i < array_length(ms); ++i) {
			    var m = ms[i];
				if(m.frame == active_sprite_image) {
					handle_sprite_event(m.message);	
				}
			}
		}
	}
}