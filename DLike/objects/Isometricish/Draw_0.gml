/// @description Insert description here
// You can write your code in this editor

draw_self();


var s = get_top_sprite(direction);

if(draw_top_during_draw_end == false and draw_top_sprite and sprite_exists(s)) {
	gpu_set_blendmode(top_blendmode);
	draw_sprite_ext(s, shared_image_index, x, y,image_xscale, image_yscale, image_angle, image_blend, image_alpha)	
	gpu_set_blendmode(bm_normal);
}


if(array_length(watch_variables) > 0) {
	draw_set_font(FontDebug);
	draw_set_halign(fa_middle);
	draw_set_valign(fa_top);
	draw_set_color(c_white);
	var watch_text = "";
	for (var i = 0; i < array_length(watch_variables); ++i) {
		var watch_variable = watch_variables[i];
		var variable = variable_instance_get(id, watch_variable);
		watch_text += string("{0}: {1}\n", watch_variable, variable)
	}
	draw_text(x,y+16, watch_text);
}