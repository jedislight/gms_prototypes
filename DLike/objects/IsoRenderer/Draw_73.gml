/// @description Insert description here
// You can write your code in this editor

var iso_instances = array_create(instance_number(Isometricish));
var index = 0;
with(Isometricish){
	if (iso_visible) {
		iso_instances[index++] = id;
	}
}
array_delete(iso_instances, index, array_length(iso_instances) - index)
array_sort(iso_instances, iso_sort);


for (var i = 0; i < array_length(iso_instances); ++i) {
    var iso = iso_instances[i];
	with(iso) event_perform(ev_draw, ev_draw_end);
}