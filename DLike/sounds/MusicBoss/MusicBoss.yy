{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "MusicBoss",
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "bitDepth": 1,
  "bitRate": 128,
  "compression": 0,
  "conversionMode": 0,
  "duration": 101.851425,
  "parent": {
    "name": "Music",
    "path": "folders/Music.yy",
  },
  "preload": false,
  "sampleRate": 44100,
  "soundFile": "MusicBoss.mp3",
  "type": 0,
  "volume": 0.41,
}