{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "SoundCheckpoint",
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "bitDepth": 1,
  "bitRate": 128,
  "compression": 0,
  "conversionMode": 0,
  "duration": 1.081655,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "preload": false,
  "sampleRate": 44100,
  "soundFile": "SoundCheckpoint.ogg",
  "type": 0,
  "volume": 0.8,
}