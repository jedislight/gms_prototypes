{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "SoundRainLoop",
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "bitDepth": 1,
  "bitRate": 128,
  "compression": 0,
  "conversionMode": 0,
  "duration": 45.035103,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "preload": false,
  "sampleRate": 44100,
  "soundFile": "SoundRainLoop.mp3",
  "type": 0,
  "volume": 0.3,
}