{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "MusicDungeon",
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "bitDepth": 1,
  "bitRate": 128,
  "compression": 0,
  "conversionMode": 0,
  "duration": 157.75346,
  "parent": {
    "name": "Music",
    "path": "folders/Music.yy",
  },
  "preload": false,
  "sampleRate": 44100,
  "soundFile": "MusicDungeon.mp3",
  "type": 0,
  "volume": 0.66,
}