{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "MusicForestPeace",
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "bitDepth": 1,
  "bitRate": 128,
  "compression": 0,
  "conversionMode": 0,
  "duration": 81.432,
  "parent": {
    "name": "Music",
    "path": "folders/Music.yy",
  },
  "preload": false,
  "sampleRate": 44100,
  "soundFile": "MusicForestPeace.mp3",
  "type": 0,
  "volume": 1.0,
}