{
  "name": "Sprite6",
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "bbox_left": 37,
  "bbox_right": 157,
  "bbox_top": 71,
  "bbox_bottom": 123,
  "origin": 9,
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "swfPrecision": 2.525,
  "width": 192,
  "height": 192,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"34d57407-7030-4bd5-abf8-7c6d8824fcd6","path":"sprites/Sprite6/Sprite6.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"34d57407-7030-4bd5-abf8-7c6d8824fcd6","path":"sprites/Sprite6/Sprite6.yy",},"LayerId":{"name":"2cfb118f-4180-47b1-a497-7dcfb127b4ee","path":"sprites/Sprite6/Sprite6.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite6","path":"sprites/Sprite6/Sprite6.yy",},"resourceVersion":"1.0","name":"34d57407-7030-4bd5-abf8-7c6d8824fcd6","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a067e2d9-fcba-44f4-b07f-1f917d0b6a78","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"34d57407-7030-4bd5-abf8-7c6d8824fcd6","path":"sprites/Sprite6/Sprite6.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "spriteId": {"name":"Sprite6","path":"sprites/Sprite6/Sprite6.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 64,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"Sprite6","path":"sprites/Sprite6/Sprite6.yy",},
    "resourceVersion": "1.4",
    "name": "Sprite6",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"2cfb118f-4180-47b1-a497-7dcfb127b4ee","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Mathtory",
    "path": "Mathtory.yyp",
  },
  "resourceVersion": "1.0",
  "tags": [],
  "resourceType": "GMSprite",
}