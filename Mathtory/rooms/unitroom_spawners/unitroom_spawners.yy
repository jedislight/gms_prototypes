{
  "isDnd": false,
  "volume": 1.0,
  "parentRoom": null,
  "views": [
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1366,"hview":768,"xport":0,"yport":0,"wport":1366,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1366,"hview":768,"xport":0,"yport":0,"wport":1366,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1366,"hview":768,"xport":0,"yport":0,"wport":1366,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1366,"hview":768,"xport":0,"yport":0,"wport":1366,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1366,"hview":768,"xport":0,"yport":0,"wport":1366,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1366,"hview":768,"xport":0,"yport":0,"wport":1366,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1366,"hview":768,"xport":0,"yport":0,"wport":1366,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
    {"inherit":false,"visible":false,"xview":0,"yview":0,"wview":1366,"hview":768,"xport":0,"yport":0,"wport":1366,"hport":768,"hborder":32,"vborder":32,"hspeed":-1,"vspeed":-1,"objectId":null,},
  ],
  "layers": [
    {"instances":[
        {"properties":[],"isDnd":false,"objectId":{"name":"Game","path":"objects/Game/Game.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":448.0,"y":-64.0,"resourceVersion":"1.0","name":"inst_53FFF637_2","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestTickLimit100","path":"objects/UnitTestTickLimit100/UnitTestTickLimit100.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1280.0,"y":0.0,"resourceVersion":"1.0","name":"inst_5CE30D92","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":512.0,"y":512.0,"resourceVersion":"1.0","name":"inst_A5F698A","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":512.0,"y":448.0,"resourceVersion":"1.0","name":"inst_21F19D5","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":512.0,"y":384.0,"resourceVersion":"1.0","name":"inst_3E527E68","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":512.0,"y":320.0,"resourceVersion":"1.0","name":"inst_153DE813","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":512.0,"y":256.0,"resourceVersion":"1.0","name":"inst_6910BF17","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":512.0,"y":192.0,"resourceVersion":"1.0","name":"inst_4B71F286","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":832.0,"y":512.0,"resourceVersion":"1.0","name":"inst_485C500","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":832.0,"y":448.0,"resourceVersion":"1.0","name":"inst_7BF7B775","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":832.0,"y":384.0,"resourceVersion":"1.0","name":"inst_43916594","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":832.0,"y":320.0,"resourceVersion":"1.0","name":"inst_6AFD2BF6","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":832.0,"y":256.0,"resourceVersion":"1.0","name":"inst_19973597","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":832.0,"y":192.0,"resourceVersion":"1.0","name":"inst_662B1903","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1152.0,"y":512.0,"resourceVersion":"1.0","name":"inst_56885BD8","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1152.0,"y":448.0,"resourceVersion":"1.0","name":"inst_64EA398","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1152.0,"y":384.0,"resourceVersion":"1.0","name":"inst_12AB0165","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1152.0,"y":320.0,"resourceVersion":"1.0","name":"inst_3A3E2FA4","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1152.0,"y":256.0,"resourceVersion":"1.0","name":"inst_35248B19","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"BeltNorth","path":"objects/BeltNorth/BeltNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1152.0,"y":192.0,"resourceVersion":"1.0","name":"inst_72244CAE","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"Naner","path":"objects/Naner/Naner.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1152.0,"y":128.0,"resourceVersion":"1.0","name":"inst_65014F1","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"Naner","path":"objects/Naner/Naner.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":832.0,"y":128.0,"resourceVersion":"1.0","name":"inst_22166103","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"Naner","path":"objects/Naner/Naner.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":512.0,"y":128.0,"resourceVersion":"1.0","name":"inst_F2D18BB","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"OneSpawnerEast","path":"objects/OneSpawnerEast/OneSpawnerEast.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":448.0,"y":512.0,"resourceVersion":"1.0","name":"inst_78440174","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"OneSpawnerEast","path":"objects/OneSpawnerEast/OneSpawnerEast.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":768.0,"y":512.0,"resourceVersion":"1.0","name":"inst_3D6980E","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"OneSpawnerEast","path":"objects/OneSpawnerEast/OneSpawnerEast.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1088.0,"y":512.0,"resourceVersion":"1.0","name":"inst_386E2A5C","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"OneSpawnerNorth","path":"objects/OneSpawnerNorth/OneSpawnerNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":832.0,"y":576.0,"resourceVersion":"1.0","name":"inst_4F62A3EB","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"OneSpawnerNorth","path":"objects/OneSpawnerNorth/OneSpawnerNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1152.0,"y":576.0,"resourceVersion":"1.0","name":"inst_43506B60","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"OneSpawnerWest","path":"objects/OneSpawnerWest/OneSpawnerWest.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1216.0,"y":512.0,"resourceVersion":"1.0","name":"inst_7D535D17","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestAnyNumber","path":"objects/UnitTestAnyNumber/UnitTestAnyNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":512.0,"y":512.0,"resourceVersion":"1.0","name":"inst_64E85D78","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestAnyNumber","path":"objects/UnitTestAnyNumber/UnitTestAnyNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":512.0,"y":384.0,"resourceVersion":"1.0","name":"inst_495230D1","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestAnyNumber","path":"objects/UnitTestAnyNumber/UnitTestAnyNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":512.0,"y":256.0,"resourceVersion":"1.0","name":"inst_129CA318","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestAnyNumber","path":"objects/UnitTestAnyNumber/UnitTestAnyNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":832.0,"y":512.0,"resourceVersion":"1.0","name":"inst_3E4A2A4E","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestAnyNumber","path":"objects/UnitTestAnyNumber/UnitTestAnyNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":832.0,"y":384.0,"resourceVersion":"1.0","name":"inst_2CC343D5","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestAnyNumber","path":"objects/UnitTestAnyNumber/UnitTestAnyNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":832.0,"y":256.0,"resourceVersion":"1.0","name":"inst_535B5CF9","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestAnyNumber","path":"objects/UnitTestAnyNumber/UnitTestAnyNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1152.0,"y":512.0,"resourceVersion":"1.0","name":"inst_6C222DCE","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestAnyNumber","path":"objects/UnitTestAnyNumber/UnitTestAnyNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1152.0,"y":384.0,"resourceVersion":"1.0","name":"inst_627A909A","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"OneSpawnerNorth","path":"objects/OneSpawnerNorth/OneSpawnerNorth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":256.0,"y":256.0,"resourceVersion":"1.0","name":"inst_63558816","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"OneSpawnerSouth","path":"objects/OneSpawnerSouth/OneSpawnerSouth.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":256.0,"y":384.0,"resourceVersion":"1.0","name":"inst_4DC921B2","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"OneSpawnerEast","path":"objects/OneSpawnerEast/OneSpawnerEast.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":320.0,"y":320.0,"resourceVersion":"1.0","name":"inst_66C15A42","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"OneSpawnerWest","path":"objects/OneSpawnerWest/OneSpawnerWest.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":192.0,"y":320.0,"resourceVersion":"1.0","name":"inst_6B8EED32","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestAnyNumber","path":"objects/UnitTestAnyNumber/UnitTestAnyNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":128.0,"y":320.0,"resourceVersion":"1.0","name":"inst_39F4897","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestAnyNumber","path":"objects/UnitTestAnyNumber/UnitTestAnyNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":256.0,"y":192.0,"resourceVersion":"1.0","name":"inst_2554A682","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestAnyNumber","path":"objects/UnitTestAnyNumber/UnitTestAnyNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":384.0,"y":320.0,"resourceVersion":"1.0","name":"inst_49B6F9FE","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestAnyNumber","path":"objects/UnitTestAnyNumber/UnitTestAnyNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":256.0,"y":448.0,"resourceVersion":"1.0","name":"inst_75B9C789","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestNoNumber","path":"objects/UnitTestNoNumber/UnitTestNoNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":320.0,"y":384.0,"resourceVersion":"1.0","name":"inst_225BB81F","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestNoNumber","path":"objects/UnitTestNoNumber/UnitTestNoNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":192.0,"y":384.0,"resourceVersion":"1.0","name":"inst_4DB6B179","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestNoNumber","path":"objects/UnitTestNoNumber/UnitTestNoNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":192.0,"y":256.0,"resourceVersion":"1.0","name":"inst_7906E33F","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestNoNumber","path":"objects/UnitTestNoNumber/UnitTestNoNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":320.0,"y":256.0,"resourceVersion":"1.0","name":"inst_408D7CDD","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestNoNumber","path":"objects/UnitTestNoNumber/UnitTestNoNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":256.0,"y":320.0,"resourceVersion":"1.0","name":"inst_4DF7CB62","tags":[],"resourceType":"GMRInstance",},
        {"properties":[],"isDnd":false,"objectId":{"name":"UnitTestAnyNumber","path":"objects/UnitTestAnyNumber/UnitTestAnyNumber.yy",},"inheritCode":false,"hasCreationCode":false,"colour":4294967295,"rotation":0.0,"scaleX":1.0,"scaleY":1.0,"imageIndex":0,"imageSpeed":1.0,"inheritedItemId":null,"frozen":false,"ignore":false,"inheritItemSettings":false,"x":1152.0,"y":256.0,"resourceVersion":"1.0","name":"inst_151183D1","tags":[],"resourceType":"GMRInstance",},
      ],"visible":true,"depth":0,"userdefinedDepth":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"gridX":64,"gridY":64,"layers":[],"hierarchyFrozen":false,"effectEnabled":true,"effectType":null,"properties":[],"resourceVersion":"1.0","name":"Instances","tags":[],"resourceType":"GMRInstanceLayer",},
    {"spriteId":null,"colour":4278190080,"x":0,"y":0,"htiled":false,"vtiled":false,"hspeed":0.0,"vspeed":0.0,"stretch":false,"animationFPS":15.0,"animationSpeedType":0,"userdefinedAnimFPS":false,"visible":true,"depth":100,"userdefinedDepth":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"gridX":32,"gridY":32,"layers":[],"hierarchyFrozen":false,"effectEnabled":true,"effectType":null,"properties":[],"resourceVersion":"1.0","name":"Background","tags":[],"resourceType":"GMRBackgroundLayer",},
  ],
  "inheritLayers": false,
  "creationCodeFile": "",
  "inheritCode": false,
  "instanceCreationOrder": [
    {"name":"inst_53FFF637_2","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_5CE30D92","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_A5F698A","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_21F19D5","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_3E527E68","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_153DE813","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_6910BF17","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_4B71F286","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_485C500","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_7BF7B775","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_43916594","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_6AFD2BF6","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_19973597","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_662B1903","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_56885BD8","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_64EA398","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_12AB0165","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_3A3E2FA4","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_35248B19","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_72244CAE","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_65014F1","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_22166103","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_F2D18BB","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_78440174","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_3D6980E","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_386E2A5C","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_4F62A3EB","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_43506B60","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_7D535D17","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_64E85D78","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_495230D1","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_129CA318","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_3E4A2A4E","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_2CC343D5","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_535B5CF9","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_6C222DCE","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_627A909A","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_63558816","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_4DC921B2","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_66C15A42","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_6B8EED32","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_39F4897","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_2554A682","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_49B6F9FE","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_75B9C789","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_225BB81F","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_4DB6B179","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_7906E33F","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_408D7CDD","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_4DF7CB62","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
    {"name":"inst_151183D1","path":"rooms/UnitRoom_Spawners/UnitRoom_Spawners.yy",},
  ],
  "inheritCreationOrder": false,
  "sequenceId": null,
  "roomSettings": {
    "inheritRoomSettings": false,
    "Width": 1366,
    "Height": 768,
    "persistent": false,
  },
  "viewSettings": {
    "inheritViewSettings": false,
    "enableViews": false,
    "clearViewBackground": false,
    "clearDisplayBuffer": true,
  },
  "physicsSettings": {
    "inheritPhysicsSettings": false,
    "PhysicsWorld": false,
    "PhysicsWorldGravityX": 0.0,
    "PhysicsWorldGravityY": 10.0,
    "PhysicsWorldPixToMetres": 0.1,
  },
  "parent": {
    "name": "Rooms",
    "path": "folders/Rooms.yy",
  },
  "resourceVersion": "1.0",
  "name": "UnitRoom_Spawners",
  "tags": [],
  "resourceType": "GMRoom",
}