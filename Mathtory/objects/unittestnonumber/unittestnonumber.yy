{
  "spriteId": {
    "name": "Sprite78",
    "path": "sprites/Sprite78/Sprite78.yy",
  },
  "solid": false,
  "visible": true,
  "managed": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "UnitTestValidation",
    "path": "objects/UnitTestValidation/UnitTestValidation.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 1,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Testing",
    "path": "folders/Objects/System/Testing.yy",
  },
  "resourceVersion": "1.0",
  "name": "UnitTestNoNumber",
  "tags": [],
  "resourceType": "GMObject",
}