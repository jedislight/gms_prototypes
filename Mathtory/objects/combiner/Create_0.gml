/// @description Insert description here
// You can write your code in this editor

event_inherited();

left_x_offset = -1;
left_y_offset = 0;

function combine(l, r) {
	return l + r;	
}

function update() {
	var ng = Game.number_grid
	var ngn = Game.number_grid_next
	var gx = x div 64;
	var gy = y div 64;
	
	var lgx = gx + left_x_offset;
	var lgy = gy + left_y_offset;
	
	var rgx = gx - left_x_offset;
	var rgy = gy - left_y_offset;
	
	var ogx = gx + left_y_offset;
	var ogy = gy + left_x_offset;
	
	var l = ng[# lgx, lgy];
	var r = ng[# rgx, rgy];
	var o = ng[# ogx, ogy];
	
	if (is_nan(o) and !is_nan(l) and !is_nan(r)){
		ng[# lgx, lgy] = NaN;
		ng[# rgx, rgy] = NaN;
		ngn[# rgx, rgy] = NaN;
		ngn[# lgx, lgy] = NaN;
		ngn[# ogx, ogy] = combine(l, r);
	}
}