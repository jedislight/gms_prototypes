/// @description Insert description here
// You can write your code in this editor

if (status == VALIDATION_RESULTS.FAIL) exit;
status = validate();
room_speed = 999;
switch(status) {
	case VALIDATION_RESULTS.PENDING:
		image_blend = c_blue
		break;
	
	case VALIDATION_RESULTS.PASS: 
		image_blend = c_lime;
		var global_status = status;
		with (UnitTestValidation) {
			global_status = min(global_status, status);
		}
		if (global_status == VALIDATION_RESULTS.PASS) {
			room_speed = 60;
			show_debug_message("Unit Test Room PASS: " + room_get_name(room) + " @" + string(get_timer()/1000000)+"s");
			room_goto_next()	
		}
	break;
	
	case VALIDATION_RESULTS.FAIL:
		image_blend = c_red;
		Game.tick_rate = 0;
		Game.alarm[0] = -1;
		show_debug_message("Unit Test Room FAIL: " + room_get_name(room) + " @" + string(get_timer()/1000000)+"s");
	break;
}