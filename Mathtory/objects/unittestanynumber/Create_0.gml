/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

function validate() {
	var number = Game.number_grid[# x div 64, y div 64];
	if (is_nan(number)) return VALIDATION_RESULTS.PENDING;
	else return VALIDATION_RESULTS.PASS;
}