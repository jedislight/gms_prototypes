/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

target_x_offset = 1;
target_y_offset = 0;

function transform(number) {
	return number;	
}

function update() {
	var ng = Game.number_grid
	var ngn = Game.number_grid_next
	var gx = x div 64;
	var gy = y div 64;
	
	var tgx = x div 64 + target_x_offset;
	var tgy = y div 64 + target_y_offset;
	
	var number = ng[# gx, gy];
	var target = ng[# tgx, tgy];
	var targetN = ngn[# tgx, tgy];

	if (!is_nan(number) and is_nan(target)) {
		if (!is_nan(targetN)) {
			ngn[# gx, gy] = number;
		} else {
			ngn[# tgx, tgy] = transform(number);
		}
	} else if(!is_nan(number)) {
		ngn[# gx, gy] = number;	
	}
}