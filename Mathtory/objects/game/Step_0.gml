/// @description Insert description here
// You can write your code in this editor


function _update(filter) {
	
	if ( reverse_update ) {
		for(var xx = 0; xx < w; ++xx) for(var yy = 0; yy < h; ++yy) {
			var entity = ds_grid_get(entity_grid, xx, yy);
			if (instance_exists(entity) and filter(entity)) {
				entity.update();
			}
		}
	} else {
		for (var xx = w - 1; xx >= 0; --xx) for (var yy = h - 1; yy >= 0; --yy) {
			var entity = ds_grid_get(entity_grid, xx, yy);
			if (instance_exists(entity) and filter(entity)) {
				entity.update();
			}
		}
	}
}

if (do_update){	
	_update(function (entity) {return !object_is_ancestor(entity, Belt)});
	_update(function (entity) {return object_is_ancestor(entity, Belt)});
	reverse_update = choose(true, false);
	
	for(var xx = 0; xx < w; ++xx) for(var yy = 0; yy < h; ++yy) {
		var number = ds_grid_get(number_grid, xx, yy);
		var entity = ds_grid_get(entity_grid, xx, yy);
		if (!is_nan(number) and !instance_exists(entity)) number_grid_next[# xx, yy] = number;
	}
}