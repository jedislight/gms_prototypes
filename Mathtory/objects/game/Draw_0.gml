/// @description Insert description here
// You can write your code in this editor

function draw_number(px, py, number) {
	px += 16;
	py += 16;
	draw_rectangle_color(px,py, px+32, py+32, c_dkgray, c_ltgray, c_maroon, c_blue, false);
	draw_rectangle_color(px,py, px+32, py+32, c_blue, c_dkgray, c_ltgray, c_maroon, true);
	draw_set_font(Font1);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	draw_text(px+16, py+16, string(number));
}
for(var xx = 0; xx < w; ++xx) for(var yy = 0; yy < h; ++yy) {
	var entity = ds_grid_get(entity_grid, xx, yy);
	
	var px = xx * 64;
	var py = yy * 64;
	
	if (instance_exists(entity)) {
		entity.x = px;
		entity.y = py;
		with(entity) {
			draw_self();	
		}
	}
}

for(var xx = 0; xx < w; ++xx) for(var yy = 0; yy < h; ++yy) {
	var number = ds_grid_get(number_grid, xx, yy);
	
	var px = xx * 64;
	var py = yy * 64;
		
	if (!is_nan(number)) {
		draw_number(px, py, number)
	}
}