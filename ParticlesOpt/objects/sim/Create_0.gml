/// @description Insert description here
// You can write your code in this editor

randomize();
countPerType = 40
ps = [];
rs = [];
tcs = [c_white, c_red, c_aqua, c_lime, c_yellow];

function create(type, count) {
	while(array_length(ps) <= type) array_push(ps, []);
	repeat(count) array_push(ps[type], new P(type));	
}

function rule(type, type2, g, o) {
	array_push(rs, new R(type, type2, g, o));	
}

function update() {
	// apply rules
	for(var i = 0; i < array_length(rs); ++i) {
		var r = rs[i];
		r.apply(ps[r.type], ps[r.type2]);
	}
	
	// update particles
	for(var type = 0; type < array_length(ps); ++type) {
		var tps = ps[type];
		for(var i = 0; i < array_length(tps); ++i) {
			var p = tps[i];
			p.vx *= 0.95;
			p.vy *= 0.95;
			p.x += p.vx;
			p.y += p.vy;
			
			if (p.x > p.width){
				p.x += (p.width - p.x)*2;
				p.vx *= -1;
			} else if (p.x < 0 ) {
				p.x = -p.x;
				p.vx = -p.vx;
			}
			
			if (p.y > p.height){
				p.y += (p.height - p.y)*2;
				p.vy = -p.vy;
			} else if (p.y < 0) {
				p.y = -p.y;
				p.vy = -p.vy
			}
		}
	}
}

function render() {
	for(var type = 0; type < array_length(ps); ++type) {
		draw_set_color(tcs[type]);
		var tps = ps[type];
		for(var i = 0; i < array_length(tps); ++i) {
			var p = tps[i];
			draw_circle(p.x, p.y, 2, true);
		}
	}
}

function renderV(scale) {
	for(var type = 0; type < array_length(ps); ++type) {
		draw_set_color(tcs[type]);
		var tps = ps[type];
		for(var i = 0; i < array_length(tps); ++i) {
			var p = tps[i];
			draw_line(p.x,p.y, p.x+p.vx*scale, p.y+p.vy*scale);
		}
	}	
}

function P(type_,) constructor {
	static width = room_width;
	static height = room_height;
	
	x = random(width);
	y = random(height);
	vx = 0;
	vy = 0;
	type = type_;
}

function R(type_, type2_, g_, o_) constructor {
	type = type_;
	type2 = type2_;
	g = g_;
	o = o_
	
	function unzero(number) {
		var range = 32;
		if (number >= 0 and number < range) {
			return range;
		} else if (number < 0 and number > -range) {
			return -range;	
		}
	
		return number;
	}
	
	function apply(ps, ps2) {
		for(var i = 0; i < array_length(ps); ++i) {
			var p = ps[i];
			for(var j = 0; j < array_length(ps2); ++j) {
				if(i == j) continue;
				var p2 = ps2[j];
				var d = point_distance(p.x, p.y, p2.x, p2.y);
				d =  d - o;
				var f = g / unzero(d);
				var dir = point_direction(p.x, p.y, p2.x, p2.y);
				p.vx += lengthdir_x(f, dir);
				p.vy += lengthdir_y(f, dir);
				
			}
		}
	}
}