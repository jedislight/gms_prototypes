/// @description Insert description here
// You can write your code in this editor


orange = 256;
frange = 2;

for(var i = 0; i < array_length(tcs); ++i) {
	create(i, countPerType);
}

function full_random() {
	rs = [];
	for(var t1 = 0; t1 < array_length(tcs); ++t1) {
		for(var t2 = 0; t2 < array_length(tcs); ++t2) {
			rule(t1, t2, random_range(0, frange), random_range(0,orange));	
		}
	}
}

function mono_type(type, count) {
	rs = [];
	create(0, count);
	rule(0,0, random_range(0, frange), random_range(0, orange));
}

full_random();