/// @description Insert description here
// You can write your code in this editor
alarm_set(0, 5);
var water_cell = Water.water_get_cell(x, y);
if (reach == flowering_reach) {
	instance_destroy(id);
	var n = instance_create_depth(x+lengthdir_x(64, image_angle),y+lengthdir_y(64, image_angle), depth, Goal);
	n.seed = Trunk;
	n.sprite_index = Sprite38;
	n.image_xscale = 0.5;
	n.image_yscale = 0.5;
	n.image_blend = c_white;
	n.dna[? "illumination_goal"] = 1.0

}
if (instance_exists(anchor) 
	and (anchor.object_index == Trunk or (instance_exists(anchor.anchor) and anchor.image_xscale == 1.0) )
	and image_xscale == 1.0) {
		
	if (anchor.object_index == Vine) {
		x = anchor.x + lengthdir_x(distance, anchor.image_angle);
		y = anchor.y + lengthdir_y(distance, anchor.image_angle);

		image_angle += 0.02 * angle_difference(water_cell.dir, image_angle) / (spawned+1)
		image_angle += 0.02 * angle_difference(original_angle, image_angle) * (spawned+1)
		
	}
	
	if (random_range(0,1) < Illumination.get_illumination(x,y)*0.001/(spawned*2+1+reach*2)) {
		var v = instance_create_depth(x,y, depth, Vine);
		v.anchor = id;
		v.image_angle = image_angle + random_range(-15,15);
		v.original_angle = image_angle;
		v.reach = reach + 1;
		++spawned
	}
} else  {
	anchor = noone;
	image_blend = make_color_hsv(color_get_hue(c_maroon), 255, 255*image_xscale);
	image_xscale *= 0.999;
	image_yscale = image_xscale;
	if (image_xscale < 0.1){
		instance_destroy(id);	
	}
	
	motion_add(water_cell.dir, (water_cell.flow*0.01) / image_xscale);
	if (speed > water_cell.flow) {
		speed = water_cell.flow;
	} else {
		speed *= 0.95;	
	}
}