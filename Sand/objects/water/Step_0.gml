/// @description Insert description here
// You can write your code in this editor

update_cursor_x += 1;
if (update_cursor_x >= ds_grid_width(grid)) {
	update_cursor_x = 0;
	update_cursor_y += 1;
	if (update_cursor_y >= ds_grid_height(grid)) {
		update_cursor_x = 0;
		update_cursor_y = 0;
	}
}


var cell = ds_grid_get(grid, update_cursor_x, update_cursor_y);
cell.dir += random_range(-1.0, 1.0);

var afx = 0;
var afy = 0;
for (var xx = -1; xx <= 1; ++xx) for (var yy = -1; yy <= 1; ++yy) {
	var ix = update_cursor_x + xx;
	var iy = update_cursor_y + yy;
	if (ix < 0 or iy < 0 or ix >= ds_grid_width(grid) or iy >= ds_grid_height(grid)) {
		continue;
	}
	var n = ds_grid_get(grid, ix, iy);
	
	var contribution = 1.0;
	var cdir = point_direction(update_cursor_x, update_cursor_y, ix, iy);
	var adiff = abs(angle_difference(cell.flow, cdir))
	var asign = sign(angle_difference(cell.flow, cdir))
	if (adiff > 90) adiff = 180-adiff;
	contribution = adiff / 90;
	var dir_offset = 0;
	if (contribution < 0.5 ) {
		contribution = 1.0 - contribution;	
		dir_offset = -asign * 90;
	}
	
	contribution *= contribution;
	
	afx += lengthdir_x(n.flow*contribution, n.dir + dir_offset);
	afy += lengthdir_y(n.flow*contribution, n.dir + dir_offset);
}

var cfx = lengthdir_x(cell.flow, cell.dir);
var cfy = lengthdir_y(cell.flow, cell.dir);

var sdir = point_direction(update_cursor_x * cell_width, update_cursor_y * cell_width, Sun.x, Sun.y);
var sdist = point_distance(update_cursor_x * cell_width, update_cursor_y * cell_width, Sun.x, Sun.y);
var sflow = 100000 / (sdist * sdist);

var sfx = lengthdir_x(sflow, sdir);
var sfy = lengthdir_y(sflow, sdir);

afx += sfx;
afy += sfy;

var fx = lerp(cfx, afx, 0.01);
var fy = lerp(cfy, afy, 0.01);

cell.dir = point_direction(0,0, fx, fy);
cell.flow = point_distance(0,0, fx, fy);

cell.flow *= 0.975;

cell.flow = clamp(cell.flow, 0.0, 1.0);