/// @description Insert description here
// You can write your code in this editor
cell_width = 128;
grid = ds_grid_create(room_width div cell_width, room_height div cell_width);
update_cursor_x = 0;
update_cursor_y = 0;
function WaterCell() constructor {
	flow = 0;
	dir = 0;
}

for(var xx = 0; xx < ds_grid_width(grid); ++xx) for(var yy = 0; yy < ds_grid_height(grid); ++yy) {
	ds_grid_set(grid, xx, yy, new WaterCell());	
}

function water_get_cell(xx, yy) {
	var ix = clamp(round(xx / cell_width), 0, ds_grid_width(grid) - 1);
	var iy = clamp(round(yy / cell_width), 0, ds_grid_height(grid) - 1);
	
	return ds_grid_get(grid, ix, iy);
}