/// @description Insert description here
// You can write your code in this editor
for(var xx = 0; xx < ds_grid_width(grid); ++xx) for(var yy = 0; yy < ds_grid_height(grid); ++yy) {
	var center_x = xx * cell_width + 0.5 * cell_width;
	var center_y = yy * cell_width + 0.5 * cell_width;
	
	var cell = ds_grid_get(grid, xx ,yy);
	var flow_x = lengthdir_x(cell.flow*cell_width/2, cell.dir);
	var flow_y = lengthdir_y(cell.flow*cell_width/2, cell.dir);
	
	draw_line_color(center_x, center_y, center_x + flow_x, center_y + flow_y, c_aqua, c_dkgray)
}