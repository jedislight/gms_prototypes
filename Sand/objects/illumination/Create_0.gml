/// @description Insert description here
// You can write your code in this editor
depth = -1
update_cursor_x = 0;
update_cursor_y = 0;
cell_width = room_width / 20;
grid = ds_grid_create(4+room_width / cell_width, 4+room_height / cell_width);
offset = -2 * cell_width;
ds_grid_clear(grid, 1.0)
function get_illumination(xx, yy) {
	var ix = (xx+offset) / ds_grid_width(grid);
	var iy = (yy + offset)/ ds_grid_height(grid);
	
	ix = clamp(ix, 0, ds_grid_width(grid)-1);
	iy = clamp(iy, 0, ds_grid_height(grid)-1);
	
	return ds_grid_get(grid, ix, iy);
}