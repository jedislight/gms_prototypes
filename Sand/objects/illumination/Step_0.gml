/// @description Insert description here
// You can write your code in this editor
update_cursor_x += 1;
if (update_cursor_x >= ds_grid_width(grid)) {
	update_cursor_x = 0;
	update_cursor_y += 1;
	if (update_cursor_y >= ds_grid_height(grid)) {
		update_cursor_x = 0;
		update_cursor_y = 0;
	}
}

var cx = offset + update_cursor_x * cell_width;
var cy = offset + update_cursor_y * cell_width;

instance_deactivate_region(cx, cy, cell_width, cell_width, true, true);
var list = ds_list_create()
collision_line_list(cx,cy, Sun.x, Sun.y, Goal, true, true, list, false);
var opacity = 1 + sqrt(ds_list_size(list));
ds_list_destroy(list);
instance_activate_all();

var sunlight = random_range(0.9, 1.1);

var illumination = sunlight / opacity;

var final = lerp(ds_grid_get(grid, update_cursor_x, update_cursor_y), illumination, 0.9);
final = clamp(final, 0, 1);
ds_grid_set(grid, update_cursor_x, update_cursor_y, final);