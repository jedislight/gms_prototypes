/// @description Insert description here
// You can write your code in this editor
gpu_set_blendmode(bm_subtract)
for(var xx = 0; xx < ds_grid_width(grid); ++xx) for(var yy = 0; yy < ds_grid_height(grid); ++yy) {
	var center_x = offset + xx * cell_width + 0.5 * cell_width;
	var center_y = offset + yy * cell_width + 0.5 * cell_width;
	
	var cell = ds_grid_get(grid, xx ,yy);
	var color = c_orange;
	color = make_color_hsv(color_get_hue(color), color_get_saturation(color), color_get_value(color)*(1-power(cell,1/15)));
	draw_circle_color(center_x, center_y, cell_width*2.5, color, c_black, false);
}
gpu_set_blendmode(bm_normal)
/*
for(var xx = 0; xx < ds_grid_width(grid); ++xx) for(var yy = 0; yy < ds_grid_height(grid); ++yy) {
	var center_x = xx * cell_width + 0.5 * cell_width;
	var center_y = yy * cell_width + 0.5 * cell_width;
	
	var cell = ds_grid_get(grid, xx ,yy);
	draw_text(center_x, center_y, string(cell));
}
*/