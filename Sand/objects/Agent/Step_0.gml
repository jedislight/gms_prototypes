/// @description Insert description here
// You can write your code in this editor

species_age += 1;
if (image_xscale < 1.0 ) {
image_xscale += 0.0001;
image_yscale += 0.0001;
}
if (true)
{
	ttl -= 0.1
	brain_inputs = array_create(BI.COUNT, 0);
	brain_inputs[BI.TTL] = ttl / ttl_max
	brain_inputs[BI.SIZE] = image_xscale;

	var list = ds_list_create();
	collision_line_list(x, y, x+lengthdir_x(vision_range, image_angle), y+lengthdir_y(vision_range, image_angle), Goal, true, true, list, true);
	if (not ds_list_empty(list)){
		var nearest = list[|0];
		var distance = distance_to_object(nearest);
		var vision_distance = clamp(distance / vision_range, 0, 1);
		brain_inputs[BI.VISON] = vision_distance;
	}
	ds_list_destroy(list);
	
	var n = instance_nearest(x,y, Goal);
	if (instance_exists(n)){ 
		var ndist = point_distance(x,y, n.x, n.y);
		var ndir = point_direction(x,y, n.x, n.y);
		var nad = angle_difference(image_angle, ndir);
		var nadn = nad / 180;
		brain_inputs[BI.NEAR_DIR] = (clamp(nadn, -1, 1)+1)/2;
		brain_inputs[BI.NEAR_DIST] = clamp(ndist/vision_range, 0, 1);
	}

	brain.set_inputs(brain_inputs);

	brain_outputs = brain.get_outputs()
}
var dir_total = (brain_outputs[BO.TURN]-0.5)*2;
motion_add(image_angle, brain_outputs[BO.SPEED] * speed_mul);
image_angle += dir_total * turn_mul;
vision_range = vision_range_max

var exertion = abs(dir_total) + brain_outputs[BO.SPEED] + 1;

ttl -= exertion * image_xscale;

var water_cell = Water.water_get_cell(x, y);
motion_add(water_cell.dir, (water_cell.flow*0.01) / image_xscale);
speed *= 0.75;	

if (ttl < 0) {
	instance_destroy(id);
	exit;
}

x = clamp(x, 0, room_width); y = clamp(y, 0, room_height)