/// @description Insert description here
// You can write your code in this editor

if ( ttl < brain_outputs[BO.EAT]*ttl_max ){
	ttl_max -= 60;
	ttl_max = clamp(ttl_max, 60, 99999999);
	ttl += ttl_max * (other.image_xscale * 2);
	var extra = ttl - ttl_max;
	extra = max(0, extra);
	ttl = clamp(ttl, 0, ttl_max);
	if(image_xscale == 1.0 and extra > 0 and random_range(0.0, 1.0) < brain_outputs[BO.SPAWN]) {
		instance_deactivate_object(id);
		var nearest = instance_nearest(x,y, Agent)
		var distance = 999;
		if (instance_exists(nearest)) {
			distance = point_distance(x,y, nearest.x, nearest.y);	
		}
		instance_activate_object(id)
		if (distance > 64) {
			repeat(clamp(ceil((extra/2) div (ttl_max * 0.1)), 1, 3)) {
				var n = instance_create_depth(x+random_range(-12, 12), y+random_range(-12, 12), depth, Agent);
				n.brain.copy(brain);
				n.brain.mutate();
				n.ttl = ttl_max * 0.1;
				n.image_xscale = 0.1;
				n.image_yscale = n.image_xscale;
				n.species_age = species_age;
			}
		}
	} else if (extra) {
		lerper_lerp(id, "image_xscale", min(1.0, image_xscale + extra / ttl_max), 60);
		lerper_lerp(id, "image_yscale", min(1.0, image_xscale + extra / ttl_max), 60);
	}


	other.image_xscale = clamp(extra/2/ttl_max, 0, other.image_xscale-.1);
	other.image_yscale = other.image_xscale;
	if (other.image_xscale < 0.1) {
		instance_destroy(other);
	}
}