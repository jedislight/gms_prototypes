/// @description Insert description here
// You can write your code in this editor
enum BI {
	VISON = 0,
	TTL,
	NEAR_DIR,
	NEAR_DIST,
	SIZE,
	COUNT
}

enum BO {
	TURN = 0,
	SPEED,
	EAT,
	SPAWN,
	COUNT
}

brain_inputs = array_create(BI.COUNT, 0);
brain_outputs = array_create(BO.COUNT, 0);
brain = new NNBrain(array_length(brain_inputs), array_length(brain_outputs));

speed_mul = 1.0;
turn_mul = 5;
vision_range_max = 512;
vision_range = vision_range_max

ttl_max = 60*60;
ttl = ttl_max;

image_xscale = 0.1;
image_yscale = image_xscale;

species_age = 0;