/// @description Insert description here
// You can write your code in this editor


var count = 0;
image_xscale = sqrt(image_xscale);
image_yscale = image_xscale;

if (image_xscale > 0.9){
	with(Vine) {
		if (anchor == other.id) {
			++count;	
		}
	}

	if (count < 4 ) {
		var v = instance_create_depth(x,y, depth, Vine);
		v.anchor = id;
	}
}
if (count == 0 and image_xscale >0.9) {
	energy -= 0.1;
	if (energy <= 0) {
		instance_destroy(id);	
	}
	
	image_blend = make_color_hsv(255, 0, 255*energy);
}
alarm_set(0, 500);