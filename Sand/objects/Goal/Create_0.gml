/// @description Insert description here
// You can write your code in this editor
image_xscale = 0.1;
image_yscale = 0.1;
image_angle = random(360);
image_blend = 0;
water_cell = Water.water_get_cell(x, y);
seed = noone;

dna = ds_map_create();
dna[? "illumination_goal"] = random_range(0,1);

function get_blend() {
	var mix = get_expression("illumination_goal");
	var hue = lerp(color_get_hue(c_purple), color_get_hue(c_lime), mix);
	var sat = 255;
	var value = 255;
	return make_color_hsv(hue, sat, value);
}

function get_expression(key) {
	var value = dna[? key]	
	if (is_undefined(value)) {
		return 0.0;	
	}
	
	return value;
}

function mutate_expression() {
	for(var key = ds_map_find_first(dna); not is_undefined(key); key = ds_map_find_next(dna, key)){
		if (random_range(0,1) < 0.1) {
			var value = dna[? key] + random_range(-0.1, 0.1);
			dna[? key] = clamp(value, 0, 1);
		}
	}
}