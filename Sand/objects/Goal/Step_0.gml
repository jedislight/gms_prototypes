/// @description Insert description here
// You can write your code in this editor

if(image_blend == 0)
	image_blend = get_blend()

if(irandom_range(0, 500) == 42) {
	if (seed != noone and random_range(0,1) < 0.1 and place_empty(x,y)){
		instance_create_depth(x,y,depth, seed);
		instance_destroy(id);
		exit;
	}
	water_cell = Water.water_get_cell(x, y);	
	var list = ds_list_create()
	collision_circle_list(x,y, 32*image_xscale, Goal, true, true, list, false);
	for(var i = 0; i < ds_list_size(list); ++i){	
		nearest = list[| i];
		motion_add(point_direction(nearest.x, nearest.y, x, y), 0.1);	
	}
	ds_list_destroy(list);
	
	var illumunation = Illumination.get_illumination(x,y);
	var illumination_goal = get_expression("illumination_goal");
	var illumination_error = abs(illumination_goal - illumunation);
	var spawn_conditions = 1 - illumination_error;
	if (random_range(0.5, 1.0) < power(spawn_conditions,5)) {
		lerper_lerp(id, "image_xscale", sqrt(image_xscale), 60);
		lerper_lerp(id, "image_yscale", sqrt(image_yscale), 60);
		speed *= 0.25;
		if (image_xscale > 0.75) {
			var xx = x + random_range(-256, 256)
			var yy = y + random_range(-256, 256)
			xx = clamp(xx, 0, room_width);
			yy = clamp(yy, 0, room_height);
			if (noone == collision_circle(xx,yy, 32,  Goal, true, false )) {
				var n = instance_create_depth(xx,yy, depth, Goal);	
				ds_map_copy(n.dna, dna);
				n.mutate_expression();
			}
		}
	} else if (illumination_error > random_range(0.1,1)) {
		image_xscale *= 0.9;
		image_xscale = clamp(image_xscale, 0, 1);
		image_yscale = image_xscale;
		
		if (image_xscale < 0.1) {
			instance_destroy(id);	
		}
	}
}


motion_add(water_cell.dir, (water_cell.flow*0.01) / image_xscale);
if (speed > water_cell.flow) {
	speed = water_cell.flow;
} else {
	speed *= 0.95;	
}

x = clamp(x, 0, room_width); y = clamp(y, 0, room_height)