/// @description Insert description here
// You can write your code in this editor

room_speed = 60;

if (speed_toggle != mouse_check_button(mb_right)) {
	room_speed = 9999999;	
}

sim_time++;

if(agent_spawn_enabled and (random_range(0,1) < 0.01/60)) {
	for(var i = 0; i < 7; ++i) {
		var n = instance_create_depth(random(room_width), random(room_height), depth, Agent);
		if (is_struct(best_brain)) {
			n.brain.copy(best_brain);
			repeat(i*i*i*i){
				n.brain.mutate();
			}
		} else {
			repeat(1000) n.brain.mutate();
		}
	}
}


if(instance_number(Goal) == 0 or random_range(0,1) < 0.001/60) {
	var n = instance_create_depth(random(room_width), random(room_height), depth, choose(Goal, Trunk));
}
current_max_species_age = 0;
with(Agent) {
	other.current_max_species_age = max(other.current_max_species_age, species_age);
	if (species_age > other.max_species_age	) {		
		other.max_species_age = species_age;
		if (!is_struct(other.best_brain)) {
			other.best_brain = new NNBrain(brain.input_size, brain.output_size);	
		}
		other.best_brain.copy(brain);
	}
}

for(var i = 0; i < ds_list_size(global.lerp_list); ++i) {
	var lt = global.lerp_list[| i];
	if (instance_exists(lt.o)) {
		var nv = lerp(lt.orig, lt.t, 1-(lt.d / lt.od));
		lt.d -= 1;
		variable_instance_set(lt.o, lt.vn, nv);
	}	
}

for(var i = ds_list_size(global.lerp_list)-1; i >= 0 ; --i) {
	var lt = global.lerp_list[| i];
	if (lt.d <= 0 ) {
		ds_list_delete(global.lerp_list, i);
	}
	
}