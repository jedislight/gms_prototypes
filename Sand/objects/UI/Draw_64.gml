/// @description Insert description here
// You can write your code in this editor

var to_minutes = function(frames) {
	return round(100*(frames / 60 / 60))/100;	
}

text = "";
text += "Oldest Species: " + string(to_minutes(max_species_age))+"min";
text += "\nCurrent Oldest Species: " + string(to_minutes(current_max_species_age))+"min";
text += "\nFPS: " + string(round(10*fps/60)/10)+"x";
text += "\nSim Time: " + string(to_minutes(sim_time))+"min";
draw_text(0,0, text);

mouse_text = "";
//mouse_text = string(Illumination.get_illumination(mouse_x, mouse_y);
draw_text(mouse_x, mouse_y, mouse_text);

function draw_array(array, xx, yy, s) {
	for(var	i = 0; i < array_length(array); ++i) {
		var value = array[i];
		var color = make_color_hsv(123, 255, 255*value);
		draw_rectangle_color(xx+i*s, yy, xx+i*s+s,yy+s, color, color, color, color, false);	
	}
}

with(Agent) {
	if(other.draw_brains) {
		draw_array(brain_outputs, x, y, 10);
		draw_array(brain_inputs, x, y-10, 10);	
	}
	
	if (other.draw_vision) {
		var vis = brain_inputs[0]
		draw_line_color(x, y, x+lengthdir_x(vision_range*vis, image_angle), y+lengthdir_y(vision_range*vis, image_angle), c_white, c_ltgray); 
	}
}


if (draw_object_counts) {
	var counts = ds_map_create();
	with(all) {
		if(!ds_map_exists(counts, object_index)) {
			counts[? object_index] = 1;
		} else {
			counts[? object_index] += 1;	
		}
	}
	
	var text = "Objects:"
	for(var it = ds_map_find_first(counts); !is_undefined(it); it = ds_map_find_next(counts,it)){
		var name = object_get_name(it);
		var count = counts[? it];
		text += "\n" + name + ": " + string(count);
	}
	
	ds_map_destroy(counts);	
	draw_set_halign(fa_right);
	draw_text(room_width,0, text);
	draw_set_halign(fa_left);
}
