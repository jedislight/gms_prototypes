// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
global.lerp_list = ds_list_create();
function LerpTodo(object, variable_name, target, duration) constructor {
	o = object;
	vn = variable_name;
	t = target;
	orig = variable_instance_get(o, vn);
	d = duration;
	od = duration;
}
function lerper_lerp(object, variable_name, target, duration){
	ds_list_add(global.lerp_list, new LerpTodo(object, variable_name, target, duration));
}