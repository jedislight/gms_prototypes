// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Brain(input_size_, output_size_) constructor{
	output_size = output_size_;
	input_size = input_size_;
	function set_inputs(array) {
		// extend me	
	}
	
	function get_outputs() {
		return [];
	}
	
	function destroy() {
		// extend me	
	}
	
	function mutate() {
		// extend me	
	}
	
	function merge(o) {
		// extend me 	
	}
	
	function copy(o) {
		//extend me	
	}
}

function MeshBrain(input_size_, output_size_) : Brain(input_size_, output_size_) constructor {
	width = input_size + 1;
	height = output_size + 1;
	state = ds_grid_create(width, height);
	ds_grid_set_region(state, 0, 0, width, height, 0);
	
	operators = ds_grid_create(width, height);
	ds_grid_set_region(operators, 0, 0, width, height, 0);
	
	pairings_x = ds_grid_create(width, height);
	ds_grid_set_region(pairings_x, 0, 0, width, height, 0);
	
	pairings_y = ds_grid_create(width, height);
	ds_grid_set_region(pairings_y, 0, 0, width, height, 0);
	
	static operator_funcs = [
		function (l, r) { return clamp(l+r, 0, 1)},
		function (l, r) { return abs(l-r)},
		function (l, r) { return l*r},
		function (l,r) { return (l+r)*0.5},
		function (l,r) { return l},
		function (l,r) { return r},
		function (l,r) { return 1.0-l},
		function (l,r) { return 1.0-r},
		function (l,r) { return 1.0},
		function (l,r) { return 0.75},
		function (l,r) { return 0.5},
		function (l,r) { return 0.25},
		function (l,r) { return 0.0}
	]
	
	function set_inputs(array) {
		for(var i = 0; i < array_length(array); ++i) {
			ds_grid_set(state, i+1, 0, array[0]);
		}
		
		for(var xx = 0; xx < width; ++xx) for(var yy = 0; yy < height; ++yy) {
			var op_index = ds_grid_get(operators, xx, yy)
			var op = operator_funcs[op_index];
			var l = ds_grid_get(state, xx, yy);
			var pair_x = ds_grid_get(pairings_x, xx ,yy);
			var pair_y = ds_grid_get(pairings_y, xx ,yy);
			var r = ds_grid_get(state, pair_x, pair_y);
			var new_value = op(l,r);
			if (is_nan(new_value)) new_value = 0.0;
			ds_grid_set(state, xx, yy, new_value);
		}

	}
	
	function get_outputs() {
		var array = [];
		for(var i = 0; i < output_size; ++i) {
			array[i] = ds_grid_get(state, 0, i+1);
		}
		
		return array;
	}
	
	function destroy() {
		ds_grid_destroy(state);	
		ds_grid_destroy(operators);
		ds_grid_destroy(pairings_x);
		ds_grid_destroy(pairings_y);
	}
	
	function mutate() {
		var mode = irandom_range(0,1);	
		var xx = irandom_range(0, width-1);
		var yy = irandom_range(0, height-1);
		if (mode == 0) { // ops
			ds_grid_set(operators, xx, yy, irandom_range(0, array_length(operator_funcs)-1));
		} else if (mode == 1) { // pairings
			ds_grid_set(pairings_x, xx, yy, irandom_range(0, width-1));
			ds_grid_set(pairings_y, xx, yy, irandom_range(0, height-1));
		}
	}
	
	function merge(o) {
		for(var xx = 0; xx < width; ++xx) for(var yy = 0; yy < height; ++yy) {
			ds_grid_set(operators, xx, yy, ds_grid_get(choose(operators, o.operators), xx, yy));
			ds_grid_set(pairings_x, xx, yy, ds_grid_get(choose(pairings_x, o.pairings_x), xx, yy));
			ds_grid_set(pairings_y, xx, yy, ds_grid_get(choose(pairings_y, o.pairings_y), xx, yy));
		}
	}
	
	function copy(o) {
		ds_grid_copy(operators, o.operators)
		ds_grid_copy(state, o.state)
		ds_grid_copy(pairings_x, o.pairings_x)
		ds_grid_copy(pairings_y, o.pairings_y)
	}
}


function NNBrain(input_size_, output_size_): Brain(input_size_, output_size_) constructor{

	in = array_create(input_size,0);
	out = array_create(output_size, 0);
	bias = array_create(output_size, 0);
	weights = array_create(output_size);
	
	for(var i = 0; i < array_length(in); ++i) for(var o = 0; o < array_length(out); ++o) {
		weights[o][i] = random_range(0,1);	
	}
	
	for(var o = 0; o < array_length(out); ++o) {
		bias[o] = random_range(-1,1);	
	}
	
	function set_inputs(array) {
		array_copy(in, 0, array, 0, array_length(array));
		
		for(var o = 0; o < output_size; ++o) {
			var total_weight = 0;
			var weights_O = weights[o];
			for(var i = 0; i < input_size; ++i) {
				total_weight += abs(weights_O[i]);	
			}
			
			var weight_mul = 1 / total_weight
			
			var raw = 0;
			for(var i = 0; i < input_size; ++i) {
				raw += in[i] * weights_O[i] * weight_mul;
			}
			
			var biased = raw + bias[o];
			var final = clamp(biased, 0, 1);
			out[o] = final;
		}
	}
	
	function get_outputs() {
		return out;
	}
	
	function destroy() {
		
	}
	
	function mutate() {
		if(choose(1,0)) {
			var index = irandom_range(0, output_size-1);
			bias[index] = clamp(bias[index] + random_range(-0.1, 0.1), -1, 1);	
		} else {
			var io = irandom_range(0, output_size-1);
			var ii = irandom_range(0, input_size-1);
			
			weights[io][ii] = clamp(weights[io][ii] + random_range(-0.1,0.1), -1, 1);
		}
	}
	
	function merge(oth) {
		for(var i = 0; i < array_length(in); ++i) for(var o = 0; o < array_length(out); ++o) {
			weights[o][i] = choose(oth.weights[o][i], 	weights[o][i]);
		}
	
		for(var o = 0; o < array_length(out); ++o) {
			bias[o] = choose(oth.bias[o], bias[o]);
		}
	}
	
	function copy(oth) {
		for(var i = 0; i < array_length(in); ++i) for(var o = 0; o < array_length(out); ++o) {
			weights[o][i] = oth.weights[o][i]	
		}
	
		for(var o = 0; o < array_length(out); ++o) {
			bias[o] = oth.bias[o];
		}
	}
}