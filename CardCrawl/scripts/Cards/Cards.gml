// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

#region Drawing

function card_preview_draw_gui() {
	if (card!=noone) {
		image_blend = card.blend;
	}
	
	if (mouse_inside or (variable_instance_exists(id, "selected") and variable_instance_get(id, "selected"))) {
		image_blend = make_color_value(image_blend, 200);	
	}

	var text_color = make_color_value(image_blend, 230);
	var shadow_color = make_color_value(image_blend, 45);

	draw_self();
	var delay_sprite = object_get_sprite(TurnOrderTrackerUI);
	draw_sprite_ext(delay_sprite, 0, x+40, y+8, 0.5, 0.5, 0,c_white, 1.0);

	if (card != noone) {
		draw_text_outline(card.name, FontCardTextSmall, fa_right, fa_middle, x+280, y+28, text_color, shadow_color);
		draw_text_outline(string(card.delay), FontCardText, fa_left, fa_middle, x+75, y+28, text_color, shadow_color);
	}
}
#endregion

#region CardBases

function CardBaseDefend(name_, defense_, delay_) : Card(name_, Target.NONE, Type.HELP) constructor {
	delay = delay_;
	text = "Shield " + string(defense_);
	defense = defense_;
	function play(by, on) {
		by.shield(defense);
	}
}

function CardBaseAttackAndDefend(name_, defense_, attack_, target_, delay_) : Card(name_, target_, Type.HARM) constructor {
	delay = delay_;
	text = "Shield " + string(defense_) + "\nAttack " + string(attack_);
	defense = defense_;
	attack = attack_;
	function play(by, on) {
		by.shield(defense);
		on.damage(attack);
	}
}

function CardBaseMoveToBackAndDefend(name_, defense_, delay_) : Card(name_, Target.NONE, Type.HELP) constructor {
	delay = delay_;
	text = "Shield " + string(defense_) + "\nReposition Back";
	defense = defense_;
	function play(by, on) {
		by.shield(defense);
		var stack = global.game.find_character_stack(by);
		array_remove(stack.members, by);
		array_push(stack.members, by);
	}
}

function CardBaseMoveToBack(name_, delay_) : Card(name_, Target.NONE, Type.HARM) constructor {
	delay = delay_;
	text = "Reposition Back";
	function play(by, on) {
		var stack = global.game.find_character_stack(by);
		array_remove(stack.members, by);
		array_push(stack.members, by);
	}
}

function CardBaseFearAttack(name_, target_, attack_, delay_) : Card(name_, target_, Type.HARM) constructor {
	delay = delay_;
	text = "Attack " + string(attack_) + "\nTarget Reposition Back";
	attack = attack_;
	function play(by, on) {
		on.damage(attack);
		var stack = global.game.find_character_stack(on);
		array_remove(stack.members, on);
		array_push(stack.members, on);
	}
}

function CardBaseMoveToFront(name_, delay_) : Card(name_, Target.NONE, Type.HARM) constructor {
	delay = delay_;
	text = "Reposition Front";
	function play(by, on) {
		var stack = global.game.find_character_stack(by);
		array_remove(stack.members, by);
		array_insert(stack.members, 0, by);
	}
}

function CardBaseChargeAttack(name_, target_, attack_, delay_) : Card(name_, target_, Type.HARM) constructor {
	delay = delay_;
	attack = attack_;
	text = "Attack " +string(attack_) + "\nReposition Front";
	function play(by, on) {
		var stack = global.game.find_character_stack(by);
		array_remove(stack.members, by);
		array_insert(stack.members, 0, by);
		
		on.damage(attack);
	}
}

function CardBaseSelfHeal(name_, heal_, delay_) : Card(name_, Target.NONE, Type.HELP) constructor {
	heal_amount = heal_;
	delay = delay_;
	text = "Heal self " + string(heal_);
	function play(by, on) {
		by.heal(heal_amount);
	}
}

function CardBaseHeal(name_, target_, heal_, delay_) : Card(name_, target_, Type.HELP) constructor {
	heal_amount = heal_;
	delay = delay_;
	text = "Heal " + string(heal_);
	
	function play(by, on) {
		on.heal(heal_amount);	
	}
}

function CardBaseAttack(name_, target_, attack_, delay_) : Card(name_, target_, Type.HARM) constructor {
	attack = attack_
	delay = delay_;
	text = "Attack " + string(attack_);
	function play(by, on) {
		on.damage(attack);
	}
}

function CardBaseAttackBurn(name_, target_, attack_, burn_, delay_) : Card(name_, target_, Type.HARM) constructor {
	attack = attack_
	delay = delay_;
	burn = burn_;
	text = "Attack " + string(attack_) +"\nBurn " + string(burn_);
	function play(by, on) {
		on.damage(attack);
		on.burn(burn);
	}
}

function CardBaseKeepAttack(name_, target_, attack_, delay_) : CardBaseAttack(name_, target_, attack_, delay_) constructor {
	keep = true;
	text += "\nKeep";
}

function CardBaseExpose(name_, target_, expose_, delay_) : Card(name_, target_, Type.HARM) constructor {
	delay = delay_;
	expose = expose_;
	text = "Expose " + string(expose_);
	function play(by, on) {
		on.expose(expose);
	}
}

function CardBaseExposeDrain(name_, target_, expose_, attack_, delay_) : Card(name_, target_, Type.HARM) constructor {
	attack = attack_
	delay = delay_;
	expose = expose_;
	text = "Expose " + string(expose_) +"\nDrain " + string(attack_);
	function play(by, on) {
		on.expose(expose);
		var dmg = on.damage(attack);
		by.heal(dmg);
	}
}

function CardBaseDrain(name_, target_, attack_, delay_) : Card(name_, target_, Type.HARM) constructor {
	attack = attack_
	delay = delay_;
	text = "Drain " + string(attack_);
	function play(by, on) {
		var dmg = on.damage(attack);
		by.heal(dmg);
	}
}

#endregion


#region Cards

function CardStab() : CardBaseKeepAttack("Stab", Target.TOP, 2, 5) constructor{}
function CardLumberingSwipe() : CardBaseAttack("Lumbering Swipe", Target.TOP, 5, 20) constructor{}
function CardTackle() : CardBaseKeepAttack("Tackle", Target.TOP, 2, 5) constructor{}
function CardSwoop() : CardBaseAttack("Swoop", Target.SELECT, 2, 3) constructor{}

function CardPhaseRegeneration() : CardBaseSelfHeal("Phase Regeneration", 10, 9) constructor {}
function CardFadeOut() : CardBaseMoveToBack("Fade Out", 4) constructor {}
function CardThrowingKnives() : CardBaseKeepAttack("Throwing Knives", Target.SELECT, 2, 4) constructor {}
function CardFromBehind() : CardBaseKeepAttack("From Behind", Target.BOTTOM, 4, 4) constructor {}
function CardPhaseSkirmishing() : CardBaseChargeAttack("Phase Skirmishing", Target.TOP, 2, 1) constructor {}

function CardFieldBandages() : CardBaseSelfHeal("Field Bandages", 5, 0) constructor {}
function CardCharge() : CardBaseChargeAttack("Charge", Target.TOP, 3, 5) constructor {}
function CardDefend() : CardBaseDefend("Defend", 4, 10) constructor {}
function CardRiposte() : CardBaseAttackAndDefend("Riposte", 2, 2, Target.TOP, 2) constructor {}

function CardCarefulShot() : CardBaseKeepAttack("Careful Shot", Target.SELECT, 5, 5) constructor {}
function CardScreamingArrow() : CardBaseFearAttack("Screaming Arrow", Target.SELECT, 1, 3) constructor {}
function CardTakeCover() : CardBaseMoveToBackAndDefend("Take Cover", 1,  5) constructor {}

function CardBurningStrike() : CardBaseAttackBurn("Burning Strike", Target.TOP, 1, 5, 5) constructor {}

function CardEssenceDrain() : CardBaseExposeDrain("Essence Drain", Target.TOP, 1, 0, 5) constructor {keep=true;}

function CardLeech() : CardBaseDrain("Leech", Target.TOP, 4, 5) constructor {keep=true;}
function CardConstrict() : CardBaseAttackAndDefend("Constrict", 4, 3, Target.TOP, 5) constructor {keep=true}
function CardRot(): CardBaseExpose("Rot", Target.TOP, 3, 1) constructor {keep=true}

function CardHealingRay() : CardBaseHeal("Healing Ray", Target.SELECT, 5, 5) constructor{}
function CardHealingRadiance() : CardBaseHeal("Healing Radiance", Target.GLOBAL, 50, 5) constructor{exhaustCard=true;text+="\nExhausts"};

function CardDevour() : Card("Devour", Target.TOP, Type.HARM) constructor {
	attack = 6;
	delay = 6;
	keep = true;
	text = "Attack " + string(attack) + "\nIf target dies:\n Heal X\nX=Target HP Max";
	
	function play(by, on) {
		var d = on.damage(attack);
		var new_on_hp = on.hp - d;
		show_debug_message("Devour expected HP after damage: " + string(new_on_hp));
		if (new_on_hp <= 0) {
			by.heal(on.hp_max);
		}
	}
}

function CardSlumber() : Card("Slumber", Target.SELECT, Type.HARM) constructor {
	text = "Exhaust 1\nKeep";
	delay = 1;
	keep = true;
	
	function play(by, on){
		var card = array_first(on.deck, function(c){return c.keep == false;});
		if (is_struct(card)) {
			array_remove(on.deck, card);
			array_push(on.exhausted, card);
			event_add(new EventCharacterPortraitFlyText(on, "Exhaust: " + string(card.name), c_red), global.game.get_event_priority_next());
		}
	}
}

#endregion