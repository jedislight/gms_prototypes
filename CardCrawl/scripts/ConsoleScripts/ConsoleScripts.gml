// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function console_write(str){
	show_debug_message(str);
	array_insert(Console.lines, 0, str);
}