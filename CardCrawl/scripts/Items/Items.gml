// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
global.item_c_map = ds_map_create();
function item_register(ctor) {
	var test_item = new ctor();
	var name = test_item.name;
	assert(not ds_map_exists(global.item_c_map, name));
	global.item_c_map[? name] = ctor;
}

function ItemAidKit(): DeckItem("Aid Kit", Sprite_misc_deck_legendary_old, DeckItemType.MINOR) constructor{
	cards = [
		new CardFieldBandages(),
	]
} item_register(ItemAidKit);

function ItemChampionShield(): DeckItem("Champion Shield", Sprite_shield_2_old, DeckItemType.MINOR) constructor{
	cards = [
		new CardCharge(),
		new CardCharge(),		
		new CardCharge(),
		
		new CardDefend(),
		new CardDefend(),
		new CardDefend(),		
		new CardDefend(),
	]
} item_register(ItemChampionShield);

function ItemDuelingSaber(): DeckItem("Dueling Saber", Sprite_sabre_1_silver, DeckItemType.MINOR) constructor{
	cards = [		
		new CardStab(),		
		new CardStab(),

		new CardRiposte(),
		new CardRiposte(),
		new CardRiposte(),
		new CardRiposte(),
		new CardRiposte(),
	]
} item_register(ItemDuelingSaber);

function ItemFadeCloak(): DeckItem("Fade Cloak", Sprite_cloak_2, DeckItemType.MINOR) constructor{
	cards = [
		new CardPhaseRegeneration(),
			
		new CardFadeOut(),
		new CardFadeOut(),
		new CardFadeOut(),		
		new CardFadeOut(),

		new CardPhaseSkirmishing(),
		new CardPhaseSkirmishing(),
		new CardPhaseSkirmishing(),
		new CardPhaseSkirmishing(),
		new CardPhaseSkirmishing(),
	]
} item_register(ItemFadeCloak);

function ItemQuietBoots(): DeckItem("Quiet Boots", Sprite_boots_4_green, DeckItemType.MINOR) constructor{
	cards = [
		new CardThrowingKnives(),		
		new CardThrowingKnives(),
		
		new CardFromBehind(),
		new CardFromBehind(),		
		new CardFromBehind(),
	]
} item_register(ItemQuietBoots);


function ItemHuntingBow(): DeckItem("Hunting Bow", Sprite_shortbow_1, DeckItemType.MINOR) constructor{
	cards = [
		new CardCarefulShot(),
		new CardCarefulShot(),
		new CardCarefulShot(),
		
		new CardScreamingArrow(),
		new CardScreamingArrow(),
		
		new CardTakeCover()
	]
} item_register(ItemHuntingBow);

function ItemLanternFlail(): DeckItem("Lantern Flail", Sprite_lantern_flail, DeckItemType.MINOR) constructor{
	cards = [
		new CardBurningStrike()
	]
} item_register(ItemLanternFlail);

function ItemStaffOfLife(): DeckItem("Staff of Life", Sprite_staff_7, DeckItemType.MINOR) constructor{
	cards = [
		new CardHealingRay(),
		new CardHealingRay(),
		new CardHealingRay(),
		
		new CardHealingRadiance()
	]
} item_register(ItemStaffOfLife);

function item_factory(name) {
	if (name == "" or is_undefined(name)) {
		return noone;	
	}
	var ctor = global.item_c_map[? name];
	if (is_undefined(ctor)) {
		return noone;	
	}
	return new ctor();	
}