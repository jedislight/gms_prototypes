// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function Creature(name_, icon_) : Character(name_, icon_) constructor {
	hand_size = 1;	
}
function CreatureSkeletalBehemoth() : Creature("Skeletal Behemoth", Sprite_skeleton_quadruped_large_old) constructor {
	hp = 8;
	hp_max = 8;
	
	deck = [
		new CardLumberingSwipe(), 
		new CardLumberingSwipe(),
		new CardTackle()
	];
}

function CreatureSkeletalBat() : Creature("Skeletal Bat", Sprite_skeleton_bat) constructor {
	hp = 2;
	hp_max = 2;
	
	deck = [
		new CardSwoop(), 
		new CardSwoop(),
		new CardTackle()
	];
}

function CreatureLesserSlumberDemon() : Creature("Lesser Slumber Demon", Sprite_lorocyproca_new) constructor {
	hp = 15;
	hp_max = 15;
	initiative = 25;
	
	deck = [
		new CardSlumber()
	];
}

function CreatureSlumberDemon() : Creature("Slumber Demon", Sprite_lorocyproca_new) constructor {
	hp = 20;
	hp_max = 20;
	initiative = 10;
	
	deck = [
		new CardSlumber()
	];
}

function CreatureWraith() : Creature("Wraith", Sprite_spectral_thing) constructor {
	hp = 1;
	hp_max = 5;
	initiative = 3;
	
	deck = [
		new CardEssenceDrain()
	];
}

function CreatureCarrionCrawler() : Creature("Carrion Crawler", Sprite_spiny_worm) constructor {
	hp = 25;
	hp_max = 25;
	initiative = 3;
	
	deck = [
		new CardLeech(),
		new CardDevour(),
		new CardConstrict(),
		new CardRot(),
	];
}

function CreatureCrystalDragon() : Creature("Crystal Dragon", Sprite_quicksilver_dragon_new) constructor {
	hp = 25;
	hp_max = 25;
	
	deck = [
		new CardTackle()
	];
}

function CreatureCrystalElemental() : Creature("Crystal Elemental", Sprite_crystal_golem) constructor {
	hp = 15;
	hp_max = 15;
	
	deck = [
		new CardTackle()
	];
}

function CreatureLivingCrystal() : Creature("Living Crystal", Sprite_fulminant_prism_3) constructor {
	hp = 1;
	hp_max = 1;
	
	deck = [
		new CardTackle()
	];
}

function CreatureCrystalShards() : Creature("Crystal Shards", Sprite_vapour) constructor {
	hp = 1;
	hp_max = 1;
	
	deck = [
		new CardTackle()
	];
}