// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
global.event_queue = ds_priority_create();
global.event_next_id = 0;
global.event_time = 0;
function Event(name_) constructor {
	name = name_;
	event_id = global.event_next_id++;
	log = true;
	
	function run() {
		show_debug_message("Running event: " + name + "(" + string(event_id) + ")");
		return true;
	}
	
	function to_string() {
		return name;	
	}
}

function event_add(event, delay) {
	var final_time = global.event_time + delay;
	show_debug_message("Event Add: " + event.to_string() + " [" + string(final_time) + "]")
	ds_priority_add(global.event_queue, event, final_time);
}

function event_run() {
	var event = ds_priority_find_min(global.event_queue);
	if (is_undefined(event)) {
		global.event_time += 1;
		return;
	}
	var event_schedule = ds_priority_find_priority(global.event_queue, event);
	if (event_schedule <= global.event_time) {
		if (event.log) {
			show_debug_message("Run event: " + event.to_string());
		}
		if (event.run()) {
			ds_priority_delete_value(global.event_queue, event);	
		}
	} else {
		global.event_time += 1;
	}
}

function event_empty() {
	return ds_priority_empty(global.event_queue);	
}

function event_array() {
	var a = [];
	var it = ds_priority_create();
	ds_priority_copy(it, global.event_queue);
	while(!ds_priority_empty(it)) {
		array_push(a, ds_priority_delete_min(it));
	}
	ds_priority_destroy(it);
	return a;
}

function event_find_time(event) {
	return ds_priority_find_priority(global.event_queue, event);	
}

function event_time_until(event) {
	var t = event_find_time(event);
	return t - global.event_time;
}