// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
global.mesh_map = ds_map_create();

enum UV {
	LEFT=0,
	TOP=1,
	RIGHT=2,
	BOTTOM=3,
}

vertex_format_begin();
vertex_format_add_position_3d();
vertex_format_add_color();
vertex_format_add_texcoord();
vertex_format_add_normal();
global.mesh_vbo_format = vertex_format_end();

function mesh(vbuff_, texture_, technique_) constructor
{
    vbuff = vbuff_;
	texture = texture_;
	technique = technique_;
	
	function draw(obj) {
		var obj_matrix = matrix_build(obj.x, obj.y, obj.z, 0, 0, obj.direction, obj.sprite_width*obj.image_xscale, obj.sprite_height*obj.image_yscale, max(obj.sprite_width, obj.sprite_height) * max(obj.image_xscale, obj.image_yscale));
		matrix_set(matrix_world, obj_matrix);
		vertex_submit(vbuff, technique, texture);
		matrix_set(matrix_world, matrix_build_identity());
	}
}

function mesh_vbo_build_cube(texture) {
	var uvs = texture_get_uvs(texture);
	var vbuff = vertex_create_buffer();
	vertex_begin(vbuff, global.mesh_vbo_format);
	
	// left 
	vertex_position_3d(vbuff, 0, 1, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.RIGHT], uvs[UV.TOP]);
	vertex_normal(vbuff, -1,0,0);
	
	vertex_position_3d(vbuff, 0, 0, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.LEFT], uvs[UV.TOP]);
	vertex_normal(vbuff, -1,0,0);
	
	vertex_position_3d(vbuff, 0, 0, 1);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.LEFT], uvs[UV.BOTTOM]);
	vertex_normal(vbuff, -1,0,0);
	
	vertex_position_3d(vbuff, 0, 0, 1);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.LEFT], uvs[UV.BOTTOM]);
	vertex_normal(vbuff, -1,0,0);
	
	vertex_position_3d(vbuff, 0, 1, 1);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.RIGHT], uvs[UV.BOTTOM]);
	vertex_normal(vbuff, -1,0,0);
	
	vertex_position_3d(vbuff, 0, 1, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.RIGHT], uvs[UV.TOP]);
	vertex_normal(vbuff, -1,0,0);
	
	// right	
	vertex_position_3d(vbuff, 1, 0, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.RIGHT], uvs[UV.TOP]);
	vertex_normal(vbuff, 1,0,0);
	
	vertex_position_3d(vbuff, 1, 1, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.LEFT], uvs[UV.TOP]);
	vertex_normal(vbuff, 1,0,0);
	
	vertex_position_3d(vbuff, 1, 0, 1);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.RIGHT], uvs[UV.BOTTOM]);
	vertex_normal(vbuff, 1,0,0);
	
	vertex_position_3d(vbuff, 1, 1, 1);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.LEFT], uvs[UV.BOTTOM]);
	vertex_normal(vbuff, 1,0,0);
	
	vertex_position_3d(vbuff, 1, 0, 1);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.RIGHT], uvs[UV.BOTTOM]);
	vertex_normal(vbuff, 1,0,0);
	
	vertex_position_3d(vbuff, 1, 1, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.LEFT], uvs[UV.TOP]);
	vertex_normal(vbuff, 1,0,0);
	
	// top
	vertex_position_3d(vbuff, 0, 0, 1);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.RIGHT], uvs[UV.BOTTOM]);
	vertex_normal(vbuff, 0,-1,0);
	
	vertex_position_3d(vbuff, 0, 0, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.RIGHT], uvs[UV.TOP]);
	vertex_normal(vbuff, 0,-1,0);
	
	vertex_position_3d(vbuff, 1, 0, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.LEFT], uvs[UV.TOP]);
	vertex_normal(vbuff, 0,-1,0);
	
	vertex_position_3d(vbuff, 1, 0, 1);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.LEFT], uvs[UV.BOTTOM]);
	vertex_normal(vbuff, 0,-1,0);
	
	vertex_position_3d(vbuff, 0, 0, 1);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.RIGHT], uvs[UV.BOTTOM]);
	vertex_normal(vbuff, 0,-1,0);
	
	vertex_position_3d(vbuff, 1, 0, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.LEFT], uvs[UV.TOP]);
	vertex_normal(vbuff, 0,-1,0);
	
	// bottom	
	vertex_position_3d(vbuff, 0, 1, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.LEFT], uvs[UV.TOP]);
	vertex_normal(vbuff, 0,1,0);
	
	vertex_position_3d(vbuff, 0, 1, 1);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.LEFT], uvs[UV.BOTTOM]);
	vertex_normal(vbuff, 0,1,0);
	
	vertex_position_3d(vbuff, 1, 1, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.RIGHT], uvs[UV.TOP]);
	vertex_normal(vbuff, 0,1,0);
	
	vertex_position_3d(vbuff, 0, 1, 1);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.LEFT], uvs[UV.BOTTOM]);
	vertex_normal(vbuff, 0,1,0);
	
	vertex_position_3d(vbuff, 1, 1, 1);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.RIGHT], uvs[UV.BOTTOM]);
	vertex_normal(vbuff, 0,1,0);
	
	vertex_position_3d(vbuff, 1, 1, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[UV.RIGHT], uvs[UV.TOP]);
	vertex_normal(vbuff, 0,1,0);
	
	vertex_end(vbuff)	
	return vbuff;
}

function mesh_vbo_build_flat(texture) {
	var uvs = texture_get_uvs(texture);
	var vbuff = vertex_create_buffer();
	vertex_begin(vbuff, global.mesh_vbo_format);
	
	// downward
	vertex_position_3d(vbuff, 1, 0, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[2], uvs[1]);
	vertex_normal(vbuff, 0,0,-1);
	
	vertex_position_3d(vbuff, 0, 0, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[0], uvs[1]);
	vertex_normal(vbuff, 0,0,-1);
		
	vertex_position_3d(vbuff, 0, 1, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[0], uvs[3]);
	vertex_normal(vbuff, 0,0,-1);
		
	vertex_position_3d(vbuff, 1, 0, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[2], uvs[1]);
	vertex_normal(vbuff, 0,0,-1);
	
	vertex_position_3d(vbuff, 0, 1, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[0], uvs[3]);
	vertex_normal(vbuff, 0,0,-1);
		
	vertex_position_3d(vbuff, 1, 1, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[2], uvs[3]);
	vertex_normal(vbuff, 0,0,-1);
	
	// uppward
	vertex_position_3d(vbuff, 0, 0, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[0], uvs[1]);
	vertex_normal(vbuff, 0,0,1);
		
	vertex_position_3d(vbuff, 1, 0, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[2], uvs[1]);
	vertex_normal(vbuff, 0,0,1);
		
	vertex_position_3d(vbuff, 0, 1, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[0], uvs[3]);
	vertex_normal(vbuff, 0,0,1);
		
	vertex_position_3d(vbuff, 0, 1, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[0], uvs[3]);
	vertex_normal(vbuff, 0,0,1);
		
	vertex_position_3d(vbuff, 1, 0, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[2], uvs[1]);
	vertex_normal(vbuff, 0,0,1);
		
	vertex_position_3d(vbuff, 1, 1, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[2], uvs[3]);
	vertex_normal(vbuff, 0,0,1);
		
	vertex_end(vbuff)	
	return vbuff;
}

function mesh_vbo_build_billboard(texture) {
	var uvs = texture_get_uvs(texture);
	var vbuff = vertex_create_buffer();
	vertex_begin(vbuff, global.mesh_vbo_format);
	var fn0 = 0;
	var fn1 = 0;
	var fn2 = -1;
			
	vertex_position_3d(vbuff, .5, 0, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[2], uvs[1]);
	vertex_normal(vbuff, fn0, fn1, fn2);
	
	vertex_position_3d(vbuff, -.5, 0, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[0], uvs[1]);
	vertex_normal(vbuff, fn0, fn1, fn2);
		
	vertex_position_3d(vbuff, -.5, 0, 1);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[0], uvs[3]);
	vertex_normal(vbuff, fn0, fn1, fn2);
	
	vertex_position_3d(vbuff, -.5, 0, 1);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[0], uvs[3]);
	vertex_normal(vbuff, fn0, fn1, fn2);
	
	vertex_position_3d(vbuff, .5, 0, 1);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[2], uvs[3]);
	vertex_normal(vbuff, fn0, fn1, fn2);
	
	vertex_position_3d(vbuff, .5, 0, 0);
	vertex_color(vbuff,c_white, 1.0);
	vertex_texcoord(vbuff, uvs[2], uvs[1]);
	vertex_normal(vbuff, fn0, fn1, fn2);
		
	vertex_end(vbuff)	
	return vbuff;
}

function mesh_add(index, vbuff, texture, technique) {
	ds_map_add(global.mesh_map, index, new mesh(vbuff, texture, technique));
}

function mesh_exists(index) {
	return ds_map_exists(global.mesh_map, index);	
}

function mesh_draw(index) {
	var m = global.mesh_map[? index];
	m.draw(id);
}

function mesh_delete(index) {
	var m =global.mesh_map[? index]	
	if (is_undefined(m)) return;
	vertex_delete_buffer(m.vbuff);
	ds_map_delete(global.mesh_map, index);
}