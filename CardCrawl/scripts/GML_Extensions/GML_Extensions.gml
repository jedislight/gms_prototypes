// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function draw_text_outline(text, a_font, h_align, v_align, xx, yy, color, shadow_color) {
	var prev_h = draw_get_halign()
	var prev_v = draw_get_valign();
	var prev_font = draw_get_font();
	draw_set_font(a_font);
	draw_set_halign(h_align);
	draw_set_valign(v_align);
	
	var offset = font_get_size(a_font) * 0.10;
	draw_text_color(xx-offset, yy, text, shadow_color, shadow_color, shadow_color, shadow_color, 1.0);
	draw_text_color(xx+offset, yy, text, shadow_color, shadow_color, shadow_color, shadow_color, 1.0);
	draw_text_color(xx, yy+offset, text, shadow_color, shadow_color, shadow_color, shadow_color, 1.0);
	draw_text_color(xx, yy-offset, text, shadow_color, shadow_color, shadow_color, shadow_color, 1.0);
	
	draw_text_color(xx+offset, yy-offset, text, shadow_color, shadow_color, shadow_color, shadow_color, 1.0);
	draw_text_color(xx-offset, yy-offset, text, shadow_color, shadow_color, shadow_color, shadow_color, 1.0);
	draw_text_color(xx+offset, yy+offset, text, shadow_color, shadow_color, shadow_color, shadow_color, 1.0);
	draw_text_color(xx-offset, yy+offset, text, shadow_color, shadow_color, shadow_color, shadow_color, 1.0);
	
	draw_text_color(xx, yy, text, color, color, color, color, 1.0);
	
	draw_set_valign(prev_v);
	draw_set_halign(prev_h);
	draw_set_font(prev_font);
}

function array_find(array, item) {
	for(var i = 0; i < array_length(array); ++i) {
		if (array[i] == item) {
			return i;	
		}
	}
	
	return -1;
}

function array_contains(array, item) {
	var i = array_find(array, item);
	
	return i >= 0;
}

function array_front(array) {
	return array[0];	
}

function array_back(array){
	return array[array_length(array)-1];	
}

function array_first(array, predicate){
	for (var i = 0; i < array_length(array); ++i) {
		var item = array[i];
		var match = predicate(item);
		if (match) {
			return item;	
		}
	}
	
	return undefined;
}

function array_last(array, predicate){
	for (var i = array_length(array)-1; i >= 0; --i) {
		var item = array[i];
		var match = predicate(item);
		if (match) {
			return item;	
		}
	}
	
	return undefined;
}

function array_filter(array, predicate) {
	var filtered = [];
	for(var i = 0; i < array_length(array); ++i) {
		var item = array[i];
		if (predicate(item)) {
			array_push(filtered, item);	
		}
	}
	
	return filtered;
}

function array_sample(array) {
	var index = irandom_range(0, array_length(array)-1);
	return array[index];
}

function array_remove(array, to_remove) {
	for (var i =0; i < array_length(array); ++i) {
		var item = array[i];
		if (item == to_remove) {
			array_delete(array, i, 1);	
			--i;
		}
	}
}

function make_color_inverted(c) {
	return make_color_rgb(
		255 - color_get_red(c),
		255 - color_get_green(c),
		255 - color_get_blue(c)
	);
}

function make_color_window_mouse() {
	var r = point_distance(0,0, window_mouse_get_x(), window_mouse_get_y());
	var g = point_distance(window_get_width(),0, window_mouse_get_x(), window_mouse_get_y());
	var b = point_distance(window_get_width(),window_get_height(), window_mouse_get_x(), window_mouse_get_y());
	
	var sum = r + g + b;
	
	var r = r/sum * 255;
	var g = g/sum * 255;
	var b = b/sum * 255;
	
	show_debug_message("MouseColor: " + string(r) + " " + string(g) + " " + string(b));
	return make_color_rgb(r,g,b);
}

function make_color_value(base, value) {
	return make_color_hsv(color_get_hue(base), color_get_saturation(base), value);
}

function assert(condition, msg) {
	if (not condition) {
		show_debug_message("==============")
		if (is_string(msg)){
			show_debug_message("ERROR: "+msg);
		}
		
		var _a = debug_get_callstack(100);
        for (var i = 1; i < array_length(_a)-1; ++i) {
            show_debug_message(_a[i]);
        }
		
		show_debug_message("==============")
		
		var a = noone;
		a.assertion.has.occured ++;
	}
}

function array_push_all(dest, src) {
	for(var i = 0; i < array_length(src); ++i) {
		array_push(dest, src[i]);	
	}
}

function string_split(str) {
	var result = [];
	var word = "";
	for(var s = 1; s <= string_length(str); ++s) {
		var char = string_char_at(str, s);
		if (char == " ") {
			if (word != "") {
				array_push(result, word);
				word = ""
			}
		}
		else {
			word += char;
		}
	}
	
	if (word != "") {
		array_push(result, word);
	}
	return result;
}