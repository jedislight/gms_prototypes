// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function EventMove(object_, xx, yy) : Event("Move") constructor{
	tx = xx;
	ty = yy;
	object = object_
	lerp_speed = 0.15;
	log = false;
	function run() {
		if(!instance_exists(object)) {
			return true;	
		}
		object.x = lerp(object.x, tx, lerp_speed);
		object.y = lerp(object.y, ty, lerp_speed);
		
		if (point_distance(object.x, object.y, tx, ty) < 1.0) {
			object.x = tx;
			object.y = ty;
			return true;
		}
		
		return false;
	}
}

function EventTurn(object_, new_dir) : Event("Turn") constructor{
	dir = new_dir
	object = object_
	log = false;
	function run() {
		var delta  = angle_difference(object.direction, dir);
		object.direction -=  delta * .1
		if (abs(delta) < 1.0) {
			object.direction = dir;
			return true;
		}
		return false;
	}
}

function EventVariableLerp(object_, variable_, value_, lerp_speed_) : Event("VariableLerp") constructor {
	object = object_;
	variable_name = variable_;
	value = value_;
	lerp_speed = lerp_speed_;
	log = false;
	
	function run() {
		var current = variable_struct_get(object, variable_name);	
		var next = lerp(current, value, lerp_speed);
		if(abs(next - value) < 0.25) {
			next = value;	
		}
		
		variable_struct_set(object, variable_name, next);
		
		return next == value;
	}
}

function EventCombatTurn(character_) : Event("CombatTurn") constructor {
	character = character_;
	log = false;
	function run() {
		character.defense = 0;
		if(global.current_enemy_stack_pawn_combatant == noone or character.hp == 0) {
			return true;	
		}
		character.draw_hand();
		if (array_length(character.hand) == 0) {
			event_add(new EventCharacterPortraitFlyText(character, "Exhausted", c_red), global.game.get_event_priority_next());
			character.damage(1);
			
			var action_delay = 5;
			event_add(new EventCombatTurn(character), action_delay);
			return true;
		}
		var selected_card = noone;
		if (global.game.is_character_in_player_party(character)) {
			with(CombatCardSelectionPreviewUI){
				if(selected) selected_card = card;
			}
			
		} else {
			selected_card = array_front(character.hand);	
		}
		if (selected_card == noone) {
			return false;	
		}
		
		CardSplashUI.card = noone;
		var target = noone;
		if (selected_card.target != Target.NONE) {
			
			if (global.game.is_character_in_player_party(character)) {
				help_harm = [global.game.player_party, global.current_enemy_stack_pawn_combatant.enemy_stack];		
			} else {
				help_harm = [global.current_enemy_stack_pawn_combatant.enemy_stack, global.game.player_party];	
			}
			
			var selected_stack = help_harm[selected_card.type];
			if(selected_card.target == Target.BOTTOM) {
				target = array_last(selected_stack.members, function (m){return m.hp>0;});
			} else if (selected_card.target == Target.TOP) {
				target = array_first(selected_stack.members, function (m){return m.hp>0;});
			} else if (selected_card.target == Target.GLOBAL) {
				target = [];
				var friends = help_harm[Type.HELP];
				var foes = help_harm[Type.HARM];
				array_push_all(target, friends.members);
				array_push_all(target, foes.members);
			} else if (selected_card.target == Target.SELECT) {
				if (global.game.is_character_in_player_party(character)){
					for (var i = 0; i < array_length(selected_stack.members); ++i){
						var target_character = selected_stack.members[i];
						with(CharacterPortraitUI) {
							if (target_character == character) {
								attention = true;
								if ( mouse_inside )
								if( mouse_check_button_pressed(mb_left))
								{
									target = character;
									break;
								}
							}
						}
					}
					
					if (target == noone and array_length(selected_stack.members) > 0) {
						return false;	
					}
				}
				else {
					var valid_targets = array_filter(selected_stack.members, function(m){return m.hp>0;});
					if (array_length(valid_targets) == 0) {
						target = noone;	
					} else {
						target = array_sample(valid_targets);
					}
				}
			}
			
			if (is_undefined(target)) {
				target = noone;	
			}
		}
		
		if (target != noone or selected_card.target == Target.NONE) {
			event_add(new EventCharacterPortraitFlyText(character, selected_card.name, selected_card.blend), global.game.get_event_priority_next());
			if (is_array(target)) {
				for(var t = 0; t < array_length(target); ++t) {
					selected_card.play(character, target[t]);	
				}
			}
			else {
				selected_card.play(character, target);		
			}
		}
		var action_delay = selected_card.delay;
		if (selected_card.exhaustCard) {
			character.exhaust_card(selected_card);
		} else {
			character.discard_card(selected_card);
		}
		event_add(new EventCombatTurn(character), action_delay);
		character.draw_hand();
		with(CombatCardSelectionPreviewUI) selected = false;
		if (character.burning > 0) {
			character.damage(character.burning);
			character.burning -= selected_card.delay;
			character.burning = max(0, character.burning);
			event_add(new EventCharacterPortraitFlyText(character, "Burning (" +string(character.burning)+" left)", c_orange), global.game.get_event_priority_next());
		}
		return true;
		
	}
	
	function to_string() {
		return name + " " + character.name;	
	}
}

function EventMultiEvent(name_, events_) : Event(name_) constructor {
	events = events_;
	log = false;
	function run() {
		for(var i = 0; i < array_length(events); ++i) {
			var e = events[i];
			var done = e.run();
			if(done){
				array_delete(events, i, 1);
				--i;
			}
		}
		return array_length(events) == 0;
	}
	
	function to_string() {
		return name + "["+string(array_length(events))+"]";	
	}
}

function EventCharacterPortraitFlyText(character_, text_, color_) : Event("FlyText") constructor {
	object = noone;
	character = character_;
	text = text_;
	color = color_;
	
	function run() {
		if (object == noone) {
			var portrait = noone;
			with(CharacterPortraitUI) {
				if (character == other.character and (instance_number(TurnOrderTrackerUI) == 0 or TurnOrderTrackerUI.y != y) ) {
					portrait = id;
					break;
				}
			}
			if (portrait == noone) {
				return true;
			}
			object = instance_create_layer(portrait.x, portrait.y, "UI", FlyTextUI);
			object.direction = 180;
			object.front_color = color;
			object.text = text;
			object.h_align = fa_right;
		}
		
		return not instance_exists(object);
	}
}

function EventPlaySound(sound_asset_, wait_) : Event("PlaySound") constructor {
	sound_asset = sound_asset_;
	wait = wait_;
	sound = noone;
	
	function run() {
		if (sound == noone) {
			sound = audio_play_sound(sound_asset, 1, false);
		}
		
		return !audio_is_playing(sound) or !wait;
	}
}

enum DialogSpeaker {
	LEFT,
	RIGHT,
	BOTH,
	NONE,
}
function EventDialog(character1_, character2_, text_, speaker_) : Event("Dialog") constructor {
	c1 = character1_;
	c2 = character2_;
	text = text_;
	speaker=speaker_;
	object = noone;
	
	function run() {
		if (object == noone) {
			object = instance_create_layer(64, 448, "UI", DialogLineUi);
			object.text = text;
			object.character = c1;
			object.character2 = c2;
			object.left_speaker = speaker == DialogSpeaker.LEFT or speaker == DialogSpeaker.BOTH;
			object.right_speaker = speaker == DialogSpeaker.RIGHT or speaker == DialogSpeaker.BOTH;
			with(SlideAnchorUI) {closed = true;}
		}
		
		return not instance_exists(object);
	}
	
}

function interact_text(text) {
	event_add(new EventDialog(noone, noone, text, DialogSpeaker.NONE),0);
}