// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function Game() constructor{
	
	function initialize() {
		key_items = ds_map_create();
	
		player_party = new CharacterStack();
		alice = new CharacterFadeStalker("Alice");
		bob = new CharacterChampion("Bob");
		player_party.add(bob);
		player_party.add(alice);
		items = [];
		last_selected_party_member = noone;
		transition_anchor = "";
		event_priority_procedural = -10000;
	}
	initialize();
	
	function get_event_priority_next() {
		return event_priority_procedural++;	
	}
	
	
	function has_key_item(name) { 
		return ds_map_exists(key_items, name) && key_items[? name] > 0;
	}
	
	function add_key_item(name, amount) {
		if (is_undefined(amount)) {
			amount = 1;
		}
		if (not ds_map_exists(key_items, name)){
			key_items[? name] = 0;	
		}
		
		key_items[? name] += amount;
	}
	
	function remove_key_item(name, amount) {
		return add_key_item(name, -amount);
	}
	
	function set_key_item(name, amount) {
		key_items[? name] = amount;	
	}
	
	function get_key_item_count(name) {
		var value = key_items[? name];
		if ( is_undefined(value)) {
			return 0;	
		}
		
		return value;
	}
	
	function destroy_if_key_item_held(obj_id, key) {
		if (has_key_item(key)) {
			instance_destroy(obj_id);	
		}
	}
	
	function is_character_in_player_party(character) {
		return array_contains(player_party.members, character)	
	}
	
	function find_character_stack(character) {
		if (is_character_in_player_party(character)) {
			return player_party;	
		} else {
			return global.current_enemy_stack_pawn_combatant.enemy_stack;	
		}
	}
	
	function get_ui_character() {
		var current_event = ds_priority_find_min(global.event_queue);
		if (is_struct(current_event) and current_event.name == "CombatTurn"){
			var c = current_event.character;
			if (global.game.is_character_in_player_party(c)){
				last_selected_party_member = c;
				return c;
			}
		}

		with(CharacterPortraitUI) {
			if (selected) {
				other.last_selected_party_member = character;
				return character;
			}
		}
		
		return noone;
	}
	
	function get_last_selected_ui_character() {
		return last_selected_party_member;	
	}
	
	function goto_room(room_id, anchor_name) {
		ds_priority_clear(global.event_queue);
		audio_stop_all();
		transition_anchor = anchor_name;
		room_goto(room_id);
	}
	
	function save() {
		save_map = ds_map_create();
		save_map[? "x"] = MapPawn.x;
		save_map[? "y"] = MapPawn.y;
		save_map[? "direction"] = MapPawn.direction;
		save_map[? "room_name"] = room_get_name(room);
		var key_item_map_copy = ds_map_create();
		ds_map_copy(key_item_map_copy, key_items);
		ds_map_add_map(save_map, "key_items", key_item_map_copy);
		save_map[? "fog_color"] = global.fog_color;
		save_map[? "fog_start"] = global.fog_start;
		save_map[? "fog_end"] = global.fog_end;
		save_map[? "ambient_light_target"] = global.ambient_light_target
		save_map[? "directional_light_target"] = global.directional_light_target
		ds_map_add_map(save_map, "alice_equipment", global.game.alice.equipment.save());
		ds_map_add_map(save_map, "bob_equipment", global.game.bob.equipment.save());
		
		var item_names = ds_list_create();
		for(var i = 0; i < array_length(items); ++i) {
			var item = items[i];
			ds_list_add(item_names, item.name);			
		}
		ds_map_add_list(save_map, "item_names", item_names);
		
		var file = file_text_open_write("save.json");
		file_text_write_string(file, json_encode(save_map));
		file_text_close(file);
		
		ds_map_destroy(save_map);
	}
	
	function load() {
		var file = file_text_open_read("save.json");
		if (file != -1) {
			initialize();
			var load_map = json_decode(file_text_readln(file));
			key_items = load_map[? "key_items"];
			ds_priority_clear(global.event_queue);
			items = [];
			var item_names = load_map[? "item_names"];
			for(var i = 0; i < ds_list_size(item_names); ++i) {
				array_push(items, item_factory(item_names[|i]));	
			}
			ds_list_destroy(item_names);
			
			global.game.alice.equipment.load(load_map[? "alice_equipment"]);
			global.game.bob.equipment.load(load_map[? "bob_equipment"]);
			global.game.alice.reset_cards()
			global.game.bob.reset_cards()
			
			instance_create_depth(0,0,0, LoadFinalizer);
			room_goto(asset_get_index(load_map[? "room_name"]));
			file_text_close(file);
		}
	}
}

global.game = new Game();