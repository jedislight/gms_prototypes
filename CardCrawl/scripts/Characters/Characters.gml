// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Equipment() constructor {
	minors = [noone, noone, noone, noone];
	
	function save() {
		function item_reference_to_name(item) {
			if (is_struct(item)) {
				return item.name;	
			}
			return "";
		}
		var json = ds_map_create();
		json[? "minors"] = [item_reference_to_name(minors[0]), item_reference_to_name(minors[1]), item_reference_to_name(minors[2])];
		return json;
	}
	
	function load(json) {
		minors = [ noone, noone, noone, noone ];
		
		var minor_names_list = json[? "minors"];
		for(var i = 0; i < array_length(minors); ++i) {
			minors[i] = item_factory(minor_names_list[| i]);	
		}
	}
}

function Character(name_, icon_) constructor{
	name = name_
	icon = icon_;
	initiative  = 3;
	hand_size = 3;
	deck = [];
	hand = []
	discard = [];
	exhausted = [];
	defense = 0;
	color = c_white;
	equipment = new Equipment();
	
	hp = 30;
	hp_max = 30;
	burning = 0;
	
	function damage(value) {
		var final = value - defense;
		
		final = max(final, 0);
		event_add(new EventMultiEvent("DamageFlyText", [
			new EventCharacterPortraitFlyText(self, string(final), c_red),
			new EventVariableLerp(self, "hp", max(0, hp-final), 0.05),
			new EventPlaySound(Sound_Critical_Hit_II, false)
		]), global.game.get_event_priority_next());
		
		return final;
	}
	
	function burn(value) {
		burning += value;
			event_add(new EventMultiEvent("DamageFlyText", [
			new EventCharacterPortraitFlyText(self, "Burn " + string(value) +" ("+string(burning)+")", c_orange),
			new EventPlaySound(Sound_Fireball_I, false)
		]), global.game.get_event_priority_next());
	}
	
	function heal(value) {
		event_add(new EventMultiEvent("HealingFlyText", [
			new EventCharacterPortraitFlyText(self, string(value), c_lime),
			new EventVariableLerp(self, "hp", min(hp_max, hp+value), 0.05),
			new EventPlaySound(Sound_Detect_Magic, false)
		]), global.game.get_event_priority_next());
	}
	
	function shield(value) {
			event_add(new EventMultiEvent("ShieldFlyText", [
				new EventCharacterPortraitFlyText(self, "Shield +" + string(value), c_white),
				new EventPlaySound(Sound_Command_, false)
		]), global.game.get_event_priority_next());
		defense = defense + value;
	}
	
	function expose(value) {
			event_add(new EventMultiEvent("ShieldFlyText", [
				new EventCharacterPortraitFlyText(self, "Expose " + string(value), c_red),
				new EventPlaySound(Sound_Psychic_Scream, false)
		]), global.game.get_event_priority_next());
		defense = defense - value;
	}
	
	function draw_hand() {
		if (array_length(deck) == 0 and array_length(discard) > 0){
			var d = irandom_range(0, array_length(discard)-1);
			var card = discard[d];
			if (!card.keep) {
				array_delete(discard, d, 1);
				array_push(exhausted, card);
			}
			deck = discard;
			discard = [];
		}
		while(array_length(hand) < hand_size and array_length(deck) > 0) {
			var d = irandom_range(0, array_length(deck)-1);
			var card = deck[d];
			array_delete(deck, d, 1);
			array_push(hand, card);
		}

	}
	
	function play_card(card, on) {
		card.play(id, on);
		discard_card(card);
	}
	
	function discard_card(card){
		for (var i = 0; i < array_length(hand); ++i) {
			var c = hand[i];
			if (c == card) {
				array_delete(hand, i, 1);
				array_push(discard, c);
				break;	
			}
		}
	}
	
	function exhaust_card(card){
		for (var i = 0; i < array_length(hand); ++i) {
			var c = hand[i];
			if (c == card) {
				array_delete(hand, i, 1);
				array_push(exhausted, c);
				break;	
			}
		}
	}
	
	function reset_cards() {
		deck = [];
		discard = [];
		exhausted = [];
		hand = [];
		
		all_items = equipment.minors;
		for (var i = 0; i < array_length(all_items); ++i){
			var item = all_items[i];
			if (item == noone) continue;
			for(var j = 0; j < array_length(item.cards); ++j){
				item.cards[j].source = item;
				item.cards[j].blend = color;
				array_push(deck, item.cards[j]);
			}
		}
	}
}

function CharacterStack() constructor{
	members = []
	
	function add(character) {
		array_push(members, character);	
	}
}

function Card(name_, target_, type_) constructor{
	name = name_;
	target = target_;
	type = type_
	text = "";
	keep = false; // never exhausts
	delay = 5;
	attack = 0;
	blend = c_white;
	source = noone;
	exhaustCard = false; // skip discard - goto exhausted directly on use
	
	enum Target {
		TOP,
		BOTTOM,
		SELECT,
		GLOBAL,
		NONE
	}
	
	enum Type {
		HELP=0,
		HARM=1
	}
	
	function play(by,on) {
		show_debug_message("DEFAULT CARD PLAY");
	}
}

enum DeckItemType {
	MAJOR,
	MINOR
	
}
function DeckItem(name_, icon_, type_) constructor {
	name = name_;
	icon = icon_;
	cards = [];
	type = type_;
}

#region Character
function CharacterFadeStalker() : Character("Alice", Sprite_deep_elf_blademaster) constructor {
	hp = 12;
	hp_max = 12;
	initiative = 1;
	color = make_color_rgb(148,58,48);
	deck = [];
	equipment.minors[0] = new ItemFadeCloak();
	equipment.minors[1] = new ItemQuietBoots();
	
	reset_cards()
}

function CharacterChampion() : Character("Bob", Sprite_deep_elf_knight_new) constructor {
	hp = 20;
	hp_max = 20;
	hand_size = 2;
	
	color = c_dkgray;
	
	equipment.minors[0] = new ItemDuelingSaber();
	equipment.minors[1] = new ItemChampionShield();
	equipment.minors[2] = new ItemAidKit();
	
	reset_cards()
}
#endregion