// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
global.debug_overlay_enabled = false;

global.view_3d = 2
global.view_map = 1
global.view_reserved = 0;
global.frame = 0;

global.current_enemy_stack_pawn_combatant = noone;

global.interior_fog_color = c_black;
global.exterior_fog_color = make_color_rgb(137, 128, 229);
global.fog_color = global.interior_fog_color;
global.fog_start = 256;
global.fog_end = 512;
global.fog_target_color = global.fog_color;
global.ambient_light = c_white;
global.ambient_light_target = make_color_value(c_white, 240);
global.directional_light = c_white;
global.directional_light_target = make_color_value(c_white, 240);

randomize();
