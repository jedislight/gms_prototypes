/// @description Insert description here
// You can write your code in this editor
event_inherited();

visible = is_struct(character);

if (character_index >= 0 and character_index < 4) {
	var ma = global.game.player_party.members;
	if (array_length(ma) > character_index) {
		character = ma[character_index];	
	}
}

if (is_struct(character)) {
	var is_party_member = global.game.is_character_in_player_party(character);
	if (character.hp == 0 and not is_party_member) {
		instance_destroy(id);
		exit;
	}
	
	var min_y = 64;
	var is_in_turn_order = instance_number(TurnOrderTrackerUI) > 0 and y == TurnOrderTrackerUI.y
	if (not is_party_member and not is_in_turn_order) {		
		var stack = global.current_enemy_stack_pawn_combatant.enemy_stack;
		var peers = stack.members;
		var index = array_find(peers, character);
		var ty = min_y + sprite_height * index;
		if (ty != y) {
			var events = event_array();
			var existing_move_events = array_filter(events, function(e){return e.name == "Move" and e.object == id;});
			if (array_length(existing_move_events) == 0) {
				event_add(new EventMove(id, x, ty), 0);		
			}
		}
	}
}

if (!global.game.is_character_in_player_party(character)) {
	if (mouse_inside and !mouse_previously_inside) {
		//enter	
		if (instance_number(TurnOrderTrackerUI) > 0 and y == TurnOrderTrackerUI.y) {
			if (array_length(character.hand) > 0) {
			CardSplashUI.card = array_front(character.hand);
			}
		}
	}

	if (!mouse_inside and mouse_previously_inside) {
		if (instance_number(TurnOrderTrackerUI) > 0 and y == TurnOrderTrackerUI.y) {
			if (array_length(character.hand) > 0) {
				if (CardSplashUI.card == array_front(character.hand)) {
					CardSplashUI.card = noone;	
				}
			}
		}
	}

}