/// @description Insert description here
// You can write your code in this editor
image_blend = c_green;
if (not global.game.is_character_in_player_party(character)) {
	image_blend = c_maroon;	
}
if (selected) {
	image_blend = c_aqua;	
}
if (mouse_inside) {
	image_blend = make_color_value(image_blend, 200);	
}
if (attention) {
	image_blend = make_color_value(image_blend, color_get_value(image_blend) + sin(global.frame/11)*100);	
}
event_inherited()
if (character != noone){
	draw_sprite(character.icon, 0, x, y);
	var w = 5;
	var left = (x+sprite_get_width(character.icon) * image_xscale) - w;
	var right = left + w;
	var amount =  character.hp / character.hp_max * 100;
	draw_healthbar(left, y, right, y+sprite_get_height(character.icon)*image_yscale, amount, c_maroon, c_lime, c_lime, 3, true, true);
	var text = display_text_override;
	if (text == "") {
		text = 	string(ceil(character.hp)) + "/" + string(ceil(character.hp_max))
	}
	draw_text_outline(text, FontItemCounter, fa_center, fa_bottom, x+0.5*sprite_width*image_xscale, y+sprite_height*image_yscale, c_white, c_black);
}