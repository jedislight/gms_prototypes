/// @description Insert description here
// You can write your code in this editor
for(var i = 1; i <= 10; ++i){
	var varname = "enemy"+string(i);
	
	var varvalue = variable_instance_get(id, varname);
	if ( is_struct(varvalue)) {
		enemy_stack.add(varvalue);
		variable_instance_set(id, varname, undefined);
	}
	
	variable_instance_set(id, varname, "");
}

for (var i = 0; i < array_length(enemy_stack.members); i++) {
	var enemy = enemy_stack.members[i];
	if ( enemy.hp == 0 ) {
		array_delete(enemy_stack.members, i, 1);
		--i;
	}
}

if (array_length(enemy_stack.members) > 0) {
	var rebuild_mesh = sprite_index != enemy_stack.members[0].icon;
	sprite_index = enemy_stack.members[0].icon;	
	
	if(rebuild_mesh and mesh_exists(id)){
		mesh_delete(id);	
	}
	if (rebuild_mesh) {
		var texture = sprite_get_texture(sprite_index, 0);
		mesh_add(id, mesh_vbo_build_billboard(texture), texture, pr_trianglelist);	
		mesh_index = id;
	}
} else {
	global.current_enemy_stack_pawn_combatant = noone;
	instance_destroy();	
	exit;
}

image_xscale = 1+sin(global.frame/1000)/33
image_yscale = image_xscale;
z = sin(global.frame/1000)*5