/// @description Insert description here
// You can write your code in this editor
if (event_empty()) {
	event_add(new EventTurn(other.id, point_direction(other.x, other.y, x+32, y+32)), 0);	
	global.current_enemy_stack_pawn_combatant = id;
	if(spawn_key != "constant") {
		global.game.add_key_item(spawn_key, 1)
	}
	
	for(var i = 0; i < array_length(enemy_stack.members); ++i){
		var enemy = enemy_stack.members[i]	;
		event_add(new EventCombatTurn(enemy), enemy.initiative);
		var n = instance_create_layer(910-64, 64+i*64, "UI", CharacterPortraitUI);
		n.character = enemy;
		for(var c = 0; c < array_length(enemy.deck); ++c) {
			enemy.deck[c].source = enemy;
		}
		enemy.draw_hand();
	}
	
	for(var i = 0; i < array_length(global.game.player_party.members); ++i){
		var character = global.game.player_party.members[i]	;
		event_add(new EventCombatTurn(character), character.initiative);
	}
	
	instance_create_layer(0,0, "UI", TurnOrderTrackerUI);
	
	with(SlideAnchorUI) {
		closed = name != "slide2";
	}
}