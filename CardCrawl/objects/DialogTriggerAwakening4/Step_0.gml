/// @description Insert description here
// You can write your code in this editor
if(entering){
	var line = 1;
	event_add(new EventDialog(global.game.alice, noone, "Did lord what's-his-face ever summon slumber demons?", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "Ehh, I don't think so? Kind of fuzzy on that one.", DialogSpeaker.RIGHT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "Yeah, everything is kind of fuzzy. I have the big picture but when I try to recall the details...", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "Shit. Oh shit...", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "What's wrong love?", DialogSpeaker.RIGHT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "Do you remember the trip to get to lord what's-his-face's castle?", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "Castle? I remember the portal being in a swamp... or was it a mountain?", DialogSpeaker.RIGHT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "I don't rememebr getting there either. There was a fight but I don't remember more than that.", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "Plus the slumber demons...", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "It's okay love, it will come back to us. We've time, we saved the world remember?", DialogSpeaker.RIGHT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "NO! That is the problem, the details are slipping away not getting clearer.", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "What are you saying?", DialogSpeaker.RIGHT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "We never saved the world!", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "It was a dream, we must have gotten in the way and taken out. Put to sleep.", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "Well shit...", DialogSpeaker.RIGHT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "...", DialogSpeaker.NONE), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "... Hopefully the world saved itself ...", DialogSpeaker.RIGHT), line++);
}

event_inherited();