{
  "spriteId": {
    "name": "MaskTrigger",
    "path": "sprites/MaskTrigger/MaskTrigger.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "DialogTriggerVolume",
    "path": "objects/DialogTriggerVolume/DialogTriggerVolume.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 1,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":3,"collisionObjectId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [
    {"varType":5,"value":"Peaceful_Harp","rangeEnabled":false,"rangeMin":0.0,"rangeMax":10.0,"listItems":[],"multiselect":false,"filters":[
        "GMSound",
      ],"resourceVersion":"1.0","name":"track","tags":[],"resourceType":"GMObjectProperty",},
  ],
  "overriddenProperties": [
    {"propertyId":{"name":"one_time_trigger_key","path":"objects/TriggerVolume/TriggerVolume.yy",},"objectId":{"name":"TriggerVolume","path":"objects/TriggerVolume/TriggerVolume.yy",},"value":"DTAMES4","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
  ],
  "parent": {
    "name": "Dialogs",
    "path": "folders/Objects/World/Triggers/Dialogs.yy",
  },
  "resourceVersion": "1.0",
  "name": "DialogTriggerAwakening4",
  "tags": [],
  "resourceType": "GMObject",
}