/// @description Insert description here
// You can write your code in this editor

if (card != noone){
	image_blend = card.blend;	
} else {
	image_blend = c_white;	
}

var text_color = make_color_value(image_blend, 230);
var shadow_color = make_color_value(image_blend, 45);

draw_self();
var delay_sprite = object_get_sprite(TurnOrderTrackerUI);
draw_sprite_ext(delay_sprite, 0, x+32, y+232, 0.5, 0.5, 0,c_white, 1.0);


if (card != noone) {
	draw_text_outline(string(card.delay), FontCardText, fa_left, fa_middle, x+64, y+250, text_color, shadow_color);
	var target_text = "Top";
	if (card.target == Target.BOTTOM) {
		var target_text = "Bottom";
	} else if (card.target == Target.SELECT) {
		var target_text = "Any";	
	} else if (card.target == Target.GLOBAL) {
		var target_text = "Global";	
	}
	draw_text_outline(target_text, FontCardText, fa_right, fa_middle, x+270, y+250, text_color, shadow_color);
	draw_text_outline(string(card.text), FontCardText, fa_center, fa_top, x+160, y+290, text_color, shadow_color);
	draw_text_outline(string(card.name), FontCardText, fa_center, fa_middle, x+160, y+94, text_color, shadow_color);
	if (card.source != noone) {
		draw_text_outline(string(card.source.name), FontCardText, fa_center, fa_middle, x+160, y+200, text_color, shadow_color);
	}
	
	if (count > 0 )
	{
		draw_text_outline("x"+string(count), FontCardText, fa_right, fa_bottom, x+280, y+430, text_color, shadow_color);	
	}
}