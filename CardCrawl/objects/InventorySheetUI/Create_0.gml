/// @description Insert description here
// You can write your code in this editor
event_inherited();
rows = 3
cols = 9

for(var row = 0; row < rows; ++row) for(var col = 0; col < cols; ++col) {
	var n = instance_create_layer(x + col*64, y+(row+1)*64, "UI", DeckItemUI);
	n.index = row*cols+(col mod cols);
	n.anchor = id;
}

eq_x_offset = 64*10;
equipment_slots = [];

for (var i = 0; i < 4; ++i) {
	var minor = instance_create_layer(x + eq_x_offset, y+i *64, "UI", EquipSlotUI)
	minor.slot = i;
	minor.anchor = id;
	array_push(equipment_slots, minor);
}