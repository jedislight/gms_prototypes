/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

draw_text_outline("Equipment", FontItemCounter, fa_left, fa_top, x, y, c_white, c_black);

character = global.game.get_last_selected_ui_character();

for(var i = 0; i < array_length(equipment_slots); ++i) {
	equipment_slots[i].character = character;	
}

if (character != noone) {
	draw_sprite(character.icon, 0, x + eq_x_offset+64*-1, y);	
}