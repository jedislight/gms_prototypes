/// @description Insert description here
// You can write your code in this editor
if(entering){
	var dark_color = make_color_value(make_color_rgb(200,200,255), 128);
	global.fog_target_color = dark_color;
	global.fog_start = 0;
	global.fog_end = 192;
	global.ambient_light_target = dark_color;
	global.directional_light_target = make_color_value(c_white, 0);
}