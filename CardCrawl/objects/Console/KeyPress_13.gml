/// @description Insert description here
// You can write your code in this editor
if(!visible) exit;

var parts = string_split(keyboard_string);
if (array_length(parts) >= 1) {
	var command = parts[0];
	var action = commands[? command];
	if (is_undefined(action)) {
		console_write("No such command: " + command);
	} else {
		var args = [];
		array_copy(args, 0, parts, 1, array_length(parts)-1);
		var raw = keyboard_string;
		raw = string_replace(raw, command, "");
		raw = string_replace(raw, " ", "");
		action(args, raw);
	}
}

array_push(history, keyboard_string);
keyboard_string = "";
