/// @description Insert description here
// You can write your code in this editor
draw_set_color(c_black);
draw_set_alpha(0.75);
draw_rectangle(x,y, x+width, y+height, false);
draw_set_alpha(1.0);

draw_set_color(c_white);
draw_set_font(font);
draw_set_valign(fa_bottom)
var line_y = y + height;
var max_lines = height / font_get_size(font);
for(var l = 0; l < min(max_lines, array_length(lines)); ++l) {
	line_y -= font_get_size(font);
	draw_text(x, line_y, lines[l]);
}

draw_text(x, height, "> "+keyboard_string);

draw_set_valign(fa_top);