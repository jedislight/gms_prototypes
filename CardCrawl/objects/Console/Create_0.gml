/// @description Insert description here
// You can write your code in this editor
event_inherited();
if (instance_number(Console) > 1) {
	instance_destroy(id);
	exit;
}

lines = ["Console Log Start - " + date_date_string(date_current_datetime())];
commands = ds_map_create();
height = 400;
width = 1000;
font = FontItemCounter;
history = []
history_index = 0;


commands[? "unsolid"] = function() {
	with(all) solid = false;
	console_write("All "+string(instance_count)+" objects made non-solid");
}

commands[? "vision"] = function() {
	global.ambient_light_target = c_white;	
}

commands[? "give"] = function(args, raw) {
	if (raw == "") {
		console_write("USAGE: give <item_name>");	
		return;
	}
	var item = item_factory(raw);
	if (item == noone) {
		console_write("No item found: \"" + raw+"\"");
	} else {
		array_push(global.game.items, item);
	}
}