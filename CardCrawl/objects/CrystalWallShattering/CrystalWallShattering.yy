{
  "spriteId": {
    "name": "Sprite_ice_3_old",
    "path": "sprites/Sprite_ice_3_old/Sprite_ice_3_old.yy",
  },
  "solid": true,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "CrystalWall",
    "path": "objects/CrystalWall/CrystalWall.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 1,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":4,"collisionObjectId":{"name":"InteractObject","path":"objects/InteractObject/InteractObject.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Walling",
    "path": "folders/Objects/World/Walling.yy",
  },
  "resourceVersion": "1.0",
  "name": "CrystalWallShattering",
  "tags": [],
  "resourceType": "GMObject",
}