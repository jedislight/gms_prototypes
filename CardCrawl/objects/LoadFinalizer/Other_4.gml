/// @description Insert description here
// You can write your code in this editor
var file = file_text_open_read("save.json");
if (file != -1) {
	var load_map = json_decode(file_text_readln(file));
	global.fog_color = load_map[? "fog_color"];
	global.fog_target_color = global.fog_color;
	global.fog_start = load_map[? "fog_start"];
	global.fog_end = load_map[? "fog_end"];
	global.ambient_light_target = load_map[? "ambient_light_target"]
	global.ambient_light = global.ambient_light_target;
	global.directional_light_target = load_map[? "directional_light_target"]
	global.directional_light = global.directional_light_target;
	
	MapPawn.x = load_map[? "x"];
	MapPawn.y = load_map[? "y"];
	MapPawn.direction = load_map[? "direction"];
	ds_map_destroy(load_map);
	file_text_close(file);
	audio_stop_all();
}

instance_destroy(id);