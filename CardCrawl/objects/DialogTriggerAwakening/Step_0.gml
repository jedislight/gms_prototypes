/// @description Insert description here
// You can write your code in this editor
if(entering){
	var line = 1;
	event_add(new EventDialog(global.game.alice, noone, "*Yawn*", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.alice, noone, "Where am I? This looks like some sort of crypt.", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.alice, noone, "Wake up love, something is wrong.", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "ZzZzZzZzZ...", DialogSpeaker.RIGHT), line++);
	event_add(new EventPlaySound(Sound_Critical_Hit_II, false), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, global.game.bob.name + "!! *smack*", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "Huh? Just a few more minutes...", DialogSpeaker.RIGHT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "No love, we're in some sort of crypt. Get up. Now please.", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "Okay okay... it's not the end of the world. Its not even the weirdest place we've woken up.", DialogSpeaker.RIGHT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "Haha, true. Off on another daring rescue I guess.", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "Of ... ourselves?", DialogSpeaker.RIGHT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "Details, details...", DialogSpeaker.LEFT), line++);
}

event_inherited();