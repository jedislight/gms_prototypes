/// @description Insert description here
// You can write your code in this editor
if (not global.game.has_key_item(locked)) {
	if (global.game.has_key_item("Key")) {
		global.game.remove_key_item("Key", 1);
		global.game.add_key_item(locked, 1);
	} else {
		interact_text("It is locked");	
	}
}

if (global.game.has_key_item(locked)) {
	event_inherited();
}