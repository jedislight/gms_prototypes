/// @description Insert description here
// You can write your code in this editor
global.game.save();
interact_text("Game Saved");
for (var i = 0; i < array_length(global.game.player_party.members); ++i) {
	var c = global.game.player_party.members[i];
	c.reset_cards();
	c.hp = c.hp_max;
}