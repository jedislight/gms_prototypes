/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if (!mesh_exists(object_index)) {
	var texture = sprite_get_texture(sprite_index, 0);
	mesh_add(object_index, mesh_vbo_build_cube(texture), texture, pr_trianglelist);	
}