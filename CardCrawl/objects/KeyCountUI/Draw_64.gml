/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
var keys = global.game.get_key_item_count("Key");
if (keys == 0) {
	image_blend = c_dkgray;	
	event_inherited();
} else {
	image_blend = c_white;	
	event_inherited();
	draw_text_outline( "x"+string(keys), FontItemCounter, fa_right, fa_bottom, x+sprite_width, y+sprite_height, c_white, c_black);
}

