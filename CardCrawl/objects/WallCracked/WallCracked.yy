{
  "spriteId": {
    "name": "Sprite_stone_brick_12",
    "path": "sprites/Sprite_stone_brick_12/Sprite_stone_brick_12.yy",
  },
  "solid": true,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "Wall",
    "path": "objects/Wall/Wall.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 1,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "World",
    "path": "folders/Objects/World.yy",
  },
  "resourceVersion": "1.0",
  "name": "WallCracked",
  "tags": [],
  "resourceType": "GMObject",
}