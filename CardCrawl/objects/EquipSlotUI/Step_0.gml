/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event

item = noone;
if (character != noone) {
	if (slot < array_length(character.equipment.minors)) {
		item = character.equipment.minors[slot];	
	}
}
item_override = true;

event_inherited();

if (mouse_inside and mouse_check_button_pressed(mb_right)) {
	if (slot >= 0 and item != noone) {
		array_push(global.game.items, item);
		character.equipment.minors[slot] = noone;
		character.reset_cards();
	}
}

if (item != noone) {
	sprite_index = item.icon;	
} else {
	sprite_index = object_get_sprite(object_index);	
}

visible = is_struct(character);