/// @description Insert description here
// You can write your code in this editor
event_inherited();
if (is_string(anchor)){
	with(ObjectUI) {
		if (name == other.anchor) {
			other.anchor = id;
			break;
		}
	}	
}
if (is_string(anchor)){
	exit;	
}
if (instance_exists(anchor)) {
	if (x_anchor == 0 and y_anchor == 0) {
		x_anchor = x - anchor.x;
		y_anchor = y - anchor.y;
	}
	
	x = anchor.x + x_anchor;
	y = anchor.y + y_anchor;
}