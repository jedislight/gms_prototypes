/// @description Insert description here
// You can write your code in this editor
image_angle = direction

var mx = window_mouse_get_x();
var my = window_mouse_get_y();
	
var mdx = mx - mouse_xp;
var mdy = my - mouse_yp;

var alt = keyboard_check_direct(vk_alt);
if (!alt) {
	freelook_vertical = 0;
	freelook_turn = 0;
} else {	
	freelook_vertical += mdy*0.25;
	freelook_vertical = clamp(freelook_vertical, -45, 45);
	freelook_turn = clamp(freelook_turn-mdx*0.25, -45, 45);
}

mouse_xp = mx
mouse_yp = my;

var interact_x = 910 / 4;
var interact_width = 910 / 2;
var interact_y = 512 / 4;
var interact_height = 512 / 2;
if (ds_priority_empty(global.event_queue) and point_in_rectangle(mx, my, interact_x, interact_y, interact_x + interact_width, interact_y+interact_height)) {
	window_set_cursor(cr_drag);	
	if (mouse_check_button_pressed(mb_left)) {
		instance_create_layer(x + lengthdir_x(64, direction), y + lengthdir_y(64, direction), "Instances", InteractObject);
	}
}else {
	window_set_cursor(cr_default);	
}
