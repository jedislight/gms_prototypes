/// @description Insert description here
// You can write your code in this editor

function enable_game_lights(enabled) {
	if (enabled) {
		draw_set_lighting(true);
		draw_light_define_ambient(global.ambient_light);
	
		// pawn light
		draw_light_define_point(0, x, y, 32, 256, c_black)
		draw_light_enable(0, true);
		has_lantern = false;
		for(var c = 0; c < array_length(global.game.player_party.members); ++c) {
			var character = global.game.player_party.members[c];
			for(var e = 0; e < array_length(character.equipment.minors); ++e) {
				var item = character.equipment.minors[e];
				if (is_struct(item) and string_count("Lantern", item.name) > 0 ) {
					has_lantern = true;
					break;
				}
			}
		}
		if (has_lantern) {
			lantern_color = make_color_value(c_orange, 200+abs(sin(global.frame)/4)*55+abs(sin(global.frame)/8)*55);
			draw_light_define_point(0, x, y, 32, 256, lantern_color);	
		}
	
		// directional light
		draw_light_define_direction(1, 1,1,1, global.directional_light);
	
		// environmental lights 2-7
		// TODO
		var light_index = 2;
		while(light_index <= 7) {
			var l = instance_nearest(x,y, LightDecor);
			if (instance_exists(l)) {
				draw_light_define_point(light_index, l.x+32, l.y+32, l.z+32, l.light_radius, l.light_color);	
				draw_light_enable(light_index, true);
				instance_deactivate_object(l);
			} else {
				draw_light_enable(light_index, false);
			}
		
			++light_index;
		}
		instance_activate_all();
	} else {
		draw_set_lighting(false);
	}
}

if (view_current == global.view_3d) {
	xlook = x + lengthdir_x(32, direction+freelook_turn);
	ylook = y + lengthdir_y(32, direction+freelook_turn);
	zlook = z + freelook_vertical;

	var projmat = matrix_build_projection_perspective(16, 16, 16, 32000);
	var viewmat = matrix_build_lookat(x, y, z, xlook, ylook, zlook, 0, 0, 1);

	camera_set_view_mat(view_camera[global.view_3d], viewmat);
	camera_set_proj_mat(view_camera[global.view_3d], projmat);

	surface_depth_disable(false);
	gpu_set_zwriteenable(true)
	gpu_set_ztestenable(true)
	gpu_set_cullmode(cull_clockwise)

	gpu_set_fog(true, global.fog_color, global.fog_start,  global.fog_end)

	layer_background_blend(layer_background_get_id("Background"), global.fog_color);
	
	enable_game_lights(true)
}
else {
	enable_game_lights(false);
	gpu_set_fog(false, global.fog_color, global.fog_start,  global.fog_end)	
	gpu_set_cullmode(cull_noculling)

}