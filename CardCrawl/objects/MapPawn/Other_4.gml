/// @description Insert description here
// You can write your code in this editor
if (global.game.transition_anchor != "") {
	var n = noone;
	with(NamedLocationObject) {
		if (location_name == global.game.transition_anchor) n = id;	
	}
	if (instance_exists(n)) {
		x = n.x+32;
		y=n.y+32;
		direction = n.image_angle;
	}
	global.game.transition_anchor = "";	
}