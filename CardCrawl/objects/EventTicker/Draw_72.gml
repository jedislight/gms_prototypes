/// @description Insert description here
// You can write your code in this editor
layer_set_visible("Ceiling", true)
layer_set_visible("Floor", true)
layer_set_visible("Instances", true)
layer_set_visible("UI", true)
layer_set_visible("UIBackground", true)

var light_lerp_speed = 0.05;
global.ambient_light = make_color_rgb(
	lerp(color_get_red(global.ambient_light), color_get_red(global.ambient_light_target), light_lerp_speed),
	lerp(color_get_green(global.ambient_light), color_get_green(global.ambient_light_target), light_lerp_speed),
	lerp(color_get_blue(global.ambient_light), color_get_blue(global.ambient_light_target), light_lerp_speed)
);

var fog_lerp_speed = 0.05;
global.fog_color = make_color_rgb(
	lerp(color_get_red(global.fog_color), color_get_red(global.fog_target_color), fog_lerp_speed),
	lerp(color_get_green(global.fog_color), color_get_green(global.fog_target_color), fog_lerp_speed),
	lerp(color_get_blue(global.fog_color), color_get_blue(global.fog_target_color), fog_lerp_speed)
);

global.directional_light = make_color_rgb(
	lerp(color_get_red(global.directional_light), color_get_red(global.directional_light_target), light_lerp_speed),
	lerp(color_get_green(global.directional_light), color_get_green(global.directional_light_target), light_lerp_speed),
	lerp(color_get_blue(global.directional_light), color_get_blue(global.directional_light_target), light_lerp_speed)
);

global.frame++;
