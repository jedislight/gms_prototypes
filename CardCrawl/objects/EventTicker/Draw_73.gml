/// @description MAP LIGHTING
// You can write your code in this editor
with(MapPawn) {
var xo = -64 *3;
var yo = -64 *3;

var x_snap = x - x mod 64;
var y_snap = y - y mod 64;
	
if (view_current == global.view_map) {
	var map_light_grid = ds_grid_create(7,7);
	
	ds_grid_clear(map_light_grid, c_black);
	
	var light_color = global.ambient_light;
	var grid = map_light_grid;
	for(var xx = 0; xx < ds_grid_width(map_light_grid); ++xx) for(var yy = 0; yy < ds_grid_height(map_light_grid); ++yy){
		var light_final = light_color;
		var tile_color = grid[# xx, yy];
		var final_color = make_color_rgb(
			clamp(color_get_red(light_final) + color_get_red(tile_color), 0, 255),
			clamp(color_get_green(light_final) + color_get_green(tile_color), 0, 255),
			clamp(color_get_blue(light_final) + color_get_blue(tile_color), 0, 255),
		);
		grid[# xx, yy] = final_color;
	}
	
	// pawn light
	
	if (has_lantern) {
		var lr = 256;
		var lx = x;
		var ly = y;
		var light_color = lantern_color;
		var grid = map_light_grid;
		for(var xx = 0; xx < ds_grid_width(map_light_grid); ++xx) for(var yy = 0; yy < ds_grid_height(map_light_grid); ++yy){
			var tx = xx * 64 + xo + x_snap + 32;			
			var ty = yy * 64 + yo + y_snap + 32;
			var td = point_distance(tx, ty, lx, ly)
			if ( td > lr ) continue;
			var li = sqrt((lr-td) / lr);
			var light_final = make_color_value(light_color, 255*li);
			var tile_color = grid[# xx, yy];
			var final_color = make_color_rgb(
				clamp(color_get_red(light_final) + color_get_red(tile_color), 0, 255),
				clamp(color_get_green(light_final) + color_get_green(tile_color), 0, 255),
				clamp(color_get_blue(light_final) + color_get_blue(tile_color), 0, 255),
			);
			grid[# xx, yy] = final_color;
		}
	}
	
	// directional light
	var light_color = global.directional_light;
	var grid = map_light_grid;
	for(var xx = 0; xx < ds_grid_width(map_light_grid); ++xx) for(var yy = 0; yy < ds_grid_height(map_light_grid); ++yy){
		var light_final = light_color;
		var tile_color = grid[# xx, yy];
		var final_color = make_color_rgb(
			clamp(color_get_red(light_final) + color_get_red(tile_color), 0, 255),
			clamp(color_get_green(light_final) + color_get_green(tile_color), 0, 255),
			clamp(color_get_blue(light_final) + color_get_blue(tile_color), 0, 255),
		);
		grid[# xx, yy] = final_color;
	}
	
	// environmental lights 2-7
	// TODO
	var light_index = 2;
	while(light_index <= 7) {
		var l = instance_nearest(x,y, LightDecor);
		if (instance_exists(l)) {		
			var lr = l.light_radius;
			lr = max(lr, 1);
			var lx = l.x+32;
			var ly = l.y+32;
			var light_color = l.light_color;
			var grid = map_light_grid;
			for(var xx = 0; xx < ds_grid_width(map_light_grid); ++xx) for(var yy = 0; yy < ds_grid_height(map_light_grid); ++yy){
				var tx = xx * 64 + xo + x_snap + 32;			
				var ty = yy * 64 + yo + y_snap + 32;
				var td = point_distance(tx, ty, lx, ly)
				if ( td > lr ) continue;
				var li = sqrt((lr-td) / lr);
				var light_final = make_color_value(light_color, 255*li);
				var tile_color = grid[# xx, yy];
				var final_color = make_color_rgb(
					clamp(color_get_red(light_final) + color_get_red(tile_color), 0, 255),
					clamp(color_get_green(light_final) + color_get_green(tile_color), 0, 255),
					clamp(color_get_blue(light_final) + color_get_blue(tile_color), 0, 255),
				);
				grid[# xx, yy] = final_color;
			}
			
			instance_deactivate_object(l);
		}
		
		++light_index;
	}
	instance_activate_all();
	
	gpu_set_blendmode_ext(bm_dest_colour, bm_zero);
	for(var xx = 0; xx < ds_grid_width(map_light_grid); ++xx) for(var yy = 0; yy < ds_grid_height(map_light_grid); ++yy){		
		var left = xo + x_snap + xx * 64
		var top = yo + y_snap + yy * 64;
		var right = left + 63;
		var bottom = top + 63;
		var light_color = map_light_grid[# xx, yy];
		draw_rectangle_color(left, top, right, bottom, light_color, light_color, light_color, light_color, false);
	}
	gpu_set_blendmode(bm_normal);
	
	ds_grid_destroy(map_light_grid);
}

}

with(MapPawn) {
	event_perform(ev_draw, 0);	
}