{
  "spriteId": {
    "name": "Sprite_stone_stairs_up",
    "path": "sprites/Sprite_stone_stairs_up/Sprite_stone_stairs_up.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "LightDecor",
    "path": "objects/LightDecor/LightDecor.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 1,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":4,"collisionObjectId":{"name":"MapPawn","path":"objects/MapPawn/MapPawn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [
    {"varType":5,"value":"noone","rangeEnabled":false,"rangeMin":0.0,"rangeMax":10.0,"listItems":[],"multiselect":false,"filters":[
        "GMRoom",
      ],"resourceVersion":"1.0","name":"target_room","tags":[],"resourceType":"GMObjectProperty",},
    {"varType":2,"value":"","rangeEnabled":false,"rangeMin":0.0,"rangeMax":10.0,"listItems":[],"multiselect":false,"filters":[],"resourceVersion":"1.0","name":"target_anchor","tags":[],"resourceType":"GMObjectProperty",},
  ],
  "overriddenProperties": [
    {"propertyId":{"name":"light_radius","path":"objects/LightDecor/LightDecor.yy",},"objectId":{"name":"LightDecor","path":"objects/LightDecor/LightDecor.yy",},"value":"80","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
    {"propertyId":{"name":"light_color","path":"objects/LightDecor/LightDecor.yy",},"objectId":{"name":"LightDecor","path":"objects/LightDecor/LightDecor.yy",},"value":"$FF727F7E","resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMOverriddenProperty",},
  ],
  "parent": {
    "name": "Decor",
    "path": "folders/Objects/World/Decor.yy",
  },
  "resourceVersion": "1.0",
  "name": "StairsUp",
  "tags": [],
  "resourceType": "GMObject",
}