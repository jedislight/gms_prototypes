/// @description Insert description here
// You can write your code in this editor
if (array_length(children) == 0 and is_struct(item)){
	var cards_by_name = ds_map_create();
	for(var i = 0; i < array_length(item.cards); ++i){
		var card = item.cards[i];
		cards_by_name[? card.name] = card;
	}
	var unique_cards = [];
	ds_map_values_to_array(cards_by_name, unique_cards);
	var card_splash_sprite = object_get_sprite(CardSplashUI);
	for(var uc = 0; uc < array_length(unique_cards); ++uc){
		var card = unique_cards[uc];
		var csu = instance_create_layer(uc*320, (uc mod 3)*0, "UI", CardSplashUI);
		children[uc] = csu;
		csu.card = card;
	}
	
	for(var c = 0 ; c < array_length(children); ++c) {
		var child = children[c];
		var card = child.card;
		card_name = card.name;
		var count = array_length(array_filter(item.cards, function(c){return c.name == card_name;}));
		child.count = count;
		card.source = item;
	}
}

for (var c = 0; c < array_length(children); ++c) {
	var child = children[c];
	child.depth = depth + abs((child.x+128) - window_mouse_get_x())/100;
}