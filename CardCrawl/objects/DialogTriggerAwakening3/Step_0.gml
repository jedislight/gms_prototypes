/// @description Insert description here
// You can write your code in this editor
if(entering){
	var line = 1;
	event_add(new EventDialog(global.game.bob, noone, "Slumber demons? That could explain waking up in a strange place.", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.bob, noone, "We just defated lord what's-his-face, maybe we were ambushed by something closing the portal?", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.bob, global.game.alice, "Lord ... huh I don't remember their name either. Damn slumber demons. It will all come back soon.", DialogSpeaker.RIGHT), line++);
}

event_inherited();