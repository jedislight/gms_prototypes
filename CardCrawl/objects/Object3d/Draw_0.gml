/// @description Insert description here
// You can write your code in this editor
if (view_current == global.view_3d and draw_3d_enabled) {
	var ad = angle_difference(point_direction(MapPawn.x, MapPawn.y, x, y), MapPawn.direction);
	
	if (keyboard_check(vk_alt) or abs(ad) < 115 and distance_to_object(MapPawn) < global.fog_end+64)
	{
		var zprevious = z;
		repeat(z_stacking){
			mesh_draw(mesh_index);
			z-=sprite_height;
		}
		z = zprevious;
	}
} else if (view_current == global.view_map and draw_2d_enabled and !map_hide) {
	var md = abs(x - MapPawn.x) + abs(y - MapPawn.y);
	if (md < 374){
		draw_self();
	}
}