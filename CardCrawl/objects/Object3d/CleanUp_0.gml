/// @description Insert description here
// You can write your code in this editor

var last_mesh_reference = true;
 with(Object3d) {
	if (mesh_index == other.mesh_index and id != other.id) {
		last_mesh_reference = false;
		break;
	}
 }
 
 if (last_mesh_reference) {
	mesh_delete(mesh_index);	 
 }