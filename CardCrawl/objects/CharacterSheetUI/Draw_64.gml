/// @description Insert description here
// You can write your code in this editor

with(CharacterPortraitUI) {
	if (selected) {
		other.character = character;	
	}
}

if (!is_struct(character)) {
	if (array_length(global.game.player_party.members)>0) {
		character  = global.game.player_party.members[0];	
	}
}

if (!is_struct(character)) exit;

var lw = 32
var line = 2;
draw_sprite(character.icon, 0, x, y);
draw_text_outline(character.name, FontItemCounter, fa_left, fa_top, x+64, y, c_white, c_black);

draw_text_outline("Initiative: " + string(character.initiative), FontItemCounter, fa_left, fa_top, x, y+line++*lw, c_white, c_black);
draw_text_outline("Hand Size: " + string(character.hand_size), FontItemCounter, fa_left, fa_top, x, y+line++*lw, c_white, c_black);