/// @description Insert description here
// You can write your code in this editor
closed = !closed

if (!closed) {
	with (SlideAnchorUI) {
		if (id != other.id and closed_x == other.closed_x) {
			closed = true;
		}
	}
}