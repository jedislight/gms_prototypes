/// @description Insert description here
// You can write your code in this editor
if(entering){
	global.fog_target_color = global.exterior_fog_color;
	global.ambient_light_target = make_color_value(c_white, 200);
	global.directional_light_target = make_color_value(c_white, 200);
	global.fog_start = 256;
	global.fog_end = 512;
}