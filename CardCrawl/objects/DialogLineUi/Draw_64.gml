/// @description Insert description here
// You can write your code in this editor
draw_self();
draw_set_font(font);
var speaker_color = c_white;
if (left_speaker and not right_speaker and is_struct(character)) {
	speaker_color = make_color_hsv(color_get_hue(character.color), color_get_saturation(character.color), (color_get_value(character.color)+255) /2);
}
if (right_speaker and not left_speaker and is_struct(character2)) {
	speaker_color = make_color_hsv(color_get_hue(character2.color), color_get_saturation(character2.color), (color_get_value(character2.color)+255) /2);
}
draw_text_ext_color(x+32, y+32, text, font_get_size(font)+5, sprite_width-64, speaker_color, speaker_color, speaker_color, speaker_color, 1.0);
if (is_struct(character)){
	var c = c_white;
	if (!left_speaker) {
		c = c_gray;	
	}
	draw_sprite_ext(character.icon,0, x+128, y-128, -2.0, 2.0, 0, c, 1.0);
}
if (is_struct(character2)) {
	var c = c_white;
	if (!right_speaker) {
		c = c_gray;	
	}
	draw_sprite_ext(character2.icon,0, x+sprite_width-128, y-128, 2.0, 2.0, 0, c, 1.0);
}