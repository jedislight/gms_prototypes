/// @description Insert description here
// You can write your code in this editor
if(entering){
	var line = 1;
	event_add(new EventDialog(global.game.alice, noone, "Ahh, that brings back memories. Do you remember the last time we fought bats together love?", DialogSpeaker.LEFT), line++);
	event_add(new EventDialog(global.game.alice, global.game.bob, "Barely. Saving the world really makes all the little fights blend together after a while.", DialogSpeaker.RIGHT), line++);
}

event_inherited();