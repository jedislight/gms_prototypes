/// @description Insert description here
// You can write your code in this editor
event_inherited();

if (not item_override) {
	item = noone;
	if (array_length(global.game.items) > index) {
		item = global.game.items[index];	
	}
}
if (item != noone) {
	sprite_index = item.icon;
} else {
	sprite_index= object_get_sprite(object_index);
}

var none_selected = true;
with(object_index) if (selected) none_selected = false;

if (mouse_inside and not mouse_previously_inside) {
	// enter
	if (item != noone and none_selected) {
		child = instance_create_layer(0,0,"UI", DeckItemDetailsUI);
		child.item = item;
		child.auto_destroy = false;
	}
} 
else if (not mouse_inside and mouse_previously_inside) {
	//exit	
	if (not selected) {
		if (instance_exists(child)) {
			instance_destroy(child);
			child = noone;
		}
	}
}
if (mouse_inside and mouse_check_button_pressed(mb_left)) {
	if (object_index == DeckItemUI){
		var selected_equip_slot_ui = noone;
		with(EquipSlotUI) {
			if(selected) selected_equip_slot_ui = id;	
		}
	
		var equip_type = DeckItemType.MAJOR;
		if (selected_equip_slot_ui != noone and selected_equip_slot_ui.slot >= 0) {
			equip_type = DeckItemType.MINOR;
		}
	
		if ( item != noone and selected_equip_slot_ui != noone and item.type == equip_type) {
			array_delete(global.game.items, index, 1);
			if (selected_equip_slot_ui.item != noone) {
				array_push(global.game.items, item);	
			}
			if (selected_equip_slot_ui.slot == -1) {
				selected_equip_slot_ui.character.equipment.major = item;
			} else {
				selected_equip_slot_ui.character.equipment.minors[selected_equip_slot_ui.slot] = item;
			}
			
			selected_equip_slot_ui.character.reset_cards();
		}
	}
	if(item != noone or object_index == EquipSlotUI) {
		selected = true;
	}
	if (item != noone and child == noone) {
		child = instance_create_layer(0,0,"UI", DeckItemDetailsUI);
		child.item = item;
		child.auto_destroy = false;
	}
}

if (mouse_check_button_pressed(mb_left) and not mouse_inside) {
	selected = false;	
	if (instance_exists(child)) {
		instance_destroy(child);
		child = noone;
	}
}

image_blend = c_white;
if (selected or (mouse_inside and none_selected)) {
	image_blend = c_aqua;	
}