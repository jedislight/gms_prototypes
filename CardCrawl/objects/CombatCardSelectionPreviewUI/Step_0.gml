/// @description Insert description here
// You can write your code in this editor
event_inherited();
var current_event = ds_priority_find_min(global.event_queue);
card = noone;
visible = false;
if (is_struct(current_event) and current_event.name == "CombatTurn"){
	var character = current_event.character;
	if (global.game.is_character_in_player_party(character)){
		if(array_length(character.hand) > hand_index) {
			card = character.hand[hand_index];
			visible = true;
		}
	}
}