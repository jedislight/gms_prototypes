/// @description Insert description here
// You can write your code in this editor
event_inherited()
if (global.current_enemy_stack_pawn_combatant == noone) {
	instance_destroy(id);
	exit;
}

portraits = []
with(CharacterPortraitUI) {
	if (other.y == y and is_struct(id.character) and id.character.hp > 0) {
		array_push(other.portraits, id);	
	}
}
all_events = event_array();
combat_turn_events = [];
times = [];
for (var i = 0; i < array_length(portraits); ++i) {
	var character = portraits[i].character;
	var ce = noone;
	var time = 0;
	for(var j = 0; j < array_length(all_events); ++j) {
		var event = all_events[j];
		if (event.name == "CombatTurn" and event.character == character) {
			ce = event;
			time = event_find_time(ce);
			portraits[i].display_text_override = string(event_time_until(event));
			break;	
		}
	}
	array_push(combat_turn_events, ce);
	array_push(times, time);
}
ranks = []
for (var i = 0; i < array_length(portraits); ++i) {
	var rank = 0;
	for(var j = 0; j < array_length(times); ++j) {
		if (times[i] < times[j]) {
			++rank;	
		}
		if (times[i] == times[j] and i < j) {
			++rank;
		}
	}
	array_push(ranks, rank);
}

var moving = false;
for (var j = 0; j < array_length(all_events); ++j){
	var event = all_events[j];
	if(event.name == "TurnOrderTrackerMultiMove"){
		moving = true;
		break;
	}
}

if (not moving) {
	var move_events = [];
	for (var i = 0; i < array_length(portraits); ++i) {
		var tx = (array_length(ranks)-ranks[i]) * 64 + x;
		var portrait = portraits[i];
		if (tx != portrait.x) {
			var e = new EventMove(portrait, tx, portrait.y)
			e.lerp_speed = .5;
			array_push(move_events, e);
		}
	}
	
	if (array_length(move_events) > 0){
		event_add(new EventMultiEvent("TurnOrderTrackerMultiMove", move_events), -1);	
	}
}