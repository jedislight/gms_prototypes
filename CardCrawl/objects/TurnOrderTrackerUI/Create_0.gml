/// @description Insert description here
// You can write your code in this editor
event_inherited();


for(var i = 0; i < array_length(global.current_enemy_stack_pawn_combatant.enemy_stack.members); ++i){
	var n = instance_create_layer(x,y, "UI", CharacterPortraitUI);
	n.character = global.current_enemy_stack_pawn_combatant.enemy_stack.members[i];
}

for(var i = 0; i < array_length(global.game.player_party.members); ++i){
	var character = global.game.player_party.members[i]	;
	var n = instance_create_layer(x,y, "UI", CharacterPortraitUI);
	n.character = character;
}