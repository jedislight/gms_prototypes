{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "Misc.wav",
  "duration": 4.0,
  "parent": {
    "name": "FX",
    "path": "folders/Sounds/FX.yy",
  },
  "resourceVersion": "1.0",
  "name": "Misc",
  "tags": [],
  "resourceType": "GMSound",
}