{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "Music_Decrepit_Temple_Tonal.mp3",
  "duration": 610.847351,
  "parent": {
    "name": "Music",
    "path": "folders/Sounds/Music.yy",
  },
  "resourceVersion": "1.0",
  "name": "Music_Decrepit_Temple_Tonal",
  "tags": [],
  "resourceType": "GMSound",
}