{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "Enemy_on_Creaky_Floorboards.wav",
  "duration": 11.1830158,
  "parent": {
    "name": "FX",
    "path": "folders/Sounds/FX.yy",
  },
  "resourceVersion": "1.0",
  "name": "Enemy_on_Creaky_Floorboards",
  "tags": [],
  "resourceType": "GMSound",
}