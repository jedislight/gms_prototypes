{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "Sound_Critical_Hit_II.wav",
  "duration": 2.652653,
  "parent": {
    "name": "FX",
    "path": "folders/Sounds/FX.yy",
  },
  "resourceVersion": "1.0",
  "name": "Sound_Critical_Hit_II",
  "tags": [],
  "resourceType": "GMSound",
}