{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "Stone_Door_Open.wav",
  "duration": 6.0,
  "parent": {
    "name": "FX",
    "path": "folders/Sounds/FX.yy",
  },
  "resourceVersion": "1.0",
  "name": "Stone_Door_Open",
  "tags": [],
  "resourceType": "GMSound",
}