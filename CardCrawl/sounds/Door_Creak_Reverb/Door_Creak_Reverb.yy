{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "Door_Creak_Reverb.wav",
  "duration": 4.296236,
  "parent": {
    "name": "FX",
    "path": "folders/Sounds/FX.yy",
  },
  "resourceVersion": "1.0",
  "name": "Door_Creak_Reverb",
  "tags": [],
  "resourceType": "GMSound",
}