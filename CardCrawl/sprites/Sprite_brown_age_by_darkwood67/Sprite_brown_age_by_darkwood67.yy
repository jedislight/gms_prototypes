{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 1365,
  "bbox_top": 0,
  "bbox_bottom": 1024,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 1366,
  "height": 1025,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"199d7a04-5025-40ba-a0d1-0715873dc1a1","path":"sprites/Sprite_brown_age_by_darkwood67/Sprite_brown_age_by_darkwood67.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"199d7a04-5025-40ba-a0d1-0715873dc1a1","path":"sprites/Sprite_brown_age_by_darkwood67/Sprite_brown_age_by_darkwood67.yy",},"LayerId":{"name":"1051ad68-dbe7-4744-b9a7-49aa9fe64ea1","path":"sprites/Sprite_brown_age_by_darkwood67/Sprite_brown_age_by_darkwood67.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite_brown_age_by_darkwood67","path":"sprites/Sprite_brown_age_by_darkwood67/Sprite_brown_age_by_darkwood67.yy",},"resourceVersion":"1.0","name":"199d7a04-5025-40ba-a0d1-0715873dc1a1","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"Sprite_brown_age_by_darkwood67","path":"sprites/Sprite_brown_age_by_darkwood67/Sprite_brown_age_by_darkwood67.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"88199b22-7390-45f6-a57f-7e3b877fd501","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"199d7a04-5025-40ba-a0d1-0715873dc1a1","path":"sprites/Sprite_brown_age_by_darkwood67/Sprite_brown_age_by_darkwood67.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"Sprite_brown_age_by_darkwood67","path":"sprites/Sprite_brown_age_by_darkwood67/Sprite_brown_age_by_darkwood67.yy",},
    "resourceVersion": "1.4",
    "name": "Sprite_brown_age_by_darkwood67",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"1051ad68-dbe7-4744-b9a7-49aa9fe64ea1","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "Sprite_brown_age_by_darkwood67",
  "tags": [],
  "resourceType": "GMSprite",
}