{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 5,
  "bbox_right": 59,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"9f212b32-d206-42f2-b168-b974e29e4968","path":"sprites/Sprite_deep_elf_knight_new/Sprite_deep_elf_knight_new.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9f212b32-d206-42f2-b168-b974e29e4968","path":"sprites/Sprite_deep_elf_knight_new/Sprite_deep_elf_knight_new.yy",},"LayerId":{"name":"d60f5dca-7a00-4dfb-8fba-0205203ca69c","path":"sprites/Sprite_deep_elf_knight_new/Sprite_deep_elf_knight_new.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite_deep_elf_knight_new","path":"sprites/Sprite_deep_elf_knight_new/Sprite_deep_elf_knight_new.yy",},"resourceVersion":"1.0","name":"9f212b32-d206-42f2-b168-b974e29e4968","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"Sprite_deep_elf_knight_new","path":"sprites/Sprite_deep_elf_knight_new/Sprite_deep_elf_knight_new.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"9b2d7bd7-f4b3-4074-bb1d-458ebf67c186","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9f212b32-d206-42f2-b168-b974e29e4968","path":"sprites/Sprite_deep_elf_knight_new/Sprite_deep_elf_knight_new.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"Sprite_deep_elf_knight_new","path":"sprites/Sprite_deep_elf_knight_new/Sprite_deep_elf_knight_new.yy",},
    "resourceVersion": "1.4",
    "name": "Sprite_deep_elf_knight_new",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d60f5dca-7a00-4dfb-8fba-0205203ca69c","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "DSS",
    "path": "folders/Sprites/DSS.yy",
  },
  "resourceVersion": "1.0",
  "name": "Sprite_deep_elf_knight_new",
  "tags": [],
  "resourceType": "GMSprite",
}