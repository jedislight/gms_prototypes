{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 63,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"42f67f78-4a9d-447b-b80c-1df906016388","path":"sprites/Sprite_damp_dirt/Sprite_damp_dirt.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"42f67f78-4a9d-447b-b80c-1df906016388","path":"sprites/Sprite_damp_dirt/Sprite_damp_dirt.yy",},"LayerId":{"name":"cb4561dd-db44-4703-b908-4a8880afe77a","path":"sprites/Sprite_damp_dirt/Sprite_damp_dirt.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"Sprite_damp_dirt","path":"sprites/Sprite_damp_dirt/Sprite_damp_dirt.yy",},"resourceVersion":"1.0","name":"42f67f78-4a9d-447b-b80c-1df906016388","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"Sprite_damp_dirt","path":"sprites/Sprite_damp_dirt/Sprite_damp_dirt.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"2cd8bd4d-33ec-49ce-95e3-ec2eb5e8c5ec","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"42f67f78-4a9d-447b-b80c-1df906016388","path":"sprites/Sprite_damp_dirt/Sprite_damp_dirt.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"Sprite_damp_dirt","path":"sprites/Sprite_damp_dirt/Sprite_damp_dirt.yy",},
    "resourceVersion": "1.4",
    "name": "Sprite_damp_dirt",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"cb4561dd-db44-4703-b908-4a8880afe77a","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "DSS",
    "path": "folders/Sprites/DSS.yy",
  },
  "resourceVersion": "1.0",
  "name": "Sprite_damp_dirt",
  "tags": [],
  "resourceType": "GMSprite",
}