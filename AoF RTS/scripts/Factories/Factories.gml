// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function generic_projectile_create(from, to, wounds, sprite, settings) {
	if(is_undefined(settings)) {
		settings = {};	
	}
	var n = instance_create_depth(from.x, from.y, from.depth, Generic_Projectile, settings);
	n.sprite_index = sprite;
	n.wounds = wounds;
	n.target = to;
	
	if(wounds == 0) {
		n.target = noone;
		n.target_x = to.x + random_range(-ONE_INCH_IN_PIXELS, ONE_INCH_IN_PIXELS);
		n.target_y = to.y + random_range(-ONE_INCH_IN_PIXELS, ONE_INCH_IN_PIXELS);
	}
	return n;
}