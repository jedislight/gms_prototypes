/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

points = 23;
quality = 5;
defense = 6;
attacks = 1;
ap = 1;
range = 12 * ONE_INCH_IN_PIXELS;
projectile = tnt;
projectile_settings = {rotation: 3};