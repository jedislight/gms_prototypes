/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

points = 320;
quality = 4;
defense = 3;
attacks = 12;
ap = 1;
toughness = 12
traits = ["Furious", "Stealth", "Poison"]
base_size_pixels = ONE_INCH_IN_PIXELS * 2;
init_sprite_scale();