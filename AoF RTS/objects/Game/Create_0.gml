/// @description Insert description here
// You can write your code in this editor

function init_table(){
	randomize();
	table_width = 12 * 4
	table_height = 12 * 4

	table = ds_grid_create(table_width, table_height);

	ds_grid_clear(table, noone);
}

// -- Configurable Variables (Set these in the Create Event) --
function init_camera() {
	// Pan speed in pixels per frame
	pan_speed = 5;

	// Zoom step (amount of zoom change per input)
	zoom_step = 3;

	// Minimum and maximum zoom factors
	zoom_min = 0.5; 
	zoom_max = 7.25; 

	// Pan extents (boundaries)
	pan_extents_x_min = -128;
	pan_extents_x_max = (1+table_width)*128;
	pan_extents_y_min = -128;
	pan_extents_y_max = (1+table_height)*128;

	// Edge margin for mouse panning (in pixels)
	edge_margin = 20;

	if (!variable_global_exists("default_view_w")) {
	    var cam = view_camera[0];
	    default_view_w = camera_get_view_width(cam);
	    default_view_h = camera_get_view_height(cam);
	}
}

// -- Camera Control Code (Place this in the Step Event) --
function step_camera() {
	// Get the current camera
	var cam = view_camera[0];

	// Get current camera properties
	var cam_x = camera_get_view_x(cam);
	var cam_y = camera_get_view_y(cam);
	var cam_w = camera_get_view_width(cam);
	var cam_h = camera_get_view_height(cam);

	// Initialize movement variables
	var move_x = 0;
	var move_y = 0;

	// Get mouse position relative to the display
	var mouse_screen_x = display_mouse_get_x();
	var mouse_screen_y = display_mouse_get_y();

	// Get display size
	var display_w = display_get_width();
	var display_h = display_get_height();

	// -- Panning Logic --

	// Mouse edge panning
	if (mouse_screen_x <= edge_margin) {
	    move_x -= pan_speed;
	} else if (mouse_screen_x >= display_w - edge_margin) {
	    move_x += pan_speed;
	}

	if (mouse_screen_y <= edge_margin) {
	    move_y -= pan_speed;
	} else if (mouse_screen_y >= display_h - edge_margin) {
	    move_y += pan_speed;
	}

	// Keyboard panning (WASD and Arrow Keys)
	if (keyboard_check(ord("A")) || keyboard_check(vk_left)) {
	    move_x -= pan_speed;
	}
	if (keyboard_check(ord("D")) || keyboard_check(vk_right)) {
	    move_x += pan_speed;
	}
	if (keyboard_check(ord("W")) || keyboard_check(vk_up)) {
	    move_y -= pan_speed;
	}
	if (keyboard_check(ord("S")) || keyboard_check(vk_down)) {
	    move_y += pan_speed;
	}

	// Update camera position
	cam_x += move_x * (cam_w / default_view_w);
	cam_y += move_y * (cam_w / default_view_w);

	// -- Zooming Logic --

	var zoom_change = 0;

	// Zoom via "+" and "-" keys (number row and keypad)
	if (keyboard_check_pressed(vk_add) || keyboard_check_pressed(ord("="))) {
	    zoom_change -= zoom_step;
	}
	if (keyboard_check_pressed(vk_subtract) || keyboard_check_pressed(ord("-"))) {
	    zoom_change += zoom_step;
	}

	// Zoom via mouse wheel
	var mouse_wheel_delta = mouse_wheel_up() - mouse_wheel_down();
	if (mouse_wheel_delta != 0) {
	    zoom_change += -mouse_wheel_delta * zoom_step * 0.1; // Adjust multiplier if needed
	}

	if (zoom_change != 0) {
	    // Current and new zoom factors
	    var current_zoom = cam_w / default_view_w;
	    var new_zoom = current_zoom + zoom_change;

	    // Clamp the zoom to min and max values
	    new_zoom = clamp(new_zoom, zoom_min, zoom_max);

	    // Calculate zoom ratio
	    var zoom_ratio = new_zoom / current_zoom;

	    // Determine the anchor point for zooming
	    if (keyboard_check_pressed(vk_add) || keyboard_check_pressed(ord("=")) ||
	        keyboard_check_pressed(vk_subtract) || keyboard_check_pressed(ord("-"))) {
	        // Anchor at the center of the view
	        var anchor_x = cam_x + cam_w / 2;
	        var anchor_y = cam_y + cam_h / 2;
	    } else if (mouse_wheel_delta != 0) {
	        // Anchor at the mouse cursor position
	        var mouse_room_x = cam_x + (mouse_screen_x / display_w) * cam_w;
	        var mouse_room_y = cam_y + (mouse_screen_y / display_h) * cam_h;
	        var anchor_x = mouse_room_x;
	        var anchor_y = mouse_room_y;
	    }

	    // Adjust camera position to maintain the anchor point
	    cam_x = anchor_x - (anchor_x - cam_x) * zoom_ratio;
	    cam_y = anchor_y - (anchor_y - cam_y) * zoom_ratio;

	    // Update camera view size
	    cam_w = default_view_w * new_zoom;
	    cam_h = default_view_h * new_zoom;
	}

	// -- Clamping and Applying Camera Settings --

	// Clamp camera position to within pan extents
	cam_x = clamp(cam_x, pan_extents_x_min, pan_extents_x_max - cam_w);
	cam_y = clamp(cam_y, pan_extents_y_min, pan_extents_y_max - cam_h);

	// Apply the updated camera settings
	camera_set_view_pos(cam, cam_x, cam_y);
	camera_set_view_size(cam, cam_w, cam_h);
}

function init_factions(factions) {
	player_faction = 0;
	var colors = [c_purple, c_lime];
	faction_map = ds_map_create();
	for (var i = 0; i < array_length(factions); ++i) {
		var f = factions[i];
		player_faction = f;
		faction_map[? f] = {
			mask: f,
			color: colors[i],
			points: 1500
		}
	}
}

function get_faction_color(faction) {
	return faction_map[? faction].color;	
}

function get_faction_points(faction) {
	return faction_map[? faction].points;	
}

function set_faction_points(faction, points) {
	faction_map[? faction].points = points;	
}

init_table();
init_camera();
init_factions([0,1]);