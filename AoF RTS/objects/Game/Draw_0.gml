/// @description Insert description here
// You can write your code in this editor

var cell_size = 128;

for (var tx = 0; tx < table_width; ++tx) for (var ty = 0; ty < table_height; ++ty) {
	var table_element = table[# tx, ty];
	if(is_struct(table_element) and variable_struct_exists(table_element, "sprite")){
		draw_set_alpha(1);
		draw_sprite_stretched(table_element.sprite, 0, tx*cell_size, ty*cell_size, cell_size, cell_size);	
	}
	draw_set_alpha(0.25);
	draw_roundrect_color(tx*cell_size, ty*cell_size, (tx+1)*cell_size, (ty+1)*cell_size, c_white, c_ltgray, true);	
}

draw_set_alpha(1);