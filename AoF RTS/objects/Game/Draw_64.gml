/// @description Insert description here
// You can write your code in this editor

function draw_faction_ui(faction) {
	draw_set_font(UI_Font)
	draw_set_halign(fa_left)
	draw_set_valign(fa_top);
	var faction_color = get_faction_color(faction);
	var faction_points = get_faction_points(faction);
	var text = $"Points: {faction_points}";
	var shadow_width = 3;
	for(var xx = -shadow_width; xx <= shadow_width; xx += shadow_width) for(var yy = -shadow_width; yy <= shadow_width; yy += shadow_width){
		draw_text_color(0+xx,0+yy, text, c_black, c_black, c_black, c_dkgray, 1);	
	}
	draw_text_color(0,0, text, faction_color, faction_color, faction_color, faction_color, 1);
}

draw_faction_ui(player_faction)