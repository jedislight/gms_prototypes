/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
var faction_points = Game.get_faction_points(faction_mask);
if(spawner_cooldown == 0 and faction_points > spawner_cost) {
	instance_create_depth(x+random_range(-ONE_INCH_IN_PIXELS, ONE_INCH_IN_PIXELS),y+random_range(-ONE_INCH_IN_PIXELS,ONE_INCH_IN_PIXELS),depth, spawner_unit, {faction_mask:faction_mask});	
	Game.set_faction_points(faction_mask, faction_points - spawner_cost);
	spawner_cooldown = spawner_cost;
}

if(spawner_cooldown > 0){
	--spawner_cooldown
}