/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

points = 20;
quality = 5;
defense = 5;
attacks = 1;
impact = 1;
move_speed_per_round_inches += 1;
traits = ["Furious", "Devour"]