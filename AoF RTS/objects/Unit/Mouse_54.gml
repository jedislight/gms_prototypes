/// @description Insert description here
// You can write your code in this editor
if(selected) {
	var hit = collision_point(mouse_x, mouse_y, Unit, true, true);

	if(instance_exists(hit)) {
		follow_target = hit;	
	} else {
		move_destination = {x:mouse_x, y:mouse_y};	
		follow_target = noone;
	}
}