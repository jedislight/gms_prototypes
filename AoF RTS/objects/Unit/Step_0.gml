/// @description Insert description here
// You can write your code in this editor
distance_to_move_destination_pixels = point_distance(x,y, move_destination.x, move_destination.y);
var conditional_move_distance = move_speed_per_round_inches*ONE_INCH_IN_PIXELS/ROUND_DURATION_FRAMES;
if(has_trait("Scout")) {
	var spawner = instance_nearest(x,y, Spawner);
	if(instance_exists(spawner) and spawner.faction_mask == faction_mask) {
		var d = distance_to_object(spawner);
		if(d < 9 * ONE_INCH_IN_PIXELS) {
			conditional_move_distance *= 2;	
		}
	}
}
if(distance_to_move_destination_pixels < conditional_move_distance) {
	x = move_destination.x;
	y = move_destination.y;
	
} else {
	move_towards_point(move_destination.x, move_destination.y, conditional_move_distance);
}


if(instance_exists(follow_target)){
	if(distance_to_object(follow_target) > 10) {
		move_destination.x = follow_target.x
		move_destination.y = follow_target.y;
	} else {
		move_destination.x = x;
		move_destination.y = y;
	}
} else {
	follow_target = noone;	
}

if(attack_cooldown_frames) {
	--attack_cooldown_frames;	
}

function deactivate_allies(faction) {
	with(Unit) {
		if ( not (faction ^ faction_mask) ) {
			instance_deactivate_object(id);	
		}
	}
}

function find_nearest_enemy_unit() {
	instance_deactivate_object(id);
	deactivate_allies(faction_mask)
	var nearest_enemy = instance_nearest(x,y, Unit);
	instance_activate_object(Unit);	
	return nearest_enemy;
}

if(attack_cooldown_frames == 0 and can_attack) {
	var attacked = false;
	var nearest_enemy = find_nearest_enemy_unit();
	if(instance_exists(nearest_enemy)) {
		var distance = distance_to_object(nearest_enemy);
		if( distance < range) {
			var roll = do_attack(id, nearest_enemy);
			if(roll == 6 and has_trait("Furious") and instance_exists(nearest_enemy)) {
				do_attack(id, nearest_enemy);	
			}
			attacked =  true;	
		}
	}
	if(attacked) {
		attack_cooldown_frames = ROUND_DURATION_FRAMES / attacks;	
		image_blend = c_ltgray;
	}
}

image_blend = lerp_color_rgp(image_blend, c_white, 0.07);

if(ai_active) {
	var nearest_enemy = find_nearest_enemy_unit();
	if(instance_exists(nearest_enemy)){
		var distance_to_nearest = distance_to_object(nearest_enemy);
		if(distance_to_nearest < range) {
			move_destination.x = x;
			move_destination.y = y;
		} else {
			follow_target = nearest_enemy;
		}
	} else  {
		move_destination.x = x;
		move_destination.y = y;
	}
}