/// @description Insert description here
// You can write your code in this editor

#macro ROUND_DURATION_FRAMES 180
#macro ONE_INCH_IN_PIXELS 128
#macro MOVE_SNAP_DISTANCE 5

quality = 5;
defense = 5;
toughness = 1;
wounds = 0;
traits = [];
selected = false;
move_speed_per_round_inches = 10;
move_destination = {x:x, y:y};
distance_to_move_destination_pixels = 0;
follow_target = noone;
attack_cooldown_frames = 0;
base_size_pixels = ONE_INCH_IN_PIXELS;
can_attack = true;
can_move = true;
ai_active = true;
points = 8;
attacks = 1;
ap = 0;
range = ONE_INCH_IN_PIXELS;
projectile = noone;
projectile_settings = {};
impact = 0;
impact_last_position = {x:x, y:y};
deadly = 1;

function do_attack(attacker, defender){
	log($"--do_attack({object_get_name(attacker.object_index)}[{attacker.quality}+, {attacker.defense}+], {object_get_name(defender.object_index)}[{defender.quality}+, {defender.defense}+])--");
	
	var attacker_roll = irandom_range(1,6);
	var defender_roll = irandom_range(1,6);
	
	log($"Attacker roll {attacker_roll}");
	log($"Defender roll {defender_roll}");
	
	if (defender.has_trait("Regeneration") and not attacker.has_trait("Rending") and not attacker.has_trait("Poison") and irandom_range(1,6) >= 5) {
		log($"Defender regeneration, ignoring attack");
		log("");
		return attacker_roll;	
	}
	
	if(attacker.has_trait("Poison") and defender_roll == 6) {
		log($"Defender re-rolling from poison");
		defender_roll = irandom_range(1,6);
		log($"Defender roll {defender_roll}");
	}
	
	if(defender_roll == 6) {
		log($"Defender critical defense");
		log("");
		return attacker_roll;
	}
	
	var conditional_ap = attacker.ap;
	if(attacker.has_trait("Headtaker") and defender.toughness >= 3) {
		log($"Attacker +2 ap from Headtaker");
		conditional_ap += 2;
	}
	
	if(defender.has_trait("Ossified")) {
		log($"Attacker -1 ap from defender's Ossified");
		conditional_ap -= 1;	
	}
	
	if(attacker_roll == 6 and attacker.has_trait("Rending")) {
		log($"Attacker +4 ap from Rending");
		conditional_ap += 4;
	}
	
	if(attacker.has_trait("War Aspect")) {
		if(choose(true,false)) {
			log($"Attacker +1 ap from War Aspect");
			conditional_ap += 1;	
		} else {
			if(attacker_roll == 6) {
				log($"Attacker +4 ap from War Aspect critical");
				conditional_ap += 4;
			} else {
				log($"Attacker no bonus from War Aspect");	
			}
		}
	}
	
	if (conditional_ap < 0) {
		conditional_ap = 0;	
	}
	log($"Final ap {conditional_ap}");
	
	if(defender.has_trait("Stealth") and point_distance(attacker.x, attacker.y, defender.x, defender.y) > 9 * ONE_INCH_IN_PIXELS){
		attacker_roll -= 1;	
		log($"-1 attacker roll from Stealth, new roll {attacker_roll}");
	}
	
	var conditional_wounds = 1 * deadly;
	if(attacker.has_trait("Devour") and attacker_roll == 6){
		conditional_wounds += 1;	
		log($"Extra wound from Attacker Devour on critical");
	}
	
	if (attacker_roll == 6 or attacker_roll >= attacker.quality and defender_roll - conditional_ap < defender.defense) {
		if(projectile == noone) {
			defender.wounds += conditional_wounds;
			log($"Defender wounded for {conditional_wounds}");
		} else {
			generic_projectile_create(attacker, defender, conditional_wounds, projectile);	
			log($"Projectile launched for {conditional_wounds}");
		}
	} else if (projectile != noone) {
		generic_projectile_create(attacker, defender, 0, projectile);	
		log($"Miss Projectile launched");
	} else {
		log("Attack misses");	
	}
	
	var impact = attacker.impact and point_distance(attacker.x, attacker.y, attacker.impact_last_position.x, attacker.impact_last_position.y) > ONE_INCH_IN_PIXELS;
	if( impact and defender.wounds < defender.toughness ) {
		attacker.impact_last_position.x = attacker.x;
		attacker.impact_last_position.y = attacker.y;
		repeat(attacker.impact) {
			if(irandom_range(1,6) >= 2)	 {
				defender.wounds += conditional_wounds;	
				log($"Defender takes wound from impact");
			} else {
				log($"Defender avoids impact wound");	
			}
		}
	}
	
	if(defender.wounds >= defender.toughness) {
		log($"Defender destroyed from wounds");
		instance_destroy(defender);	
	}
	
	log($"");
	return attacker_roll;
}

function lerp_color_rgp(c1, c2, amount) {
	var c1r = color_get_red(c1);
	var c1g = color_get_green(c1);
	var v1b = color_get_blue(c1);
	
	// lerp basic
	var r = lerp(c1r, color_get_red(c2), amount);
	var g = lerp(c1g, color_get_green(c2), amount);
	var b = lerp(v1b, color_get_blue(c2), amount);
	
	// handle fractional parts stochastically
	if (floor(r) == c1r and random(1) < frac(r)) {
		r = floor(r) + sign(color_get_red(c2) - c1r)
	}
	if (floor(g) == c1g and random(1) < frac(g)) {
		g = floor(g) + sign(color_get_green(c2) -c1g)
	}
	if (floor(b) == v1b and random(1) < frac(b)) {
		b = floor(b) + sign(color_get_blue(c2) - v1b)
	}
	
	
	return make_color_rgb(r,g,b);
}

function has_trait(trait) {
	for(var i = 0; i < array_length(traits); ++i) {
		if (traits[i] == trait) return true;	
	}
	return false;
}

function init_sprite_scale(){
	image_xscale = 1;
	image_yscale = 1;
	image_xscale = base_size_pixels / sprite_width;
	image_yscale = base_size_pixels / sprite_height;	
}

init_sprite_scale();