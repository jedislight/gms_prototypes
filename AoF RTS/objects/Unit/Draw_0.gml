/// @description Insert description here
// You can write your code in this editor
var ib = image_blend;
var xs = image_xscale;
var ys = image_yscale;
	
var highilght_scale = 1.2;
if(selected) {
	image_blend = c_dkgray
} else {
	switch (faction_mask) {
		case 0: image_blend = c_purple; break;
		case 1: image_blend = c_lime; break;
		default: image_blend = c_fuchsia;
	}
}
image_xscale *= highilght_scale;
image_yscale *= highilght_scale;
	
draw_sprite_stretched_ext(sprite_index, image_index, x-sprite_width/2, y-sprite_height/2, sprite_width, sprite_height, image_blend, image_alpha);
	
image_blend = ib;
image_xscale = xs;
image_yscale = ys;

draw_sprite_stretched_ext(sprite_index, image_index, x-sprite_width/2, y-sprite_height/2, sprite_width, sprite_height, image_blend, image_alpha);

if(selected){	
	draw_set_alpha(0.5)
	if(distance_to_move_destination_pixels > MOVE_SNAP_DISTANCE) {
		draw_line_width_color(x,y, move_destination.x, move_destination.y, 10, c_dkgray, c_black);
	}
	draw_set_alpha(1);
}

if(toughness > 1) {
	var l = x-sprite_width/2;
	var r = x+sprite_width/2;
	var t = y+sprite_height/2;
	var b = t + 12;
	var percent = (toughness-wounds) / toughness;
	draw_healthbar(l,t, r,b, percent*100, c_black, c_red, c_lime, 0, true, true)	
	var cursor = l;
	repeat(toughness-1){
		cursor += (r-l) / toughness;
		draw_line_width_color(cursor, t, cursor, b, 5, c_black, c_dkgray);
	}
}