/// @description Insert description here
// You can write your code in this editor
if(!instance_exists(id)) exit;
if(has_trait("Flying")) exit;
if (can_move) {
	var is_moving = not (move_destination.x == x and move_destination.y == y);
	var direction_to_other = point_direction(x,y, other.x, other.y) + (other.id > id)*90;
	move_outside_all(-direction_to_other, 12.8);
	
	if(not is_moving) {
		move_destination.x = x;
		move_destination.y = y;
	}
}