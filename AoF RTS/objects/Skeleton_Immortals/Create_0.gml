/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

points = 80;
quality = 3;
defense = 3;
attacks = 3;
ap = 1;
move_speed_per_round_inches -= 1;
toughness = 3;
traits = ["Ossified", "Undead", "Regeneration", "Rending"]
