/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

points = 300;
quality = 4;
defense = 3;
attacks = 10;
ap = 2;
move_speed_per_round_inches -= 1;
toughness = 12;
base_size_pixels = 3 * ONE_INCH_IN_PIXELS;
traits = ["Ossified", "Undead", "Regeneration"]

init_sprite_scale()
