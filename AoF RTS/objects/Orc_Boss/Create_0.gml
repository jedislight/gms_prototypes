/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

points = 440;
quality = 4;
defense = 3;
attacks = 18;
ap = 1;
toughness = 18;
impact = 3;
move_speed_per_round_inches += 1;
base_size_pixels = 2 * ONE_INCH_IN_PIXELS;
traits = ["Furious", "Flying"]

init_sprite_scale();