/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

points = 18;
quality = 4;
defense = 5;
attacks = 1;
range = 18 * ONE_INCH_IN_PIXELS;
projectile = dart;
traits = ["Strider", "Poison", "Stealth", "Scout"]