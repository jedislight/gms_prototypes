/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

points = 12;
quality = 4;
defense = 4;
attacks = 1;
move_speed_per_round_inches -= 1;
traits = ["Ossified", "Undead"]
