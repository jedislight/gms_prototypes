/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

points = 13;
quality = 5;
defense = 5;
attacks = 1;
range = 24 * ONE_INCH_IN_PIXELS;
projectile = arrow;
traits = ["Furious"]