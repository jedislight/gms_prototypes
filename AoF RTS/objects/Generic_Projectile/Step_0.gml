/// @description Insert description here
// You can write your code in this editor

if(instance_exists(target)) {
	target_x = target.x;
	target_y = target.y;
}

var lerp_speed = 0.1;

x = lerp(x, target_x, lerp_speed);
y = lerp(y, target_y, lerp_speed);

var d = point_distance(x,y, target_x, target_y);
if(d < 64) {
	if(instance_exists(target)){
		target.wounds += 1;
		if(target.wounds <= 0) {
			instance_destroy(target);	
		}
	}
	
	instance_destroy(id);
}

if(rotation) {
	image_angle += rotation
} else {
	image_angle = point_direction(xprevious, yprevious, x, y);	
}