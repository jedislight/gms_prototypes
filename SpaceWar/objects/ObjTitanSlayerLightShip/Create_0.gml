/// @description Insert description here
// You can write your code in this editor
event_inherited();

/// @description Insert description here
// You can write your code in this editor
class = MODEL_CLASS.LIGHT_SHIP
move = 8
cruise = 12
turn = 90
hits = 3;
max_hits = 3;
evade = 4;
armor = 4;

turret.range = 12
turret.attack = 2
turret.strength = 0;

faction = FACTION.XN;

ai = AI_MODES.SHOOTING

front_weapon = new WeaponSystem();
front_weapon.range = 12
front_weapon.attack = 4
front_weapon.strength = 0
front_weapon.fire_mode = WEAPON_SYSTEM_FIREMODE.SINGLE
front_weapon.properties = ["Anti-Ship"]


