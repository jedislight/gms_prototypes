/// @description Insert description here
// You can write your code in this editor
event_inherited();

/// @description Insert description here
// You can write your code in this editor
class = MODEL_CLASS.SQUADRON;
move = 10
cruise = 15
turn = 2000
hits = 3;
max_hits = 3;
evade = 5;
armor = 5;

turret.range = 2
turret.attack = 2
turret.strength = 0;

properties = ["Fragile", "Suicide Run"]
fragile_time = 0;
fragile_stable_hits = hits;

faction = FACTION.MA;

ai = AI_MODES.RAMMING