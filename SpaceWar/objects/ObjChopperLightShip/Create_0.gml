/// @description Insert description here
// You can write your code in this editor
event_inherited();

/// @description Insert description here
// You can write your code in this editor
class = MODEL_CLASS.LIGHT_SHIP
move = 8
cruise = 12
turn = 90
hits = 6;
max_hits = 6;
evade = 4;
armor = 5;

turret.range = 6
turret.attack = 2
turret.strength = 0;

properties = ["Battering Ram", "Reinforced Ram", "Stealth Rig"];

faction = FACTION.MA;

ai = AI_MODES.RAMMING
