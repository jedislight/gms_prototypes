/// @description Insert description here
// You can write your code in this editor



image_angle = heading;

if( hits <= 0) {
	instance_destroy(id);
}

var is_fragile = array_contains(properties, "Fragile");
var fragile_hits_disrupted = fragile_stable_hits > hits;
var fragile_time_met = ObjSimulation.sim_time > fragile_time;
if(is_fragile and fragile_time_met){
	if(fragile_hits_disrupted) {
		hits -= 1;	
		fragile_stable_hits = hits;
		fragile_time = ObjSimulation.sim_time + ObjSimulation.round_duration_seconds;
	} else {
		fragile_stable_hits = hits;	
	}
}

if(MODEL_MODES.RAM == mode) {
	if(instance_exists(ram_target)) {
		move_to = {x:ram_target.x, y: ram_target.y};
	} else {
		mode = MODEL_MODES.HOLD;
		ram_target = noone;
	}
}

