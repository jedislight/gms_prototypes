/// @description Insert description here
// You can write your code in this editor

enum FACTION {
	MA = 0,
	EM = 1,
	XN = 2,
	AL = 3,
	PG = 4,
}

enum MODEL_CLASS {
	SQUADRON,
	LIGHT_SHIP,
	MEDIUM_SHIP,
	HEAVY_SHIP,
}

class = MODEL_CLASS.MEDIUM_SHIP;
move = 10
cruise = 20
turn = 360
heading = image_angle;
move_to = {x:x + lengthdir_x(1, heading), y:y + lengthdir_y(1, heading)};
hits = 3;
max_hits = 3;
evade = 3;
armor = 3;
enum MODEL_MODES {
	HOLD,
	MOVE,
	CRUISE,
	RAM,
}
mode = MODEL_MODES.HOLD;

turret = new WeaponSystem();
front_weapon = noone;
side_weapon = noone;
back_weapon = noone;

faction = 0;

ram_target = noone;

properties = [];

fragile_time = 999999;
fragile_stable_hits = hits;

ai = AI_MODES.SHOOTING

ancient_protector_available = false;