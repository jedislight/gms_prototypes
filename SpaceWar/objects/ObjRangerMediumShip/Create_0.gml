/// @description Insert description here
// You can write your code in this editor
event_inherited();

/// @description Insert description here
// You can write your code in this editor
class = MODEL_CLASS.MEDIUM_SHIP;
move = 6
cruise = 9
turn = 90
hits = 11;
max_hits = 11;
evade = 3;
armor = 3;

properties = ["Fragile", "Paragon"]

turret.range = 24
turret.attack = 2
turret.strength = 1;
turret.properties = ["Lone Hunter"];

front_weapon = new FletchetteCannon();
side_weapon = new TurboLaser();

faction = FACTION.AL;