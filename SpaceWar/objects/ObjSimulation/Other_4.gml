/// @description Insert description here
// You can write your code in this editor

with(ObjModel){
	if(array_contains(properties, "Vanguard")){
		var dist_pixels = inches_to_pixels(move);
		x += lengthdir_x(dist_pixels, heading);
		y += lengthdir_y(dist_pixels, heading);
	}
	
	if(is_undefined(ObjSimulation.faction_morale_ship_threshold[? faction])) {
		ObjSimulation.faction_morale_ship_threshold[? faction] = 0;	
	}
	
	if(MODEL_CLASS.LIGHT_SHIP <= class) {
		ObjSimulation.faction_morale_ship_threshold[? faction] += 0.5;	
	}
}

