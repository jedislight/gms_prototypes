/// @description Insert description here
// You can write your code in this editor

var dt = dst();
if(dt > 1.0) exit;

with(ObjModel) {	
	// blocking and dangerous terrain
	var terrains = ds_list_create()
	instance_place_list(x,y, ObjTerrain, terrains, false);
	for(var i = 0; i < ds_list_size(terrains); ++i){
		var terrain = terrains[| i];
		if(terrain.dangerous) hits -= d(3)*dt / ObjSimulation.round_duration_seconds;
		if(terrain.blocking) {
			hits -= dt / ObjSimulation.round_duration_seconds;
			var eject_dir = point_direction(terrain.x, terrain.y, x, y);
			var eject_speed = move_rate*dt/ObjSimulation.round_duration_seconds;
			var eject_pixels = inches_to_pixels(eject_speed);
			x += lengthdir_x(eject_pixels, eject_dir)
			y += lengthdir_y(eject_pixels, eject_dir)
		}
	}
	ds_list_destroy(terrains);
	
	// movement
	// check for engagement overrides for squadrons
	var is_squadron = class == MODEL_CLASS.SQUADRON;
	var this = id;
	instance_deactivate_object(id);
	with(ObjModel){
		if(class >= MODEL_CLASS.LIGHT_SHIP) instance_deactivate_object(id);
		if(this.faction == faction) instance_deactivate_object(id);
	}
	var closest_enemy_squadron = instance_nearest(x,y, ObjModel);
	instance_activate_all();
	if(is_squadron and instance_exists(closest_enemy_squadron)) {
		var distance_to_closest_enemy_squadron = pixels_to_inches(distance_to_object(closest_enemy_squadron));
		var engage_distance = 2;
		if(array_contains(closest_enemy_squadron.properties, "Defender")) engage_distance = 4;
		var engaged = distance_to_closest_enemy_squadron < engage_distance;
		// check for Control Center override override
		var command_center_in_range = false;
		with(ObjModel) {
			var command_center = array_contains(properties, "Control Center");
			var in_range = pixels_to_inches(distance_to_object(this)) < 6;
			if(in_range and command_center) command_center_in_range = true;				
		}
		if(command_center_in_range) engaged = false;
			
		if(engaged) mode = MODEL_MODES.HOLD;
	}
		
	var move_rate = cruise;
	// Pulse Engine grants +3 cruise/ram move
	if(array_contains(properties, "Pulse Engine")) move_rate += 3;
	if(MODEL_MODES.MOVE == mode){
		move_rate = move;
		// Pulse Engine grants +2 base move
		if(array_contains(properties, "Pulse Engine")) move_rate += 2;
	}
		
	var terrains = ds_list_create()
	instance_place_list(x,y, ObjTerrain, terrains, false);
	for(var i = 0; i < ds_list_size(terrains); ++i){
		var terrain = terrains[| i];
		if(terrain.difficult) move_rate *= 0.5;
	}
	ds_list_destroy(terrains);
			
		
	var dir_to_move_target = point_direction(x,y, move_to.x, move_to.y);
	var angle_to_move_target = angle_difference(dir_to_move_target, heading);
	var cruise_penalty = mode == MODEL_MODES.CRUISE ? 0.5 : 1 ;
	var ram_penalty = mode == MODEL_MODES.RAM ? 0.5 : 1 ;
	var hold_bonus = mode == MODEL_MODES.HOLD ? 2 : 1;
	var turn_rate_per_second = turn / ObjSimulation.round_duration_seconds * cruise_penalty * hold_bonus * ram_penalty;
	var max_turn_this_update = turn_rate_per_second * dt;
	var turn_rate_this_update = min(abs(angle_to_move_target), max_turn_this_update)
	var turn_this_update = turn_rate_this_update * sign(angle_to_move_target);
	heading += turn_this_update;
		
	if( MODEL_MODES.HOLD != mode) {
		var alignment = 1-(abs(angle_difference(heading, dir_to_move_target)) / 180);
		var distance_to_target = pixels_to_inches(point_distance(x,y, move_to.x, move_to.y));
		var alignment_move_penalty = max(min(0.5), alignment*alignment);
		var move_rate_per_second = move_rate / ObjSimulation.round_duration_seconds * alignment_move_penalty;
		var max_move_rate_this_update = move_rate_per_second * dt;
		var move_rate_this_update = min(distance_to_target, max_move_rate_this_update);
		var px_moved_this_update = inches_to_pixels(move_rate_this_update);
		x += lengthdir_x(px_moved_this_update, heading);
		y += lengthdir_y(px_moved_this_update, heading);
		
		if(collision_point(move_to.x, move_to.y, id, true, false) == id) {
			mode = MODEL_MODES.HOLD;	
		}
	}
	
	//overlap / ramming
	var can_overlap_safely = class == MODEL_CLASS.SQUADRON;
	if(array_contains(properties, "Suicide Run") and mode == MODEL_MODES.RAM) {
		can_overlap_safely = false;		
	}
	var this = id;
	if(not can_overlap_safely) {	
		var this_in_overlap_list = ds_list_find_index(ObjSimulation.overlap_list, this.id) >= 0;
		with(ObjModel){
			if(id == this.id) continue;
			var in_overlap_list = ds_list_find_index(ObjSimulation.overlap_list, id) >= 0;
			
			// early out if both models already overlapping
			if(in_overlap_list and this_in_overlap_list) continue;
			var overlap = place_meeting(x,y, this);
			if(not overlap) continue;
			ds_list_add(ObjSimulation.overlap_list, id);
			ds_list_add(ObjSimulation.overlap_list, this);
			var is_ram = this.mode == MODEL_MODES.RAM or mode == MODEL_MODES.RAM;
			if(is_ram){
				this.hits -= 2;
				hits -= 2;
				var this_effective_size = this.max_hits;
				var effective_size = this.max_hits;
				
				// battering ram grants +3 size
				if(array_contains(this.properties, "Battering Ram")) this_effective_size += 3;
				if(array_contains(properties, "Battering Ram")) effective_size += 3;
				
				if(this_effective_size > effective_size){
					hits -= 1;	
				}
				if(this_effective_size < effective_size){
					this.hits -= 1;
				}
				
				// Reinforced Ram deals 1 extra damage when ramming
				if(MODEL_MODES.RAM == mode and array_contains(properties, "Reinforced Ram")){
					this.hits -= 1;	
				}
				if(MODEL_MODES.RAM == this.mode and array_contains(this.properties, "Reinforced Ram")){
					hits -= 1;	
				}
				
				// Ram push
				if(MODEL_MODES.RAM == mode){
					ram_push(id, this);	
				}
				if(MODEL_MODES.RAM == this.mode){
					ram_push(this, id);	
				}
				
				// ram -> hold transition (keep at end of block)
				if(this.mode == MODEL_MODES.RAM) this.mode = MODEL_MODES.HOLD;
				if(mode == MODEL_MODES.RAM) mode = MODEL_MODES.HOLD;
				
			} else {
				var this_dam = 1;
				var dam = 1;
				
				// Redemption adds damage to the other
				if(array_contains(this.properties, "Redemption")) dam += 1;
				if(array_contains(properties, "Redemption")) this_dam += 1;
				
				// Redemption cancels all damage from overlap
				if(array_contains(this.properties, "Redemption")) this_dam = 0;
				if(array_contains(properties, "Redemption")) dam = 0;
				
				this.hits -= this_dam;
				hits -= dam;
			}
		}
	}
	
	// clearing overlaps
	for(var i =0; i < ds_list_size(ObjSimulation.overlap_list); ++i){
		var model = ObjSimulation.overlap_list[| i];
		var free = false;
		with(model) if(place_empty(x,y, ObjModel)){
			ds_list_delete(ObjSimulation.overlap_list, i);
			i -= 1;
		}
	}
	
	// weapons fire
	if(MODEL_MODES.CRUISE != mode && MODEL_MODES.RAM != mode){
		var fire_weapon = function(weapon_system, dir, arc){
			// reload cooldown
			if(weapon_system.ready_time > ObjSimulation.sim_time) return false;
			
			// target finding
			var targets = [];
			var this = id;
			with(ObjModel){
				if(id == this or faction == this.faction) continue;
				var dir_to_target = point_direction(this.x, this.y, x, y);
				var angle_to_target = angle_difference(dir_to_target, dir);
				var in_arc = abs(angle_to_target) < arc / 2;
				var distance = pixels_to_inches(distance_to_object(this));
				var in_range = weapon_system.range >= distance;
				
				// special filters
				if(array_contains(weapon_system.properties, "Anti-Ship") and class == MODEL_CLASS.SQUADRON) continue;
				if(array_contains(weapon_system.properties, "Anti-Squadron") and class >= MODEL_CLASS.LIGHT_SHIP) continue;
				
				if (in_range and in_arc) array_push(targets, id);
			}
			global.temp = this;
			array_sort(targets, function(a,b){return point_distance(global.temp.x, global.temp.x, a.x, a.y)  - point_distance(global.temp.x, global.temp.y, b.x, b.y)})
			var target_count = 1
			if(array_contains(weapon_system.properties, "All-Targeting")) target_count = array_length(targets);
			
			var fired = false;
			for(var t = 0; t<min(target_count, array_length(targets)); ++t) {
				var target = targets[t];
			
				// adjust final weapon dir
				dir = point_direction(this.x, this.y, target.x, target.y);
			
				// pre-compute firing angle
				var incoming_fire_dir = dir + 180;
				var rear_dir = target.heading + 180;
				var rear_arc = 90;
				var incoming_rear_angle = angle_difference(incoming_fire_dir, rear_dir);
				var firing_on_rear = abs(incoming_rear_angle) < rear_arc/2;
			
				var front_arc = 90;
				var incoming_front_angle = angle_difference(incoming_fire_dir, target.heading);
				var firing_on_front = abs(incoming_front_angle) < front_arc / 2;
			
				var firing_on_side = not (firing_on_front or firing_on_rear);
			
				// attack modes
				var die = weapon_system.attack;
				var ready_time_modifier = 1.0;
				
				// check for holding squadrons supported by Munitions Resupply
				var is_squadron = this.class == MODEL_CLASS.SQUADRON;
				var is_holding = this.mode == MODEL_MODES.HOLD;
				with(ObjModel) {
					if(!array_contains(properties, "Munitions Resupply")) instance_deactivate_object(id);
				}
				var closest_resupply = instance_nearest(x,y, ObjModel);
				instance_activate_all();
				var resupply_in_range = instance_exists(closest_resupply) and pixels_to_inches(distance_to_object(closest_resupply)) < 4;
				if(resupply_in_range and is_squadron and is_holding){
					ready_time_modifier *= 0.5;	
				}
				
				weapon_system.ready_time = ObjSimulation.sim_time + ObjSimulation.round_duration_seconds * ready_time_modifier;
				// special broadside weapons double attack against sides
				if(array_contains(weapon_system.properties, "Broadside") and firing_on_side){
					die *= 2;
				}
				if(WEAPON_SYSTEM_FIREMODE.CONTINUAL == weapon_system.fire_mode) {
					weapon_system.ready_time = ObjSimulation.sim_time + (ObjSimulation.round_duration_seconds) / die * ready_time_modifier;
					die = 1;
				}
					
				// pre-compute cover
				var has_cover = false;
				var stealth = array_contains(target.properties, "Stealth Rig");
				var distance_to_target = pixels_to_inches(distance_to_object(target));
				if( stealth and distance_to_target > 12){ 
					has_cover = true;
				}
			
				var terrains = ds_list_create();
				collision_line_list(x,y, target.x, target.y, ObjTerrain, true, false, terrains, false);
				for(var i = 0; i < ds_list_size(terrains); ++i){
					var terrain = terrains[| i];
					if(terrain.cover) has_cover = true;
				}
				ds_list_destroy(terrains);
				
				// pre-compute lone hunter
				var lone_hunter_bonus = false;
				if(array_contains(weapon_system.properties, "Lone Hunter")){
					with(ObjModel) if(faction == this.faction) instance_deactivate_object(id);
					var closest_ally_model = instance_nearest(x,y, ObjModel);
					if(instance_exists(closest_ally_model)){
						var distance_to_closest_ally_model = pixels_to_inches(distance_to_object(closest_ally_model));
						lone_hunter_bonus = distance_to_closest_ally_model > 6;
					} else {
						lone_hunter_bonus = true;
					}
					
					instance_activate_all();
				}
			
				//attack roll
				var hazzardous_ammo = array_contains(weapon_system.properties, "Hazardous Ammo");
				if(this.hits+2 <= this.max_hits) {
					hazzardous_ammo = false;
				}
				if(hazzardous_ammo) {
					this.hits -= 1;
				}
				var hits = 0;
				repeat (die) {
					var roll = d(6);
					if(roll == 1) {
						//special - damage taken on crit miss for Overheating
						if(array_contains(weapon_system.properties, "Overheating")) this.hits -= 1;
					
						continue;
					}
					if(roll == 6) {
						hits += 1;
						continue;
					}
				
					var total = roll;
					// if in rear +1
					if(firing_on_rear) total += 1;
				
					// if squad on squad +1
					if(this.max_hits == 3 and target.max_hits == 3) total += 1;
				
					// if cover -1
					if(has_cover) total -= 1;
				
					// if Gunnery Crew and within 12" +1
					if(array_contains(weapon_system.properties, "Gunnery Crew") and distance_to_target < 12) total += 1;
				
					// if Anti-Ship or Anti-Squadron +1 (filtering done by this point already so just check existance)
					if(array_contains(weapon_system.properties, "Anti-Ship")) total += 1
					if(array_contains(weapon_system.properties, "Anti-Squadron")) total += 1
					
					// +1 for hazzardous_ammo
					if(hazzardous_ammo) total += 1;
				
					// +1 for Precision Rig
					if(array_contains(weapon_system.properties, "Precision Rig")) total += 1;
					
					// +1 for smaller class for Predator Cysts
					if(array_contains(this.properties, "Predator Cysts") and this.class > target.class) total += 1;
					
					// +1 for Lone Hunter if active
					if(lone_hunter_bonus) total += 1;
					
					// +1 for Atovs at range
					if(array_contains(this.properties, "Atov's Veterans") and distance_to_target >= 12) total += 1;
				
					var current_evade = target.evade;
					if(MODEL_MODES.HOLD == target.mode and not array_contains(target.properties, "Paragon")) current_evade = 2;
					if(total >= current_evade) hits +=1;
				}
			
				var hit_targets = [target];
				// gather blast targets
				if(array_contains(weapon_system.properties, "Blast")){
					with(ObjModel){
						if(target.id == id) continue;
						var distance = pixels_to_inches(distance_to_object(target));
						if(distance <= 4) array_push(hit_targets, id);
					}
				}
				
				for(var bt = 0; bt < array_length(hit_targets); ++bt){
					target = hit_targets[bt];
					// blocking
					var damage = 0;
					repeat(hits){
						var roll = d(6);
						if( roll == 1 ) {
							damage += 1;
							continue;
						}
						if( roll  == 6 ) continue;
				
						var total = roll - weapon_system.strength;
						// -1 for firing in rear
						if(firing_on_rear) total -= 1;
					
						// -1 for hazzardous_ammo
						if(hazzardous_ammo) total -= 1;
					
						// -1 for Predator Cysts against smaller classes
						if(array_contains(this.properties, "Predator Cysts") and this.class > target.class) total -= 1;
						
						// -1 for lone hunter bonus
						if(lone_hunter_bonus) total -= 1;
						
						// -1 for Atovs at close range
						if(array_contains(target.properties, "Atov's Veterans") and distance_to_target < 12) total -= 1;
				
						// various deadly types to enable
						var deadly = array_contains(weapon_system.properties, "Deadly");
						if(array_contains(weapon_system.properties, "Deadly-Front") and firing_on_front) deadly=true;
						if(array_contains(weapon_system.properties, "Deadly-Side") and firing_on_side) deadly=true;
						if(array_contains(weapon_system.properties, "Deadly-Rear") and firing_on_rear) deadly=true;
				
				
						// Hardened negates all strength bonuses when not damaged
						if(array_contains(target.properties, "Hardened") and target.hits == target.max_hits) {
							total = roll;	
						}
						
						// apply total vs armor, if total >= armor the damage is blocked
						if(total < target.armor) {
							damage += 1;
							if(deadly) {
								damage += 1;
							}
						}
						
						// Ancient Protector blocks all damage frmo the first hit in a round
						if(array_contains(target.properties, "Ancient Protector") and damage > 0 and target.ancient_protector_available) {
							damage = 0;	
							ancient_protector_available = false;
						}
						
						// Shield Booster blocks damage on 5+ per point
						var repeat_damage = damage;
						if(array_contains(target.properties, "Shield Booster")) repeat(repeat_damage){
							if(d(6) >= 5) {
								damage -= 1;	
							}
						}
					
						//total damage modifiers only if damage done
						if(damage > 0) {
							// Void Maw
							if(array_contains(target.properties, "Void Maw") and this.class == MODEL_CLASS.SQUADRON){
								damage += 1;	
							}
						}
					}
			
					// resolve (will need to make an event for this to do animations eventually
					weapon_system.create_effect(target, this, damage, hits);
					fired = true;
				}
			}
			return fired;
		}
		
		if(is_struct(turret)){
			fire_weapon(turret, 0, 999);
		}
		
		if (is_struct(front_weapon)){
			fire_weapon(front_weapon, heading, 90);
		}
		
		if (is_struct(side_weapon)){
			var fired = fire_weapon(side_weapon, heading+90, 90);
			if(not fired){
				fire_weapon(side_weapon, heading-90, 90);
			}
		}
		
		if (is_struct(back_weapon)){
			fire_weapon(back_weapon, heading+180, 90);
		}
	}
	
	// tractor beams
	if(array_contains(properties, "Tractor Beam")) {
		with(ObjModel){			
			if(this.id != id and
			 faction != this.faction and 
			 class >= MODEL_CLASS.LIGHT_SHIP
			 ) {
				 continue;
			 } else {
				 instance_deactivate_object(id);
			 }
		}
		instance_deactivate_object(id);
		var closest_enemy_model = instance_nearest(x,y, ObjModel);
		instance_activate_all();
		
		var distance_to_model = pixels_to_inches(distance_to_object(closest_enemy_model));
		//show_debug_message($"Distance: {distance_to_model}")
		if(instance_exists(closest_enemy_model) and distance_to_model > 1 and distance_to_model < 8){
			var pull_per_round = d(6)+1;
			var pull_this_update = pull_per_round * dt / ObjSimulation.round_duration_seconds;
			var pull_this_update_pixels = inches_to_pixels(pull_this_update);
			var tractor_direction = point_direction(closest_enemy_model.x, closest_enemy_model.y, x, y);
			closest_enemy_model.x += lengthdir_x(pull_this_update_pixels, tractor_direction);
			closest_enemy_model.y += lengthdir_y(pull_this_update_pixels, tractor_direction);
			var e = instance_create_depth(0,0,0, EffectTractorBeam);
			e.start.x = x;
			e.start.y = y;
			e.finish.x = closest_enemy_model.x;
			e.finish.y = closest_enemy_model.y;
		}
	}
}

// Mines 
with(EffectSapperMine) {
	var closest_model = instance_nearest(x,y, ObjModel);
	if(!instance_exists(closest_model)) continue;
	if(closest_model.faction == faction) continue;
	var distance = pixels_to_inches(distance_to_object(closest_model));
	if(distance > 2) continue;
	
	closest_model.hits -= 2;
	instance_destroy(id);
}

// round Tick Stuff
if(round_tick_time <= sim_time){
	round_tick_time += round_duration_seconds;

	// Void Maw
	with(ObjModel) {
		if (!array_contains(properties, "Void Maw")) continue;
		var vm_ship = id;
		with(ObjModel){
			if(faction == vm_ship.faction) continue;
			if(class != MODEL_CLASS.SQUADRON) continue;
			var distance = pixels_to_inches(distance_to_object(vm_ship));
			if( distance <= 4){
				hits = 0;	
			}
		}
	}
	
	
	// Warp Drive
	with(ObjModel) {
		if(!array_contains(properties, "Warp Drive")) continue;
		if(MODEL_MODES.HOLD == mode) continue;
		var warp_target = ram_target;
		if(warp_target == noone) warp_target = move_to;
		
		var max_distance = pixels_to_inches(distance_to_point(warp_target.x, warp_target.y));
		var rolled_distance = d(6) + 2;
		var final_distance = min(max_distance, rolled_distance);
		var final_distance_pixels = inches_to_pixels(final_distance);
		
		var dir = point_direction(x,y, warp_target.x, warp_target.y);
		
		x += lengthdir_x(final_distance_pixels, dir);
		y += lengthdir_y(final_distance_pixels, dir);
	}
	
	// Sapper
	with(ObjModel) {
		if(!array_contains(properties, "Sapper")) continue;
		
		var two_inches_in_pixels = inches_to_pixels(2);
		var mine = instance_create_depth(random_range(-two_inches_in_pixels, two_inches_in_pixels) + x, random_range(-two_inches_in_pixels, two_inches_in_pixels) +y, depth+1, EffectSapperMine)
		mine.faction = faction;
	}
	
	// Morale
	for(var morale_faction = ds_map_find_first(faction_morale_ship_threshold);not is_undefined(morale_faction); morale_faction = ds_map_find_next(faction_morale_ship_threshold, morale_faction)) {
		var faction_ship_count = 0;
		with(ObjModel) if(morale_faction == faction) faction_ship_count += 1;
		
		var threshold = faction_morale_ship_threshold[? morale_faction];
		if(faction_ship_count == 0){ // counting self so will only trigger for isolated squadrons	
			with(ObjModel) if(morale_faction == faction) hits = 0;
		}
		else if(faction_ship_count < threshold){
			//morale check
			with(ObjModel) if(morale_faction == faction) {
				var check = 1+ d(6) + hits/3;
				if(check < 6) hits = 0;
			}
		}
	}
	
	// Ancient Warden regeneration
	with(ObjModel) if(array_contains(properties, "Ancient Warden") and hits < max_hits){
		hits += 1;
	}
	
	// ancient_protector_available reset (all ships carry the variable, use is property limited
	with(ObjModel) ancient_protector_available = true;
	
	// Repair Pods heals 4" squadrons on 5+ (per squadron)
	with(ObjModel) if (array_contains(properties, "Repair Pods")) {
		var repair_pod_model = id;
		with(ObjModel) if(faction == repair_pod_model.faction and class == MODEL_CLASS.SQUADRON and hits < max_hits) {
			var distance_to_repair_pod = pixels_to_inches( distance_to_object(repair_pod_model) );
			if(distance_to_repair_pod <= 4 and d(6) >= 5) {
				hits += 1;	
			}
		}
	}
	
	// Repair Bay heals 4" ship max 1 ship
	with(ObjModel) if (array_contains(properties, "Repair Bay")) {
		var repair_bay_model = id;
		var repair_target = noone;
		with(ObjModel) if(faction == repair_pod_model.faction and class >= MODEL_CLASS.LIGHT_SHIP and hits < max_hits) {
			repair_target = id;
		}
		if(instance_exists(repair_target)) {
			repair_target.hits += 1;	
		}
	}
}

// per frame stuff (hard enforcements)
// Ancient Protector can only hold
with(ObjModel) if (array_contains(properties, "Ancient Protector")) {
	mode = MODEL_MODES.HOLD;	
}