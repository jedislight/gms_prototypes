/// @description Insert description here
// You can write your code in this editor

function ram_something(model) {
	with(ObjModel) if(faction == model.faction or class == MODEL_CLASS.SQUADRON) instance_deactivate_object(id);
	with(ObjModel) if(ds_list_find_index(ObjSimulation.overlap_list, id) >= 0) instance_deactivate_object(id);	
	var ram_candidates = [];
	with(ObjModel) array_push(ram_candidates, id);
	instance_activate_all();
	
	global.temp = model;
	array_sort(ram_candidates, function(a,b){return estimate_point_travel_time(global.temp,{x:a.x, y:a.y}) - estimate_point_travel_time(global.temp,{x:b.y, y:b.y}) });
	
	if(array_length(ram_candidates)) {
		var is_aligned = is_model_travel_aligned_to_point(model, ram_candidates[0]);
		if(is_aligned){
			model.mode = MODEL_MODES.RAM;
			model.ram_target = ram_candidates[0];
		} else {
			model.mode = MODEL_MODES.MOVE;
			model.move_to = {x:ram_candidates[0].x, y:ram_candidates[0].y};
		}
		return true;
	} 
	return false;
}

function get_blocking_hostile(model, goal_point){
	with(ObjModel) if(faction == model.faction or class == MODEL_CLASS.SQUADRON) instance_deactivate_object(id);
	var blocking_model = collision_line(model.x, model.y, goal_point.x, goal_point.y, ObjModel, true, true);
	instance_activate_all();
	if(instance_exists(blocking_model)){
		return blocking_model;
	} else {
		return noone;
	}
}

function get_best_shooting_target(model) {
	with(ObjModel) if(faction == model.faction) instance_deactivate_object(id);
	var enemy_models = [];
	with(ObjModel) array_push(enemy_models, id);
	instance_activate_all();
	if(array_length(enemy_models) == 0) return noone;
	
	global.temp = model;
	array_sort(enemy_models, function(a,b){return estimate_point_travel_time(global.temp,{x:a.x, y:a.y}) - estimate_point_travel_time(global.temp,{x:b.y, y:b.y}) });
	
	return enemy_models[0];
}

// track and early out if AI update not needed this frame
var update_needed = last_seen_sim_rate == 0 and ObjSimulation.time_rate != 0;
last_seen_sim_rate = ObjSimulation.time_rate;
if(!update_needed) {
	exit;	
}

// pre compute global state
var open_objectives = [];
with(ObjMissionObjective) if (owner == noone) array_push(open_objectives, id);

var this_ai_controller = id;
with(ObjModel) {
	if(this_ai_controller.faction != faction and not array_contains(properties, "Rogue")) continue;
	
	// pre-compute state of this model
	var this_model = id;
	var ai_to_use = this_model.ai;
	
	var is_carrying_objective = false;
	with(ObjMissionObjective) if(owner == this_model) is_carrying_objective=true;
	
	var is_this_model_a_squadron = this_model.class == MODEL_CLASS.SQUADRON;
	
	var any_ships_not_of_faction_available_for_ramming = false;
	with(ObjModel) if(faction != this_model.faction and class >= MODEL_CLASS.LIGHT_SHIP) any_ships_not_of_faction_available_for_ramming = true;
	
	var max_range = get_max_shooting_range(this_model);
	
	var best_shooting_target = get_best_shooting_target(this_model);
	var distance_to_best_shooting_target = 9999;
	if(instance_exists(best_shooting_target)){
		distance_to_best_shooting_target = pixels_to_inches( distance_to_object(best_shooting_target) );
	}
	
	//fallback AI to shooting if no ram targets available
	if(!any_ships_not_of_faction_available_for_ramming) ai_to_use = AI_MODES.SHOOTING
	
	var best_objective = get_lowest_traversal(this_model, open_objectives);
	var objective_blocker_model = noone; 
	if(best_objective != noone) {
		objective_blocker_model = get_blocking_hostile(this_model, best_objective);
	}
	
	// clear objective if squadron
	if(this_model.class == MODEL_CLASS.SQUADRON){
		best_objective = noone;
		objective_blocker_model = noone;
	}

	
	// Logic type splits and application
	switch (ai_to_use) {
		default:
		case AI_MODES.SHOOTING:
			if(is_carrying_objective or best_objective == noone) {
				if(instance_exists(best_shooting_target) and distance_to_best_shooting_target - this_model.move < max_range) {
					this_model.mode = MODEL_MODES.MOVE;
					this_model.move_to = {x:best_shooting_target.x, y:best_shooting_target.y};
				} else if(instance_exists(best_shooting_target) and is_model_travel_aligned_to_point(this_model, best_shooting_target)) {
					this_model.mode = MODEL_MODES.CRUISE;
					this_model.move_to = {x:best_shooting_target.x, y:best_shooting_target.y};
				} else if(instance_exists(best_shooting_target)) {
					this_model.mode = MODEL_MODES.MOVE;
					this_model.move_to = {x:best_shooting_target.x, y:best_shooting_target.y};
				} else {
					this_model.mode = MODEL_MODES.HOLD;
				}
			} else {
				if(instance_exists(objective_blocker_model)) {
					this_model.mode = MODEL_MODES.MOVE;
					this_model.move_to = {x:objective_blocker_model.x, y:objective_blocker_model.y};
				} else if (is_model_travel_aligned_to_point(this_model, best_objective)){
					this_model.mode = MODEL_MODES.CRUISE;
					this_model.move_to = {x:best_objective.x, y:best_objective.y};	
				} else {
					this_model.mode = MODEL_MODES.MOVE;
					this_model.move_to = {x:best_objective.x, y:best_objective.y};
				}
			}
			break;
		case AI_MODES.HYBRID: // TODO
		case AI_MODES.RAMMING:		
			if((is_carrying_objective or best_objective == noone) and (ram_something(this_model))){
				// ram something added to condition to flow out if it fails
			} else if (objective_blocker_model != noone) {
				this_model.mode = MODEL_MODES.RAM;
				this_model.ram_target = objective_blocker_model;
			} else if (instance_exists(best_objective) and is_model_travel_aligned_to_point(this_model, {x:best_objective.x, y:best_objective.y})){
				this_model.mode = MODEL_MODES.CRUISE;
				this_model.move_to = {x:best_objective.x, y:best_objective.y};
			} else if(instance_exists(best_objective)) {
				this_model.mode = MODEL_MODES.MOVE;
				this_model.move_to = {x:best_objective.x, y:best_objective.y};
			} else if(!ram_something(this_model)){
				this_model.mode = MODEL_MODES.HOLD;
			}
		break;
	}
}


