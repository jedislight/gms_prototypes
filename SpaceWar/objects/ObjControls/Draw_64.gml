/// @description Insert description here
// You can write your code in this editor


var overlay_string = "";
switch(activation_mode){
	default: overlay_string += "\nDEFAULT"; break;
	case CONTROL_MODE.MOVE: overlay_string += "\nMOVE"; break;	
	case CONTROL_MODE.HOLD: overlay_string += "\nHOLD"; break;	
	case CONTROL_MODE.CRUISE: overlay_string += "\nCRUISE"; break;	
	case CONTROL_MODE.RAM: overlay_string += "\nRAM"; break;	
}

overlay_string += $"\nSpeed: x{ObjSimulation.time_rate}";
overlay_string += $"\nRound: {current_game_round}";

overlay_string += $"\nVP Marauders: {victory_points[? FACTION.MA]}";
overlay_string += $"\nVP Empire: {victory_points[? FACTION.EM]}";
overlay_string += $"\nVP Xeno: {victory_points[? FACTION.XN]}";
overlay_string += $"\nVP Alliance: {victory_points[? FACTION.AL]}";
overlay_string += $"\nVP Progenitors: {victory_points[? FACTION.PG]}";

draw_text(0,0, overlay_string);