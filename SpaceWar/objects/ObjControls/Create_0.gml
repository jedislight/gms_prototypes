/// @description Insert description here
// You can write your code in this editor

selected = noone
enum CONTROL_MODE {
	DEFAULT,
	MOVE,
	CRUISE,
	RAM,
	HOLD,
}

mode = CONTROL_MODE.DEFAULT;
activation_mode = CONTROL_MODE.MOVE;

auto_pause_next_sim_time = 0;
current_game_round = 0;

victory_points = ds_map_create();

on_mission_objective_pickup = function(token){
	ObjControls.victory_points[? token.faction] += 1;
}

on_mission_objective_drop = function(token){
	ObjControls.victory_points[? token.faction] -= 1;
}

function reset_victory_points() {
	victory_points[? FACTION.EM] = 0;
	victory_points[? FACTION.MA] = 0;
	victory_points[? FACTION.XN] = 0;
	victory_points[? FACTION.AL] = 0;
	victory_points[? FACTION.PG] = 0;
}

reset_victory_points();