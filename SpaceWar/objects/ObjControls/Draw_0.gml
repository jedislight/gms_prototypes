/// @description Insert description here
// You can write your code in this editor

function draw_model_weapon_arcs(selected, heading, weapon_center_point) {
	if(is_undefined(weapon_center_point)) weapon_center_point = {x:selected.x, y:selected.y};
	if(is_undefined(heading)) heading = selected.heading;
	var h = heading;
	if(is_struct(selected.turret)) draw_arc(weapon_center_point.x, weapon_center_point.y, inches_to_pixels(selected.turret.range), h + 0,h + 360, c_gray, 3);
	if(is_struct(selected.front_weapon)) draw_arc(weapon_center_point.x, weapon_center_point.y, inches_to_pixels(selected.front_weapon.range), h + -45,h + 45, c_gray, 3);
	if(is_struct(selected.side_weapon)) draw_arc(weapon_center_point.x, weapon_center_point.y, inches_to_pixels(selected.side_weapon.range), h + 45,h + 45+90, c_gray, 3);
	if(is_struct(selected.side_weapon)) draw_arc(weapon_center_point.x, weapon_center_point.y, inches_to_pixels(selected.side_weapon.range), h + 45+180,h + 45+90+180, c_gray, 3);	
	if(is_struct(selected.back_weapon)) draw_arc(weapon_center_point.x, weapon_center_point.y, inches_to_pixels(selected.front_weapon.range), h + 45+90,h + 45+180, c_gray, 3);
}

if(instance_exists(selected)) {
	var w = 1.25 * max(selected.sprite_height, selected.sprite_width)
	var thick = 10;
	for(var i=0; i < thick; ++i){
		draw_circle_color(selected.x, selected.y, i+w/2, c_lime, c_green, true);
	}
}

if(instance_exists(selected)) {
	var c1 = c_red;
	var c2 = c_maroon;
	
	if(CONTROL_MODE.MOVE == mode) {
		c1 = c_lime;
		c2 = c_green
	}
	
	if(CONTROL_MODE.CRUISE == mode) {
		c1 = c_blue;
		c2 = c_aqua;
	}
	
	if(CONTROL_MODE.HOLD == mode) {
		c1 = c_fuchsia;
		c2 = c_purple;
	}
	
	draw_line_width_color(selected.x, selected.y, mouse_x, mouse_y, 10, c1, c2)
	var width = 10
	draw_rectangle_color(mouse_x-width, mouse_y-width, mouse_x+width, mouse_y+width, c1, c2, c1, c2, false);
	
	var weapon_center_point = {x:mouse_x, y:mouse_y}
	if(CONTROL_MODE.HOLD == mode) {
		weapon_center_point = {x:selected.x, y:selected.y}	
	}
	
	var final_heading = point_direction(selected.x, selected.y, mouse_x, mouse_y);
	draw_model_weapon_arcs(selected, final_heading ,weapon_center_point);
}

with(ObjModel) {
	if(MODEL_MODES.MOVE == mode){
		draw_line_width_color(x,y, move_to.x, move_to.y, 5, c_green, c_lime);	
	}
	
	if(MODEL_MODES.CRUISE == mode){
		draw_line_width_color(x,y, move_to.x, move_to.y, 5, c_blue, c_aqua);	
	}
	
	if(MODEL_MODES.RAM == mode){
		draw_line_width_color(x,y, move_to.x, move_to.y, 5, c_red, c_red);	
	}
	
	if(MODEL_MODES.HOLD == mode){
		draw_line_width_color(x,y, move_to.x, move_to.y, 5, c_fuchsia, c_purple);	
	}
	
	var draw_hits = function(num, pos_x, pos_y, width, height){
		for(var pip = 0; pip < 3; ++pip) {
			var c = c_lime;
			if(num <= pip) c = c_red;			
			var left = pos_x+width/3*pip;
			var right = pos_x+width/3*(pip+1);
			var top = pos_y;
			var bot = pos_y+height;
			draw_rectangle_color(left, top, right, bot, c,c,c,c_ltgray, false);
			draw_rectangle_color(left, top, right, bot, 0,0,0,0, true);
		}
	}
	
	var width = sprite_width;
	var height = 10;
	var pos_y = y - sprite_width/2-height;
	var pos_x = x - sprite_width/2;
	for(var row = 0; row < max_hits/3; ++row){
		var last_pip_hit_matches = row*3 + 2;
		var to_draw = 0;
		if ( hits > last_pip_hit_matches ) to_draw+=1;
		if ( hits > last_pip_hit_matches - 1 ) to_draw+=1;
		if ( hits > last_pip_hit_matches - 2) to_draw+=1;
		draw_hits(to_draw, pos_x, pos_y-row*height, width, height);
	}
	
	if(collision_point(mouse_x, mouse_y,id, true, false) != noone) {
		draw_model_weapon_arcs(id);	
	}
}
