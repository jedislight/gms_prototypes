/// @description Insert description here
// You can write your code in this editor


if(ObjSimulation.sim_time >= auto_pause_next_sim_time) {
	ObjSimulation.time_rate = 0.0;
	current_game_round += 1;
	auto_pause_next_sim_time += ObjSimulation.round_duration_seconds;
}

reset_victory_points();
with(ObjMissionObjective) {
	if(!instance_exists(owner)) continue;
	ObjControls.victory_points[? owner.faction] += 1;
}
