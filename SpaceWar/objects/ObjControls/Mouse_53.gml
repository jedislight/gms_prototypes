/// @description Insert description here
// You can write your code in this editor

var model_at_cursor = collision_point(mouse_x, mouse_y, ObjModel, true, true);

if(CONTROL_MODE.DEFAULT == mode) {
	if(instance_exists(model_at_cursor)){
		selected = model_at_cursor;
		mode = activation_mode;
		exit;
	}
}

if(CONTROL_MODE.MOVE == mode and instance_exists(selected)){
	selected.move_to = {x:mouse_x, y:mouse_y};
	selected.mode = MODEL_MODES.MOVE;
	mode = CONTROL_MODE.DEFAULT;
	selected = noone;
	exit;
}

if(CONTROL_MODE.HOLD == mode and instance_exists(selected)){
	selected.move_to = {x:mouse_x, y:mouse_y};
	selected.mode = MODEL_MODES.HOLD;
	mode = CONTROL_MODE.DEFAULT;
	selected = noone;
	exit;
}

if(CONTROL_MODE.CRUISE == mode and instance_exists(selected)){
	selected.move_to = {x:mouse_x, y:mouse_y};
	selected.mode = MODEL_MODES.CRUISE;
	mode = CONTROL_MODE.DEFAULT;
	selected = noone;
	exit;
}

if(CONTROL_MODE.RAM == mode and instance_exists(selected)){
	selected.move_to = {x:mouse_x, y:mouse_y};
	selected.ram_target = model_at_cursor;
	selected.mode = MODEL_MODES.RAM;
	mode = CONTROL_MODE.DEFAULT;
	selected = noone;
	exit;
}