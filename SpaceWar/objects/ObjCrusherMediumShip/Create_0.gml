/// @description Insert description here
// You can write your code in this editor
event_inherited();

/// @description Insert description here
// You can write your code in this editor
class = MODEL_CLASS.MEDIUM_SHIP;
move = 6
cruise = 9
turn = 90
hits = 9;
max_hits = 9;
evade = 3;
armor = 4;

turret.range = 12
turret.attack = 2
turret.strength = 1;
turret.properties = ["Gunnery Crew"];

front_weapon = new WeaponSystem();
front_weapon.range = 12
front_weapon.attack = 3
front_weapon.strength = 1
front_weapon.fire_mode = WEAPON_SYSTEM_FIREMODE.SINGLE
front_weapon.properties = ["Anti-Ship"];

side_weapon = new WeaponSystem();
side_weapon.range = 6;
side_weapon.attack = 3;
side_weapon.strength = 1;
side_weapon.properties = ["Broadside"];

properties = ["Battering Ram"];

faction = FACTION.MA;

ai = AI_MODES.HYBRID
