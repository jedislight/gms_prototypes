/// @description Insert description here
// You can write your code in this editor
event_inherited();

/// @description Insert description here
// You can write your code in this editor
class = MODEL_CLASS.HEAVY_SHIP;
move = 4
cruise = 6
turn = 90
hits = 12;
max_hits = 12;
evade = 2;
armor = 2;

turret.range = 24
turret.attack = 2
turret.strength = 2;
turret.properties = ["Precision Rig"]

front_weapon = new WeaponSystem();
front_weapon.range = 24
front_weapon.attack = 3
front_weapon.strength = 1
front_weapon.fire_mode = WEAPON_SYSTEM_FIREMODE.SINGLE
front_weapon.properties = ["Overheating"]

side_weapon = new WeaponSystem();
side_weapon.range = 12;
side_weapon.attack = 3;
side_weapon.strength = 0;
side_weapon.properties = ["Deadly-Front"]

properties = ["Munitions Resupply", "Pulse Engine"];

faction = FACTION.EM;