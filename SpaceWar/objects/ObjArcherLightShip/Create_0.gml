/// @description Insert description here
// You can write your code in this editor
event_inherited();

/// @description Insert description here
// You can write your code in this editor
class = MODEL_CLASS.LIGHT_SHIP;
move = 8
cruise = 12
turn = 90
hits = 8;
max_hits = 8;
evade = 4;
armor = 4;

turret.range = 18
turret.attack = 2
turret.strength = 0;

front_weapon = new FletchetteCannon();

properties = ["Fragile", "Paragon"];

faction = FACTION.AL;