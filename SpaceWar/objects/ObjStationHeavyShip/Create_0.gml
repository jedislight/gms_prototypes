/// @description Insert description here
// You can write your code in this editor
event_inherited();

/// @description Insert description here
// You can write your code in this editor
class = MODEL_CLASS.HEAVY_SHIP;
move = 2
cruise = 3
turn = 90
hits = 12;
max_hits = 12;
evade = 2;
armor = 2;

turret.range = 24
turret.attack = 2
turret.strength = 2;

properties = ["Hardened", "Ancient Protector", "Ancient Warden", "Redemption", "Repair Pods", "Repair Bay", "Shield Booster", "Tractor Beam"];

faction = FACTION.PG;

ai = AI_MODES.SHOOTING