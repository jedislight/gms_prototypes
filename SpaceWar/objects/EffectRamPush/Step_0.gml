/// @description Insert description here
// You can write your code in this editor

if(not instance_exists(target) or ObjSimulation.sim_time >= over_at_time){
	instance_destroy(id);
	exit;
}

var dt = dst();
var movement_this_update = distance / duration * dt;
var movement_this_update_pixels = inches_to_pixels(movement_this_update);

target.x += lengthdir_x(movement_this_update_pixels, dir);
target.y += lengthdir_y(movement_this_update_pixels, dir);
