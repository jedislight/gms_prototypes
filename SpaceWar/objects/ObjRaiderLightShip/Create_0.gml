/// @description Insert description here
// You can write your code in this editor
event_inherited();

/// @description Insert description here
// You can write your code in this editor
class = MODEL_CLASS.LIGHT_SHIP;
move = 10
cruise = 15
turn = 90
hits = 6;
max_hits = 6;
evade = 4;
armor = 4;

turret.range = 6
turret.attack = 3
turret.strength = 0;

side_weapon = new WeaponSystem();
side_weapon.range = 4;
side_weapon.attack = 2;
side_weapon.strength = 0;
side_weapon.properties = ["Anti-Squadron", "All-Targeting"];

properties = ["Control Center"];

faction = FACTION.EM;