/// @description Insert description here
// You can write your code in this editor
event_inherited();

/// @description Insert description here
// You can write your code in this editor
class = MODEL_CLASS.LIGHT_SHIP
move = 8
cruise = 12
turn = 90
hits = 6;
max_hits = 6;
evade = 4;
armor = 4;

turret.range = 12
turret.attack = 2
turret.strength = 0;

properties = ["Warp Drive", "Pulse Engine"];

faction = FACTION.PG;

ai = AI_MODES.SHOOTING
