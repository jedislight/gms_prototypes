/// @description Insert description here
// You can write your code in this editor



draw_set_alpha(0.5)
var t = 1-(seconds_to_live / 0.25);
var t_end = clamp(t + 0.125, 0, 1);
var t_start = clamp(t - 0.125, 0, 1);
var dir = point_direction(start.x, start.y, finish.x, finish.y);
var length = point_distance(start.x, start.y, finish.x, finish.y);

draw_line_width_color(
	start.x + lengthdir_x(length*t_start, dir),
	start.y + lengthdir_y(length*t_start, dir),
	
	start.x + lengthdir_x(length*t_end, dir),
	start.y + lengthdir_y(length*t_end, dir),
	
	w,
	
	c_dkgray,
	c_white
);
draw_set_alpha(1.0);
