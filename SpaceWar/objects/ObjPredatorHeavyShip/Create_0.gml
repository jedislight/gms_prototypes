/// @description Insert description here
// You can write your code in this editor
event_inherited();

/// @description Insert description here
// You can write your code in this editor
class = MODEL_CLASS.HEAVY_SHIP;
move = 4
cruise = 6
turn = 90
hits = 9;
max_hits = 9;
evade = 2;
armor = 2;

turret.range = 24
turret.attack = 2
turret.strength = 2;

side_weapon = new WeaponSystem();
side_weapon.range = 12;
side_weapon.attack = 4;
side_weapon.strength = 0;

properties = ["Warp Drive", "Predator Cysts", "Sapper", "Vanguard"];

faction = FACTION.XN;

ai = AI_MODES.SHOOTING