// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function pixels_to_inches(px){
	var ratio = 18 + (1/3);
	return px / ratio;
}

function inches_to_pixels(inches){
	var ratio = 18 + (1/3);
	return ratio * inches;
}

function d(size) {
	return irandom_range(1, size);	
}

// delta-real-time in seconds
function drt() {
	return delta_time / 1000000;
}

// delta-simulation-time in seconds
function dst() {
	return ObjSimulation.time_rate * drt();
}

function is_model_travel_aligned_to_point(model, point) {
	if(model.turn > 360) return true;
	var direction_to_point = point_direction(model.x, model.y, point.x, point.y);
	var difference = abs( angle_difference(model.heading, direction_to_point) );
	return difference < 45;
}

function estimate_point_travel_time(model, point_or_model) {
	var direction_to_point = point_direction(model.x, model.y, point_or_model.x, point_or_model.y);
	var difference = abs( angle_difference(model.heading, direction_to_point) );
	var turn_modifier = 1 + difference / 90;
	if(model.turn > 360) turn_modifier = 1;
	
	var distance = 0;
	if(instance_exists(point_or_model)) with(model) distance = distance_to_object(point_or_model);
	else with(model) distance = distance_to_point(point_or_model.x, point_or_model.y);
	
	return distance / model.move * turn_modifier;
}

function get_max_shooting_range(model){
	max_range = 0;
	if(is_struct(model.turret)){
		max_range = max(max_range, model.turret.range);	
	}
	
	if(is_struct(model.turret)){
		max_range = max(max_range, model.turret.range);	
	}
	
	if(is_struct(model.front_weapon)){
		max_range = max(max_range, model.front_weapon.range);	
	}
	
	if(is_struct(model.side_weapon)){
		max_range = max(max_range, model.side_weapon.range);	
	}
	
	if(is_struct(model.back_weapon)){
		max_range = max(max_range, model.back_weapon.range);	
	}
	
	return max_range;
}

function get_lowest_traversal(model, array_of_goal_objects){
	if(array_length(array_of_goal_objects) == 0) return noone;
	
	global.temp = model;
	array_sort(array_of_goal_objects, function(a,b){return estimate_point_travel_time(global.temp,{x:a.x, y:a.y}) - estimate_point_travel_time(global.temp,{x:b.y, y:b.y}) });
	return array_of_goal_objects[0];
}


function ram_push(ramming_model, rammed_model){
	if(ramming_model.max_hits < rammed_model.max_hits) return;
	
	var ram_direction = point_direction(ramming_model.x, ramming_model.y, rammed_model.x, rammed_model.y);
	var ram_distance = d(6);
	
	var rp = instance_create_depth(0,0,0, EffectRamPush);
	rp.dir = ram_direction;
	rp.distance = ram_distance;
	rp.target = rammed_model;
}

function get_point_on_circle(xx, yy, radius, dir){
	return {
		x: xx + lengthdir_x(radius, dir),
		y: yy + lengthdir_y(radius, dir)
	}
}

function draw_arc(xx, yy, radius, angle_low, angle_high, color, width) {
	var is_circle = angle_high - angle_low >= 360;
	var line_strip = [];
	
	// start at origin
	if (not is_circle) array_push(line_strip, {x:xx, y:yy});
	
	// arc start
	array_push(line_strip, get_point_on_circle(xx, yy, radius, angle_low));
	
	// arc curve
	var step = 10;
	for(var dir = angle_low+step; dir < angle_high-step; dir += step) {
		array_push(line_strip, get_point_on_circle(xx, yy, radius, dir));		
	}
	
	// arc end
	array_push(line_strip, get_point_on_circle(xx, yy, radius, angle_high));
	
	// close to origin
	if (not is_circle) array_push(line_strip, {x:xx, y:yy});
	
	// draw
	for(var i = 0; i < array_length(line_strip)-1; ++i) {
		var start_p = line_strip[i];
		var end_p = line_strip[i+1];
		draw_line_width_color(start_p.x, start_p.y, end_p.x, end_p.y, width, color, color);
	}
}