// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

enum WEAPON_SYSTEM_FIREMODE {
	SINGLE,
	CONTINUAL
}

function WeaponSystem() constructor{
	attack = 3;
	strength = 1;
	range = 12;
	properties = [];
	fire_mode = WEAPON_SYSTEM_FIREMODE.CONTINUAL;
	ready_time = 0;
	create_effect = function(to, from, damage, hits) {
		var e = instance_create_depth(0,0,0, EffectTraceFade);
		e.start.x = from.x;
		e.start.y = from.y;
		e.finish.x = to.x;
		e.finish.y = to.y;
		e.damage = damage;
		e.target_model = to;
		e.source_model = from;
		if(hits == 0) {
			e.finish.x += random_range(-to.sprite_width, to.sprite_width);
			e.finish.y += random_range(-to.sprite_height, to.sprite_height);
		}
		
		return e;
	}
}

function TurboLaser() : WeaponSystem() constructor {
	range = 30;
	attack = 1;
	strength = 1;
}

function FletchetteCannon() : WeaponSystem() constructor {
	range = 24
	attack = 1
	strength = 0
	properties = ["Blast"];	
}