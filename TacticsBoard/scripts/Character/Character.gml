// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Character(_faction) constructor{
	move = 2;
	moved = 0;
	faction = _faction;
	sprite = Sprite1;
	block = 0;
	blocked = 0;
	attacks = 1
	attacked = 0;
	cone_attack = false;
	attack_range = 1;
	
	function terrain_los_check(from_point, to_point, board){
		var from_point_centroid = {x:from_point.x+0.5,y:from_point.y+0.5};
		var to_point_centroid = {x:to_point.x+0.5,y:to_point.y+0.5};
		var steps = 100;
		var step = 0;
		repeat(steps) {
			var gx = floor(lerp(from_point_centroid.x, to_point_centroid.x, step));	
			var gy = floor(lerp(from_point_centroid.y, to_point_centroid.y, step));
			step += 1/steps;
			
			if(from_point.x == gx and from_point.y == gy) continue;
			
			if(board.terrain_grid[# gx, gy] != 0) return false;
		}
		return true;
		
	}
	
	function get_actions(from_point, to_point, board) {
		actions = [];
		
		var to_character = board.character_grid[# to_point.x, to_point.y];
		
		var distance = board.distance_points_raw(from_point, to_point);
		if(distance <= move-moved and to_character == noone) {
			var mpg = board.get_temp_motion_planning_grid();
			mp_grid_clear_cell(mpg, from_point.x, from_point.y);
			var path = path_add();
			var path_available = mp_grid_path(mpg, path, from_point.x, from_point.y, to_point.x, to_point.y, false);
			
			if(path_available and path_get_number(path) <= move+1){
				array_push(actions, "move");	
			}
			path_delete(path);
			mp_grid_destroy(mpg)
		}
		
		if(distance <= attack_range 
			and (distance == 1 or terrain_los_check(from_point, to_point, board))
			and to_character != noone 
			and to_character.faction != faction
			and attacked < attacks) {
			array_push(actions, "attack");	
		}
		
		return actions;
	}
}

function WarriorRex(_faction) : Character(_faction) constructor {
	move = 4;
	sprite = SpriteTrexFighter;
	block = 1;
	attacks = 2
}

function RaptorPyromancer(_faction) : Character(_faction) constructor {
	move = 2;
	sprite = SpriteRaptorPyro;
	cone_attack = true;
}

function PteranodonArcher(_faction) : Character(_faction) constructor {
	move = 3;
	sprite = SpritePterryArcher;
	attack_range = 3;
}

function TrikePaladin(_faction) : Character(_faction) constructor {
	move = 2;
	sprite = SpriteTrikePaladin;
	block = 2;
}

function MamothBarbarian(_faction) : Character(_faction) constructor {
	move = 1;
	sprite = SpriteMamothBarbarian;
}

function OtterRanger(_faction) : Character(_faction) constructor {
	move = 2;
	sprite = SpriteOtterRanger;
	attack_range = 2;
}

function RabbitNinja(_faction) : Character(_faction) constructor {
	move = 4;
	sprite = SpriteRabbitNinja;
}

function character_factory(character_name, faction){
	switch(character_name) {
		case "WarriorRex": return new WarriorRex(faction);
		case "RabbitNinja": return new RabbitNinja(faction);
		case "OtterRanger": return new OtterRanger(faction);
		case "MamothBarbarian": return new MamothBarbarian(faction);
		case "TrikePaladin": return new TrikePaladin(faction);
		case "PteranodonArcher": return new PteranodonArcher(faction);
		case "RaptorPyromancer": return new RaptorPyromancer(faction);
		default: show_error($"Unknown character {character_name}", true);
	}
}