/// @description Insert description here
// You can write your code in this editor

mouse_drag_start_point = {x: window_mouse_get_x(), y: window_mouse_get_y()};
mouse_drag_start_point_room = {x: mouse_x, y: mouse_y};
if(ai_running) exit;

function handle_attack_on_cell_by_character(grid_coord, character){
	var attacked_character = character_grid[# grid_coord.x, grid_coord.y];
	if(attacked_character != noone and not is_undefined(attacked_character)){
		if(attacked_character.block > attacked_character.blocked) {
			attacked_character.blocked += 1;
		} else {
			character_grid[# grid_coord.x, grid_coord.y] = noone;
			Game.on_death(attacked_character);
		}
		character.attacked += 1
	}
}

if(window_mouse_get_x()>672 and window_mouse_get_y()>672) {
	end_turn();
	ai_running = true;
	time_source_reset(ai_timer)
	time_source_start(ai_timer)
	exit;
}

var grid_coord = {x: room_to_grid_x(mouse_x), y:room_to_grid_y(mouse_y)};
if(grid_coord.x < 0 || grid_coord.y < 0 || grid_coord.x >= grid_width || grid_coord.y >= grid_height){
	exit; // mouse not in grid	
}
	
if(selected_grid_point == noone) {
	if(character_grid[# grid_coord.x, grid_coord.y] != noone) {
		selected_grid_point = grid_coord;
	}
} else {
	var character = character_grid[# selected_grid_point.x, selected_grid_point.y];
	var actions = character.get_actions(selected_grid_point, grid_coord, id)
	var action = array_first(actions)	
	do_action(action, selected_grid_point, grid_coord);
	
	selected_grid_point = noone;
}