/// @description Insert description here
// You can write your code in this editor


for(var gx = 0; gx < grid_width; ++gx) for(var gy = 0; gy < grid_height; ++gy) {
	var rl = (gx + 1) * cell_width;
	var rt = (gy + 1) * cell_height;
	var color = c_white;
	var color2 = c_ltgray;
	var fill = false;
	var character = character_grid[# gx, gy]
	var terrain_index = terrain_grid[# gx, gy];
	var terrain_sprite = noone;
	terrain_sprite = terrain_sprites[terrain_index];
	var selected_character = noone;
		var actions = []

	if(selected_grid_point != noone){
		selected_character = character_grid[# selected_grid_point.x, selected_grid_point.y];
		actions = selected_character.get_actions(selected_grid_point, {x:gx, y:gy}, id);
	}
	if(selected_grid_point != noone && selected_grid_point.x == gx && selected_grid_point.y == gy){
		fill = true;
	}
	if(array_length(actions) > 0){
		color = #FFCCCC
		switch(actions[0]) {
			case "attack": color = c_red;
		}
		var f = sin(current_time/400)
		var h = color_get_hue(color) / 255
		var s = color_get_saturation(color) /255 
		var v = color_get_value(color) / 255
		

		v = abs(v - max(abs(f), 0.75)*0.125)
		
		while(h < 0) h += 1
		while(h > 1) h -= 1
		
		color2 = make_color_hsv(h *255, s *255, v *255)
		fill=true;
	}
	
	draw_set_alpha(0.5)
	draw_roundrect_color(rl, rt, rl+cell_width, rt+cell_height, c_black, c_black, false);
	if(character != noone) {
		draw_roundrect_color(rl, rt, rl+cell_width, rt+cell_height, faction_colors[character.faction], c_black, false);
	}
	draw_set_alpha(1)
	draw_roundrect_color(rl, rt, rl+cell_width, rt+cell_height, color, color2, !fill);
	
	if(terrain_sprite != noone){
		draw_sprite_stretched(terrain_sprite, 0, rl, rt, cell_width, cell_height);
	}
	if(character != noone) {
		draw_sprite_stretched(character.sprite, 0, rl, rt, cell_width, cell_height);
		
		var attacks = character.attacks - character.attacked;
		var blocks = character.block - character.blocked;
		draw_set_alpha(0.75);
		draw_sprite_stretched(SpriteAttacksIcon, clamp(attacks, 0, sprite_get_number(SpriteAttacksIcon)), rl, rt, cell_width, cell_height);
		draw_sprite_stretched(SpriteBlocksIcon, clamp(blocks, 0, sprite_get_number(SpriteBlocksIcon)), rl, rt, cell_width, cell_height);
		draw_set_alpha(1);
	}
	
	draw_text(rl,rt, $"{distance_grid[# gx, gy]}");
}
