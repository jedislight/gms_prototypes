/// @description Insert description here
// You can write your code in this editor

faction_colors = [c_aqua, c_red];
if(not variable_instance_exists(id, "grid_width")) 
	grid_width = 20;
if(not variable_instance_exists(id, "grid_height")) 
	grid_height = 6;
	
ai_running = false;
mouse_drag_start_point = noone;
mouse_drag_start_point_room = noone;
cell_width = 96;
cell_height = 96;
room_height = (grid_height+2)*cell_height;
room_width = (grid_width+2)*cell_width;
zoom_rate = 1/10;

function get_temp_motion_planning_grid(include_characters=true) {
	var mpg = mp_grid_create(0,0, grid_width, grid_height, 1, 1);	
	reset_mp_grid(mpg, include_characters);
	return mpg;
}
character_grid = ds_grid_create(grid_width, grid_height)
ds_grid_clear(character_grid, noone);

terrain_grid = ds_grid_create(grid_width, grid_height)
ds_grid_clear(terrain_grid, 0);
terrain_sprites = [noone, SpriteWall];


function room_to_grid_x(room_x) {return floor(room_x / cell_width - 1)}
function room_to_grid_y(room_y) {return floor(room_y / cell_height - 1)}

selected_grid_point = noone

function end_turn(){
	for(var gx = 0; gx < grid_width; ++gx) for(var gy = 0; gy < grid_height; ++gy) {
		var character = character_grid[# gx, gy]
		if(character == noone) continue;
		
		character.moved = 0;
		character.blocked = 0;
		character.attacked = 0;
	}
}

function distance_points_free(from_point, to_point){
	reset_mp_grid(distance_mp_grid, true);
	return __distance_points(from_point, to_point, distance_mp_grid);
}

function distance_points_terrain_only(from_point, to_point){
	reset_mp_grid(distance_mp_grid, false);
	return __distance_points(from_point, to_point, distance_mp_grid);
}

function __distance_points(from_point,to_point, mpg ){
	
	var original_mp_from = mp_grid_get_cell(mpg, from_point.x, from_point.y)
	var original_mp_to = mp_grid_get_cell(mpg, to_point.x, to_point.y)
	mp_grid_clear_cell(mpg, from_point.x, from_point.y);
	mp_grid_clear_cell(mpg, to_point.x, to_point.y);
	
	var distance = 9999;
	var path = path_add();
	var path_available = mp_grid_path(mpg, path, from_point.x, from_point.y, to_point.x, to_point.y, false);
	if(path_available) {
		distance = round(path_get_length(path));
	}
	
	path_delete(path);

	//distance = abs(from_point.x - to_point.x) + abs(from_point.y - to_point.y);	
	return distance;
}


function distance_points_raw(from_point,to_point ){
	var distance = 9999;
	distance = abs(from_point.x - to_point.x) + abs(from_point.y - to_point.y);	
	return distance;
}

function faction_count(faction){
	var count = 0;
	for(var gx = 0; gx < grid_width; ++gx) for(var gy = 0; gy < grid_height; ++gy) {
		var character = character_grid[# gx, gy]
		if(character == noone or character.faction != faction) continue;
		++count;
	}
	
	return count;
}

function do_action(action, from_point, to_point) {
	var character = character_grid[# from_point.x, from_point.y];
	if (not is_undefined(action)) switch(action){
		case "move": 
			var distance = distance_points_free(from_point, to_point);
			character.moved += distance;
			character_grid[# from_point.x, from_point.y] = noone;
			character_grid[# to_point.x, to_point.y] = character;
		break;
		
		case "attack":
			handle_attack_on_cell_by_character(to_point, character)
			if(character.cone_attack) {
				var direction_vector = 	{x:to_point.x - from_point.x, y: to_point.y - from_point.y};
				var next_point = {x:to_point.x + direction_vector.x, y: to_point.y + direction_vector.y};
				var perpendicular_direction_vector = {x:direction_vector.y, y:direction_vector.x};
				var side_point = {x:next_point.x + perpendicular_direction_vector.x, y:next_point.y + perpendicular_direction_vector.y};
				var other_side_point = {x:next_point.x - perpendicular_direction_vector.x, y: next_point.y - perpendicular_direction_vector.y};
				handle_attack_on_cell_by_character(next_point, character);
				handle_attack_on_cell_by_character(side_point, character);
				handle_attack_on_cell_by_character(other_side_point, character);
			}
		break;
		
		default: show_error($"Invalid action: {action}", true);	
	}
}

function reset_mp_grid(mpg, include_characters=true){
	mp_grid_clear_all(mpg);
	for(var gx = 0; gx < grid_width; ++gx) for(var gy = 0; gy < grid_height; ++gy) {
		if(include_characters and character_grid[# gx, gy] != noone) { 
			mp_grid_add_cell(mpg, gx, gy);
		}
		if(terrain_grid[# gx, gy] != 0) {
			mp_grid_add_cell(mpg, gx, gy);
		}
	}	
}

distance_grid = ds_grid_create(grid_width, grid_height);
function on_ai_tick(board){
	with(board){
		if(not ai_running) return;
		var faction = 1;
	
		ds_grid_clear(distance_grid, 0);
		
		for(var gx = 0; gx < grid_width; gx++) for(var gy = 0; gy < grid_height; ++gy) {
			var character = character_grid[# gx, gy]
			if(character != noone and character.faction != faction) {
				for(var xx = 0; xx < grid_width; ++xx) for(var yy = 0; yy < grid_height; ++yy){
					var d = distance_points_terrain_only({x: gx, y: gy}, {x: xx, y: yy});
					distance_grid[# xx, yy] += d;
				}
			}
		}
		
	
		ai_characters = []
		for(var gx = 0; gx < grid_width; gx++) for(var gy = 0; gy < grid_height; ++gy) {
			var character = character_grid[# gx, gy]
			if(character != noone and character.faction == faction) array_push(ai_characters, {character:character, point:{x:gx, y:gy}});
		}
	
		array_sort(ai_characters, function(a,b){return b.character.move - a.character.move});
		for(var i = 0; i < array_length(ai_characters); ++i){
			var character = ai_characters[i].character;	
			var from_point = ai_characters[i].point;
			var actions_array = [];
			for(var gx = 0; gx < grid_width; gx++) for(var gy = 0; gy < grid_height; ++gy) {
				var cell_actions = character.get_actions(from_point, {x:gx,y:gy}, id);
				for(var a =0; a < array_length(cell_actions); ++a){ 
					var action = cell_actions[a];
					var action_rank = 0;
					switch(action){
						case "move": action_rank = distance_grid[# gx, gy]; break;
						case "attack": action_rank = -1;
					}
					array_push(actions_array, {x:gx,y:gy, action:action, action_rank:action_rank});
				}
		
			}
			if(array_length(actions_array) == 0) continue;
			array_sort(actions_array, function(a,b){return a.action_rank - b.action_rank;});
			var action_item = actions_array[0];
			if(action_item.action_rank <= distance_grid[# from_point.x, from_point.y]){
				do_action(action_item.action, from_point, action_item);
				return;
			}
		}
	
		ai_running = false;
	}
}

ai_timer = time_source_create(time_source_game, 0.4, time_source_units_seconds, on_ai_tick, [id], -1);
time_source_start(ai_timer);

ai_mp_grid = get_temp_motion_planning_grid();
distance_mp_grid = get_temp_motion_planning_grid();