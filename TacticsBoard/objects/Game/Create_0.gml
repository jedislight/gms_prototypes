/// @description Insert description here
// You can write your code in this editor


// Singleton
if(instance_number(object_index) > 1){
	instance_destroy(id);
	exit;
}

randomize();

squad = [choose("WarriorRex")]
level = 1

squad_dead_list = [];

function advance_level() {
	level += 1;
	array_push(squad, choose("WarriorRex", "TrikePaladin", "PteranodonArcher", "RaptorPyromancer"));
	array_sort(squad_dead_list, function(a,b){return b-a});
	for(var i = 0; i < array_length(squad_dead_list); ++i){
		array_delete(squad, squad_dead_list[i], 1);	
	}
	squad_dead_list = [];
	
	room_restart();
}

function array_sample(array){
	return array[irandom(array_length(array)-1)];	
}

function on_death(character){
	var si = character[$ "si"];
	if(is_undefined(si)) return;
	array_push(squad_dead_list, si);
	
}

