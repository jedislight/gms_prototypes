/// @description Insert description here
// You can write your code in this editor

var grid_width = 6
var grid_height = 6;

grid_width = max(array_length(squad), grid_width, level);

instance_destroy(Board);
instance_create_depth(0,0,0, Board, {grid_width: grid_width, grid_height: grid_height});

//terrain_grid[# 0, 0] = 1;
//character_grid[# 3, 1] = new WarriorRex(0)

for(var si = 0; si < array_length(squad); ++si){
	var character = character_factory(squad[si], 0);;
	character.si = si;
	Board.character_grid[# si, 0] = character;
}

var placed_count = 0;
var to_place_count = level;
var spawn_array = ["MamothBarbarian", "RabbitNinja", "OtterRanger"];
while(placed_count < to_place_count) {
	var xx = irandom_range(0, grid_width-1);
	var yy = irandom_range(2, grid_height-1);
	if(Board.character_grid[# xx, yy] != noone) continue;
	Board.character_grid[# xx, yy] = character_factory(array_sample(spawn_array), 1);
	placed_count += 1;
}

var terrain_miss = 0;
while(terrain_miss < 3){
	var xx = irandom_range(0, grid_width-1);
	var yy = irandom_range(2, grid_height-1);
	
	if(Board.character_grid[# xx, yy] != noone) {terrain_miss += 1; continue};
	if(Board.terrain_grid[# xx, yy] != 0) {terrain_miss += 1; continue};
	
	var mpg = Board.get_temp_motion_planning_grid();
	mp_grid_clear_all(mpg);
	for(var tx = 0; tx < grid_width; ++ tx) for(var ty = 0; ty < grid_height; ++ ty){
		if(Board.terrain_grid[# tx, ty] == 0) continue;
		mp_grid_add_cell(mpg, tx, ty);	
	}
	mp_grid_add_cell(mpg, xx, yy);
	
	var all_characters_can_reach_all_characters = true;	
	for(var csx = 0; csx < grid_width; ++ csx) for(var csy = 0; csy < grid_height; ++csy){
		var cs = Board.character_grid[# csx, csy];
		if(cs == noone) continue;
		
		for(var cex = 0; cex < grid_width; ++ cex) for(var cey = 0; cey < grid_height; ++cey){
			if(cex == csx and csy == cey) continue;
			
			var ce = Board.character_grid[# cex, cey];
			if(ce == noone) continue;
			
			var path = path_add();
			var path_available = mp_grid_path(mpg, path, csx, csy, cex, cey, false);
			path_delete(path);
			
			if(not path_available) all_characters_can_reach_all_characters = false;
		}
	}
	
	mp_grid_destroy(mpg);
	
	if(not all_characters_can_reach_all_characters) {terrain_miss += 1; continue};
		
	Board.terrain_grid[# xx, yy] = 1;
}