/// @description Insert description here
// You can write your code in this editor


draw_rectangle_color(x,y, x+width, y+height, color, color, color, color, false)
draw_rectangle_color(x,y, x+width, y+height, c_black, c_black, c_black, c_black, true);

if(check == noone) exit;

draw_set_font(font);
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_text_shadowed(x+margin, y+margin, check.attacker_unit.name + " " + string(check.attacker_value), attacker_color, c_black);

draw_set_halign(fa_right);
draw_set_valign(fa_bottom);
draw_text_shadowed(x+width-margin, y+height-margin, check.defender_unit.name+ " " + string(check.defender_value), encounter_color, c_black);

var healthbar_y = y + height/2;
var healthbar_y2 = healthbar_y + 32;
var healthbar_offset_y = 16;
var healthbar_width = width * .4;
draw_healthbar(
	x+margin,
    healthbar_y-healthbar_offset_y,
	x+margin+healthbar_width,
	healthbar_y2-healthbar_offset_y,
	check.attacker_value / attacker_starting_value * 100,
	c_black,
	attacker_color,
	attacker_color, 
	1,
	true,
	true
)
draw_bar_dividers(
    x+margin,
    healthbar_y-healthbar_offset_y,
	x+margin+healthbar_width,
	healthbar_y2-healthbar_offset_y,
	attacker_starting_value
)

draw_healthbar(
	x+width-margin-healthbar_width,
    healthbar_y+healthbar_offset_y,
	x+width-margin,
	healthbar_y2+healthbar_offset_y,
	check.defender_value / encounter_starting_value * 100,
	c_black,
	encounter_color,
	encounter_color, 
	0,
	true,
	true
)

draw_bar_dividers(
    x+width-margin-healthbar_width,
    healthbar_y+healthbar_offset_y,
	x+width-margin,
	healthbar_y2+healthbar_offset_y,
	encounter_starting_value
)

