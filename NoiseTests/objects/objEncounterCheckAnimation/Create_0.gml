/// @description Insert description here
// You can write your code in this editor
objTimeKeeper.add_pause_instance(id);
function eca_wait_transition() {
	if (current_time - wait_from > wait_ms) {
		state = wait_transition_state;	
	}
}

function eca_next_check() {
	check = check.next;
	state = eca_wait_transition;
	wait_from = current_time;
	wait_transition_state = eca_next_check;
	
	if(check == noone) {
		instance_destroy(id);
		return;
	}
}

state = eca_wait_transition;
wait_ms = 1000;
wait_from = current_time;
wait_transition_state = eca_next_check;

width = 600;
height = 200;
x = 100;
y = 300;

attacker_color = c_lime;
encounter_color = c_maroon;
color = c_ltgray;

margin = 16;
font = fontDebug

attacker_starting_value = check.attacker_value;
encounter_starting_value = check.defender_value;


