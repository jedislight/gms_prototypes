/// @description Insert description here
// You can write your code in this editor
depth+=1;
width = 256;
surface_width = 625;

map_surface = surface_create(surface_width,surface_width)

function room_to_map(value){
	var origin = floor(surface_width/2);
	return origin + floor(value / 32);
}

surface_set_target(map_surface)
draw_set_color(c_black);
draw_rectangle(0,0, surface_width,surface_width, false)
surface_reset_target();

walking_mp_grid = mp_grid_create(-9984,-9984, 624, 624, 32, 32);
flying_mp_grid = mp_grid_create(-9984,-9984, 624, 624, 32, 32);
swiming_mp_grid = mp_grid_create(-9984,-9984, 624, 624, 32, 32);