/// @description Insert description here
// You can write your code in this editor


if (state == cml_open) {
	draw_set_font(font);
	draw_rectangle_color(
		window_x,
		window_y,
		window_x + context_menu_width,
		window_y + array_length(menu_items)*line_height,
		c_black, c_black, c_black, c_black,
		false
	);
	draw_set_color(c_white);
	
	draw_set_halign(fa_left)
	draw_set_valign(fa_top);
	for(var i = 0; i < array_length(menu_items); ++i) {
		if (i == selection_index) {
			draw_set_color(c_yellow);	
		} else {
			draw_set_color(c_white)	
		}
		draw_text(window_x, window_y + i * line_height, menu_items[i].get_label());
	}	
}