/// @description Insert description here
// You can write your code in this editor

context_menu_width = 300;
selection_index = -1;
font = fontDebug;
draw_set_font(font);
line_height =string_height("M")+4

clamp_x = 800-context_menu_width;
clamp_y = 800-line_height*6;

function cml_closed() {
	if (objTimeKeeper.game_speed == 0) {
		return;	
	}
	x = mouse_x;
	y = mouse_y;
	
	window_x = min(clamp_x, window_mouse_get_x());
	window_y = min(clamp_y, window_mouse_get_y());
	
	if (mouse_check_button_pressed(mb_right)) {
		state = cml_opening;	
	}
}

function cml_opening() {
	var l = ds_list_create();
	var any_hit = collision_point_list(x,y, all, false, true, l, false);
	var hits = ds_list_as_array(l);
	ds_list_destroy(l);
	hits = array_filter(hits, function(item){return variable_instance_exists(item, "get_context_menu_items");});
	array_sort(hits, function(a,b){return a.depth - b.depth});
	menu_items = [];
	array_foreach(hits, function(hit){array_concat(menu_items, hit.get_context_menu_items())});
	if (array_length(menu_items) == 0) {
		state = cml_closed;
		return;
	}
	state = cml_open;
	selection_index = -1;
}

function cml_open() {	
	if (window_mouse_get_x() > window_x and window_mouse_get_x() < window_x + context_menu_width) {
		selection_index = (window_mouse_get_y() - window_y) div line_height;
	} else {
		selection_index = -1	
	}
	
	if (mouse_check_button_pressed(mb_right)) {
		state = cml_closed;
	}
	
	if (mouse_check_button_pressed(mb_left)) {
		if (selection_index >= 0 and selection_index < array_length(menu_items)){
			var activated_item = menu_items[selection_index]
			activated_item.activate();
		}
		
		state = cml_closed;
	}
}

state = cml_closed;