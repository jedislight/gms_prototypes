/// @description Insert description here
// You can write your code in this editor


var debug_text = ""

for(var it = ds_map_find_first(debug_groups); not is_undefined(it); it = ds_map_find_next(debug_groups, it)){
	var group = debug_groups[? it];
	
	debug_text += "---" + string(it) +"---\n";
	for(var jt = ds_map_find_first(group); not is_undefined(jt); jt = ds_map_find_next(group, jt)) {
		debug_text += " " + string(jt) + ": " + string(group[? jt]) +"\n";
	}
	debug_text += "\n";
}


draw_set_font(fontDebug);
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_text_shadowed(0,0, debug_text, c_white, c_black);

if(inspect_cursor_mode){
	var l = ds_list_create();
	var hit = collision_point_list(mouse_x, mouse_y, all, false, true, l, true);
	var instances_under_cursor = ds_list_as_array(l);
	ds_list_destroy(l);

	var cursor_text ="";
	for(var j = 0; j < array_length(instances_under_cursor); ++j) {
		var instance = instances_under_cursor[j];	
		cursor_text += object_get_name(instance.object_index)+":"+string(instance.id)+"\n";
		var variables_under_cursor = variable_instance_get_names(instance);
		for(var i =0; i < array_length(variables_under_cursor); ++i){
			var variable = variables_under_cursor[i];
			var value = variable_instance_get(instance.id, variable);
			if (not is_method(value) and not is_undefined(value)) {
				cursor_text += " " + variable + ": " + string(value) +"\n";	
			}
		}
	}
	if(cursor_text != "") {
		draw_text_box(cursor_text, window_mouse_get_x()+16 ,window_mouse_get_y() + 16, c_black, c_green, c_lime)	
	}
}