/// @description Insert description here
// You can write your code in this editor

debug_groups = ds_map_create()
inspect_cursor_mode = false;
function get_debug_group(key) {
	if (is_undefined(debug_groups[? key])) {
		debug_groups[? key] = ds_map_create()
	}
	return debug_groups[? key];
}

function delete_debug_group(key){
	ds_map_destroy(debug_groups[? key]);
	ds_map_delete(debug_groups, key);
}

function update(debug_group, key, value) {
	debug_group[? key] = string(value);
}