/// @description Insert description here
// You can write your code in this editor


// Inherit the parent event
event_inherited();

tt_left = (x + 3200000) mod sprite_width;
tt_top =  (y + 3200000) mod sprite_height;



function TerrainExplorationMenuItem(terrain_instance) : ContextMenuItem() constructor {
	var terrain_label = "";
	with(terrain_instance) terrain_label = object_index_to_tag(object_index);
	name = "Explore " + terrain_label;
	encounter = new Unit(name);
	tile = instance_create_layer(terrain_instance.x, terrain_instance.y, "Dynamics", objPlaceholder);
	array_push(encounter.tags, terrain_label);
	array_push(encounter.divine, "Scrying");
	array_push(encounter.arcane, "Clairvoyance")
	array_push(encounter.finesses, "Scouting");
	
	function activate() {
		//todo selection UI not just nearest - first hero
		instance_create_layer(tile.x, tile.y,"UI", objTravelUnitSelectionMenu, {
			encounter_: encounter,
			tile_: tile,
			verb: encounter.name,
			on_unit_selected: function(unit, from){
				array_delete_item(from.heros, unit);
				travel_encounter_create(unit, encounter_, from, tile_);
				instance_destroy(id);
			}	
		});
	}
		
}

get_context_menu_items = function() {	
 return [new TerrainExplorationMenuItem(id), new ContextMenuItem()]
}

mask_index = maskTerrainTile;