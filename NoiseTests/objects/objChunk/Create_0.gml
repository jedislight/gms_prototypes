/// @description Insert description here
// You can write your code in this editor

with(objChunk) {
	if(id != other.id and x == other.x and y == other.y) {
		instance_destroy(other.id, false);
		exit;
	}
}

chunk_file_path = objGenerator.world_fs_root+"/chunk_"+string(x)+"_"+string(y);
chunk_dirty = true;
function chunk_save(){
	var writer = file_text_open_write(chunk_file_path);
	var terrain_save_grid = ds_grid_create(10, 10);
	ds_grid_copy(terrain_save_grid, terrain_canvas);
	for(var gx = 0; gx < 10; ++gx) for(var gy = 0; gy < 10; ++gy) {
		var t = terrain_save_grid[# gx, gy];
		terrain_save_grid[# gx, gy] = object_get_name(t.object_index);
	}
	var terrain_data = ds_grid_write(terrain_save_grid);
	ds_grid_destroy(terrain_save_grid);
	file_text_write_string(writer, terrain_data);
	file_text_writeln(writer)

	var statics_save_grid = ds_grid_create(10, 10);
	ds_grid_copy(statics_save_grid, static_canvas);
	for(var gx = 0; gx < 10; ++gx) for(var gy = 0; gy < 10; ++gy) {
		var s = statics_save_grid[# gx, gy];
		var name = "";
		if (s != noone) {
			name = object_get_name(s.object_index)	
		}
		terrain_save_grid[# gx, gy] = name;
	}
	var statics_data = ds_grid_write(statics_save_grid);
	ds_grid_destroy(statics_save_grid);
	file_text_write_string(writer, statics_data);
	file_text_close(writer);
}

function chunk_load() {
	chunk_dirty = false;
	var reader = file_text_open_read(chunk_file_path);
	var terrain_data = file_text_readln(reader);
	ds_grid_read(terrain_canvas, terrain_data);
	for(var gx = 0; gx < 10; ++gx) for(var gy = 0; gy < 10; ++gy) {
		var t = terrain_canvas[# gx, gy];
		var obj = asset_get_index(t);
		terrain_canvas[# gx, gy] = obj;
	}

	var statics_data = file_text_readln(reader);
	ds_grid_read(static_canvas, statics_data)
	for(var gx = 0; gx < 10; ++gx) for(var gy = 0; gy < 10; ++gy) {
		var s = static_canvas[# gx, gy];
		var index = noone;
		if(s != "") {
			index = asset_get_index(s);	
		}
		static_canvas[# gx, gy] = index;
	}
	file_text_close(reader);
}

function update_map() {
	surface_set_target(objMap.map_surface);
	var map_surface_half_width = floor(objMap.surface_width * 0.5);
	var l = ds_list_create();
	collision_rectangle_list(x,y, x +width, y+width, abcMappedObject, false, true, l, false);
	var a = array_create(ds_list_size(l), 0);
	for(var i = 0; i < ds_list_size(l); ++i) a[i] = l[|i];
	ds_list_destroy(l);
	array_sort(a, function(a,b){return a.depth - b.depth});
	for(var i = 0; i < array_length(a); ++i) {
		var obj = a[i];	
		obj.draw_map();
	}
	
	surface_reset_target()	
}

var start = current_time
width = 320;
terrain_canvas = ds_grid_create(10,10);
ds_grid_clear(terrain_canvas, abcTerrainTile)

static_canvas = ds_grid_create(10,10);
ds_grid_clear(static_canvas, noone)

var city_placed = false;

if (file_exists(chunk_file_path)) {
	chunk_load();	
} else {
	// paint terrain
	for(var xx = 0; xx < width; xx += 32) for (var yy = 0; yy < width; yy += 32) {
		var obj = terrain_noise.get_value(x+xx, y+yy);
		if (obj == objGrass and forest_noise.get_value(x+xx, y+yy)){
			obj = objForest;	
		}
		var gx = xx div 32;
		var gy = yy div 32;
		ds_grid_set(terrain_canvas, gx, gy, obj);
	}

	// paint statics
	for(var gx = 1; gx < 9; ++gx) for(var gy = 1; gy <9; ++gy) {
		var obj = terrain_canvas[# gx, gy];
		if (city_placed == false and ( obj == objGrass || obj == objForest || obj == objSand)) {
			var n = terrain_canvas[# gx, gy-1];
			var s = terrain_canvas[# gx, gy+1];
			var e = terrain_canvas[# gx+1, gy];
			var w = terrain_canvas[# gx-1, gy];
		
			var types = ds_map_create();
			types[?obj] = 0;
			types[?n] = 0;
			types[?s] = 0;
			types[?e] = 0;
			types[?w] = 0;
			var type_count = ds_map_size(types);
			ds_map_destroy(types);
		
			if (type_count >= 4) {
				static_canvas[# gx, gy] = objCity;
				city_placed = true;
			}
		}
	}
}

// create objects
for(var gx = 0; gx < 10; ++gx) for(var gy = 0; gy <10; ++gy) {
	var obj = terrain_canvas[# gx, gy];
	terrain_canvas[# gx, gy] = instance_create_layer(gx*32+x, gy*32+y, "Terrain", obj);
}

for(var gx = 0; gx < 10; ++gx) for(var gy = 0; gy <10; ++gy){	
	var sobj = static_canvas[# gx, gy];
	if (object_exists(sobj)) {
		var rx = gx*32+x;
		var ry = gy*32+y;
		var found = noone;
		with(sobj) {
			if (x == rx and y ==ry) {
				found = id;
			}
		}

		var instance = noone;
		if (instance_exists(found)) {
			instance = found;
		} else {
			instance = instance_create_layer(gx*32+x, gy*32+y, "Statics", sobj);
		}
		static_canvas[# gx, gy] = instance;
	}
}

update_map();

var endt = current_time
var duration = endt - start;
if (duration > 16) {
	show_debug_message("WARNING SLOW CHUNK CREATED IN " + string(duration) + " ms");
}