/// @description Insert description here
// You can write your code in this editor


if(chunk_dirty) {
	chunk_save();
}

for(var gx = 0; gx < 10; ++gx) for(var gy = 0; gy <10; ++gy) {
	if (instance_exists(terrain_canvas[# gx, gy])){
		instance_destroy(terrain_canvas[# gx, gy]);
	}
}

ds_grid_destroy(static_canvas)
ds_grid_destroy(terrain_canvas );