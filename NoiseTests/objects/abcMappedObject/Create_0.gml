/// @description Insert description here
// You can write your code in this editor

function draw_map() {
	var xx = objMap.room_to_map(x) - 0.5*mapped_size;
	var yy = objMap.room_to_map(y) - 0.5*mapped_size;
	draw_sprite_stretched(sprite_index, 0, xx,yy,mapped_size,mapped_size)
}


if (not variable_instance_exists(id, "mapped_size"))
	mapped_size = 1;
	
if (not variable_instance_exists(id, "walkable"))
	walkable = true;
	
if (not variable_instance_exists(id, "flyable"))
	flyable = true;
	
if (not variable_instance_exists(id, "swimable"))
	swimable = true;

if(not walkable)
	mp_grid_add_rectangle(objMap.walking_mp_grid, x+1, y+1,x+2,y+2);
if(not flyable)
	mp_grid_add_rectangle(objMap.flying_mp_grid, x+1, y+1,x+2,y+2);
if(not swimable) 
	mp_grid_add_rectangle(objMap.swiming_mp_grid, x+1, y+1,x+2,y+2);