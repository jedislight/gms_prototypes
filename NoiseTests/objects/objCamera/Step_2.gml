/// @description Insert description here
// You can write your code in this editor

var _vw = camera_get_view_width(view_camera[0]) / 2;
var _vh = camera_get_view_height(view_camera[0]) / 2;
camera_set_view_pos(view_camera[0], x - _vw, y - _vh);

var humidity = objGenerator.humidity_percent_noise.get_value(objCamera.x, objCamera.y);
var tempuature = objGenerator.tempuature_noise.get_value(objCamera.x, objCamera.y);

var performance_group = objDebug.get_debug_group("Performance");
objDebug.update(performance_group, "FPS", fps_real);
objDebug.update(performance_group, "Chunks", string(instance_number(objChunk)));
objDebug.update(performance_group, "Instances", string(instance_count));

var world_group = objDebug.get_debug_group("World");
objDebug.update(world_group, "Cities", string(instance_number(objCity)))
objDebug.update(world_group, "Pos", string(round(x)) + ", " +string(round(y)));
objDebug.update(world_group, "Danger", string(danger_rating(objCamera.x,objCamera.y)));
objDebug.update(world_group, "Tempuature", string(tempuature)+"F");
objDebug.update(world_group, "Humidity", string(humidity)+"%");


// chunk streaming
var chunk_x = x div objGenerator.chunk_width;
var chunk_y = y div objGenerator.chunk_width;

ds_map_clear(scratch_map);
for(var xx = chunk_x - live_chunk_width; xx <= chunk_x + live_chunk_width; ++xx) for(var yy = chunk_y-live_chunk_width; yy <= chunk_y +live_chunk_width; ++yy) {
	var rx = xx * objGenerator.chunk_width;	
	var ry = yy * objGenerator.chunk_width;
	var exists = false;
	with(objChunk) {
		if (x == rx and y == ry) {
			exists = true;	
			ds_map_add(other.scratch_map, id, 0);
		}
	}
	
	if (not exists) {
		var n = instance_create_layer(rx,ry,"Terrain", objChunk, objGenerator.chunk_initalizer);	
		ds_map_add(scratch_map, n, 0);
	}
}

	
with(objChunk) {
	if (not ds_map_exists(other.scratch_map, id)) {
		instance_destroy(id);	
	}
}