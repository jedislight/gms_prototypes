/// @description Insert description here
// You can write your code in this editor

if (not ds_list_empty(pause_instances)) {
	game_speed = 0.0;	
} else {
	game_speed = configured_speed;	
}

for(var i = 0; i < ds_list_size(pause_instances); ++i) {
	var instance = pause_instances[| i];
	if (not instance_exists(instance)) {
		ds_list_delete(pause_instances, i);
	}
}