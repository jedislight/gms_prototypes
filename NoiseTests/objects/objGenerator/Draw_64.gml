
if (loading_progress < array_length(loading_tasks)) {
	draw_healthbar(250, 350, 550, 450, loading_progress / array_length(loading_tasks) * 100, c_black, c_purple, c_purple, 0, true, true)
	var text = "LOADING   ";
	repeat(abs(sin(current_time/750))*4) text = string_replace(text," ", ">");
	text += "\n" + string(objGenerator.loading_progress) + " / " + string(array_length(objGenerator.loading_tasks));
	draw_set_font(fontSplash);
	draw_set_halign(fa_center)
	draw_set_valign(fa_middle);
	draw_text_shadowed(400,400, text, c_white, c_black);
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
}
