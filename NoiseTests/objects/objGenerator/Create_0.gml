/// @description Insert description here
// You can write your code in this editor

function ChunkCreationTask(rx_, ry_): LoadingTask() constructor {
	rx = rx_;
	ry = ry_;
	function run() {
		var chunk = instance_create_layer(rx, ry,"Terrain", objChunk, objGenerator.chunk_initalizer);
		instance_destroy(chunk);
	}
}

function ClearLoadingTasksTask() : LoadingTask() constructor {
	function run() {
		with(objGenerator) {
			loading_tasks = [];	
		}
	}
}

function FinalizeMapTask(): LoadingTask() constructor {
	function run() {
		surface_set_target(objMap.map_surface)
		with(abcMappedObject) {
			draw_map();	
		}
		surface_reset_target();	
	}
}

function MakeRoadsTask(): LoadingTask() constructor {
	function run() {
		// path roads
		var list = ds_list_create();
		var road_key_set = ds_map_create();
		with(objCity) {
			var src = id;
			instance_deactivate_object(src)
			var nearest = instance_nearest(src.x, src.y, objCity);
			instance_activate_object(src);
			with(nearest) {
				var dst = id;
				var road_key = string(min(real(src),dst)) + "|" + string(max(real(src),dst));
				if (not ds_map_exists(road_key_set, road_key)){
					var road = path_add();
					var success = mp_grid_path(objMap.walking_mp_grid, road, src.x+16, src.y+16, dst.x+16, dst.y+16, true);
					if (not success) {
						path_delete(road);	
					} else {
						ds_map_add(road_key_set, road_key, 0);
						ds_list_add(list, road);	
					}
				}
			}
		}
	
		ds_map_destroy(road_key_set);
	
		// create road objects
		for(var i = 0; i < ds_list_size(list); ++i) {
			var road_path = list[| i];
		
			for(var p = 0; p < path_get_number(road_path); ++p) {
				var px = path_get_point_x(road_path, p);
				var py = path_get_point_y(road_path, p);
				var rx = floor(px / 32) * 32;
				var ry = floor(py / 32) * 32;
						if (rx == 0 and ry == 0) {
				show_debug_message("?");	
			}
				var existing = noone;
				with(objRoad) {
					if (x == rx and y == ry) {
						existing = id;
					}
				}
				if(not instance_exists(existing))
					instance_create_layer(rx, ry, "Statics", objRoad);
			}
		}
	
		// connect roads
		with(objRoad) {
			instance_deactivate_object(id);
			var near = instance_nearest(x,y,objRoad)	
			instance_activate_object(id);
			var dir = point_direction(x,y, near.x, near.y);
			switch (dir) {
				case 0:
				case 180:
					image_index = 0;
					break;
				case 90:
				case 270:
					image_index = 2;
					break;
				case 45:
				case 225:
					image_index = 1;
					break;
				default:
					image_index = 3;
					break;
			}
			
			ds_list_destroy(list)
		}	
	}
}

var init_start = -30;
var init_end = -init_start;
var init_x = init_start;
var init_y = init_start;

chunk_width = 320;
loading_tasks = [];
loading_progress = 0;
while(init_x < init_end) {
	var rx = init_x * chunk_width;
	var ry = init_y * chunk_width;
	array_push(loading_tasks, new ChunkCreationTask(rx, ry));
	if(init_y > init_end) {
		init_y = init_start;
		init_x +=1;
	} else {
		init_y += 1;	
	}
}

array_push(loading_tasks, new MakeRoadsTask());
array_push(loading_tasks, new FinalizeMapTask());

world_fs_root = "world";
world_seed_file = world_fs_root + "/generator";
world_seed = 42;
if(not directory_exists(world_fs_root)) {
	directory_create(world_fs_root)	
	var writer = file_text_open_write(world_seed_file);
	file_text_write_real(writer, world_seed);
	file_text_close(writer);
} else {
	var reader = file_text_open_read(world_seed_file);
	world_seed = file_text_read_real(reader);
	file_text_close(reader);
}

random_set_seed(world_seed);
seeds = [irandom(1000000),irandom(1000000),irandom(1000000),irandom(1000000)];
forest_noise = new Noise2(seeds[0], 864, 933, function(v) {
	return v > 0.75
});

tempuature_noise = new Noise2(seeds[1], 1000, 5000, function(v) {
	return v * 100;
});

humidity_percent_noise = new Noise2(seeds[2], 1000, 5000, function(v) {
	return v * 100;
});

terrain_base_object_noise = new Noise2(seeds[3], 817, 8092, function(v, xx, yy) {
	// islandise
	var d = 1-point_distance(0,0, xx, yy) / 11000;
	v = (v + d) * 0.5;
	v = max(0, v);
	
	var humidity = humidity_percent_noise.get_value(xx,yy);
	var tempuature = tempuature_noise.get_value(xx,yy);
	
	if (v < 0.40) {
		return objDeepWater;	
	} else if (v < 0.5) {
		if (humidity < 25 and tempuature < 32) {
			return objIceShelf;	
		} else if (humidity > 25 and tempuature > 80) {
			return objWhiteBeach;	
		} else if (humidity > 75 and tempuature <32) {
			return objSnowBeach;	
		} else if (humidity > 75 and tempuature > 80) {
			return objMudFlats;
		}
		return objWater;	
	} else if (v < 0.51) {
		return objSand;	
	} else if (v < 0.85) {
		if(humidity < 33) {
			if(tempuature < 32)	return objIcefield;
			if(tempuature < 80)	return objAridPlain;
			return objDesert;
		} else if (humidity < 66) {
			if(tempuature < 32)	return objSnowFields;
			if(tempuature < 80)	return objGrass;
			return objSavanah;
		} else {
			if(tempuature < 32)	return objSnowForest;
			if(tempuature < 80)	return objForest;
			return objJungle;
		}
	} else {
		if(humidity < 20) {
			if(tempuature < 32)	return objIceFissures;
			if(tempuature < 80)	return objCanyons;
			return objMesa;
		} else if (humidity < 66) {
			if(tempuature < 32)	return objSnowMountains;
			if(tempuature < 80)	return objSnowCapMountain;
			return objMountain;
		} else {
			if(tempuature < 32)	return objGlaciers;
			if(tempuature < 80)	return objSnowForestMountains;
			return objForestMountains;
		}
	}
});

chunk_initalizer = {terrain_noise: terrain_base_object_noise, forest_noise: forest_noise};

init_roads = true;
init_map = true;
