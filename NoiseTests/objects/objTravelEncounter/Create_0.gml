/// @description Insert description here
// You can write your code in this editor

//input
unit ??= noone;
tile_goal ??= noone;
tile_embark ??= noone;
encounter ??= noone;

// instance vars
path_out = path_add();
path_back = noone;
pawn = noone;
check = noone;
strategy = ""

function te_creating() {
	var mp_grid = objMap.walking_mp_grid; // todo grid by unit skills
	var path_found = mp_grid_path(mp_grid, path_out, tile_embark.x, tile_embark.y, tile_goal.x, tile_goal.y, true);
	if (not path_found) {
		state = te_no_path;
		return
	}
	
	path_back = path_duplicate(path_out);
	path_reverse(path_back);
	
	pawn = instance_create_layer(path_get_x(path_out,0), path_get_y(path_out,0), "Dynamics", objPathJourneyPawn); 
	pawn.unit = unit;
	with(pawn) path_start(other.path_out, 1, path_action_stop, true);
	state = te_traveling
	objCamera.follow = pawn;
}
state = te_creating;

function te_no_path() {
	state = te_encounter_done;	
}

function te_traveling() {
	if (pawn.path_position == 1.0){
		state = te_choosing_strategy;	
	}
}

strategy_selection_menu = noone;
function te_choosing_strategy() {
	if (strategy_selection_menu == noone) {
		strategy_selection_menu = instance_create_layer(0,0, "UI", objEncounterStrategySelectionMenu, {unit: unit, encounter:encounter});	
	}
	
	strategy = strategy_selection_menu.strategy;
	if(strategy != ""){
		instance_destroy(strategy_selection_menu.id);
		check = check_build(
			unit,
			encounter,
			variable_instance_get(unit, strategy),
			variable_instance_get(encounter, strategy)
		);
	
		instance_create_layer(0,0,"UI", objEncounterCheckAnimation, {check:check});
		state = te_encounter_animation;
	}
}

function te_encounter_animation() {
	var animation_done = instance_number(objEncounterCheckAnimation) == 0;
	if (animation_done) {
		show_debug_message(encounter.to_string());
		var check_step = check;
		var success = false;
		while(true){
			show_debug_message(check_step.to_string());
			success = check_step.attacker_value > 0;
			check_step = check_step.next
						
			if (check_step == noone) {
				break;	
			}
		}
					
		if (success) {
			instance_create_layer((tile_goal.x div 32) *32, (tile_goal.y  div 32) * 32, "Statics", objAdventureSite);
		}
					
		with(pawn) path_start(other.path_back, 1, path_action_stop, true);
		state = te_encounter_returning;
	}
}

function te_encounter_returning() {
	if (pawn.path_position == 1.0){
		state = te_encounter_repopulate;	
	}
}

function te_encounter_repopulate() {
	if(variable_instance_exists(tile_embark, "heros")) {
		array_push(tile_embark.heros, unit);
	}
	state = te_encounter_done;
}

function te_encounter_done() {
	if (instance_exists(pawn)) instance_destroy(pawn);
	if(path_exists(path_out)) path_delete(path_out)
	if(path_exists(path_back)) path_delete(path_back)
	if(tile_goal.object_index == objPlaceholder) instance_destroy(tile_goal.id);
	if(tile_embark.object_index == objPlaceholder) instance_destroy(tile_embark.id);
	instance_destroy(id);
}
		


