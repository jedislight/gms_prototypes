/// @description Insert description here
// You can write your code in this editor
objTimeKeeper.add_pause_instance(id);



var city_heros = [];
var scratch_path = path_add();
with(objCity){
	var path_found = mp_grid_path(objMap.walking_mp_grid, scratch_path, other.x, other.y, x, y, true);
	if(path_found){
		distance_ = path_get_length(scratch_path) div 32;
		array_foreach(heros, function(hero){
			hero.x = x;
			hero.y = y;
			hero.city = id;
		})
		array_concat(city_heros, heros);
	}
}
path_delete(scratch_path);
rows = [];

var header_row = grid_menu_item_create_unit_info_header_items();
array_push(header_row, new GridMenuItemHeader("Distance", ""));
var button_header = "Select"
var button_hover = "";
if (variable_instance_exists(id, "verb")){
	button_header = verb;	
}
if(variable_instance_exists(id, "encounter_")){
	button_hover = encounter_.to_string();
}
array_push(header_row, new GridMenuItemHeader(button_header, button_hover));
array_push(rows, header_row);
array_sort(city_heros, function(a, b) { return a.city.distance_ - b.city.distance_;});

for(var i =0; i < array_length(city_heros) and i < 20; ++i) {
	var row_items = grid_menu_item_create_unit_info_items(city_heros[i]);
	var button_confirm = function(context) {
		context.on_unit_selected(context.unit, context.from);
	}
	
	var hero = city_heros[i];
	var distance = hero.city.distance_;
	var row_distance_item = new GridMenuItemHeader(string(round(distance)), hero.city.name);
	array_push(row_items, row_distance_item);
	
	var row_select_button_item = new GridMenuItemButton(SpriteButton32x32, button_confirm);
	row_select_button_item.unit = hero;
	row_select_button_item.from = hero.city;
	row_select_button_item.on_unit_selected = on_unit_selected;
	array_push(row_items, row_select_button_item);
	array_push(rows, row_items);
}

event_inherited();