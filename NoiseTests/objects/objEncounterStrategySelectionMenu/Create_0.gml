/// @description Insert description here
// You can write your code in this editor

objTimeKeeper.add_pause_instance(id);

strategy = "";
encounter ??= noone
unit ??= noone
rows = [];

function make_strat_row(variable) {
	var unit_tags = variable_struct_get(unit, variable);
	var encounter_tags = variable_struct_get(encounter, variable);
	var confirm_buttom = new GridMenuItemButton(SpriteButton32x32, function(item){
			item.owner.strategy = item.variable;
		});
	confirm_buttom.owner = id;
	confirm_buttom.variable = variable;
	var row = [
		new GridMenuItemHeader(variable, ""),
		new GridMenuItemHeader(string(array_length(unit_tags)), string_join(unit_tags, "\n")),
		new GridMenuItemHeader(string(array_length(encounter_tags)), string_join(encounter_tags, "\n")),
		confirm_buttom
	];
		
	return row;	
}

var header_row = [
	new GridMenuItemHeader("Strategy", ""),
	new GridMenuItemHeader(unit.name, unit.to_string()),
	new GridMenuItemHeader(string_replace_all(encounter.name, " ", "\n"), encounter.to_string()),
	new GridMenuItemHeader("Select", "")
]
array_push(rows, header_row);

var variables = ["combat", "finesses", "persuade", "arcane", "divine"];
for(var i = 0; i < array_length(variables); ++i) {
	var variable = variables[i];
	var encounter_tags = variable_struct_get(encounter, variable);
	if(array_length(encounter_tags) == 0) continue;
	
	array_push(rows, make_strat_row(variable));
}

event_inherited();