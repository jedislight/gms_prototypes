function NoiseTile(posx, posy, seed) constructor {
	width = 1;
	x = posx
	y = posy;
	rand = function() {
		return random_range(0,1);
	}
	
	var xn = x + 1
	var yn = y + 1;

	var nw_seed = seed + (x + (y << 16))
	random_set_seed(nw_seed);
	nw = rand();

	var sw_seed = seed + (x + (yn << 16))
	random_set_seed(sw_seed);
	sw = rand();

	var ne_seed = seed + (xn + (y << 16))
	random_set_seed(ne_seed);
	ne = rand();

	var se_seed = seed + (xn + (yn << 16));
	random_set_seed(se_seed);
	se = rand();
	
	center = rand();
	
	fade = function(v) {
		return (6 * power(v,5)) - (15*power(v,4)) + (10*power(v,3))
	}

	function get(xx,yy) {
		var ew = fade(xx);
		var ns = fade(yy);
	
		var n = lerp(nw, ne, ew);
		var s = lerp(sw, se, ew);
	
		var f = lerp(n,s, ns);
		
		f = lerp(f, center, clamp(-point_distance(0.5,0.5,xx,yy)+.29, 0, 1))
	
		return f;
	}
}
function Noise2(seed_, width1_, width2_, transform_) constructor {
	seed = seed_;
	if (seed == 0) {
		randomize();
		seed = random_range(100,100000);
	}
	width1 = width1_;
	width2 = width2_;
	transform = transform_
	tile_cache = ds_map_create();
	
	function get_value(xx,yy) {
		
		var xx1 = xx / width1;
		var yy1 = yy / width1;
		
		var fx = floor(xx1);
		var fy = floor(yy1);
		
		var xx2 = xx /width2;
		var yy2 = yy /width2;
		
		var fx2 = floor(xx2);
		var fy2 = floor(yy2);
		
		var tile1 = get_tile(fx,fy);
		var tile2 = get_tile(fx2, fy2);
		
		var lx = xx1 - fx;
		var ly = yy1 - fy;
		
		var lx2 = xx2 - fx2;
		var ly2 = yy2 - fy2;
		
		var v = tile1.get(lx, ly);
		var v2 = tile2.get(lx2, ly2);
		
		var fv = (v + v2) * 0.5;
		
		return transform(fv, xx, yy);
	}
	
	function get_tile(xx,yy) {
		var tile_key = string(xx)+"|"+string(yy);
		var tile = ds_map_find_value(tile_cache, tile_key);
		if (is_undefined(tile)) {
			tile = new NoiseTile(xx, yy, seed);
			ds_map_add(tile_cache, tile_key, tile);
		} else if (ds_map_size(tile_cache) > 50) {
			ds_map_clear(tile_cache)	
		}
		return tile;
	}
}