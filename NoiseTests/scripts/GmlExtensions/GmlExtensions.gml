// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

/// @pure
/// @param {Array} array to count elements of
/// @param item to count in array
/// @returns {real}
function array_count(array, item) {
	var count = 0;
	for(var i = 0; i < array_length(array); ++i) {
		if (array[i] == item) {
			++count;	
		}
	}
	
	return count
}

function array_concat(dest, src) {
	array_copy(dest, array_length(dest), src, 0, array_length(src));	
}

function array_delete_item(array, item) {
	for(var i = 0; i < array_length(array); ++i){
		if (array[i] == item) {
			array_delete(array, i, 1);
			return;
		}
	}
}

/// @pure
function ds_list_as_array(list) {
	var array = array_create(ds_list_size(list), 0);
	for(var i = 0; i < array_length(array); ++i) {
		array[i] = list[| i];	
	}
	
	return array;
}

/// @pure
function array_filter(array, predicate) {
	var filtered = [];
	for(var i = 0; i < array_length(array); ++i) {
		if (predicate(array[i])){
			array_push(filtered, array[i]);	
		}
	}
	
	return filtered;
}


function array_foreach(array, func) {
	for(var i = 0; i < array_length(array); ++i) {
		var halt = func(array[i]);
		if (not is_undefined(halt) and halt) {
			break;	
		}
	}	
}

/// @pure
function string_join(array, seperator) {
	if (is_undefined(seperator)) {
		seperator = ", ";	
	}
	
	var s = "";
	for(var i = 0; i < array_length(array); ++i) {
		var item = array[i];
		if (i > 0){
			s += seperator;		
		}
		s += item;
	}
	
	return s;
}

/// @pure
function array_sample(array) {
	return array[round(random_range(0, array_length(array)-1))];
}

/// @pure
function array_join(){ 
	var joined = [];
	for(var i = 0; i < argument_count; ++i) {
		array_concat(joined, argument[i])
	}
	
	return joined;
}

function draw_text_box(text, xx, yy, back_color, outline_color, text_color){
	
	draw_set_font(fontDebug)
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
	
	var width = string_width(text);
	var height = string_height(text);
	xx = fit_in_range(xx, width, 0, 800);
	yy = fit_in_range(yy, height, 0, 800);
	
	draw_set_color(back_color);
	draw_rectangle(xx,yy, xx+width, yy+height, false)
	draw_set_color(outline_color);
	draw_rectangle(xx,yy, xx+width, yy+height, true)
	
	draw_set_color(text_color);
	draw_text(xx,yy, text);
}

function fit_in_range(v, width, bound1, bound2){
	var right = v+width;
	if (right > bound2) {
		right -= abs(bound2 - right);
	}
	
	v = right - width;
	if(v < bound1) return bound1;
	return v;
}

function draw_text_shadowed(xx, yy, text, color, shadow_color) {
	draw_set_color(shadow_color);
	draw_text(xx+1,yy+0, text);
	draw_text(xx+0,yy+1, text);
	draw_set_color(color);
	draw_text(xx+0,yy+0, text);	
}