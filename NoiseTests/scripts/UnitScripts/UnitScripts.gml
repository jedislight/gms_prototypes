// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Unit(name_) constructor {
	name = name_;
	tags = [];
	combat = [];
	persuade = [];
	finesses = [];
	arcane = [];
	divine = [];
	vulnerabilities = [];
	
	function to_string() {
		var dash_if_empty = function(str) {
			if (str != "") {
				return str;	
			}
			
			return "-";
		}
		var s = "";
		s += name;
		s += "\n Tags: " + dash_if_empty(string_join(tags));
		s += "\n Vulnerabilities: " + dash_if_empty(string_join(vulnerabilities));
		s += "\n Arcane: " + dash_if_empty(string_join(arcane));
		s += "\n Combat: " + dash_if_empty(string_join(combat));
		s += "\n Divine: " + dash_if_empty(string_join(divine));
		s += "\n Finesses: " + dash_if_empty(string_join(finesses));
		s += "\n Persuade: " + dash_if_empty(string_join(persuade));
		return s;
	}
}

function Check(attacker_unit_, defender_unit_, attacker_value_, defender_value_) constructor{
	attacker_unit = attacker_unit_;
	defender_unit = defender_unit_;
	attacker_value = attacker_value_;
	defender_value = defender_value_;
	next = noone;
	roll = 0;
	attacker_win_chance = 1;
	
	
	if (attacker_value > 0 and defender_value > 0) {
		attacker_win_chance = attacker_value / (attacker_value + defender_value);
		roll = random_range(0,1);
		var next_attacker_value = attacker_value;
		var next_defender_value = defender_value;
		if (roll < attacker_win_chance) {
			--next_defender_value;
		} else {
			--next_attacker_value;	
		}
		next = new Check(attacker_unit, defender_unit, next_attacker_value, next_defender_value);
	}
	
	function to_string() {
		return attacker_unit.name + " " + string(attacker_value) + " vs " + defender_unit.name + " " + string(defender_value) + " d100: " + string(round(roll * 100)) + " ("+string(round(attacker_win_chance*100))+")% ";	
	}
}

function Party(units) : Unit("") constructor {
	name = units[0].name + "'s party"
	for(var i = 0; i < array_length(units); ++i) {
		var unit = units[i];	
		array_concat(arcane, unit.arcane)
		array_concat(combat, unit.combat)
		array_concat(finesses, unit.finesses)
		array_concat(divine, unit.divine)
		array_concat(persuade, unit.persuade)
		array_concat(tags, unit.tags)
		array_concat(vulnerabilities, unit.vulnerabilities);
		
	}
}

/// @param {Struct.Unit} attacker_unit attacking unit
/// @param {Struct.Unit} defender_unit defending unit
/// @param {array[string]} attacker_active_tags active tags of the attacker
/// @param {array[string]} defender_active_tags active tags of the defender
/// @returns {Struct.Check}
function check_build(attacker_unit, defender_unit, attacker_active_tags, defender_active_tags) {
	var raw_attacker_value = array_length(attacker_active_tags)+2;
	var raw_defender_value = array_length(defender_active_tags)+2;
	var attacker_multiplier = 1;
	var defender_multiplier = 1;
		
	for(var i = 0; i < array_length(attacker_active_tags); ++i) {
		var attacker_tag = attacker_active_tags[i];
		attacker_multiplier += array_count(defender_unit.tags, attacker_tag);
		attacker_multiplier += array_count(defender_unit.vulnerabilities, attacker_tag);
		attacker_multiplier += array_count(defender_active_tags, attacker_tag);
	}
		
	for(var i = 0; i < array_length(defender_active_tags); ++i) {
		var attacker_tag = defender_active_tags[i];
		defender_multiplier += array_count(attacker_unit.tags, attacker_tag);
		defender_multiplier += array_count(attacker_unit.vulnerabilities, attacker_tag);
	}
		
	return new Check(attacker_unit, defender_unit, raw_attacker_value * attacker_multiplier, raw_defender_value * defender_multiplier)
}

global.species_tags = ["Human", "Crabfolk", "Undead", "Elf", "Halfling", "Dwarf", "Gnome", "Half-Elf", "Orc", "Gnoll", "Fey", "Merfolk"];
global.combat_tags = ["Mounted", "Warfare", "Dueling", "Archery", "Minions", "Wards", "Curses", "Polearms", "Axes", "Maces", "Unarmed", "Grappling", "Shields", "Evocation", "Fire", "Ice", "Lightning", "Poison", "Necrotic", "Radiant", "Sneak Attack", "Bleed", "Skirmishing", "Rage", "Sunder", "Healing", "War Beasts", "Trip", "Traps", "Breath Weapon"];
global.finesses_tags = ["Scouting", "Swimming", "Climbing", "Lockpicking", "Sneaking", "Ambush", "Tracking", "Jumping", "Running", "Riding", "Tumbling", "Escape", "Ilusion"]
global.persuade_tags = ["Rude", "Tyranny", "Manners", "Outlaws", "Nobility", "Merchants", "Training", "Bluff", "Diplomacy", "Peasants", "Clergy", "Devils", "Celestials", "Aberations", "Immortals", "Fey"];
global.arcane_tags = ["Clairvoyance", "Curses", "Lore", "Wards", "Evocation", "Abjuration", "Transmutation", "Conjuration", "Necromancy"]
global.divine_tags = ["Lore", "Scrying", "Healing", "Blessings", "Wild Magic", "Pacts", "Prayers", "Rituals", "Avatars"];
global.universal_tags = [];
for(var i = 0 ; i < 1000; ++i) {
	if (object_exists(i) and i != abcTerrainTile) {
		if(object_is_ancestor(i, abcTerrainTile)) {
			array_push(global.universal_tags, object_index_to_tag(i));
		}
	}
}