// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information


function draw_item_text (xx,yy, context) {
	draw_set_font(fontDebug)
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
	draw_set_color(c_black);
	draw_text(xx,yy, context.text);
}

function get_item_text_height(context) {
	draw_set_font(fontDebug)
	return string_height(context.text)
}

function get_item_text_width(context) {
	draw_set_font(fontDebug)
	return string_width(context.text)
}
	
function draw_name (xx,yy, context) {
	draw_set_font(fontDebug)
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
	draw_set_color(c_black);
	draw_text(xx,yy, context.unit.name);
}
	
function draw_variable_array_length (xx,yy, context) {
	draw_set_font(fontDebug)
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
	draw_set_color(c_black);
	var text = string(array_length(variable_struct_get(context.unit, context.variable)))
	draw_text(xx,yy, text);
}
	
function draw_unit_variable_string_array (xx,yy, context) {
	var text = string_join(variable_struct_get(context.unit, context.variable), "\n")
	draw_text_box(text, xx, yy, c_dkgray, c_aqua, c_white);
}


function draw_variable_string_array (xx,yy, context) {
	var text = string_join(variable_struct_get(context, context.variable), "\n")

	draw_text_box(text, xx, yy, c_dkgray, c_aqua, c_white);
}
function no_activation (context) {}
	
function get_2_digit_width (context) {
	draw_set_font(fontDebug)
	return string_width("88");	
}
	
function get_1_line_string_height (context) {
	draw_set_font(fontDebug)
	return string_height("M");	
}
	
function get_unit_name_string_width (context) {
	draw_set_font(fontDebug)
	return string_width(context.unit.name);	
}

function draw_item_sprite(xx, yy, context) {
	draw_sprite(context.sprite, 0, xx+1, yy+1);
}

function get_item_sprite_height(context) {
	return sprite_get_height(context.sprite)+1;
}

function get_item_sprite_width(context) {
	return sprite_get_width(context.sprite)+1;
}

function GridMenuItem(draw_func_, hover_func_, activate_func_) constructor {
	on_draw = draw_func_;
	on_hover = hover_func_;
	on_activate = activate_func_;
	get_optimal_width = function(context) {return 80}
	get_optimal_height = function(context) {return 40};
}

function GridMenuItemHeader(text_, hover_text_) : GridMenuItem(noone, noone, noone) constructor {
	on_draw = draw_item_text;
	on_hover = no_activation;
	if(not is_undefined(hover_text_)) {
		on_hover = 	draw_variable_string_array;
		hover_text = [hover_text_];
		variable = "hover_text";
	}
	on_activate = no_activation;
	get_optimal_height = get_item_text_height;
	get_optimal_width = get_item_text_width;
	text = text_;
}

function GridMenuItemButton(text_or_sprite, on_activate_) : GridMenuItem(noone, noone, noone) constructor {
	if(is_string(text_or_sprite)) {
		on_draw = draw_item_text;
		get_optimal_height = get_item_text_height;
		get_optimal_width = get_item_text_width;
		text = text_or_sprite;
	} else {
		on_draw = draw_item_sprite;
		get_optimal_height = get_item_sprite_height;
		get_optimal_width = get_item_sprite_width;
		sprite = text_or_sprite;
	}
	on_hover = no_activation;
	on_activate = on_activate_;
}


function grid_menu_item_create_unit_info_header_items() {	
	var items = [
		new GridMenuItemHeader("Hero"),
		new GridMenuItemHeader("Com."),
		new GridMenuItemHeader("Fin."),
		new GridMenuItemHeader("Per."),
		new GridMenuItemHeader("Arc."),
		new GridMenuItemHeader("Div."),
		new GridMenuItemHeader("Vul."),
	];
	
	return items;
}

function grid_menu_item_create_unit_info_items(unit) {	
	var items = [];
	
	var name_item = new GridMenuItem(draw_name, draw_unit_variable_string_array, no_activation);
	name_item.unit = unit;
	name_item.variable = "tags";
	name_item.get_optimal_height = get_1_line_string_height;
	name_item.get_optimal_width = get_unit_name_string_width;
	array_push(items, name_item);
	
	
	var variables = ["combat", "finesses", "persuade", "arcane", "divine", "vulnerabilities"];
	for(var i = 0; i < array_length(variables); ++i){
		var variable_item = new GridMenuItem(draw_variable_array_length, draw_unit_variable_string_array, no_activation);
		variable_item.unit = unit;
		variable_item.variable = variables[i];
		variable_item.get_optimal_height = get_1_line_string_height;
		variable_item.get_optimal_width = get_2_digit_width;
		array_push(items, variable_item);
	}
	
	return items;
}

function GridMenu(height, width) constructor {
	grid = ds_grid_create(width, height);
	ds_grid_clear(grid, noone);
	
	function resize() {
		x_starts = array_create(ds_grid_width(grid), 0)
		y_starts = array_create(ds_grid_height(grid), 0)
		for(var i = 0; i < ds_grid_width(grid)-1; ++i){
			var col_width = 0;
			for(var j = 0; j < ds_grid_height(grid); ++j){
				var item = grid[# i, j];
				if (is_struct(item)){
					col_width = max(col_width, item.get_optimal_width(item));
				}
			}
			x_starts[i+1] = x_starts[i] + col_width;	
		}
		
		for(var i = 0; i < ds_grid_height(grid)-1; ++i){
			var row_height = 0;
			for(var j = 0; j < ds_grid_width(grid); ++j){
				var item = grid[# j, i];
				if (is_struct(item)){
					row_height = max(row_height, item.get_optimal_height(item));
				}
			}
			y_starts[i+1] = y_starts[i] + row_height;	
		}
	
		right = x_starts[array_length(x_starts)-1];
		bottom = y_starts[array_length(y_starts)-1];
		
		var last_col_width = 0;
		for(var r = 0; r < ds_grid_height(grid); ++r){
			var item = grid[# ds_grid_width(grid)-1, r];
			if(is_struct(item)) {
				last_col_width = max(last_col_width, item.get_optimal_width(item));	
			}
		}
		
		var last_row_height = 0;
		for(var c = 0; c < ds_grid_width(grid); ++c){
			var item = grid[# c, ds_grid_height(grid)-1];
			if(is_struct(item)) {
				last_row_height = max(last_row_height, item.get_optimal_height(item));	
			}
		}
		
		right += last_col_width;
		bottom += last_row_height;
		
		margin_x = (800 - right) /2;
		margin_y = (800 - bottom) /2;
	}
	
	resize();
	
	function destroy(){
		ds_grid_destroy(grid);	
	}
	
	function on_click() {
		var selection_x = get_grid_x_from_window_x(window_mouse_get_x());
		var selection_y = get_grid_y_from_window_y(window_mouse_get_y());	
		if (selection_x != -1 and selection_y != -1){
			var selected_item = grid[# selection_x, selection_y];
			selected_item.on_activate(selected_item);
		}
	}
	
	function get_grid_x_from_window_x(window_x) {
		window_x -= margin_x;
		if (window_x < 0 or window_x > right) return -1;
		
		for(var c = 0; c < array_length(x_starts)-1; ++c) {
			if (window_x > x_starts[c] and window_x < x_starts[c+1]) {
				return c;	
			}
		}
		
		return ds_grid_width(grid)-1;
	}
	
	function get_grid_y_from_window_y(window_y) {
		window_y -= margin_y;
		if (window_y < 0 or window_y > bottom) return -1;
		
		for(var r = 0; r < array_length(y_starts)-1; ++r) {
			if (window_y > y_starts[r] and window_y < y_starts[r+1]) {
				return r;	
			}
		}
		
		return ds_grid_height(grid)-1;
	}
	
	
	function draw() {
		var selection_x = get_grid_x_from_window_x(window_mouse_get_x());
		var selection_y = get_grid_y_from_window_y(window_mouse_get_y());
		
		draw_set_color(c_ltgray);
		draw_rectangle(
			x_starts[0]+margin_x,
			y_starts[0]+margin_y,
			right+margin_x,
			bottom+margin_y,
			false
		);
		for(var xx = 0; xx < ds_grid_width(grid); ++xx) for(var yy = 0; yy <ds_grid_height(grid); ++yy) {
			var item = grid[# xx, yy];
			if(not is_struct(item)) continue;
			
			var xs = x_starts;
			var ys = y_starts;
			item.on_draw(xs[xx]+margin_x, ys[yy]+margin_y, item);
		}
		draw_set_color(c_black);
		draw_rectangle(
			x_starts[0]+margin_x,
			y_starts[0]+margin_y,
			right+margin_x,
			bottom+margin_y,
			true
		);
		for(var i = 0; i < array_length(x_starts); ++i) {
			draw_line(x_starts[i]+margin_x, y_starts[0]+margin_y, x_starts[i]+margin_x, bottom+margin_y);	
		}
		
		for(var i = 0; i < array_length(y_starts); ++i) {
			draw_line(x_starts[0]+margin_x, y_starts[i]+margin_y, right+margin_x, y_starts[i]+margin_y);	
		}
		
		if (selection_x != -1 and selection_y != -1) {
			var selected_item = grid[# selection_x, selection_y];
			draw_set_alpha(0.12)
			draw_set_color(c_blue);
			var l = x_starts[selection_x];
			var r = right;
			if(selection_x+1 < array_length(x_starts)) {
				r = x_starts[selection_x+1];	
			}
			var t = y_starts[selection_y];
			var b = bottom;
			if(selection_y +1 < array_length(y_starts)){
				b = y_starts[selection_y+1];	
			}
			
			draw_rectangle(margin_x + l, margin_y+t, margin_x + r, margin_y +b, false);
			draw_set_alpha(1.0);
			if(selected_item != noone){
				selected_item.on_hover(window_mouse_get_x()+16, window_mouse_get_y()+16, selected_item);
			}
		}
	}
}