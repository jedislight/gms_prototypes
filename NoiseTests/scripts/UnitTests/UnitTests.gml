// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information



//unit tests
var alice = new Unit("Alice")
array_push(alice.combat, "Mounted", "Dueling", "Warfare");
array_push(alice.persuade, "Nobility", "Manners");
array_push(alice.divine, "Lore");
array_push(alice.tags, "Human", "Nobel", "Knight");
array_push(alice.vulnerabilities, "Archery");

var lomar = new Unit("Lomar")
array_push(lomar.tags, "Crabfolk", "Evil")
array_push(lomar.combat, "Undead", "Curses", "Wards", "Minions")
array_push(lomar.finesses, "Swimming");
array_push(lomar.persuade, "Rude", "Tyranny", "Undead");
array_push(lomar.divine, "Undead");
array_push(lomar.arcane, "Undead", "Lore", "Curses", "Rituals");
array_push(lomar.vulnerabilities, "Fire");

var encounter = new Unit("Rebel Camp Raid")
array_push(encounter.combat, "Archery", "Tricks");
array_push(encounter.persuade, "Outlaws", "Bandits", "Thieves", "Anarchists");
array_push(encounter.tags, "Human", "Outlaws");
array_push(encounter.vulnerabilities, "Mounted");

var test_check = check_build(alice, encounter, alice.combat, encounter.combat);
var it = test_check;
do {
	show_debug_message(it.to_string())
	it = it.next;	
} until (it == noone);
show_debug_message("---")

var test_party = new Party([alice, lomar])
var test_check_party = check_build(test_party, encounter, test_party.combat, encounter.combat);
it = test_check_party;
do {
	show_debug_message(it.to_string())
	it = it.next;	
} until (it == noone);
show_debug_message("---")