// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function LoadingTask()constructor {
	function run() {};
}

function danger_rating(xx, yy){
	var city = instance_nearest(xx,yy, objCity);
	var road = instance_nearest(xx, yy, objRoad);
	var rating = 0;
	if (not instance_exists(city)){
		rating += 500;
	}
	else {
		rating += min(point_distance(xx,yy, city.x, city.y), 500);
	}
	
	if (not instance_exists(road)) {
		rating += 500;	
	}
	else {
		rating += min(point_distance(xx,yy, road.x, road.y), 500);
	}
	
	return rating/1000;
}


function ContextMenuItem() constructor {
	name = "Default Context Menu Item Name"
	function is_enabled() { return true; }
	function is_visible() { return true; }
	function get_label() {return name}
	function activate() { }
}

function generate_random_hero_name() {
	var consanats = ["b", "bb", "c", "ch", "d","f","g", "gh", "ght","h", "j", "k", "l", "m", "n","nn", "p", "qu", "r", "s", "ss", "st", "t", "v", "w", "x", "y", "z"];
	var vowels = ["a", "ae", "eau", "e", "eue", "ei", "ie", "o", "oo", "u", "y"]
	
	var length = round(random_range(1, 3) + random_range(1,2));
	var set = consanats;
	var next_set = vowels;
	var name = "";
	repeat(length) {
		name += array_sample(set);
		// swap arrays
		var tmp = set;
		set = next_set;
		next_set = tmp;
	}
	
	var upper =  string_upper(string_char_at(name,0));
	name = string_replace(name, string_char_at(name,0), upper);
	return name;
}

function generate_random_city_name() {
	var base = generate_random_hero_name();	
	var suffixes = [" Fortress", "vil", "shire", "ton", " City", "berg", " Village", " Outpost", " Camp", " Encapment", "gard"];
	
	return base+array_sample(suffixes);
}

function object_index_to_tag(index) {
	var ob_name = object_get_name(index);
	var camel_case = string_replace(ob_name, "obj", "");
	return camel_case;
}


function generate_random_city_hero() {
	var hero = new Unit(generate_random_hero_name());
	hero.tags = [array_sample(global.species_tags)];
	repeat(3){
		array_push(
			hero.vulnerabilities,
			array_sample(array_join(global.universal_tags, global.species_tags, global.combat_tags, global.finesses_tags, global.arcane_tags, global.divine_tags, global.persuade_tags))
		);
	}
	var count = random_range(2, 5);
	while(count-- > 0) {
		var mode = array_sample( [
			{ dest: hero.combat, src:   array_join(global.combat_tags, global.universal_tags, global.species_tags)},
			{ dest: hero.finesses, src: array_join(global.finesses_tags , global.universal_tags , global.species_tags)},
			{ dest: hero.arcane, src:   array_join(global.arcane_tags , global.species_tags)},
			{ dest: hero.divine, src:   array_join(global.divine_tags , global.species_tags)},
			{ dest: hero.persuade, src: array_join(global.persuade_tags , global.species_tags)},
		]);
		// feather ignore once all
		array_push(mode.dest, array_sample(mode.src));
	}
	
	show_debug_message(hero.to_string());
	
	return hero;
}

function travel_encounter_create(unit_, encounter_, start_obj_, end_obj_) {
	return instance_create_layer(0,0,"UI",objTravelEncounter, 
		{
			unit : unit_,
			encounter : encounter_,
			tile_embark : start_obj_,
			tile_goal : end_obj_
		}
	);
}

function draw_bar_dividers(x1, y1, x2, y2, segments) {
	var width = abs(x2-x1);
	var segment_width = width / segments;
	var backup_color = draw_get_color();
	draw_set_color(c_black)
	for(var i = 1; i < segments; ++i){
		draw_line_width(x1+i*segment_width, y1, x1+i*segment_width, y2, 5);
	}
	draw_set_color(backup_color);
}